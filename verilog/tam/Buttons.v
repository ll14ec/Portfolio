
module Buttons(
	input [3:0] buttons,
	input clock,
	output reg [3:0] oButtons
);

reg [3:0] latches;
integer loopVal = 0;

always @(posedge clock or negedge buttons[0] or negedge buttons[1] or negedge buttons[2] or negedge buttons[3]) begin 
 

 for (loopVal = 0; loopVal < 4; loopVal = loopVal + 1) begin
	
	if (buttons[loopVal] == 0) begin
		if (latches[loopVal] == 0) begin
			latches[loopVal] = 1;
			oButtons[loopVal] = 1;
		end else begin
			oButtons[loopVal] = 0;
		end
   end else begin
		latches[loopVal] = 1'b0;
		oButtons[loopVal] = 1'b0;
	end
 end

end 

endmodule
