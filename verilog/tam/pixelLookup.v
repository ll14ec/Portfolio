module PixelLookup(
	//x and y pixel coord
	input [7:0] pixelX,		
	input [8:0] pixelY,		
	//game state
	input [63:0] state,
	//address in ram for pixel colour
	output reg [23:0] lookupAddress
	
);

integer xSize = 100;
integer ySize = 100;
integer xOffset = 0;
integer yOffset = 0;
integer counter = 0;

//state mappings
wire [7:0] tiredness;
wire [7:0] calmness;
wire [7:0] hunger;
wire [7:0] hygine;
wire [2:0] room;
wire [7:0] xPos;
wire [8:0] yPos;
wire [2:0] action;
wire characterVisible;
wire [4:0] menu;

//assign state mappings to input state
assign tiredness = state[7:0];
assign calmness = state[15:8];
assign hunger = state[23:16];
assign hygine = state[31:24];
assign room = state[34:32];
assign xPos = state[42:35];
assign yPos = state[51:43];
assign action = state[54:52];
assign characterVisible = state[55];
assign menu = state[60:56];

localparam BG_IMAGE_OFFSET = 24'd76800;
localparam SMALL_MENU_OFFSET = 24'd19200;
localparam BIG_MENU_OFFSET = 24'd57600;

localparam SMALL_MENU_X_SIZE = 24'd60;
localparam BIG_MENU_X_SIZE = 24'd180;

localparam MENU_Y_SIZE = 24'd320;


localparam smallMenuX1 = 24'd180;
localparam smallMenuX2 = 24'd240;
localparam smallMenuY1 = 24'd0;
localparam smallMenuY2 = 24'd320;

localparam bigMenuX1 = 24'd0;
localparam bigMenuX2 = 24'd180;
localparam bigMenuY1 = 24'd0;
localparam bigMenuY2 = 24'd320;

localparam BAR_START_Y = 24'd182;

localparam TIREDNESS_BAR_START_X = 24'd35;
localparam CALMNESS_BAR_START_X = 24'd71;
localparam HUNGER_BAR_START_X = 24'd107;
localparam HYGINE_BAR_START_X = 24'd142;

reg [1:0] statusColourAddon = 2'b00;


reg [99:0] characterMask2 [99:0];


always @ (pixelX) begin


	//background layer
	if (room == 3'b000) begin
	
		lookupAddress = pixelX + (24'd240 * pixelY);
	
	end else if (room == 3'b001) begin
	
		lookupAddress = BG_IMAGE_OFFSET + BG_IMAGE_OFFSET + BG_IMAGE_OFFSET + pixelX + (24'd240 * pixelY);
	
	end else if (room == 3'b010) begin
	
		lookupAddress = BG_IMAGE_OFFSET + BG_IMAGE_OFFSET + pixelX + (24'd240 * pixelY);
	
	end else begin
	
		lookupAddress = BG_IMAGE_OFFSET + pixelX + (24'd240 * pixelY);
	
	end
	

	if ( ( (pixelX >= xPos) && (pixelX < (xPos+xSize)) ) && ( (pixelY >= yPos) && (pixelY < (yPos+ySize)) ) ) begin
						  
		//256 * 320 = 81920
		xOffset = pixelX - xPos;
		yOffset = pixelY - yPos;					
		lookupAddress =   BG_IMAGE_OFFSET + 
								BG_IMAGE_OFFSET + 
								BG_IMAGE_OFFSET + 
								BG_IMAGE_OFFSET + 
								SMALL_MENU_OFFSET +
								SMALL_MENU_OFFSET +
								BIG_MENU_OFFSET +
								BIG_MENU_OFFSET +
								BIG_MENU_OFFSET +
								BIG_MENU_OFFSET +
								BIG_MENU_OFFSET +
								3 +
								xOffset + 
								(xSize * yOffset);
	end


	
	case (menu[0]) 
	
		1'b0: begin	end	//no menu open
	
		1'b1: begin		//small menu open
		
			case (menu[1])
			
				1'b0: begin 		//status highlighted in small menu
				
					if ( ( (pixelX >= smallMenuX1) && (pixelX < smallMenuX2 ) ) && ( (pixelY >= smallMenuY1) && (pixelY < smallMenuY2 ) ) ) begin
					
							xOffset = pixelX - smallMenuX1;
							yOffset = pixelY;
							lookupAddress =  	BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													SMALL_MENU_OFFSET + 
													xOffset + 
													(SMALL_MENU_X_SIZE * yOffset);
					
					end
				end
				
				1'b1: begin			//room highlighted in small menu
				
					if ( ( (pixelX >= smallMenuX1) && (pixelX < smallMenuX2 ) ) && ( (pixelY >= smallMenuY1) && (pixelY < smallMenuY2 ) ) ) begin
					
							xOffset = pixelX - smallMenuX1;
							yOffset = pixelY;
							lookupAddress =  	BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													xOffset + 
													(SMALL_MENU_X_SIZE * yOffset);
					end
				end
			endcase
		end
	endcase
	
	case (menu[2])		

		1'b0: begin	end		//big menu closed
	
		1'b1: begin			//big menu open
		
			case (menu[1])
			
				1'b0: begin		//status big menu open
				
					if ( ( (pixelX >= bigMenuX1) && (pixelX < bigMenuX2) ) && ( (pixelY >= bigMenuY1) && (pixelY < bigMenuY2 ) ) ) begin
					
						xOffset = pixelX - bigMenuX1;
						yOffset = pixelY;
						lookupAddress =  	BG_IMAGE_OFFSET + 
												BG_IMAGE_OFFSET + 
												BG_IMAGE_OFFSET + 
												BG_IMAGE_OFFSET + 
												SMALL_MENU_OFFSET + 
												SMALL_MENU_OFFSET + 
												BIG_MENU_OFFSET + 
												BIG_MENU_OFFSET + 
												BIG_MENU_OFFSET + 
												BIG_MENU_OFFSET + 
												xOffset + 
												(BIG_MENU_X_SIZE * yOffset);
						
						if ( (pixelX == TIREDNESS_BAR_START_X) && (pixelY < BAR_START_Y) && (pixelY > BAR_START_Y - tiredness) ) begin
						
							if (tiredness>95) begin
								statusColourAddon = 2'b10;
								
							end else if (tiredness > 50) begin
								statusColourAddon = 2'b01;
								
							end else begin
								statusColourAddon = 2'b00;
							end
							
							lookupAddress =	BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													SMALL_MENU_OFFSET + 
													SMALL_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													statusColourAddon;
													
					
						end		
		
						if ( (pixelX == CALMNESS_BAR_START_X) && (pixelY < BAR_START_Y) && (pixelY > BAR_START_Y - calmness) ) begin
						
							if (calmness>95) begin
								statusColourAddon = 2'b10;
								
							end else if (calmness > 50) begin
								statusColourAddon = 2'b01;
								
							end else begin
								statusColourAddon = 2'b00;
							end
							
							lookupAddress =	BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													SMALL_MENU_OFFSET + 
													SMALL_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													statusColourAddon;
					
						end
			
						if ( (pixelX == HUNGER_BAR_START_X) && (pixelY < BAR_START_Y) && (pixelY > BAR_START_Y - hunger) ) begin
						
							if (hunger>95) begin
								statusColourAddon = 2'b10;
								
							end else if (hunger > 50) begin
								statusColourAddon = 2'b01;
								
							end else begin
								statusColourAddon = 2'b00;
							end
							
							lookupAddress =	BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													SMALL_MENU_OFFSET + 
													SMALL_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													statusColourAddon;
					
						end
			
						if ( (pixelX == HYGINE_BAR_START_X) && (pixelY < BAR_START_Y) && (pixelY > BAR_START_Y - hygine) ) begin
						
							if (hygine>95) begin
								statusColourAddon = 2'b10;
								
							end else if (hygine > 50) begin
								statusColourAddon = 2'b01;
								
							end else begin
								statusColourAddon = 2'b00;
							end
							
							lookupAddress =	BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													BG_IMAGE_OFFSET + 
													SMALL_MENU_OFFSET + 
													SMALL_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													BIG_MENU_OFFSET + 
													statusColourAddon;
					
						end			
					end
				end
				
				1'b1: begin		//room big menu open
				
					case (menu[4:3]) 
					
						2'b00: begin		//kitchen
						
							if ( ( (pixelX >= bigMenuX1) && (pixelX < bigMenuX2) ) && ( (pixelY >= bigMenuY1) && (pixelY < bigMenuY2) ) ) begin
							
								xOffset = pixelX - bigMenuX1;
								yOffset = pixelY;
								lookupAddress =  	BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														SMALL_MENU_OFFSET + 
														SMALL_MENU_OFFSET + 
														xOffset + 
														(BIG_MENU_X_SIZE * yOffset);
							
							end
						end
						
						2'b01: begin		//garden
						
							if ( ( (pixelX >= bigMenuX1) && (pixelX < bigMenuX2) ) && ( (pixelY >= bigMenuY1) && (pixelY < bigMenuY2) ) ) begin
							
								xOffset = pixelX - bigMenuX1;
								yOffset = pixelY;
								lookupAddress =	BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														SMALL_MENU_OFFSET + 
														SMALL_MENU_OFFSET +  
														BIG_MENU_OFFSET + 
														xOffset + 
														(BIG_MENU_X_SIZE * yOffset);
														
							end
						end
						
						2'b10: begin		//bedroom
						
							if ( ( (pixelX >= bigMenuX1) && (pixelX < bigMenuX2) ) && ( (pixelY >= bigMenuY1) && (pixelY < bigMenuY2) ) ) begin
							
								xOffset = pixelX - bigMenuX1;
								yOffset = pixelY;
								lookupAddress =	BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														SMALL_MENU_OFFSET + 
														SMALL_MENU_OFFSET + 
														BIG_MENU_OFFSET + 
														BIG_MENU_OFFSET + 
														xOffset + 
														(BIG_MENU_X_SIZE * yOffset);
							
							end
						end
						
						2'b11: begin		//bathroom
						
							if ( ( (pixelX >= bigMenuX1) && (pixelX < bigMenuX2) ) && ( (pixelY >= bigMenuY1) && (pixelY < bigMenuY2) ) ) begin
							
								xOffset = pixelX - bigMenuX1;
								yOffset = pixelY;
								lookupAddress =	BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														BG_IMAGE_OFFSET + 
														SMALL_MENU_OFFSET + 
														SMALL_MENU_OFFSET + 
														BIG_MENU_OFFSET + 
														BIG_MENU_OFFSET + 
														BIG_MENU_OFFSET + 
														xOffset + 
														(BIG_MENU_X_SIZE * yOffset);
							
							end
						end
					endcase
				end
			endcase
		end
	endcase	
end

endmodule
