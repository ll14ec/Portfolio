/*

Core Module for Tamagotchi game

State is controlled via state module
Images are loaded from locations in RAM based on state
Each pixel's location in RAM is calculated using the pixelLookup module

*/


module TamaCoreModule (
    // Application Clock
    input         clock,                  //Main application clock signal (UART, Save Request, LT24 all synchronous to this clock)
    
    // Application Reset
    input         reset,                  //Application Reset from LT24 - Use For All Logic Clocked with "clock"
    
    // Controll Buttons
	 input [3:0] input_buttons,
	 input 		 save_switch, 
	 
	 // UART Interface
    input         uart_rx,                //UART Receive Data Line (Data In to FPGA)
    output        uart_tx,                //UART Transmit Data Line (Data Out to USB)

    // DDR Read Interface
    output        ddr_read_clock,         //Clock for DDR3 Read Logic. Can be connected to "clock"
    input         ddr_read_reset,         //Reset for DDR3 Read Logic. If "ddr_read_clock" is connected to "clock", use "reset" for DDR read logic instead of this wire.
    output [23:0] ddr_read_address,       //64MB Chunk of DDR3. Word Address (unit of address is 32bit).
    input         ddr_read_waitrequest,   //When wait request is high, read is ignored.
    output        ddr_read_read,          //Assert read for one cycle for each word of data to be read.
    input         ddr_read_readdatavalid, //Read Data Valid will be high for each word of data read, but latency varies from read.
    input  [31:0] ddr_read_readdata,      //Read Data should only be used if read data valid is high.

    // DDR Write Interface
    output        ddr_write_clock,        //Clock for DDR3 Write Logic. Can be connected to "clock"
    input         ddr_write_reset,        //Reset for DDR3 Write Logic. If "ddr_read_clock" is connected to "clock", use "reset" for DDR write logic instead of this wire.
    output [23:0] ddr_write_address,      //64MB Chunk of DDR3. Word Address (unit of address is 32bit).  
    input         ddr_write_waitrequest,  //When wait request is high, write is ignored.
    output        ddr_write_write,        //Assert write for one cycle for each word of data to be written
    output [31:0] ddr_write_writedata,    //Write data should be valid when write is high.
    output [ 3:0] ddr_write_byteenable,   //Byte enable should be valid when write is high.

    // LT24 Data Interface
    output [ 7:0] xAddr,                  // - X Address
    output [ 8:0] yAddr,                  // - Y Address
    output [15:0] pixelData,              // - Data
    output        pixelWrite,             // - Write Request
    input         pixelReady,             // - Write Done

    // LT24 Command Interface
    output        pixelRawMode,           // - Raw Pixel Mode
    output [ 7:0] cmdData,                // - Data
    output        cmdWrite,               // - Write Request
    output        cmdDone,                // - Command Done
    input         cmdReady,               // - Ready for command
    
    // Save DDR3 to SD Card Interface
    output        save_req,               //Asserting (setting high) requests that 64MB chunk of DDR be saved to the SD card. Keep signal high until save_ack goes high.
    output [15:0] save_req_info,          //Amount of data to be saved, in units of 1024-byte chunks. 1=1kB, 2=2kB, 3=3kB, ..., 65535=65535kB, 0=64MB. Note: Saved file size ~= save_req_info * 11kB
    input         save_ack,               //Save Complete. ACK will be high for a single clock cycle once the memory has been saved.
    input  [15:0] save_ack_info           //Status Code. 0 = Success. Otherwise Failed to Save.
);



/*
 * Test UART
 *
 * This is just a simple loop-back to test the UART is working.
 * In practice you need to make your own UART controller as the TX
 * and RX are just the raw pins.
 *
 */

assign uart_tx = uart_rx; //Loopback




/*
 * Test DDR Memory
 *
 * This will write 16kB to the DDR memory using the write interface
 *
 */


//We will use a single clock domain, so DDR write clock is the same as clock
assign ddr_write_clock = clock;


wire        writeWait;
reg         writeRequest;
reg  [23:0] writeAddress;
reg  [31:0] writeData;
reg  [ 3:0] writeByteEn;

reg         writeDone; //All addresses written when high.
reg 			savePhase = 0; //Save to RAM, or render phase
reg[1:0]		loadPhase = 0;
integer		saveWord = 1;

//External interface signals
assign writeWait = ddr_write_waitrequest;
assign ddr_write_address = writeAddress;
assign ddr_write_writedata = writeData;
assign ddr_write_byteenable = writeByteEn;
assign ddr_write_write = writeRequest;


/*
 * Test LCD + DDR Read
 *
 * Let's test the DDR read interface to write data to
 * the LCD
 *
 */

//Not using command interface for LT24, so will tie control signals to zero as in Lab 5.
assign pixelRawMode = 1'b0; // - Raw Pixel Mode
assign cmdData      = 8'b0; // - Data
assign cmdWrite     = 1'b0; // - Write Request
assign cmdDone      = 1'b0; // - Command Done

//As we are using DDR read to drive the LCD, it *must* be on the same clock domain as the LCD (i.e. "clock")
assign ddr_read_clock = clock;

//LCD signals for state machine
reg        pixelWriteGen;
reg [15:0] pixelDataGen;
reg [16:0] pixelAddressGen;

//DDR signals for state machine
wire        readValid;
reg         readRequest;
reg  [23:0] readAddress = 24'b0;
wire [31:0] readData;
wire        readWait;

//State machine for RAM reading / screen writing
reg [1:0] RAMstateMachine;
localparam IDLE_STATE = 2'b00;
localparam READ_STATE = 2'b01;
localparam WAIT_STATE = 2'b10;
localparam DONE_STATE = 2'b11;

//x and y pixel positions
reg [7:0] currentX = 0;
reg [8:0] currentY = 0;

//wire links to pixel lookup module
wire [7:0] xLink;
wire [8:0] yLink;
wire [63:0] stateLink;
wire [23:0] addressLink;

//insatntiate pixel lookup module
PixelLookup pl(
	.pixelX(xLink),
	.pixelY(yLink),
	.state(stateLink),
	.lookupAddress(addressLink)
);

//assign links to pixel lookup module
assign xLink = currentX;
assign yLink = currentY;

//button inputs
wire [3:0] inputButtonsLink;

//initial state (loaded) from RAM
reg [63:0] initialState = 0;
reg [63:0] resetState = 32'd2139062143 | 3'd0 << 32 | 8'd1 << 35 | 9'd1 << 43 | 3'd0 << 52 | 1'd0 << 55;
reg loadState = 0;

//instantiate game state machine module
StateMachine GameStateMachine(
	.buttons(input_buttons),
	.clock(clock),
	.loadState(loadState),
	.inputState(initialState),
	.outputState(stateLink)
);



/*
 * Test Handshake
 *
 * This module tests the Save-To-SDCard interface.
 * When the DDR write address reaches 0x2000 (32kB),
 * it will request saving of 32kB to SD.
 *
 */


reg  saveRequest;
wire saveAcknowledge;

assign save_req_info = 16'd1; //Save 32 x 1kB chunks (i.e. 32kB).
assign save_req = saveRequest;
assign saveAcknowledge = save_ack;


always @ (posedge clock or posedge reset) begin
    if (reset) begin
        readRequest     <= 1'b0;
        readAddress     <= 24'h0;
        pixelWriteGen   <= 1'b0;
        pixelAddressGen <= 17'b0;
        pixelDataGen    <= 16'b0;
        RAMstateMachine    <= IDLE_STATE;
    end 
	 
	 //save state to ram in save phase (only after initial load phase)
    else if (savePhase && loadPhase > 2) begin
		if (saveWord == 3) begin
			saveWord = 1;
		end
		else begin
			saveWord = saveWord + 1;
		end
		
      writeByteEn  <= 4'hF;                           //All bytes written in each 32-bit word 
      writeRequest <= 1;                    				//Request to write
      
		//Each time a write is accepted by the memory (i.e. writeRequest is high, and writeWait is low)
		if (writeRequest && !writeWait) begin
			if (saveWord == 1) begin
				//if character dead
				if (stateLink[7:0] == 0 || stateLink[15:8] == 0 || stateLink[23:16] == 0 || stateLink[31:24] == 0) begin
					//write reset state (first half)
					writeData <= resetState[31:0];
				end
				else begin
					//write curent state (second half)
					writeData <= stateLink[31:0];
				end
			end
			else begin
				//if character dead
				if (stateLink[7:0] == 0 || stateLink[15:8] == 0 || stateLink[23:16] == 0 || stateLink[31:24] == 0) begin
					//write reset state (first half)
					writeData <= resetState[63:32];
				end
				else begin
					//write curent state (second half)
					writeData <= stateLink[63:32];
				end
			end
			//set write address from save word
			writeAddress <= saveWord;   
      end
		savePhase <= 0;
    end
	 
	 //read data from ram and render to screen
	 else begin
        case (RAMstateMachine)
            IDLE_STATE: begin
                if (1) begin
							//If the display is read to receive data
							//Set current pixel x y coord
							currentX = pixelAddressGen[7:0];
							currentY = pixelAddressGen[16:8];
						   //Set the RAM state machine to read state
							//Issue read request for current address
							readRequest     <= 1'b1;         
                     //And jump to read state							
							RAMstateMachine    <= READ_STATE;    
							if (loadPhase == 0) begin
								readAddress  <= readAddress + 1;
							end
							else if (loadPhase == 1) begin
								readAddress <= readAddress + 1;
							end
							
                end
            end
            READ_STATE: begin
                if (!readWait && !writeWait) begin
                    //Once the read request has been accepted
						  //Drop request signal
                    readRequest     <= 1'b0; 
                    //And wait for valid data						  
                    RAMstateMachine    <= WAIT_STATE;             
                end
            end
            WAIT_STATE: begin
                if (readValid) begin
						 		
						  if (loadPhase>2) begin
								//Assign new read address to output of [ pixel location => ram lookup module ]
								readAddress <= addressLink;
						  end
                    //Once valid data has arrived
						  //Begin writing to LCD
                    pixelWriteGen   <= 1'b1;                   
						  //If the x coord is off screen, increment the y coord and reset x to 0
						  if (pixelAddressGen[7:0] > 17'd239) begin
								pixelAddressGen = pixelAddressGen & 17'b11111111100000000;
								pixelAddressGen = pixelAddressGen + 17'b00000000100000000;
						  end
						  //Save the data from ram into the pixelDataGen register
						  //Data is lower 16-bits of DDR read data	
						  pixelDataGen    <= readData[15:0];   
                    //And jump to done state						  
						  RAMstateMachine    <= DONE_STATE;   
						  
						  //check load phase (0,1,2) are stages of loading the saved data from RAM into the game state
						  //load phase > 2 is normal game play i.e. no loading, save may be requested using switch
						  case (loadPhase)
								// loadPhase 0 - save the read data into the first half of the initial state reg
								0: begin
									if (readData!=0) begin
										initialState[31:0] <= readData;
										loadPhase <= loadPhase + 1;
									end
								end
								// load phase 1 - save the read data into the second half of the initial state reg
								1: begin
									if (readData!=0) begin
										initialState[63:32] <= readData;
										loadPhase <= loadPhase + 1;
										loadState <= 1;
									end
								end
								// load phase 2 - keep the loadState flag high so that the state machine loads the initial state
								2: begin
									loadPhase <= loadPhase + 1;
									loadState <= 1;
								end
								// load phase > 2 - play game as normal with no state loading, saves can be requested using the switch
								default: begin
									loadState <= 0;
									if (saveAcknowledge) begin
										saveRequest <= 1'b0;  //Clear request signal
									end
									else if (save_switch) begin
										saveRequest <= 1'b1;		//Request save
									end
								end
						  endcase
                end
            end
            DONE_STATE: begin
                if (pixelReady) begin
                    //Once pixel has been accepted, we are finished
						  //Ensure we only send one write to the LCD
                    pixelWriteGen       <= 1'b0;                   
						  //If the pixel address is off screen then that is equiv of pixelAddressGen being > 81920
						  if (pixelAddressGen > 24'd81920) begin
								//If off screen, then we need to start again from 0,0 so we reset the pixel address
								pixelAddressGen	= 	24'b0;
						  end else begin
						      //Increment to next read address
								pixelAddressGen	<= 	pixelAddressGen + 24'b1;    
						  end
						  //Return to idle ready to read next pixel.
                    RAMstateMachine        <= IDLE_STATE;
						  if (loadPhase>2) begin
								savePhase <= 1;
						  end
                end
            end
        endcase
    end
end

//External interface signals
assign readWait = ddr_read_waitrequest;
assign readValid = ddr_read_readdatavalid;
assign readData  = ddr_read_readdata;
assign ddr_read_read = readRequest;
assign ddr_read_address = readAddress;

assign pixelWrite = pixelWriteGen;
assign pixelData = pixelDataGen;
assign {yAddr,xAddr} = pixelAddressGen;

endmodule
