`timescale 1 ns/100 ps

module TransposeFormFilter_tb;
//
// Parameter Declarations
//
localparam NUM_CYCLES = 640*420*3; //Simulate this many clock cycles. Max. 1 billion
localparam CLOCK_FREQ = 50000000; //Clock frequency (in Hz)
localparam RST_CYCLES = 2; //Number of cycles of reset at beginning.

//
// Test Bench Generated Signals
//
reg 		clock;
reg 		clken;
reg 		reset;
reg 		row_border;
reg 		col_border;
reg [23:0] 	in;

//DUT output singal

wire [23:0] ou;

//Device Under Test - DUT
TransposeFormFilter #(
	.NUM_CHANNELS(3),
	.WIDTH(5),
	.CHANNEL_WIDTH(8)

) dut (

   .clock    	( clock ),
   .clken		( clken ),
   .row_border	( row_border ),
   .col_border	( col_border ),
   .signal   	( in  ),
   .filtered 	( ou  )
);

//
// Reset Logic
//
initial begin
	 reset = 1'b1; //Start in reset.
	 repeat(RST_CYCLES) @(posedge clock); //Wait for a couple of clocks
	 reset = 1'b0; //Then clear the reset signal.
end

//
//Clock generator + simulation time limit.
//
initial begin
	clock = 1'b0; //Initialise the clock to zero.
end

//Next we convert our clock period to nanoseconds and half it
//to work out how long we must delay for each half clock cycle
//Note how we convert the integer CLOCK_FREQ parameter it a real
real HALF_CLOCK_PERIOD = (1000000000.0 / $itor(CLOCK_FREQ)) / 2.0;

//Now generate the clock
integer half_cycles = 0;

always begin
 //Generate the next half cycle of clock
	 #(HALF_CLOCK_PERIOD); //Delay for half a clock period.
	 clock = ~clock; //Toggle the clock
	 half_cycles = half_cycles + 1; //Increment the counter
	
	//Print to console that the simulation has started. $time is the current simulation time.
    $display("%d ns\tSimulation Started",$time);  
    //Monitor any changes to any values listed. 
    
	
	 //Check if we have simulated enough half clock cycles
	 if (half_cycles == (2*NUM_CYCLES)) begin
		 //Once the number of cycles has been reached
		 $stop; //Break the simulation
		 //Note: We can continue the simualation after this breakpoint using
		 //"run -continue" or "run ### ns" in modelsim.
	 end
end

integer count_pix = 0;
integer count_row = 0;
initial begin 
	in = 0;
	clken = 1;
	col_border = 1'b0;
	row_border = 1'b0;
end 

always @(posedge clock) begin
	
	in = 24'b100000001000000010000000;
	
	if (count_pix == 636 ) begin
		row_border <= 1'b1;
	
	end else begin 
		row_border <= 1'b0;
		
	end 
	
	if (count_row == 416 ) begin
		col_border <= 1'b1;
		
	end else begin 
		col_border <= 1'b0;
		
	end 
	
	
end

always @(negedge clock) begin
	
	count_pix = count_pix + 1; 
	if (count_pix == 640 ) begin
		count_pix <= 0;
		count_row <= count_row + 1; 
		
	end else if (count_row == 420 ) begin
		count_row = 1'b0;
		
	end
end

endmodule