
module StateMachine(
	input [3:0] buttons,
	input clock,
	input loadState,
	input [63:0] inputState,
	output [63:0] outputState
);

//declare state mappings
reg [7:0] tiredness;
reg [7:0] calmness;
reg [7:0] hunger;
reg [7:0] hygine;
reg [2:0] room;
reg [7:0] xPos;
reg [8:0] yPos;
reg [2:0] action;
reg characterVisible;
reg [4:0] menu;
reg toggle;

//declare counter
integer counter = 0;
integer toggleCounter = 0;
integer actionCounter = 0;

//declare initial state
reg [63:0] state = 64'b0;

//assign to output state bus
assign outputState = state;

localparam GARDEN = 2'b11;
localparam KITCHEN = 2'b00;
localparam BATHROOM = 2'b01;
localparam BEDROOM = 2'b10;

localparam NO_MENU = 1'b0;
localparam FIRST_PAGE = 1'b1;

localparam TRAVEL_HIGHLIGHTED = 1'b1;
localparam STATUS_HIGHLIGHTED = 1'b0;

localparam SECOND_PAGE_OPEN = 1'b1;
localparam SECOND_PAGE_CLOSED =1'b0;

localparam BUTTON_0 = 2'b00;
localparam BUTTON_1 = 2'b01;
localparam BUTTON_2 = 2'b10;
localparam BUTTON_3 = 2'b11; 

integer B0_COUNTER = 0;
integer B1_COUNTER = 0;
integer B2_COUNTER = 0;
integer B3_COUNTER = 0;

//button latches`
reg [3:0] latches; 

reg buttonPressed = 1'b0;

function buttonLatch;
	//inputs
	input [1:0] id;
	
	begin
		//if button pressed
		if(buttons[id] == 0) begin 
			//if not already latched, latch the input and return 1
			if (latches[id] == 0) begin
				latches[id] = 1;
				buttonLatch = 1;
			end
		//if button not pressed, set latch to 0 and return 0
		end else begin
			latches[id] = 0;
			buttonLatch = 0;
		end
	end

endfunction


initial begin
	//assign state mappings to input state
	tiredness = state[7:0];
	calmness = state[15:8];
	hunger = state[23:16];
	hygine = state[31:24];
	room = state[34:32];
	xPos = state[42:35];
	yPos = state[51:43];
	action = state[54:52];
	characterVisible = state[55];
	menu = state[60:56];
	toggle = state[61];
end


always @ (posedge clock) begin

	if (loadState) begin
		//assign state mappings to input state
		tiredness = inputState[7:0];
		calmness = inputState[15:8];
		hunger = inputState[23:16];
		hygine = inputState[31:24];
		room = inputState[34:32];
		xPos = inputState[42:35];
		yPos = inputState[51:43];
		action = inputState[54:52];
		characterVisible = inputState[55];
		menu = inputState[60:56];
		toggle = inputState[61];
	end

	//Action progress counter 
	if (actionCounter > 32'd40000000) begin
		if (action > 0 ) begin 
			action = action - 1;
		end 
		actionCounter = 0;
	end else begin
		actionCounter = actionCounter + 1;
	end

	//toggler for thought bubbles
	if (toggleCounter > 32'd60000000) begin
		toggleCounter = 0;
		toggle = ~toggle;
	end else begin
		toggleCounter = toggleCounter + 1;
	end

	if (counter > 32'd100000000) begin
		if (tiredness == 0 || calmness == 0 || hunger == 0 || hygine == 0) begin
			//character dead
		end 
		//if alive, slowly reduce tiredness, calmness, hunger and hygine over time
		else begin
			tiredness = tiredness - 1;
			hygine = hygine - 1;
			hunger = hunger - 1;
			if (room == GARDEN) begin
				if (calmness + 8 < 128) begin
					calmness = calmness + 8;
				end
			end
			else begin
				calmness = calmness - 1;
			end
		end
		//reset counter
		counter = 0;
	end 
	else begin
		counter = counter + 1;
	end
	 
	// MENU STATE MACHINE //
	if (tiredness == 0 || calmness == 0 || hunger == 0 || hygine == 0) begin
			//character dead
	end else begin
		if (room != GARDEN) begin
			//set character position
			if (room == KITCHEN) begin
				xPos = 130;
				yPos = 118;
			end
			else if (room == BEDROOM) begin
				xPos = 120;
				yPos = 230;
			end
			else if (room == BATHROOM) begin
				xPos = 130;
				yPos = 75;
			end

		
			//menu can only open if room is not garden
			case(menu[0])
			//if the menu is not open 
			NO_MENU: begin 
				//if open menu button pressed, open menu
				buttonPressed = buttonLatch(BUTTON_0);
				if(buttonPressed) begin 
					menu[0] = 1'b1; 
				end
				//if action button pressed, enact action
				buttonPressed = buttonLatch(BUTTON_3);
				if(buttonPressed) begin 
					action = 3'b111;
					
					case(room)
						KITCHEN:
							if(hunger + 8'b00011111 > 8'b01111111 )begin 
								hunger = 8'b01111111;
							end  else begin 
								hunger = hunger + 8'b00011111;
							end 
						BEDROOM:
							if(tiredness + 8'b00011111 > 8'b01111111 )begin 
								tiredness = 8'b01111111;
							end  else begin 
								tiredness = tiredness + 8'b00011111;
							end 
						BATHROOM:
							if(hygine + 8'b00011111 > 8'b01111111 )begin 
								hygine = 8'b01111111;
							end  else begin 
								hygine = hygine + 8'b00011111;
							end 
					endcase
					
				end
			end
			//if the small menu is open
			FIRST_PAGE: begin 
				//if close is pressed, close the menu
				buttonPressed = buttonLatch(BUTTON_0); 
				if(buttonPressed) begin 
					menu = 5'b00000; 
				end else begin 
					//menu[1] controls which option is selected in small menu
					case(menu[1]) 
					//if travel highlighted
					TRAVEL_HIGHLIGHTED:begin
						//check big menu open status
						case (menu[2])
							//if big menu closed
							SECOND_PAGE_CLOSED: begin
								buttonPressed = buttonLatch(BUTTON_1);
								//if toggle pressed, switch to status highlighted
								if(buttonPressed) begin 
									menu[1] = STATUS_HIGHLIGHTED;
								//if open button pressed, open large menu page
								end else begin	
									buttonPressed = buttonLatch(BUTTON_2); // select
									if(buttonPressed) begin 
										menu[2] = SECOND_PAGE_OPEN;
									end
								end
							end
								
							SECOND_PAGE_OPEN: begin
								//if toggle is pressed
								buttonPressed = buttonLatch (BUTTON_1);	
								if (buttonPressed) begin
									//change the room option in the menu
									menu[4:3] = menu[4:3] + 1'b1;	
								end else begin
									//if select is pressed
									buttonPressed = buttonLatch(BUTTON_2);
									if (buttonPressed) begin
										//update room selection and close menu
										room = menu[4:3];
										menu = 5'b00000;
									end
								end
							end
						endcase
					end
					
					STATUS_HIGHLIGHTED:begin 
						//if status highlighted, and toggle pressed, and big menu closed, change to travel page
						buttonPressed = buttonLatch(BUTTON_1); 
						if(buttonPressed) begin 
							if (menu[2] == SECOND_PAGE_CLOSED) begin
								menu[1] = TRAVEL_HIGHLIGHTED; 
							end
						//if status  highlighted and select pressed, open big menu
						end else begin	
							buttonPressed = buttonLatch(BUTTON_2); // select
							if(buttonPressed) begin 
								menu[2] = SECOND_PAGE_OPEN;
							end
						end
					end
					endcase
				end 
			end	
			endcase 
		end
		
		//if in garden, update position from buttons
		else begin
			if (!buttons[0]) begin
				B0_COUNTER = B0_COUNTER + 1;
				if (B0_COUNTER > 32'd1000000) begin
					if (xPos > 7) begin
						xPos = xPos - 1;
					end
					B0_COUNTER = 0;
				end
			end
			if (!buttons[1]) begin
				B1_COUNTER = B1_COUNTER + 1;
				if (B1_COUNTER > 32'd1000000) begin
					if (xPos < 225) begin
						xPos = xPos + 1;
					end
					B1_COUNTER = 0;
				end
			end
			if (!buttons[2]) begin
				B2_COUNTER = B2_COUNTER + 1;
				if (B2_COUNTER > 32'd1000000) begin
					if (yPos > 7) begin
						yPos = yPos - 1;
					end
					B2_COUNTER = 0;
				end
			end
			if (!buttons[3]) begin
				B3_COUNTER = B3_COUNTER + 1;
				if (B3_COUNTER > 32'd1000000) begin
					if (yPos < 305) begin
						yPos = yPos + 1;
					end
					B3_COUNTER = 0;
				end
			end
			//if in the top right corner of the screen, go back to the kitchen
			if (xPos < 70 && yPos < 85) begin
				room = KITCHEN;
			end
		end
	end
	//Set the output state to the new values
	state = tiredness  | calmness << 8 | hunger << 16 |      hygine << 24      | room << 32  |
	        xPos << 35 |   yPos << 43  | action << 52 | characterVisible << 55 | menu << 56  |
			  toggle << 61;	

			  
end

endmodule
