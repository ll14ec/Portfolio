
import java.io.FileNotFoundException;
import java.io.IOException;

/*
 *                       TEST PROGRAM
 * 
 * This program implements the files Track.java and Point.java.
 * This file attempts to use all of the core functionality of the 
 * Point and Track class definitions, as defined in the assignment  
 * specification. The tests for Simple, Intermediate and Advanced
 * aspects of the assignment are split into Test1, Test2 and Test3.
 * 
 *                         Test1()
 * 
 * This method manually creates 12 points. It then calls the basic
 * Track() constructor to instantiate a new track object. The 
 * previously defined points are then added to the track one at a 
 * time, and basic information about the path created is computed.
 * This particular path is one I used to loathe (cycling to and 
 * from work to do a retail job before I came back to university).
 * 
 *                         Test2()
 * 
 * This method creates a new track with the constructor using a 
 * filename to read from as its argument. A track is then created 
 * - note the file order must be in "longitude latitude elevation "
 * with white-space in between and no other superfluous information
 * for this to work. It is a very basic file format that must be 
 * adhered to. This is because the readFile() function will take 
 * information in the defined order, looking for whitespace in
 * between values.
 * 
 * The test then calls the highest and lowest point methods. As
 * I wrote these to return an array of equally high or low points,
 * the method must first assess the size() of the result. Once 
 * this is obtained, a suitable message is printed to the console,
 * and the results are printed out in a loop. After this the number 
 * of points and total length of the path are displayed.
 *
 *                         Test3()
 *                         
 * This method creates a track via the readFile() method as in the
 * previous test. After this a generic point (not from the track)
 * is created and the nearest track method called. This searches
 * the track for the nearest point to the generic one and prints
 * the result. The path is then written to a KML file within the 
 * current directory. This file will open in Google Earth and give
 * a visual representation of the point data on the track.                        
 */


public class Test {

	public static void main (String args[]) throws IOException, FileNotFoundException{	
	
		Test1();                                       	
		Test2();				           
		Test3();	

		System.out.print("\n");					

	}

	private static void Test3() throws IOException, FileNotFoundException{

		System.out.println("\nBegin Test3()\n");

		Track AdvancedTrack = new Track("points.txt");
		Point p2 = new Point(-1.538340, 53.796411, 37.1);
		
		System.out.println("Nearest point to [" + p2.toString() + " ] is [" + AdvancedTrack.nearestPoint(p2) + " ]\n" );
		System.out.println("Writing 'AdvancedTrack.kml' to the current directory...");
		
		//error message prints here if writeFile finds a file of the specified name already exists
		//can obiviously choose any filename we like... the writeFile method will add the correct extension

		AdvancedTrack.writeFile("./AdvancedTrack");
	}

	private static void Test2() throws FileNotFoundException{

		System.out.println("\nBegin Test2()\n");

		Track IntermediateTrack = new Track("points.txt");
		
		//figure out number of high points and print a relevant message
		int noOfHighPoints = IntermediateTrack.highestPoint().size();
		if (noOfHighPoints>1){
			System.out.println("There are " + noOfHighPoints + " highest points in the track:");
		}
		if (noOfHighPoints==1){
			System.out.println("There is 1 highest point in the track:");
		}
		//print all high points (if k=0 and never changes this will return a single highest point.)
		int k = 0;
		while (k < noOfHighPoints){
			System.out.print(IntermediateTrack.highestPoint().get(k).getLongitude() + " | ");
			System.out.print(IntermediateTrack.highestPoint().get(k).getLatitude() + " | ");
			System.out.println(IntermediateTrack.highestPoint().get(k).getElevation());
			k++;
		}
		
		System.out.print("\n");		

		//reload track
		IntermediateTrack = new Track("points.txt");
		
		//figure out number of low points and print a relevant message
		int noOfLowPoints = (IntermediateTrack.lowestPoint().size());
		if (noOfLowPoints>1){
			System.out.println("There are " + noOfLowPoints + " lowest points in the track:");
		}
		if (noOfLowPoints==1){
			System.out.println("There is 1 lowest point in the track:");
		}
		
		//print all low points (if k=0 and never changes this will return a single lowest point.)
		k=0;
		while (k < noOfLowPoints){
			System.out.print(IntermediateTrack.lowestPoint().get(k).getLongitude() + " | ");
			System.out.print(IntermediateTrack.lowestPoint().get(k).getLatitude() + " | ");
			System.out.println(IntermediateTrack.lowestPoint().get(k).getElevation());
			k++;
		}
		
		System.out.print("\n");	

		//print total points and length of the track
		System.out.println("There are " + IntermediateTrack.size() + " total points in the track.");
		System.out.println("Total distance: " + (int)IntermediateTrack.totalDistance() + "m (To the nearest metre)." );

	}

	private static void Test1() {

		System.out.println("\nBegin Test1()\n");		

		//create points
		Point p1 =  new Point(-1.53965235, 53.79999183, 48 );
		Point p2 =  new Point(-1.53960943, 53.79872451, 45);
		Point p3 =  new Point(-1.54544592, 53.79945956, 60);
		Point p4 =  new Point(-1.5523982, 53.7995863, 43);
		Point p5 =  new Point(-1.55892134, 53.79847104, 36);
		Point p6 =  new Point(-1.56325579, 53.79895263, 34);
		Point p7 =  new Point(-1.57780409, 53.80490868, 34);
		Point p8 =  new Point(-1.59720182, 53.81134533, 44);
		Point p9 =  new Point(-1.60213709, 53.81570344, 41);
		Point p10 =  new Point(-1.6024375, 53.81788233, 48);
		Point p11 =  new Point(-1.60308123, 53.81876905, 49);
		Point p12 =  new Point(-1.60239458, 53.81907307, 55);

		//create track
		Track SimpleTrack = new Track();

		//add points to track
		SimpleTrack.addPoint(p1);
		SimpleTrack.addPoint(p2);
		SimpleTrack.addPoint(p3);
		SimpleTrack.addPoint(p4);
		SimpleTrack.addPoint(p5);
		SimpleTrack.addPoint(p6);
		SimpleTrack.addPoint(p7);
		SimpleTrack.addPoint(p8);
		SimpleTrack.addPoint(p9);
		SimpleTrack.addPoint(p10);
		SimpleTrack.addPoint(p11);
		SimpleTrack.addPoint(p12);

		//print information
		System.out.print("From Vicar Lane to De Lacy Mount is: ");
		System.out.print((int)SimpleTrack.totalDistance());
		System.out.println(" metres, to the nearest metre.");

		return;
	}

}

