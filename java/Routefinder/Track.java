import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

/*
 *                            CLASS DEFINITION - TRACK
 * 
 *                                  DESCRIPTION:
 * 
 * This class represents a geographical path, comprised of objects of the Point class.
 * 
 *                                  ATTRIBUTES:
 * 
 * -Variables and Arrays
 * 
 * 
 * numberOfPoints - The number of points in the track
 * Path - An ArrayList of Point objects.
 * ElevationList - An ArrayList of sorted elevation levels from the track
 * Outliers - An ArrayList generated of single the highest or lowest point(s) of the track
 * contents - A Scanner user to read file input
 * KmlHeader - A String that stores the header used for writing KML files
 * KmlFooter - "  "           "   "     footer        "   "        "   "
 * 
 * 
 * -Methods ({ methodName(Input Arguments) {Return Type} - Description.  })
 * 
 * 
 * Constructor(),(String) - Can be overloaded with a String file path or called empty to 
 *                          generate a new Track.
 * 
 * addPoint(Point) {void} - Will add a specified point on to the end of a Path.
 * 
 * size() {int} - Will return the amount of points in the path.
 * 
 * nearestPoint(Point) {Point} - Returns the nearest point in the track from any input point.
 * 
 * lowestPoint() {ArrayList <Point>} - Returns the lowest point(s) in the form of an array 
 *                                     starting at [0] if 1 point.
 * 
 * highestPoint() {Point ArraylList <Point>} - Returns the highest point(s) in the form of 
 *                                            an array starting at [0] if 1 point.
 * 
 * totalDistance() {Double} - Returns the total distance of the path in metres.
 * 
 * readFile(String filename) {void} - Creates points from a file 'filename' in the order 
 *                                    longitude,latitude, elevation; adds these to Path.
 * 
 * writeFile(String filename) {void} - Writes a .KML file 'filename' of points from Path
 *                                     that can be opened in google earth.
 * 				  
 * 
 * @ll14ec
 * 
 */ 


public class Track{

	private int numberOfPoints = 0; 
	private ArrayList <Point> Path = new ArrayList<>();
	private ArrayList <Double> ElevationList= new ArrayList<>();
	private ArrayList <Point> Outliers = new ArrayList<>();
	private Scanner contents;
	private String KmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <kml xmlns=\"http://www.opengis.net/kml/2.2\">";
	private String KmlAttributes = "<Style id=\"Mint\">\n<LineStyle>\n<color>7f66ff99</color>\n<width>3</width>\n</LineStyle>\n</Style>";
	private String KmlFooter = "</kml>";

	public Track(){
		//default constructor
	}
	/**
	 * Creates a track from a specified filename containing points.
	 *
	 * @param fileName
	 * @throws FileNotFoundException
	 */
	public Track(String fileName) throws FileNotFoundException{              
		readFile(fileName);
	}
	/**
	 * @param x - adds the point 'x' onto the end of the track.
	 */
	public void addPoint(Point x){
		Path.add(x);
		numberOfPoints++;
	}
	/**
	 * @return number of points in the path.
	 */
	public int size(){
		return Path.size();
	}
	/**
	 * @param x - generic point.
	 * @return - the nearest point within the path to point x.
	 */
	public Point nearestPoint(Point x){
		int i = 0;
		int finalPoint = 0;
		double aDistance = 0;
		double bDistance = Point.greatCircleDistance(x, Path.get(0));
		//loop constantly saves the lowest returned value into 'bDistance' for the length of the path
		for (i=1 ; i < Path.size() ; i++){
			aDistance = Point.greatCircleDistance(x, Path.get(i));
			if (aDistance < bDistance){
				bDistance = aDistance;
				finalPoint = i;
			}	
			//value of i == the point in the path with lowest distance from point x
		}
		return Path.get(finalPoint);
	}

	/**
	 * Finds the lowest point(s) in the path.
	 * @return these points in a point array.
	 */

	public ArrayList<Point> lowestPoint(){
		int i = 0;

		for (i=0; i<numberOfPoints-1 ;i++){
			//creates an array of doubles in the form elevation, longitude, latitude
			//from low-->high elevation (last element is of highest elevation)
			if ((Path.get(i).getElevation())<(Path.get(i+1).getElevation())){
				ElevationList.add(Path.get(i).getElevation());
				ElevationList.add(Path.get(i).getLongitude());
				ElevationList.add(Path.get(i).getLatitude());
				i+=2;
			}
			else{
				ElevationList.add(Path.get(i+1).getElevation());
				ElevationList.add(Path.get(i+1).getLongitude());
				ElevationList.add(Path.get(i+1).getLatitude());
				i+=2;
			}
		}

		int tracker = 1;

		double a = ElevationList.get(0);
		double b = ElevationList.get(0+3);

		i=3;
		//compares elevation values from the start of the array
		//if they are equal, then tracker is incremented
		while (i<Path.size()-1){
			if (a==b){
				a = ElevationList.get(i);
				b = ElevationList.get(i + 3);	
				tracker++;
				i+=3;
			}
			else {
				i+=3;
			}

		}
		i=0;
		//add all of the points (of equal and lowest elevation) from the start of the list
		while (i<tracker){
			Point z = new Point( (ElevationList.get(i+1)),(ElevationList.get(i+2)),(ElevationList.get(i)) );
			Outliers.add(z);		
			i+=3;
		}

		return (Outliers);
	}
	/**
	 * Finds the highest point(s) in the path. 
	 * @return these points in a point array.
	 */
	public ArrayList<Point> highestPoint(){
		int i = 0;
		//similar way to lowestPoint()
		for (i=0; i<numberOfPoints-2 ;i++){
			if ((Path.get(i).getElevation())>(Path.get(i+1).getElevation())){
				ElevationList.add(Path.get(i).getElevation());
				ElevationList.add(Path.get(i).getLongitude());
				ElevationList.add(Path.get(i).getLatitude());
			}
			else{
				ElevationList.add(Path.get(i+1).getElevation());
				ElevationList.add(Path.get(i+1).getLongitude());
				ElevationList.add(Path.get(i+1).getLatitude());
			}
		}

		i*=3;
		int eListLength = i;
		i-=3;
		int tracker = 1;
		// look at the elevations of the points in the list, starting at the end (highest)
		double a = ElevationList.get(i);
		double b = ElevationList.get(i-3);
		// find all equal highest values in the list, increment the tracker variable
		while (i>4){
			if (a==b){
				tracker++;
				i-=3;
				a = ElevationList.get(i);
				b = ElevationList.get(i-3);	
			}
			else {
				i-=3;
			}
		}

		i=0;
		//add the highest point(s) to the list of Outliers
		while (tracker>0){
			Point z = new Point( (ElevationList.get(eListLength-2)),(ElevationList.get(eListLength-1)),(ElevationList.get(eListLength-3)) );
			eListLength-=3;
			Outliers.add(z);		
			tracker-=1;
			i+=1;
		}

		return (Outliers);
	}
	/**
	 * @return the total distance of the path (m) using the greatCircleDistance method from Path.java.
	 */
	public double totalDistance(){
		double totalDistance = 0;
		int i = 0;		
		while (i<numberOfPoints-1){
			totalDistance += Point.greatCircleDistance(Path.get(i),Path.get(i+1));
			i++;
		}
		return totalDistance;
	}
	/**
	 * Reads a user specified file and adds points to the track.
	 * @param fileName (name of file to read)
	 */
	public void readFile(String fileName) throws FileNotFoundException{
		contents = new Scanner(new File(fileName));
		while (contents.hasNext()){
			//take doubles from the file in the order longitude, latitude, elevation
			//turns these doubles into points, and adds them to the path
			Point genericPoint = new Point( Double.parseDouble(contents.next()), Double.parseDouble(contents.next()), Double.parseDouble(contents.next()) );
			Path.add(genericPoint);
			numberOfPoints++;		
		}
	}
	/**
	 * Writes a KML format file with the data from the track .
	 * @param fileName - name to write to.
	 * @throws IOException - if file already created.
	 */
	public void writeFile(String fileName) throws IOException{

		java.io.File testFile = new java.io.File(fileName + ".kml");
		if (testFile.exists()){
			System.out.print("File not created; ");
			System.out.println("Please enter a filepath that does not already exist.");
			return;
		}

		//add .kml extension
		FileWriter newFile = new FileWriter(fileName  + ".kml");  
		
		//add KML formatting data
		newFile.write(KmlHeader);
		newFile.write("\n<Document>\n");	
		newFile.write(KmlAttributes);
		newFile.write("\n<Placemark>");
		newFile.write("\n<styleUrl>#Mint</styleUrl>");
		newFile.write("\n<LineString>");
		newFile.write("\n<coordinates>\n");
		//add points to the file. eg. -1.5234535,145.2344755,3577
		int i = 0;	
		for (i=0 ; i<Path.size(); i++){
			newFile.write(Double.toString(Path.get(i).getLongitude()));
			newFile.write(",");
			newFile.write(Double.toString(Path.get(i).getLatitude()));
			newFile.write(",");
			newFile.write(Double.toString(Path.get(i).getElevation()));
			newFile.write(" \n"); //whitespace after each point
		}
		//close all tags
		newFile.write("</coordinates>");
		newFile.write("\n</LineString>");
		newFile.write("\n</Placemark>");
		newFile.write("\n</Document>\n");
		newFile.write(KmlFooter);
		newFile.close();
	}

}

