import static java.lang.Math.abs;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;


/*                               CLASS DEEFINITION - POINT
 * 
 *                                      DESCRIPTION:
 *                                 
 * This class represents a single geographical point such as a placemark.
 * 
 *                                      ATTRIBUTES:
 *                                  
 * -Constants - MIN/MAX LONGITUDE/LATITUDE - Used for data verification.
 *              MEAN_EARTH_RADIUS - Used for finding the distance between two points.
 *             
 * -Variables 
 * 
 *  
 * longitude, latitude, elevation (DOUBLE) - Of the geographical point. 
 * 
 * 
 * -Methods ({ methodName(Input Arguments) {Return Type} - Description.  })
 * 
 *                                 
 * Constructors(),(long., lat., ele.) - Creates a point without/with geographical data.
 *
 * toString(void) {void} - Prints a string containing the Point's information to the console.
 * 
 * getLongitude(void) {double} - returns the value of the Points's longitude variable.
 * 
 * getLatitude (void {double} - returns the value of the Points's latitude variable.
 * 
 * getElevation(void) {double} - returns the value of the Points's elevation variable.
 * 
 * greatCircleDistance(Point, Point) {Double} - Returns the distance between two Points.
 * 
 */

/**
 * Representation of a point in space recorded by a GPS sensor.
 *
 * @author Nick Efford
 * @author ll14ec
 */
public class Point
{
	// Constants useful for bounds checking
	private static final double MIN_LONGITUDE = -180.0;
	private static final double MAX_LONGITUDE = 180.0;
	private static final double MIN_LATITUDE = -90.0;
	private static final double MAX_LATITUDE = 90.0;
	private static final double MEAN_EARTH_RADIUS = 6.371009e+6;

	// Fields of a point
	private double longitude = 0;   // in degrees, West is negative
	private double latitude = 0;    // in degrees
	private double elevation = 0;   // above sea level, in metres
	private String pointInfo = "";

	public Point(){
	}
	/**
	 * Alternate constructor - will check the bounds of lo/la against pre-defined MAX values.
	 * @param lo - longitude
	 * @param la - latitude
	 */
	public Point(double lo, double la){					
		if (lo>MAX_LONGITUDE || lo<MIN_LONGITUDE ){
			throw new IllegalArgumentException("Longitude must be between "+ MIN_LONGITUDE + "and " + MAX_LONGITUDE + ", you entered: " + lo);
		}
		if (la>MAX_LATITUDE || la<MIN_LATITUDE ){
			throw new IllegalArgumentException("Latitude must be between "+ MIN_LATITUDE + "and " + MAX_LATITUDE + ", you entered: " + la);
		}
		this.longitude = lo;
		this.latitude = la;
	}
	/**
	 * Alternate constructor - will check the bounds of lo/la against pre-defined MAX values.
	 * @param lo - longitude
	 * @param la - latitude
	 * @param el - elevation
	 */
	public Point(double lo, double la, double el){				
		if (lo>MAX_LONGITUDE || lo<MIN_LONGITUDE ){
			throw new IllegalArgumentException("Longitude must be between "+ MIN_LONGITUDE + " and " + MAX_LONGITUDE + ", you entered: " + lo);
		}
		if (la>MAX_LATITUDE || la<MIN_LATITUDE ){
			throw new IllegalArgumentException("Latitude must be between "+ MIN_LATITUDE + " and " + MAX_LATITUDE + ", you entered: " + la);
		}
		this.longitude = lo;
		this.latitude = la;
		this.elevation = el;
	}

	/**
	 * @return a String representation of the point.
	 */
	
	public String toString (){					
		pointInfo = (Double.toString(this.getLongitude())+ ", " + Double.toString(this.getLatitude()) + ", " + Double.toString(this.getElevation()));
		return pointInfo;
	}

	/**
	 * @return Longitude of this point, in degrees.
	 */
	public double getLongitude(){
		return longitude;
	}

	/**
	 * @return Latitude of this point, in degrees.
	 */
	public double getLatitude(){
		return latitude;
	}

	/**
	 * @return Elevation of this point above sea level, in metres.
	 */
	public double getElevation(){
		return elevation;
	}

	/**
	 * Computes the great-circle distance or orthodromic distance between
	 * two points on a spherical surface, using Vincenty's formula.
	 *
	 * @param p First point.
	 * @param q Second point.
	 * @return Distance between the points, in metres.
	 */
	public static double greatCircleDistance(Point p, Point q){
		double phi1 = toRadians(p.getLatitude());
		double phi2 = toRadians(q.getLatitude());

		double lambda1 = toRadians(p.getLongitude());
		double lambda2 = toRadians(q.getLongitude());
		double delta = abs(lambda1 - lambda2);

		double firstTerm = cos(phi2)*sin(delta);
		double secondTerm = cos(phi1)*sin(phi2) - sin(phi1)*cos(phi2)*cos(delta);
		double top = sqrt(firstTerm*firstTerm + secondTerm*secondTerm);

		double bottom = sin(phi1)*sin(phi2) + cos(phi1)*cos(phi2)*cos(delta);

		return MEAN_EARTH_RADIUS * atan2(top, bottom);
	}
}


