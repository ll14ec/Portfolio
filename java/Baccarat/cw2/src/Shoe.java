import java.util.Collections;

/*	Class Shoe
 * 
 * 	Defines the class for the collection of decks used to play baccarat.
 */

public class Shoe extends CardCollection 
{	
	
	/*	Constructor
	 * 
	 * 	Takes the number of decks desired and creates a new shoe.
	 * 	Throws an exception if the value entered is not valid.
	 * 	Value must be 4, 6, or 8.
	 */
	
	public Shoe(int decks) throws IllegalArgumentException
	{
		if (decks != 4 && decks !=6 && decks != 8)
		{
			throw new IllegalArgumentException(Integer.toString(decks) + "\nMust be 4, 6 or 8 decks in the shoe.\n");
		}
		
		else
		{
			int i = 0;
			int j = 0;
			int k = 0;
			
			for (i=0;i<decks;i++) 
			{
				for (j=0; j<12; j++)
				{
					for(k=0; k<4; k++)
					{
						Card c = new Card(Card.getRanks().get(j), Card.getSuits().get(k) );
						this.add(c);
					}
				}
			}
		}

	}

	/*	Deal
	 * 
	 * 	Takes the top card from the shoe, removes it, and returns the card.
	 */
	
	public Card deal()
	{
		Card c = new Card ( cards.get(cards.size()-1).getRank(), cards.get(cards.size()-1).getSuit() );
		cards.remove(cards.size()-1);
		return c;
	}
	
	/*	Shuffle
	 * 
	 * 	Randomises the order of the cards using the built in collections.shuffle() method.
	 */
	
	public void shuffle()
	{
		Collections.shuffle(cards);
	}
}
