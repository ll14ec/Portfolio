import java.util.Scanner;

/* 	Class - Game
 * 
 *	Contains all the code to create a game of Baccarat. 
 *
 */

public class Game 
{
	//banker and player hand
	private Hand bank;
	private Hand player;
	//Shoe of cards to be chosen for the game
	private Shoe gameShoe;
	//1 if a round has been played already
	private int roundPlayed;
	//tracks the number of player wins, banker wins, ties and games played
	private int bankWins;
	private int playerWins;
	private int ties;
	private int gamesPlayed;
	//used to take user input
	private Scanner s;

	/*	Constructor
	 * 
	 * 	Creates all the objects needed for the game and sets variables to 0
	 */

	public Game(int decks)
	{
		bank = new Hand();
		player = new Hand();
		gameShoe = new Shoe(decks);
		bankWins = 0;
		playerWins = 0;
		ties = 0;
	}

	/*	Play game with user prompt */

	public void playWithPrompt()
	{
		while (gameShoe.size() > 5 && playRoundPrompt() != -1)
		{
			; // keep playing until the user ends the game or the shoe runs out of cards
		}

		if (gameShoe.size() < 6)
		{
			System.out.println("The shoe has run out of cards");
		}

		getStats();
	}

	/*	Play game without user prompt */

	public void play()
	{
		while (gameShoe.size() > 5 && playRound() != -1)
		{
			; // keep playing until the user ends the game or the shoe runs out of cards
		}

		if (gameShoe.size() < 6)
		{
			System.out.println("The shoe has run out of cards");
		}

		getStats();
	}

	/* Begins the round */

	private void beginRound(){
		roundPlayed = 1;
		gameShoe.shuffle();
		bank.add(gameShoe.deal());
		bank.add(gameShoe.deal());
		player.add(gameShoe.deal());
		player.add(gameShoe.deal());

		System.out.println("Player hand: "+ player.toString());
		System.out.println("Bank hand: "+ bank.toString());

	}

	/* Checks if anyone has won the game by a 'natural' */
	
	private int initialCheck(){
		if (player.value() == 8)
		{
			if (bank.value() == 8)
			{
				System.out.println("Game ends in a draw, 8 to 8.");
				System.out.print("\n");
				gamesPlayed++;
				ties++;
				return 0;
			}
			if (bank.value() == 9)
			{
				System.out.println("Banker wins 9 to 8.");
				System.out.print("\n");
				gamesPlayed++;
				bankWins++;
				return 0;
			}
			else
			{
				System.out.println("Player wins 8 to " + bank.value() + ".");
				System.out.print("\n");
				gamesPlayed++;
				playerWins++;
				return 0;
			}
		}


		if (player.value() == 9)
		{
			if (bank.value() == 9)
			{
				System.out.println("Game ends in a draw, 9 to 9.");
			}
			else 
			{
				System.out.println("Player wins, 9 to " + bank.value() + ".");
				System.out.print("\n");
				gamesPlayed++;
				playerWins++;
				return 0;
			}			
		}


		if (bank.value() == 8)
		{
			if (player.value() < 8)
			{
				System.out.println("Banker wins on a natural, 8 to " + player.value() + ".");
				System.out.print("\n");
				gamesPlayed++;
				bankWins++;
				return 0;
			}
		}


		if (bank.value() == 9)
		{
			if (player.value() < 9)
			{
				System.out.println("Banker wins on a natural, 9 to " + player.value() + ".");
				System.out.print("\n");
				gamesPlayed++;
				bankWins++;
				return 0;
			}
		}
		return 1;
	}

	/* Chooses the third card according to the rules of the game */
	
	private void thirdCard(){

		int player3rdCard=0;

		s = new Scanner(System.in);
		s.nextLine();

		if (player.value()<6)
		{
			player3rdCard = 1;
			String cardString =  gameShoe.deal().toString();
			Card c = new Card(cardString);
			String cardString3 = gameShoe.deal().toString();
			Card c3 = new Card(cardString3);

			player.add(c);
			System.out.println("Player drew " +  c.getRank() +  c.getSuit() + ".");

			//If Player drew a 2 or 3, Banker draws with 0�4 and stands with 5�7.
			if ( c.getRank() == '2' ||  c.getRank() == '3')	
			{
				if (bank.value() < 5)
				{
					bank.add(c3);
					System.out.println("Banker drew " + c3.getRank() + c3.getSuit() + ".");
				}
				else
				{
					System.out.println("Banker Stands at " + bank.value());
				}
			}


			// If Player drew a 4 or 5, Banker draws with 0�5 and stands with 6�7.
			if ( c.getRank() == '4' ||  c.getRank() == '5')	
			{
				if (bank.value() < 6)
				{
					bank.add(c3);
					System.out.println("Banker drew " + c3.getRank() + c3.getSuit() + ".");
				}
				else
				{
					System.out.println("Banker Stands at " + bank.value());
				}
			}


			// If Player drew a 6 or 7, Banker draws with 0�6 and stands with 7.
			if ( c.getRank() == '6' ||  c.getRank() == '7')	
			{
				if (bank.value() < 7)
				{
					bank.add(c3);
					System.out.println("Banker drew " + c3.getRank() + c3.getSuit() + ".");
				}
				else
				{
					System.out.println("Banker Stands at " + bank.value());
				}
			}


			//   If Player drew an 8, Banker draws with 0�2 and stands with 3�7.
			if ( c.getRank() == '8')
			{
				if (bank.value() < 4)
				{
					bank.add(c3);
					System.out.println("Banker drew " + c3.getRank() + c3.getSuit() + ".");
				}
				else
				{
					System.out.println("Banker Stands at " + bank.value());
				}
			}


			// If Player drew an ace, 9, 10, or face-card, the Banker draws with 0�3 and stands with 4�7.
			if ( c.getRank() == 'A' ||  c.getRank() == '9' ||  c.getRank() == 'T' ||  c.getRank()== 'J' ||  c.getRank() == 'Q' ||  c.getRank() == 'K')
			{
				if (bank.value() < 4)
				{
					bank.add(c3);
					System.out.println("Banker drew " + c3.getRank() + c3.getSuit() + ".");
				}
				else
				{
					System.out.println("Banker Stands at " + bank.value());
				}
			}
		}

		if (player3rdCard == 0)
		{
			if (bank.value() < 6)
			{
				String cardString =  gameShoe.deal().toString();
				Card c2 = new Card(cardString);
				bank.add(c2);
				System.out.println("Player sticks at " + player.value() + ".");
				System.out.println("Banker takes " + cardString);
			}
		}

		System.out.println("Player score = " + player.value() + " ( hand = " + player.toString() + ").");
		System.out.println("Banker score = " + bank.value() + " ( hand = " + bank.toString() + ").");

	}

	/* Checks who has won the game after the third card has been drawn */
	
	private void finalCheck(){

		if (bank.value() > player.value())
		{
			System.out.println("Bank wins, " + bank.value() + " to " + player.value() + ".");
			System.out.print("\n");
			gamesPlayed++;
			bankWins++;
			return;
		}

		if (bank.value() < player.value())
		{
			System.out.println("Player wins, " + player.value() + " to " + bank.value() + ".");
			System.out.print("\n");
			gamesPlayed++;
			playerWins++;
			return;
		}

		if (bank.value() == player.value())
		{
			System.out.println("Game ends in a draw, " + player.value() + " to " + bank.value() + ".");
			System.out.print("\n");
			gamesPlayed++;
			ties++;
			return;
		}
	}

	/* Calls all the functions to play a round with user input */
	
	private int playRoundPrompt()
	{
		if (roundPlayed ==1)
		{
			bank.discard();
			player.discard();
			System.out.println("Play next round? Enter 1 for yes, 0 for no.");
			s = new Scanner(System.in);
			int userOption = s.nextInt();
			System.out.println(userOption);
			if (userOption == 0)
			{
				return -1;
			}
		}

		beginRound();

		if (initialCheck()  == 0)
		{
			return 0;
		}

		System.out.println("Player Score = " + player.value() + " Banker Score = " + bank.value());
		System.out.println("Press any key to continue with round");

		thirdCard();
		finalCheck();

		return 0;
	}

	/* Calls all the functions to play a round without user input */
	
	private int playRound()
	{
		if (roundPlayed == 1)
		{
			bank.discard();
			player.discard();
			System.out.println("Beggining new round... \n");
		}

		beginRound();

		if (initialCheck()  == 0)
		{
			return 0;
		}

		System.out.println("Player Score = " + player.value() + " Banker Score = " + bank.value());
		System.out.println("Press any key to continue with round");

		thirdCard();
		finalCheck();

		return 0;

	}

	/* Shows the user the statistics for the last game played */
	
	private void getStats()
	{
		System.out.println(gamesPlayed + " total games.");
		System.out.println(bankWins + " banker wins.");
		System.out.println(playerWins + " player wins.");
		System.out.println(ties + " total ties.");
		return;
	}

	public static void main(String [] args)
	{
		//BaccaratCard c1 = new BaccaratCard('A', 'C');
		//BaccaratCard c2 = new BaccaratCard('K', 'S');
		//BaccaratCard c3 = new BaccaratCard("3S");

		//System.out.println(c1.value());
		//System.out.println(c2.value());
		//System.out.println(c3.value());

		
		/* Makes a new game */
		Game mainGame = new Game(4);
		/*plays the game with user prompt*/
		mainGame.playWithPrompt();
		
		/*plays the game without user prompt*/
		//mainGame.play();

		//System.out.println(h1.toString());

	}
}
