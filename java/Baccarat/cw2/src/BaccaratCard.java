/* Class - BaccaratCard
 * 
 * Uses the super class Card to create a specific version.
 * 
 */

public class BaccaratCard extends Card
{
	
	/*	Constructor 1
	 *   
	 * 	Takes the rank and suit (char) as arguments and returns a new bacarratCard.
	 */
	
	public BaccaratCard(char rank, char suit)
	{
		super(rank,suit);
	}
	
	/*	Constructor 2 
	 *   
	 * 	Takes the rank and suit together (String) as argument and returns a new bacarratCard.
	 */

	public BaccaratCard(String code)
	{
		super(code);
	}

	/*	Value 
	 *   
	 *  Returns the value of the card using the method defined in Card.java
	 */
	
	public int value()
	{
		return super.value();
	}
}
