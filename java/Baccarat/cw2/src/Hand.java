/* 	Class Hand
 * 
 * 	Defines the methods necessary to create a hand of cards - extends the CardCollestion abstract class.
 */

public class Hand extends CardCollection 
{
	/* 	ToString
	 * 
	 * 	Returns a string containing the rank and suit data of all the cards in the hand.
	 */
	
	public String toString()
	{
		String hand = "";
		int i=0;
		while (i<cards.size())
		{
			hand += cards.get(i).getRank();
			hand += cards.get(i).getSuit();
			hand += " ";
			i++;
		}
		return hand;
	}

	/*	Value
	 * 
	 * 	Returns the total value of the hand according to the rules of baccarat.
	 * 
	 */
	
	public int value()
	{
		int value = 0;
		int i=0;
		while (i<cards.size())
		{
			value += cards.get(i).value();
			i++;
		}

		while (value>9)
		{
			value-=10;
		}
		
		return value;
	}

}
