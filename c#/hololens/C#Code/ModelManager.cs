﻿using Academy.HoloToolkit.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System;
using Net3dBool;

public class ModelManager : Singleton<ModelManager>
{
    float expandAnimationCompletionTime;
    // Store a bool for whether our astronaut model is expanded or not.
    bool isModelExpanding = false;

    // KeywordRecognizer object.
    KeywordRecognizer keywordRecognizer;

    public string interactOption { get; set; }
    public string xyz { get; set; }

    // Defines which function to call when a keyword is recognized.
    delegate void KeywordAction(PhraseRecognizedEventArgs args);
    Dictionary<string, KeywordAction> keywordCollection;

    void Start()
    {
        keywordCollection = new Dictionary<string, KeywordAction>();

        // Add keyword to start manipulation.
        keywordCollection.Add("Move", MoveModelCommand);

        keywordCollection.Add("Rotate", RotateModelCommand);

        keywordCollection.Add("Scale", ScaleModelCommand);

        keywordCollection.Add("Draw", DrawCommand);

        keywordCollection.Add("Build", BuildCommand);

        keywordCollection.Add("Group", SelectCommmand);

        keywordCollection.Add("Ok", DeSelectCommmand);

        keywordCollection.Add("Combine", CombineCommmand);

        keywordCollection.Add("Subtract", SubtractCommmand);

        // Initialize KeywordRecognizer with the previously added keywords.
        keywordRecognizer = new KeywordRecognizer(keywordCollection.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }


    void OnDestroy()
    {
        keywordRecognizer.Dispose();
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        KeywordAction keywordAction;

        if (keywordCollection.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke(args);
        }
    }

    private void MoveModelCommand(PhraseRecognizedEventArgs args)
    {
        GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
        interactOption = "move";
    }

    private void RotateModelCommand(PhraseRecognizedEventArgs args)
    {
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
        interactOption = "rotate";
    }

    private void ScaleModelCommand(PhraseRecognizedEventArgs args)
    {
        GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
        interactOption = "scale";
    }

    private void DrawCommand(PhraseRecognizedEventArgs args)
    {
        GestureManager.Instance.Transition(GestureManager.Instance.ManipulationRecognizer);
        interactOption = "draw";
    }

    private void BuildCommand(PhraseRecognizedEventArgs args) {
        if (LineDraw.Instance.Positions.Count > 0)
        {
            MeshGen.Instance.CreateSelection(LineDraw.Instance.Positions);
            LineDraw.Instance.DeleteLines();
        }
        //MeshGen.Instance.test();
    }

    private void SelectCommmand(PhraseRecognizedEventArgs args)
    {
        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
        interactOption = "select";
    }

    private void DeSelectCommmand(PhraseRecognizedEventArgs args)
    {
        if (GestureManager.Instance.selectedObjects.Count > 0)
        {
            interactOption = "deselect";
            GestureManager.Instance.deSelect();
        }
    }

    public void DeSelectCommmand()
    {
        if (GestureManager.Instance.selectedObjects.Count > 0)
        {
            interactOption = "deselect";
            GestureManager.Instance.deSelect();
        }
    }

    private void CombineCommmand(PhraseRecognizedEventArgs args)
    {
        if (GestureManager.Instance.selectedObjects.Count > 0)
        {
            MeshGen.Instance.makechild(GestureManager.Instance.selectedObjects);
        }
    }

    private void SubtractCommmand(PhraseRecognizedEventArgs args)
    {
        List<GameObject> objectList = GestureManager.Instance.selectedObjects;
        if (objectList.Count == 2)
        {
            MeshGen.Instance.objectDifference(objectList[0], objectList[1]);
        }
    }

    public void Update()
    {

    }

}


