﻿using UnityEngine;
using UnityEngine.VR.WSA.Input;
using System.Collections.Generic;

namespace Academy.HoloToolkit.Unity
{
    public class GestureManager : Singleton<GestureManager>
    {
        GameObject menuItem1;
        GameObject menuItem2;
        GameObject menuItem3;
        GameObject menuItem4;
        GameObject menuItem5;
        GameObject menuItem6;
        GameObject menuItem7;
        GameObject menuItem8;
        GameObject xTab;
        GameObject yTab;
        GameObject zTab;
        GameObject yesTab;
        GameObject noTab;
        GameObject cubeTab;
        GameObject cylTab;
        GameObject sphereTab;
        GameObject capTab;

        Material x;
        Material y;
        Material z;
        Material xi;
        Material yi;
        Material zi;

        Material M1;
        Material M2;
        Material M3;
        Material M4;
        Material M5;
        Material M6;
        Material M7;
        Material M8;
        Material Shapes;

        Material M1I;
        Material M2I;
        Material M3I;
        Material M4I;
        Material M5I;
        Material M6I;
        Material M7I;
        Material M8I;
        Material ShapesI;

        Material block;

        public char gridType { get; set; }
        public float gridSize { get; set; }
        public Vector3 gridConstant { get; set; }

        bool xyzExpanded = false;
        bool ynExpanded = false;
        bool shapesExpanded = false;

        Vector3 mStart = new Vector3();
        Vector3 mEnd = new Vector3();
        float amount = new float();

        public List<GameObject> selectedObjects = new List<GameObject>();
        public List<Color> gameObjectColours = new List<Color>();
        List<GameObject> menuItems = new List<GameObject>();

        public void deSelect()
        {
            int i = 0;
            foreach (var obj in selectedObjects)
            {
                obj.GetComponent<Renderer>().material.color = gameObjectColours[i];
                i++;
            }
            selectedObjects.Clear();
            gameObjectColours.Clear();
        }
        public GestureRecognizer NavigationRecognizer { get; private set; }

        public GestureRecognizer ManipulationRecognizer { get; private set; }

        public GestureRecognizer ActiveRecognizer { get; private set; }

        public bool IsNavigating { get; private set; }

        public Vector3 NavigationPosition { get; private set; }

        public bool IsManipulating { get; private set; }

        public Vector3 ManipulationPosition { get; private set; }

        void Awake()
        {
            gridSize = 0.05f;
            gridConstant = new Vector3(0.0f, 0.0f, 0.0f);

            NavigationRecognizer = new GestureRecognizer();

            NavigationRecognizer.SetRecognizableGestures(
                GestureSettings.Tap |
                GestureSettings.NavigationX |
                GestureSettings.NavigationY |
                GestureSettings.NavigationZ);

            NavigationRecognizer.TappedEvent += NavigationRecognizer_TappedEvent;
            NavigationRecognizer.NavigationStartedEvent += NavigationRecognizer_NavigationStartedEvent;
            NavigationRecognizer.NavigationUpdatedEvent += NavigationRecognizer_NavigationUpdatedEvent;
            NavigationRecognizer.NavigationCompletedEvent += NavigationRecognizer_NavigationCompletedEvent;
            NavigationRecognizer.NavigationCanceledEvent += NavigationRecognizer_NavigationCanceledEvent;

            ManipulationRecognizer = new GestureRecognizer();

            ManipulationRecognizer.SetRecognizableGestures(
                GestureSettings.ManipulationTranslate |
                GestureSettings.Tap);

            ManipulationRecognizer.ManipulationStartedEvent += ManipulationRecognizer_ManipulationStartedEvent;
            ManipulationRecognizer.ManipulationUpdatedEvent += ManipulationRecognizer_ManipulationUpdatedEvent;
            ManipulationRecognizer.ManipulationCompletedEvent += ManipulationRecognizer_ManipulationCompletedEvent;
            ManipulationRecognizer.ManipulationCanceledEvent += ManipulationRecognizer_ManipulationCanceledEvent;
            ManipulationRecognizer.TappedEvent += ManipulationRecognizer_TappedEvent;

            //ResetGestureRecognizers();
            Transition(NavigationRecognizer);

            x = new Material(Resources.Load("Materials/X", typeof(Material)) as Material);
            y = new Material(Resources.Load("Materials/Y", typeof(Material)) as Material);
            z = new Material(Resources.Load("Materials/Z", typeof(Material)) as Material);

            xi = new Material(Resources.Load("Materials/XI", typeof(Material)) as Material);
            yi = new Material(Resources.Load("Materials/YI", typeof(Material)) as Material);
            zi = new Material(Resources.Load("Materials/ZI", typeof(Material)) as Material);

            M1 = new Material(Resources.Load("Materials/Draw", typeof(Material)) as Material);
            M1I = new Material(Resources.Load("Materials/DrawI", typeof(Material)) as Material);

            M3 = new Material(Resources.Load("Materials/Move", typeof(Material)) as Material);
            M3I = new Material(Resources.Load("Materials/MoveI", typeof(Material)) as Material);

            M4 = new Material(Resources.Load("Materials/Scale", typeof(Material)) as Material);
            M4I = new Material(Resources.Load("Materials/ScaleI", typeof(Material)) as Material);

            M5 = new Material(Resources.Load("Materials/Rotate", typeof(Material)) as Material);
            M5I = new Material(Resources.Load("Materials/RotateI", typeof(Material)) as Material);

            M7 = new Material(Resources.Load("Materials/Group", typeof(Material)) as Material);
            M7I = new Material(Resources.Load("Materials/GroupI", typeof(Material)) as Material);

            M8 = new Material(Resources.Load("Materials/Extrude", typeof(Material)) as Material);
            M8I = new Material(Resources.Load("Materials/ExtrudeI", typeof(Material)) as Material);

            Shapes = new Material(Resources.Load("Materials/Shapes", typeof(Material)) as Material);
            ShapesI = new Material(Resources.Load("Materials/ShapesI", typeof(Material)) as Material);

            block = new Material(Resources.Load("x", typeof(Material)) as Material);

        }

        void OnDestroy()
        {
            NavigationRecognizer.TappedEvent -= NavigationRecognizer_TappedEvent;

            NavigationRecognizer.NavigationStartedEvent -= NavigationRecognizer_NavigationStartedEvent;
            NavigationRecognizer.NavigationUpdatedEvent -= NavigationRecognizer_NavigationUpdatedEvent;
            NavigationRecognizer.NavigationCompletedEvent -= NavigationRecognizer_NavigationCompletedEvent;
            NavigationRecognizer.NavigationCanceledEvent -= NavigationRecognizer_NavigationCanceledEvent;

            ManipulationRecognizer.ManipulationStartedEvent -= ManipulationRecognizer_ManipulationStartedEvent;
            ManipulationRecognizer.ManipulationUpdatedEvent -= ManipulationRecognizer_ManipulationUpdatedEvent;
            ManipulationRecognizer.ManipulationCompletedEvent -= ManipulationRecognizer_ManipulationCompletedEvent;
            ManipulationRecognizer.ManipulationCanceledEvent -= ManipulationRecognizer_ManipulationCanceledEvent;
            ManipulationRecognizer.TappedEvent -= ManipulationRecognizer_TappedEvent;
        }

        public void ResetGestureRecognizers()
        {
            //Transition(NavigationRecognizer);
        }

        public void Transition(GestureRecognizer newRecognizer)
        {
            if (newRecognizer == null)
            {
                return;
            }

            if (ActiveRecognizer != null)
            {
                if (ActiveRecognizer == newRecognizer)
                {
                    return;
                }
                ActiveRecognizer.CancelGestures();
                ActiveRecognizer.StopCapturingGestures();
            }

            newRecognizer.StartCapturingGestures();
            ActiveRecognizer = newRecognizer;
        }

        private void NavigationRecognizer_NavigationStartedEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                IsNavigating = true;
                NavigationPosition = relativePosition;
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("SetXYZ", ModelManager.Instance.xyz);
            }
        }

        private void NavigationRecognizer_NavigationUpdatedEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                IsNavigating = true;
                NavigationPosition = relativePosition;
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("SetXYZ", ModelManager.Instance.xyz);
            }
        }

        private void NavigationRecognizer_NavigationCompletedEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            IsNavigating = false;
        }

        private void NavigationRecognizer_NavigationCanceledEvent(InteractionSourceKind source, Vector3 relativePosition, Ray ray)
        {
            IsNavigating = false;
        }

        private void ManipulationRecognizer_ManipulationStartedEvent(InteractionSourceKind source, Vector3 position, Ray ray)
        {
            if (ModelManager.Instance.interactOption == "draw" || ModelManager.Instance.interactOption == "extrude")
            {
                mStart = HandsTrackingManager.Instance.lastPos;
            }
            else if (ModelManager.Instance.interactOption == "extruding")
            {
                mStart = HandsTrackingManager.Instance.lastPos;
                MeshGen.Instance.extrude(gridType, 0.0f);
            }
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                IsManipulating = true;
                ManipulationPosition = position;
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("setXYZ", ModelManager.Instance.xyz);
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("PerformManipulationStart", position);
            }
        }

        private void ManipulationRecognizer_ManipulationUpdatedEvent(InteractionSourceKind source, Vector3 position, Ray ray)
        {
            LineDraw.Instance.destroyLastLine();
            if (ModelManager.Instance.interactOption == "draw" || ModelManager.Instance.interactOption == "extrude")
            {
                mEnd = HandsTrackingManager.Instance.lastPos;
                LineDraw.Instance.DrawLine(mStart, mEnd, Color.red, false);
            }
            else if (ModelManager.Instance.interactOption == "extruding")
            {
                mEnd = HandsTrackingManager.Instance.lastPos;
                if (gridType == 'y')
                {
                    amount = mEnd.y - mStart.y;
                }
                else if (gridType == 'x')
                {
                    amount = mEnd.x - mStart.x;
                }
                else if (gridType == 'z')
                {
                    amount = mEnd.z - mStart.z;
                }
                if (amount != 0)
                {
                    MeshGen.Instance.extrudeUpdate(gridType, amount);
                }
            }

            if (HandsManager.Instance.FocusedGameObject != null)
            {
                IsManipulating = true;
                ManipulationPosition = position;
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("PerformManipulationUpdate", position);
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("SetXYZ", ModelManager.Instance.xyz);
            }
        }

        private void ManipulationRecognizer_ManipulationCompletedEvent(InteractionSourceKind source, Vector3 position, Ray ray)
        {
            if (ModelManager.Instance.interactOption == "draw" || ModelManager.Instance.interactOption == "extrude")
            {
                mEnd = HandsTrackingManager.Instance.lastPos;
                LineDraw.Instance.DrawLine(mStart, mEnd, Color.blue, true);
            }
            else if (ModelManager.Instance.interactOption == "extruding")
            {
                mEnd = HandsTrackingManager.Instance.lastPos;
                if (gridType == 'y')
                {
                    amount = mEnd.y - mStart.y;
                }
                else if (gridType == 'x')
                {
                    amount = mEnd.x - mStart.x;
                }
                else if (gridType == 'z')
                {
                    amount = mEnd.z - mStart.z;
                }
                MeshGen.Instance.extrudeUpdate(gridType, amount);
                MeshGen.Instance.extrudeComplete();
                LineDraw.Instance.DeleteLines();
                ModelManager.Instance.interactOption = "move";
            }

            IsManipulating = false;
        }

        private void ManipulationRecognizer_ManipulationCanceledEvent(InteractionSourceKind source, Vector3 position, Ray ray)
        {
            if (ModelManager.Instance.interactOption == "draw" || ModelManager.Instance.interactOption == "extrude")
            {
                mEnd = HandsTrackingManager.Instance.lastPos;
                LineDraw.Instance.DrawLine(mStart, mEnd, Color.blue, true);
            }
            if (ModelManager.Instance.interactOption == "extruding")
            {
                LineDraw.Instance.DeleteLines();
                MeshGen.Instance.extrudeComplete();
            }
            IsManipulating = false;
        }

        private void ManipulationRecognizer_TappedEvent(InteractionSourceKind source, int tapCount, Ray ray)
        {
            if (HandsManager.Instance.FocusedGameObject != null)
            {
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("SetXYZ", ModelManager.Instance.xyz);
            }

            GameObject focusedObject = InteractibleManager.Instance.FocusedGameObject;
            
            if (focusedObject != null)
            {
                if (focusedObject.name == "Menu")
                {
                    expandCollapseMenu(focusedObject);
                }

                else if (focusedObject.name == "M1")
                {
                    highlight("draw");
                    ModelManager.Instance.interactOption = "draw";
                    Transition(ManipulationRecognizer);
                    collapseXYZ();
                    collapseShapes();
                    gridType = 'n';
                }

                else if (focusedObject.name == "M2")
                {
                    if (LineDraw.Instance.Positions != null)
                    {
                        if (LineDraw.Instance.Positions.Count > 0)
                        {
                            MeshGen.Instance.CreateSelection(LineDraw.Instance.Positions);
                            LineDraw.Instance.DeleteLines();
                        }
                        collapseXYZ();
                        collapseShapes();
                    }
                }

                else if (focusedObject.name == "M3")
                {
                    highlight("move");
                    Transition(ManipulationRecognizer);
                    ModelManager.Instance.interactOption = "move";
                    collapseShapes();
                    expandXYZ();
                }

                else if (focusedObject.name == "M4")
                {
                    highlight("scale");
                    Transition(ManipulationRecognizer);
                    ModelManager.Instance.interactOption = "scale";
                    collapseShapes();
                    expandXYZ();
                }

                else if (focusedObject.name == "M5")
                {
                    highlight("rotate");
                    Transition(NavigationRecognizer);
                    ModelManager.Instance.interactOption = "rotate";
                    collapseShapes();
                    expandXYZ();
                }

                else if (focusedObject.name == "M6")
                {
                    List<GameObject> objectList = selectedObjects;
                    if (objectList.Count == 2)
                    {
                        //Task task = new Task(
                        //    async () =>
                        //    {
                        //        MeshGen.Instance.objectDifference(objectList[0], objectList[1]);
                        //    }
                        //    );
                        //task.Start();
                        MeshGen.Instance.objectDifference(objectList[0], objectList[1]);
                    }
                    collapseXYZ();
                    collapseShapes();
                }

                else if (focusedObject.name == "M7")
                {
                    highlight("select");
                    if (ModelManager.Instance.interactOption != "select")
                    {
                        Transition(NavigationRecognizer);
                        ModelManager.Instance.interactOption = "select";
                    }

                    else
                    {
                        if (selectedObjects.Count > 0)
                        {
                            ModelManager.Instance.interactOption = "deselect";
                            deSelect();
                        }
                    }
                    collapseXYZ();
                    collapseShapes();
                }

                else if (focusedObject.name == "M8")
                {
                    highlight("extrude");
                    ModelManager.Instance.interactOption = "selectexdir";
                    collapseShapes();
                    expandXYZ();
                    Transition(ManipulationRecognizer);
                }

                else if (focusedObject.name == "Shapes")
                {
                    highlight("shapes");
                    ModelManager.Instance.interactOption = "shapes";
                    collapseXYZ();
                    collapseYN();
                    expandShapes();
                }

                else if (focusedObject.name == "Yes")
                {
                    if (!LineDraw.Instance.checkPolygon())
                    {
                        LineDraw.Instance.DeleteLines();
                    }
                    else
                    {
                        ModelManager.Instance.interactOption = "extruding";
                    }
                }

                else if (focusedObject.name == "No")
                {
                    LineDraw.Instance.DeleteLines();
                }

                else if (focusedObject.name == "Y")
                {
                    if (ModelManager.Instance.interactOption == "selectexdir")
                    {
                        gridType = 'y';
                        LineDraw.Instance.DrawPlane(gridType, gridSize, gridConstant);
                        collapseXYZ();
                        collapseShapes();
                        expandYN();
                        ModelManager.Instance.interactOption = "extrude";
                    }
                    else
                    {
                        ModelManager.Instance.xyz = "Y";
                        focusedObject.GetComponent<MeshRenderer>().material = yi;

                        GameObject.Find("Z").GetComponent<MeshRenderer>().material = z;
                        GameObject.Find("X").GetComponent<MeshRenderer>().material = x;
                    }
                }

                else if (focusedObject.name == "X")
                {
                    if (ModelManager.Instance.interactOption == "selectexdir")
                    {
                        gridType = 'x';
                        LineDraw.Instance.DrawPlane(gridType, gridSize, gridConstant);
                        collapseXYZ();
                        collapseShapes();
                        expandYN();
                        ModelManager.Instance.interactOption = "extrude";
                    }
                    else
                    {
                        ModelManager.Instance.xyz = "X";
                        focusedObject.GetComponent<MeshRenderer>().material = xi;
                        GameObject.Find("Z").GetComponent<MeshRenderer>().material = z;
                        GameObject.Find("Y").GetComponent<MeshRenderer>().material = y;
                    }
                }

                else if (focusedObject.name == "Z")
                {
                    if (ModelManager.Instance.interactOption == "selectexdir")
                    {
                        gridType = 'z';
                        LineDraw.Instance.DrawPlane(gridType, gridSize, gridConstant);
                        collapseXYZ();
                        collapseShapes();
                        expandYN();
                        ModelManager.Instance.interactOption = "extrude";
                    }
                    else
                    {
                        ModelManager.Instance.xyz = "Z";
                        focusedObject.GetComponent<MeshRenderer>().material = zi;

                        GameObject.Find("Y").GetComponent<MeshRenderer>().material = y;
                        GameObject.Find("X").GetComponent<MeshRenderer>().material = x;
                    }
                }

                else if (focusedObject.name == "Cub")
                {
                    createNewPrimitave("Cuboid");
                }

                else if (focusedObject.name == "Cap")
                {
                    createNewPrimitave("Capsule");
                }

                else if (focusedObject.name == "Cyl")
                {
                    createNewPrimitave("Cylinder");
                }

                else if (focusedObject.name == "Sph")
                {
                    createNewPrimitave("Sphere");
                }
            }
        }

        private void NavigationRecognizer_TappedEvent(InteractionSourceKind source, int tapCount, Ray ray)
        {

            if (HandsManager.Instance.FocusedGameObject != null)
            {
                HandsManager.Instance.FocusedGameObject.SendMessageUpwards("SetXYZ", ModelManager.Instance.xyz);
            }

            GameObject focusedObject = InteractibleManager.Instance.FocusedGameObject;

            if (focusedObject != null)
            {
                focusedObject.SendMessageUpwards("OnSelect");

                if (focusedObject.name == "Menu")
                {
                    expandCollapseMenu(focusedObject);
                }

                else if (focusedObject.name == "M1")
                {
                    highlight("draw");
                    ModelManager.Instance.interactOption = "draw";
                    Transition(ManipulationRecognizer);
                    collapseXYZ();
                    collapseShapes();
                    gridType = 'n';
                }

                else if (focusedObject.name == "M2")
                {
                    if (LineDraw.Instance.Positions != null)
                    {
                        if (LineDraw.Instance.Positions.Count > 0)
                        {
                            MeshGen.Instance.CreateSelection(LineDraw.Instance.Positions);
                            LineDraw.Instance.DeleteLines();
                        }
                        collapseXYZ();
                        collapseShapes();
                    }
                }

                else if (focusedObject.name == "M3")
                {
                    highlight("move");
                    Transition(ManipulationRecognizer);
                    ModelManager.Instance.interactOption = "move";
                    collapseShapes();
                    expandXYZ();
                }

                else if (focusedObject.name == "M4")
                {
                    highlight("scale");
                    Transition(ManipulationRecognizer);
                    ModelManager.Instance.interactOption = "scale";
                    collapseShapes();
                    expandXYZ();
                }

                else if (focusedObject.name == "M5")
                {
                    highlight("rotate");
                    Transition(NavigationRecognizer);
                    ModelManager.Instance.interactOption = "rotate";
                    collapseShapes();
                    expandXYZ();
                }

                else if (focusedObject.name == "M6")
                {
                    List<GameObject> objectList = selectedObjects;
                    if (objectList.Count == 2)
                    {
                        //Task task = new Task(
                        //    async() =>
                        //    {
                        //        MeshGen.Instance.objectDifference(objectList[0], objectList[1]);
                        //    }
                        //    );
                        //task.Start();
                        MeshGen.Instance.objectDifference(objectList[0], objectList[1]);
                    }
                    collapseXYZ();
                    collapseShapes();
                }

                else if (focusedObject.name == "M7")
                {
                    highlight("select");
                    if (ModelManager.Instance.interactOption != "select")
                    {
                        Transition(NavigationRecognizer);
                        ModelManager.Instance.interactOption = "select";
                    }

                    else
                    {
                        if (selectedObjects.Count > 0)
                        {
                            ModelManager.Instance.interactOption = "deselect";
                            deSelect();
                        }
                    }
                    collapseXYZ();
                    collapseShapes();
                }

                else if (focusedObject.name == "M8")
                {
                    highlight("extrude");
                    ModelManager.Instance.interactOption = "selectexdir";
                    collapseShapes();
                    expandXYZ();
                    Transition(ManipulationRecognizer);
                }

                else if (focusedObject.name == "Shapes")
                {
                    highlight("shapes");
                    ModelManager.Instance.interactOption = "shapes";
                    collapseXYZ();
                    collapseYN();
                    expandShapes();
                }

                else if (focusedObject.name == "Yes")
                {
                    if (!LineDraw.Instance.checkPolygon())
                    {
                        LineDraw.Instance.DeleteLines();
                    }
                    else
                    {
                        ModelManager.Instance.interactOption = "extruding";
                    }
                }

                else if (focusedObject.name == "No")
                {
                    LineDraw.Instance.DeleteLines();
                }

                else if (focusedObject.name == "Y")
                {
                    if (ModelManager.Instance.interactOption == "selectexdir")
                    {
                        gridType = 'y';
                        LineDraw.Instance.DrawPlane(gridType, gridSize, gridConstant);
                        collapseXYZ();
                        collapseShapes();
                        expandYN();
                        ModelManager.Instance.interactOption = "extrude";
                    }
                    else
                    {
                        ModelManager.Instance.xyz = "Y";
                        focusedObject.GetComponent<MeshRenderer>().material = yi;

                        GameObject.Find("Z").GetComponent<MeshRenderer>().material = z;
                        GameObject.Find("X").GetComponent<MeshRenderer>().material = x;
                    }
                }

                else if (focusedObject.name == "X")
                {
                    if (ModelManager.Instance.interactOption == "selectexdir")
                    {
                        gridType = 'x';
                        LineDraw.Instance.DrawPlane(gridType, gridSize, gridConstant);
                        collapseXYZ();
                        collapseShapes();
                        expandYN();
                        ModelManager.Instance.interactOption = "extrude";
                    }
                    else
                    {
                        ModelManager.Instance.xyz = "X";
                        focusedObject.GetComponent<MeshRenderer>().material = xi;
                        GameObject.Find("Z").GetComponent<MeshRenderer>().material = z;
                        GameObject.Find("Y").GetComponent<MeshRenderer>().material = y;
                    }
                }

                else if (focusedObject.name == "Z")
                {
                    if (ModelManager.Instance.interactOption == "selectexdir")
                    {
                        gridType = 'z';
                        LineDraw.Instance.DrawPlane(gridType, gridSize, gridConstant);
                        collapseXYZ();
                        collapseShapes();
                        expandYN();
                        ModelManager.Instance.interactOption = "extrude";
                    }
                    else
                    {
                        ModelManager.Instance.xyz = "Z";
                        focusedObject.GetComponent<MeshRenderer>().material = zi;

                        GameObject.Find("Y").GetComponent<MeshRenderer>().material = y;
                        GameObject.Find("X").GetComponent<MeshRenderer>().material = x;
                    }
                }

                else if (focusedObject.name == "Cub")
                {
                    createNewPrimitave("Cuboid");
                }

                else if (focusedObject.name == "Cap")
                {
                    createNewPrimitave("Capsule");
                }

                else if (focusedObject.name == "Cyl")
                {
                    createNewPrimitave("Cylinder");
                }

                else if (focusedObject.name == "Sph")
                {
                    createNewPrimitave("Sphere");
                }

                else if (ModelManager.Instance.interactOption == "select")
                {
                    if (focusedObject.transform.parent != null)
                    {
                        GameObject temp;
                        int i = 0;
                        int childCount = focusedObject.transform.parent.childCount;
                        for (i = 0; i < childCount; i++)
                        {
                            temp = focusedObject.transform.parent.GetChild(i).gameObject;
                            if (!selectedObjects.Contains(temp))
                            {
                                selectedObjects.Add(temp);
                                gameObjectColours.Add(temp.GetComponent<Renderer>().material.color);
                                temp.GetComponent<Renderer>().material.color = Color.red;
                            }
                        }
                    }

                    else if (!selectedObjects.Contains(focusedObject))
                    {
                        selectedObjects.Add(focusedObject);
                        gameObjectColours.Add(focusedObject.GetComponent<Renderer>().material.color);
                        focusedObject.GetComponent<Renderer>().material.color = Color.red;
                    }
                }
            }
        }

        private void expandCollapseMenu(GameObject focusedObject)
        {
            menuItem1 = focusedObject.transform.GetChild(0).gameObject;
            menuItem2 = focusedObject.transform.GetChild(1).gameObject;
            menuItem3 = focusedObject.transform.GetChild(2).gameObject;
            menuItem4 = focusedObject.transform.GetChild(3).gameObject;
            menuItem5 = focusedObject.transform.GetChild(4).gameObject;
            menuItem6 = focusedObject.transform.GetChild(5).gameObject;
            menuItem7 = focusedObject.transform.GetChild(6).gameObject;
            menuItem8 = focusedObject.transform.GetChild(7).gameObject;

            if (!menuItem1.activeSelf)
            {
                menuItem1.SetActive(true);
                menuItem2.SetActive(true);
                menuItem3.SetActive(true);
                menuItem4.SetActive(true);
                menuItem5.SetActive(true);
                menuItem6.SetActive(true);
                menuItem7.SetActive(true);
                menuItem8.SetActive(true);
            }
            else
            {
                menuItem1.SetActive(false);
                menuItem2.SetActive(false);
                menuItem3.SetActive(false);
                menuItem4.SetActive(false);
                menuItem5.SetActive(false);
                menuItem6.SetActive(false);
                menuItem7.SetActive(false);
                menuItem8.SetActive(false);
            }
        }

        private void expandShapes()
        {
            GameObject focusedObject = GameObject.Find("Menu");
            cubeTab = focusedObject.transform.GetChild(14).gameObject;
            capTab = focusedObject.transform.GetChild(15).gameObject;
            cylTab = focusedObject.transform.GetChild(16).gameObject;
            sphereTab = focusedObject.transform.GetChild(17).gameObject;

            cubeTab.SetActive(true);
            capTab.SetActive(true);
            cylTab.SetActive(true);
            sphereTab.SetActive(true);

            shapesExpanded = true;

        }

        private void collapseShapes()
        {
            if (shapesExpanded)
            {
                GameObject focusedObject = GameObject.Find("Menu");
                cubeTab = focusedObject.transform.GetChild(14).gameObject;
                capTab = focusedObject.transform.GetChild(15).gameObject;
                cylTab = focusedObject.transform.GetChild(16).gameObject;
                sphereTab = focusedObject.transform.GetChild(17).gameObject;

                cubeTab.SetActive(false);
                capTab.SetActive(false);
                cylTab.SetActive(false);
                sphereTab.SetActive(false);

                shapesExpanded = false;
            }
        }

        private void expandXYZ()
        {
            GameObject focusedObject = GameObject.Find("Menu");
            yTab = focusedObject.transform.GetChild(8).gameObject;
            xTab = focusedObject.transform.GetChild(9).gameObject;
            zTab = focusedObject.transform.GetChild(10).gameObject;

            yTab.SetActive(true);
            xTab.SetActive(true);
            zTab.SetActive(true);

            xyzExpanded = true;
        }

        private void collapseXYZ()
        {
            if (xyzExpanded)
            {
                GameObject focusedObject = GameObject.Find("Menu");
                yTab = focusedObject.transform.GetChild(8).gameObject;
                xTab = focusedObject.transform.GetChild(9).gameObject;
                zTab = focusedObject.transform.GetChild(10).gameObject;

                GameObject.Find("Y").GetComponent<MeshRenderer>().material = y;
                GameObject.Find("X").GetComponent<MeshRenderer>().material = x;
                GameObject.Find("Z").GetComponent<MeshRenderer>().material = z;

                yTab.SetActive(false);
                xTab.SetActive(false);
                zTab.SetActive(false);

                ModelManager.Instance.xyz = "XYZ";
                xyzExpanded = false;
            }
        }

        private void expandYN()
        {
            GameObject focusedObject = GameObject.Find("Menu");
            yesTab = focusedObject.transform.GetChild(11).gameObject;
            noTab = focusedObject.transform.GetChild(12).gameObject;

            yesTab.SetActive(true);
            noTab.SetActive(true);

            ynExpanded = true;
        }

        private void collapseYN()
        {
            if (ynExpanded)
            {
                GameObject focusedObject = GameObject.Find("Menu");
                yesTab = focusedObject.transform.GetChild(11).gameObject;
                noTab = focusedObject.transform.GetChild(12).gameObject;

                yesTab.SetActive(false);
                noTab.SetActive(false);

                ynExpanded = false;
            }
        }

        private void highlight(string opt)
        {
            string io = ModelManager.Instance.interactOption;

            if (io == "draw")
            {
                GameObject.Find("M1").GetComponent<MeshRenderer>().material = M1;
            }

            else if (io == "move")
            {
                GameObject.Find("M3").GetComponent<MeshRenderer>().material = M3;
            }

            else if (io == "scale")
            {
                GameObject.Find("M4").GetComponent<MeshRenderer>().material = M4;
            }

            else if (io == "rotate")
            {
                GameObject.Find("M5").GetComponent<MeshRenderer>().material = M5;
            }

            else if (io == "select")
            {
                GameObject.Find("M7").GetComponent<MeshRenderer>().material = M7;
            }

            else if (io == "extrude")
            {
                GameObject.Find("M8").GetComponent<MeshRenderer>().material = M8;
            }

            else if(io == "shapes")
            {
                GameObject.Find("Shapes").GetComponent<MeshRenderer>().material = Shapes;
            }


            if (opt == "draw")
            {
                GameObject.Find("M1").GetComponent<MeshRenderer>().material = M1I;
            }

            else if (opt == "move")
            {
                GameObject.Find("M3").GetComponent<MeshRenderer>().material = M3I;
            }

            else if (opt == "scale")
            {
                GameObject.Find("M4").GetComponent<MeshRenderer>().material = M4I;
            }

            else if (opt == "rotate")
            {
                GameObject.Find("M5").GetComponent<MeshRenderer>().material = M5I;
            }

            else if (opt == "select")
            {
                GameObject.Find("M7").GetComponent<MeshRenderer>().material = M7I;
            }

            else if (opt == "extrude")
            {
                GameObject.Find("M8").GetComponent<MeshRenderer>().material = M8I;
            }

            else if (io == "shapes")
            {
                GameObject.Find("Shapes").GetComponent<MeshRenderer>().material = ShapesI;
            }
        }

        private void createNewPrimitave(string option)
        {

            GameObject newPrimitave = new GameObject();

            if (option == "Cuboid")
            {
                newPrimitave = GameObject.CreatePrimitive(PrimitiveType.Cube);
            }

            else if (option == "Sphere")
            {
                newPrimitave = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            }

            else if (option == "Cylinder")
            {
                newPrimitave = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            }

            else if (option == "Capsule")
            {
                newPrimitave = GameObject.CreatePrimitive(PrimitiveType.Capsule);
            }


            newPrimitave.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            newPrimitave.AddComponent<Interactible>();
            newPrimitave.AddComponent<GestureAction>();
            newPrimitave.AddComponent<ExpandModel>();
            Renderer renderer = newPrimitave.GetComponent<MeshRenderer>();
            renderer.material = block;
            renderer.material.color = Color.cyan;
        }
    }   
}