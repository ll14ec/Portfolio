﻿using Academy.HoloToolkit.Unity;
using UnityEngine;

public class GestureAction : MonoBehaviour
{
    public float RotationSensitivity = 4.0f;
    private Vector3 manipulationPreviousPosition;
    public string xyz;
    //public string xyz { get; set; }

    void Update()
    {
        PerformRotation(); 
    }

    private void PerformRotation()
    {
        if (GestureManager.Instance.IsNavigating)
        {
            if (GestureManager.Instance.selectedObjects.Count > 0)
            {
                foreach (var obj in GestureManager.Instance.selectedObjects)
                {
                    Vector3 nav = GestureManager.Instance.NavigationPosition;
                    if (obj.transform.parent != null)
                    {
                       Vector3 pos = obj.transform.parent.position;
                        if (xyz == "X")
                        {
                            obj.transform.parent.RotateAround(pos, new Vector3(0, 1, 0), nav.x * RotationSensitivity);
                        }

                        if (xyz == "Y")
                        {
                            obj.transform.parent.RotateAround(pos, new Vector3(1, 0, 0), nav.y * RotationSensitivity);
                        }

                        if (xyz == "Z")
                        {
                            obj.transform.parent.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                        }

                        //else
                        //{
                        //    obj.transform.parent.RotateAround(pos, new Vector3(1, 0, 0), nav.y * RotationSensitivity);
                        //    obj.transform.parent.RotateAround(pos, new Vector3(0, 1, 0), nav.x * RotationSensitivity);
                        //    obj.transform.parent.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                        //}

                    }
                    else
                    {
                        Vector3 pos = obj.GetComponent<Renderer>().bounds.center;
                        if (xyz == "X")
                        {
                            obj.transform.RotateAround(pos, new Vector3(1, 0, 0), nav.x * RotationSensitivity);
                        }

                        if (xyz == "Y")
                        {
                            obj.transform.RotateAround(pos, new Vector3(0, 1, 0), nav.y * RotationSensitivity);
                        }

                        if (xyz == "Z")
                        {
                            obj.transform.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                        }

                        //else
                        //{
                        //    obj.transform.RotateAround(pos, new Vector3(1, 0, 0), nav.y * RotationSensitivity);
                        //    obj.transform.RotateAround(pos, new Vector3(0, 1, 0), nav.x * RotationSensitivity);
                        //    obj.transform.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                        //}
                    }
                }
            }

            else
            {
                GameObject obj = HandsManager.Instance.FocusedGameObject;
                if (obj.name != "Menu")
                {
                    {
                        Vector3 nav = GestureManager.Instance.NavigationPosition;
                        if (obj.transform.parent != null)
                        {
                            Vector3 pos = obj.transform.parent.position;
                            if (xyz == "X")
                            {
                                obj.transform.parent.RotateAround(pos, new Vector3(1, 0, 0), nav.x * RotationSensitivity);
                            }

                            if (xyz == "Y")
                            {
                                obj.transform.parent.RotateAround(pos, new Vector3(0, 1, 0), nav.y * RotationSensitivity);
                            }

                            if (xyz == "Z")
                            {
                                obj.transform.parent.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                            }

                            //if (xyz == "XYZ")
                            //{
                            //    obj.transform.parent.RotateAround(pos, new Vector3(1, 0, 0), nav.y * RotationSensitivity);
                            //    obj.transform.parent.RotateAround(pos, new Vector3(0, 1, 0), nav.x * RotationSensitivity);
                            //    obj.transform.parent.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                            //}
                        }
                        else
                        {
                            Vector3 pos = obj.GetComponent<Renderer>().bounds.center;
                            if (xyz == "X")
                            {
                                obj.transform.RotateAround(pos, new Vector3(1, 0, 0), nav.y * RotationSensitivity);
                            }

                            if (xyz == "Y")
                            {
                                obj.transform.RotateAround(pos, new Vector3(0, 1, 0), nav.x * RotationSensitivity);
                            }

                            if (xyz == "Z")
                            {
                                obj.transform.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                            }

                            //if (xyz == "XYZ")
                            //{
                            //    obj.transform.RotateAround(pos, new Vector3(1, 0, 0), nav.y * RotationSensitivity);
                            //    obj.transform.RotateAround(pos, new Vector3(0, 1, 0), nav.x * RotationSensitivity);
                            //    obj.transform.RotateAround(pos, new Vector3(0, 0, 1), nav.z * RotationSensitivity);
                            //}
                        }
                    }
                }
            }
        }
    }

    void SetXYZ(string value)
    {
        xyz = value;
    }

    void PerformManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }

    void PerformManipulationUpdate(Vector3 position)
    {
        string interactOption = ModelManager.Instance.interactOption;
        int objectCount = GestureManager.Instance.selectedObjects.Count;

        if (GestureManager.Instance.IsManipulating)
        {
            if ((objectCount > 0) && (interactOption == "move"))
            {
                Vector3 moveVector = Vector3.zero;
                moveVector = position - manipulationPreviousPosition;
                manipulationPreviousPosition = position;

                if (xyz == "X")
                {
                    moveVector.z = 0;
                    moveVector.y = 0;
                }

                if (xyz == "Y")
                {
                    moveVector.z = 0;
                    moveVector.x = 0;
                }

                if (xyz == "Z")
                {
                    moveVector.x = 0;
                    moveVector.y = 0;
                }

                foreach (var obj in GestureManager.Instance.selectedObjects)
                {
                    if (obj.transform.parent != null)
                    {
                        obj.transform.parent.position += moveVector * 5.0f;
                    }
                    else
                    {
                        obj.transform.position += moveVector * 5.0f;
                    }
                }

            }

            else if ((objectCount > 0) && (interactOption == "scale"))
            {
                System.Diagnostics.Debug.WriteLine("Resizing");
                Vector3 moveVector = Vector3.zero;
                moveVector = position - manipulationPreviousPosition;
                manipulationPreviousPosition = position;

                if (xyz == "X")
                {
                    moveVector.z = 0;
                    moveVector.y = 0;
                }

                if (xyz == "Y")
                {
                    moveVector.z = 0;
                    moveVector.x = 0;
                }

                if (xyz == "Z")
                {
                    moveVector.x = 0;
                    moveVector.y = 0;
                }

                foreach (var obj in GestureManager.Instance.selectedObjects)
                {
                    if (obj.transform.parent != null)
                    {
                        obj.transform.parent.localScale += moveVector * 5.0f;
                    }
                    else
                    {
                        obj.transform.localScale += moveVector * 5.0f;
                    }
                }
            }

            else if (interactOption == "move")
            {
                GameObject obj = HandsManager.Instance.FocusedGameObject;
                if (obj.name != "Menu")
                {
                    Vector3 moveVector = Vector3.zero;
                    moveVector = position - manipulationPreviousPosition;
                    manipulationPreviousPosition = position;

                    if (xyz == "X")
                    {
                        moveVector.z = 0;
                        moveVector.y = 0;
                    }

                    if (xyz == "Y")
                    {
                        moveVector.z = 0;
                        moveVector.x = 0;
                    }

                    if (xyz == "Z")
                    {
                        moveVector.x = 0;
                        moveVector.y = 0;
                    }

                    if (obj.transform.parent != null)
                    {
                        obj.transform.parent.position += moveVector * 5.0f;
                    }
                    else
                    {
                        obj.transform.position += moveVector * 5.0f;
                    }
                }
            }

            else if (interactOption == "scale")
            {
                GameObject obj = HandsManager.Instance.FocusedGameObject;
                if (obj.name != "Menu")
                {
                    Vector3 moveVector = Vector3.zero;
                    moveVector = position - manipulationPreviousPosition;
                    manipulationPreviousPosition = position;

                    if (xyz == "X")
                    {
                        moveVector.z = 0;
                        moveVector.y = 0;
                    }

                    if (xyz == "Y")
                    {
                        moveVector.z = 0;
                        moveVector.x = 0;
                    }

                    if (xyz == "Z")
                    {
                        moveVector.x = 0;
                        moveVector.y = 0;
                    }

                    if (obj.transform.parent != null)
                    {
                        obj.transform.parent.localScale += moveVector * 5.0f;
                    }
                    else
                    {
                        obj.transform.localScale += moveVector * 5.0f;
                    }
                }
            }
        }
    }
}