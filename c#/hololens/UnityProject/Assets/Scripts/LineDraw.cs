﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MIConvexHull;
using System.Linq;
using Academy.HoloToolkit.Unity;

namespace Academy.HoloToolkit.Unity
{
    public class LineDraw : Singleton<LineDraw>
    {
        List<GameObject> lines = new List<GameObject>();
        List<GameObject> gridLines = new List<GameObject>();
        GameObject lastLine;
        public List<Vector3> linePositions = new List<Vector3>();
        public List<Vector3> startPositions = new List<Vector3>();
        public List<Vector3> endPositions = new List<Vector3>();

        Material redLineMaterial;
        Material blueLineMaterial;

        public List<Vector3> Positions { get; private set; }

        private void addLinePosition(Vector3 p1, Vector3 p2) {
            linePositions.Add(p1);
            linePositions.Add(p2);
            Positions = linePositions;
        }

        public void DrawLine(Vector3 start, Vector3 end, Color color, bool finalLine)
        {
            GameObject l = new GameObject();
            if (finalLine) {
                addLinePosition(start, end);
                startPositions.Add(start);
                endPositions.Add(end);
            }
            lines.Add(l);

            l.transform.position = start;
            l.AddComponent<LineRenderer>();
            LineRenderer lr = l.GetComponent<LineRenderer>();
            lr.material = redLineMaterial;
            lr.startColor = lr.endColor = color;
            lr.SetPosition(0, start);
            lr.SetPosition(1, end);
            lr.startWidth = (0.001f);
            lr.endWidth = (0.001f);
            lastLine = l;
        }

        public void DrawGridLine(Vector3 start, Vector3 end, Color color)
        {
            GameObject l = new GameObject();
            gridLines.Add(l);
            l.transform.position = start;
            l.AddComponent<LineRenderer>();
            LineRenderer lr = l.GetComponent<LineRenderer>();
            lr.material = blueLineMaterial;
            lr.startColor = lr.endColor = Color.gray;
            lr.SetPosition(0, start);
            lr.SetPosition(1, end);
            lr.startWidth = (0.001f);
            lr.endWidth = (0.001f);
        }

        public void DeleteLines()
        {
            foreach (var line in lines)
            {
                GameObject.Destroy(line);
            }
            linePositions.Clear();
            Positions = linePositions;
            startPositions.Clear();
            endPositions.Clear();
        }

        public void DeleteGrid()
        {
            foreach (var line in gridLines)
            {
                GameObject.Destroy(line);
                Destroy(line);
            }
        }
       
        public void DrawGrid(float divisionSize, Vector3 centre)
        {
            DeleteGrid();

            //y plane
            int y = 0;
            int i = 0;
            int gridNumber = 2;

            for (y=0; y<gridNumber; y++)
            {
                //x plane
                i = 0;
                for (i = 0; i < gridNumber; i++)
                {
                    DrawGridLine(new Vector3(centre.x + (divisionSize * i), centre.y + (y * divisionSize), centre.z - (gridNumber * divisionSize)), new Vector3(centre.x + (divisionSize * i), centre.y + (y * divisionSize), centre.z + (gridNumber * divisionSize)), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (divisionSize * i), centre.y + (y * divisionSize), centre.z - (gridNumber * divisionSize)), new Vector3(centre.x - (divisionSize * i), centre.y + (y * divisionSize), centre.z + (gridNumber * divisionSize)), Color.blue);

                    DrawGridLine(new Vector3(centre.x - (gridNumber * divisionSize), centre.y + (y * divisionSize), centre.z + (divisionSize * i)), new Vector3(centre.x + (gridNumber * divisionSize), centre.y + (y * divisionSize), centre.z + (divisionSize * i)), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (gridNumber * divisionSize), centre.y + (y * divisionSize), centre.z - (divisionSize * i)), new Vector3(centre.x + (gridNumber * divisionSize), centre.y + (y * divisionSize), centre.z - (divisionSize * i)), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * i), centre.y - (y * divisionSize), centre.z - (gridNumber * divisionSize)), new Vector3(centre.x + (divisionSize * i), centre.y - (y * divisionSize), centre.z + (gridNumber * divisionSize)), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (divisionSize * i), centre.y - (y * divisionSize), centre.z - (gridNumber * divisionSize)), new Vector3(centre.x - (divisionSize * i), centre.y - (y * divisionSize), centre.z + (gridNumber * divisionSize)), Color.blue);

                    DrawGridLine(new Vector3(centre.x - (gridNumber * divisionSize), centre.y - (y * divisionSize), centre.z + (divisionSize * i)), new Vector3(centre.x + (gridNumber * divisionSize), centre.y - (y * divisionSize), centre.z + (divisionSize * i)), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (gridNumber * divisionSize), centre.y - (y * divisionSize), centre.z - (divisionSize * i)), new Vector3(centre.x + (gridNumber * divisionSize), centre.y - (y * divisionSize), centre.z - (divisionSize * i)), Color.blue);
                }
            }
        }

        public void DrawPlane(char dir, float divisionSize, Vector3 centre)
        {
            int planeNumber = 10;
            int i = 0;
            DeleteGrid();

            if (dir == 'y')
            {
                for (i = 0; i < planeNumber; i++)
                {
                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y, centre.z + (divisionSize * i)), new Vector3(centre.x - (divisionSize * planeNumber), centre.y, centre.z + (divisionSize * i)), Color.blue);
                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y, centre.z - (divisionSize * i)), new Vector3(centre.x - (divisionSize * planeNumber), centre.y, centre.z - (divisionSize * i)), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * i), centre.y, centre.z + (divisionSize * planeNumber)), new Vector3(centre.x + (divisionSize * i), centre.y, centre.z + (divisionSize * planeNumber)), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (divisionSize * i), centre.y, centre.z + (divisionSize * planeNumber)), new Vector3(centre.x - (divisionSize * i), centre.y, centre.z + (divisionSize * planeNumber)), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * i), centre.y, centre.z + (divisionSize * planeNumber)), new Vector3(centre.x + (divisionSize * i), centre.y, centre.z - (divisionSize * planeNumber)), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (divisionSize * i), centre.y, centre.z + (divisionSize * planeNumber)), new Vector3(centre.x - (divisionSize * i), centre.y, centre.z - (divisionSize * planeNumber)), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y, centre.z + (divisionSize * i)), new Vector3(centre.x + (divisionSize * planeNumber), centre.y, centre.z + (divisionSize * i)), Color.blue);
                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y, centre.z - (divisionSize * i)), new Vector3(centre.x + (divisionSize * planeNumber), centre.y, centre.z - (divisionSize * i)), Color.blue);
                }
            }

            else if (dir == 'z')
            {
                for (i = 0; i < planeNumber; i++)
                {
                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y + (divisionSize * i), centre.z), new Vector3(centre.x - (divisionSize * planeNumber), centre.y + (divisionSize * i), centre.z), Color.blue);
                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y - (divisionSize * i), centre.z), new Vector3(centre.x - (divisionSize * planeNumber), centre.y - (divisionSize * i), centre.z), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * i), centre.y + (divisionSize * planeNumber), centre.z), new Vector3(centre.x + (divisionSize * i), centre.y + (divisionSize * planeNumber), centre.z), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (divisionSize * i), centre.y + (divisionSize * planeNumber), centre.z), new Vector3(centre.x - (divisionSize * i), centre.y + (divisionSize * planeNumber), centre.z), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * i), centre.y + (divisionSize * planeNumber), centre.z), new Vector3(centre.x + (divisionSize * i), centre.y - (divisionSize * planeNumber), centre.z), Color.blue);
                    DrawGridLine(new Vector3(centre.x - (divisionSize * i), centre.y + (divisionSize * planeNumber), centre.z), new Vector3(centre.x - (divisionSize * i), centre.y - (divisionSize * planeNumber), centre.z), Color.blue);

                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y + (divisionSize * i), centre.z), new Vector3(centre.x + (divisionSize * planeNumber), centre.y + (divisionSize * i), centre.z), Color.blue);
                    DrawGridLine(new Vector3(centre.x + (divisionSize * planeNumber), centre.y - (divisionSize * i), centre.z), new Vector3(centre.x + (divisionSize * planeNumber), centre.y - (divisionSize * i), centre.z), Color.blue);

                }
            }

            else if (dir == 'x')
            {
                for (i = 0; i < planeNumber; i++)
                {
                    DrawGridLine(new Vector3(centre.x, centre.y + (divisionSize * i), centre.z + (divisionSize * planeNumber)), new Vector3(centre.x, centre.y + (divisionSize * i), centre.z - (divisionSize * planeNumber)), Color.blue);
                    DrawGridLine(new Vector3(centre.x, centre.y - (divisionSize * i), centre.z + (divisionSize * planeNumber)), new Vector3(centre.x, centre.y - (divisionSize * i), centre.z - (divisionSize * planeNumber)), Color.blue);

                    DrawGridLine(new Vector3(centre.x, centre.y + (divisionSize * planeNumber), centre.z + (divisionSize * i)), new Vector3(centre.x, centre.y + (divisionSize * planeNumber), centre.z + (divisionSize * i)), Color.blue);
                    DrawGridLine(new Vector3(centre.x, centre.y + (divisionSize * planeNumber), centre.z - (divisionSize * i)), new Vector3(centre.x, centre.y + (divisionSize * planeNumber), centre.z - (divisionSize * i)), Color.blue);

                    DrawGridLine(new Vector3(centre.x, centre.y + (divisionSize * planeNumber), centre.z + (divisionSize * i)), new Vector3(centre.x, centre.y - (divisionSize * planeNumber), centre.z + (divisionSize * i)), Color.blue);
                    DrawGridLine(new Vector3(centre.x, centre.y + (divisionSize * planeNumber), centre.z - (divisionSize * i)), new Vector3(centre.x, centre.y - (divisionSize * planeNumber), centre.z - (divisionSize * i)), Color.blue);

                    DrawGridLine(new Vector3(centre.x, centre.y + (divisionSize * i), centre.z + (divisionSize * planeNumber)), new Vector3(centre.x, centre.y + (divisionSize * i), centre.z + (divisionSize * planeNumber)), Color.blue);
                    DrawGridLine(new Vector3(centre.x, centre.y - (divisionSize * i), centre.z + (divisionSize * planeNumber)), new Vector3(centre.x, centre.y - (divisionSize * i), centre.z + (divisionSize * planeNumber)), Color.blue);

                }
            }
        }

        public bool checkPolygon()
        {
            bool success = true;

            if (linePositions.Count < 2)
            {
                success = false;
                return success;
            }

            int[] indices = new int[linePositions.Count];

            int i = 0;
            int j = 0;

            foreach (var point in linePositions)
            {
                foreach (var pair in linePositions)
                {
                    if (point.Equals(pair))
                    {
                        if (i != j)
                        {
                            indices[i] = 1;
                        }
                    }
                    j++;
                }
                i++;
            }
            foreach (var index in indices)
            {
                if (index != 1)
                {
                    success = false;
                }
            }

            int lineAmount = lines.Count;

            i = 0;
            j = 0;

            List<Vector3> newLinePositions = new List<Vector3>();

            List<Vector3> newStartLinePositions = startPositions;
            List<Vector3> newEndLinePositions = endPositions;

            if (success)
            {
                Vector3 start = startPositions[0];
                Vector3 initialStart = start;
                Vector3 end = endPositions[0];
                newLinePositions.Add(start);
                newLinePositions.Add(end);
                Vector3 node = end;
                List<int> used = new List<int>();
                i = 0;
                j = 0;
                used.Add(0);

                for (i = 0; i < startPositions.Count - 1; i++)
                {
                    for (j = 0; j < startPositions.Count; j++)
                    {
                        if ((startPositions[j] == node) && (!used.Contains(j)))
                        {
                            used.Add(j);
                            newLinePositions.Add(startPositions[j]);
                            newLinePositions.Add(endPositions[j]);
                            node = endPositions[j];
                            j = startPositions.Count;
                        }

                        else if ((endPositions[j] == node) && (!used.Contains(j)))
                        {
                            used.Add(j);
                            newLinePositions.Add(endPositions[j]);
                            newLinePositions.Add(startPositions[j]);
                            node = startPositions[j];
                            j = startPositions.Count;
                        }
                    }
                }
                linePositions = newLinePositions;
                int u = 0;
            }
            return success;
        }

        public void destroyLastLine()
        {
            DestroyObject(lastLine);
        }

        void Start()
        {
            redLineMaterial = new Material(Resources.Load("x", typeof(Material)) as Material);
            blueLineMaterial = new Material(Resources.Load("grid", typeof(Material)) as Material);
        }

        void End()
        {
            Destroy(redLineMaterial);
            Destroy(blueLineMaterial);
        }

        void Update()
        {

        }
    }

}