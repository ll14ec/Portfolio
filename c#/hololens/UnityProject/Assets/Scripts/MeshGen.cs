﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MIConvexHull;
using System.Linq;
using Academy.HoloToolkit.Unity;
using Net3dBool;
using System.Threading.Tasks;

namespace Academy.HoloToolkit.Unity
{
    public class MeshGen : Singleton<MeshGen>
    {

        GameObject selection;
        GameObject extrudedObject;

        Material block;

        Mesh extrudeMesh;
        Vector2[] polygon;
        BooleanModeller b;
        Solid newSolid;

        public class Vertex : IVertex
        {
            public double[] Position { get; set; }
            public Vertex(double x, double y, double z)
            {
                Position = new double[3] { x, y, z };
            }
            public Vertex(Vector3 ver)
            {
                Position = new double[3] { ver.x, ver.y, ver.z };
            }
            public Vector3 ToVec()
            {
                return new Vector3((float)Position[0], (float)Position[1], (float)Position[2]);
            }
        }

        // Use this for initialization
        void Start() {
            block = new Material(Resources.Load("x", typeof(Material)) as Material); 
        }
        // Update is called once per frame
        void Update() { }

        public void CreateSelection(Mesh m)
        {
            GameObject selection = new GameObject("Selection");
            MeshFilter meshFilter = (MeshFilter)selection.AddComponent(typeof(MeshFilter));
            meshFilter.mesh = m;
            MeshRenderer renderer = selection.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
            renderer.material = block;
            renderer.material.color = Color.cyan;
            selection.AddComponent<Interactible>();
            selection.AddComponent<GestureAction>();
            selection.AddComponent<ExpandModel>();
        }

        public void CreateSelection(IEnumerable<Vector3> points)
        {
            selection = new GameObject("Selection");
            MeshFilter meshFilter = (MeshFilter)selection.AddComponent(typeof(MeshFilter));
            meshFilter.mesh = CreateMesh(points);
            MeshRenderer renderer = selection.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
            renderer.material = block;
            renderer.material.color = Color.cyan;
            selection.AddComponent<Interactible>();
            selection.AddComponent<GestureAction>();
            selection.AddComponent<ExpandModel>();
        }

        private Mesh CreateMesh(IEnumerable<Vector3> stars)
        {
            Mesh m = new Mesh();
            m.name = "ScriptedMesh";
            List<int> triangles = new List<int>();

            var vertices = stars.Select(x => new Vertex(x)).ToList();

            var result = ConvexHull.Create(vertices);
            m.vertices = result.Points.Select(x => x.ToVec()).ToArray();
            var xxx = result.Points.ToList();

            foreach (var face in result.Faces)
            {
                triangles.Add(xxx.IndexOf(face.Vertices[0]));
                triangles.Add(xxx.IndexOf(face.Vertices[1]));
                triangles.Add(xxx.IndexOf(face.Vertices[2]));
            }

            m.triangles = triangles.ToArray();
            m.RecalculateNormals();
            return m;
        }

        static Mesh createMesh(Vector2[] poly, float amount, char axis)
        {
            // convert polygon to triangles
            Triangulator triangulator = new Triangulator(poly);
            int[] tris = triangulator.Triangulate();
            Mesh m = new Mesh();
            Vector3[] vertices = new Vector3[poly.Length * 2];

            if (axis == 'y')
            {
                for (int i = 0; i < poly.Length; i++)
                {
                    vertices[i].x = poly[i].x;
                    vertices[i].z = poly[i].y;
                    vertices[i].y = amount; // front vertex
                    vertices[i + poly.Length].x = poly[i].x;
                    vertices[i + poly.Length].z = poly[i].y;
                    vertices[i + poly.Length].y = 0.0f;  // back vertex    
                }
            }
            else if (axis == 'x')
            {
                for (int i = 0; i < poly.Length; i++)
                {
                    vertices[i].x = amount;
                    vertices[i].z = poly[i].x;
                    vertices[i].y = poly[i].y; // front vertex
                    vertices[i + poly.Length].x = 0.0f;
                    vertices[i + poly.Length].z = poly[i].x;
                    vertices[i + poly.Length].y = poly[i].y;  // back vertex    
                }
            }
            else if (axis == 'z')
            {
                for (int i = 0; i < poly.Length; i++)
                {
                    vertices[i].x = poly[i].x;
                    vertices[i].z = amount;
                    vertices[i].y = poly[i].y; // front vertex
                    vertices[i + poly.Length].x = poly[i].x;
                    vertices[i + poly.Length].z = 0.0f;
                    vertices[i + poly.Length].y = poly[i].y;  // back vertex    
                }
            }
            int[] triangles = new int[tris.Length * 2 + poly.Length * 6];
            int count_tris = 0;
            for (int i = 0; i < tris.Length; i += 3)
            {
                triangles[i] = tris[i];
                triangles[i + 1] = tris[i + 1];
                triangles[i + 2] = tris[i + 2];
            } // front vertices
            count_tris += tris.Length;
            for (int i = 0; i < tris.Length; i += 3)
            {
                triangles[count_tris + i] = tris[i + 2] + poly.Length;
                triangles[count_tris + i + 1] = tris[i + 1] + poly.Length;
                triangles[count_tris + i + 2] = tris[i] + poly.Length;
            } // back vertices
            count_tris += tris.Length;
            for (int i = 0; i < poly.Length; i++)
            {
                // triangles around the perimeter of the object
                int n = (i + 1) % poly.Length;
                triangles[count_tris] = i;
                triangles[count_tris + 1] = n;
                triangles[count_tris + 2] = i + poly.Length;
                triangles[count_tris + 3] = n;
                triangles[count_tris + 4] = n + poly.Length;
                triangles[count_tris + 5] = i + poly.Length;
                count_tris += 6;
            }
            m.vertices = vertices;
            m.triangles = triangles;

            m.RecalculateNormals();

            // flip normals

            if (amount < 0)
            {

                Vector3[] normals = m.normals;
                for (int i = 0; i < normals.Length; i++)
                    normals[i] = -normals[i];
                m.normals = normals;

                for (int y = 0; y < m.subMeshCount; y++)
                {
                    int[] tria = m.GetTriangles(y);
                    for (int i = 0; i < tria.Length; i += 3)
                    {
                        int temp = tria[i + 0];
                        tria[i + 0] = tria[i + 1];
                        tria[i + 1] = temp;
                    }
                    m.SetTriangles(tria, y);
                }

            }

            ////

            m.RecalculateBounds();
            return m;
        }



        //static Mesh CreateMesh(Vector2[] poly, float amount, char axis)
        //{
        //    Vector2[] polygon = new List<Vector2>(poly).GetRange(0, poly.Length/2).ToArray();

        //    // convert polygon to triangles
        //    Triangulator triangulator = new Triangulator(polygon);
        //    int[] tris = triangulator.Triangulate();
        //    Mesh m = new Mesh();
        //    Vector3[] vertices = new Vector3[poly.Length * 2];

        //    if (axis == 'y')
        //    {
        //        for (int i = 0; i < poly.Length; i++)
        //        {
        //            vertices[i].x = poly[i].x;
        //            vertices[i].z = poly[i].y;
        //            vertices[i].y = amount; // front vertex
        //            vertices[i + poly.Length].x = poly[i].x;
        //            vertices[i + poly.Length].z = poly[i].y;
        //            vertices[i + poly.Length].y = 0.0f;  // back vertex    
        //        }
        //    }
        //    else  if (axis == 'x')
        //    {
        //        for (int i = 0; i < poly.Length; i++)
        //        {
        //            vertices[i].x = amount;
        //            vertices[i].z = poly[i].x;
        //            vertices[i].y = poly[i].y; // front vertex
        //            vertices[i + poly.Length].x = 0.0f;
        //            vertices[i + poly.Length].z = poly[i].x;
        //            vertices[i + poly.Length].y = poly[i].y;  // back vertex    
        //        }
        //    }
        //    else if (axis == 'z')
        //    {
        //        for (int i = 0; i < poly.Length; i++)
        //        {
        //            vertices[i].x = poly[i].x;
        //            vertices[i].z = amount;
        //            vertices[i].y = poly[i].y; // front vertex
        //            vertices[i + poly.Length].x = poly[i].x;
        //            vertices[i + poly.Length].z = 0.0f;
        //            vertices[i + poly.Length].y = poly[i].y;  // back vertex    
        //        }
        //    }
        //    int[] triangles = new int[tris.Length * 2 + poly.Length * 6];
        //    int count_tris = 0;
        //    for (int i = 0; i < tris.Length; i += 3)
        //    {
        //        triangles[i] = tris[i];
        //        triangles[i + 1] = tris[i + 1];
        //        triangles[i + 2] = tris[i + 2];
        //    } // front vertices
        //    count_tris += tris.Length;
        //    for (int i = 0; i < tris.Length; i += 3)
        //    {
        //        triangles[count_tris + i] = tris[i + 2] + poly.Length;
        //        triangles[count_tris + i + 1] = tris[i + 1] + poly.Length;
        //        triangles[count_tris + i + 2] = tris[i] + poly.Length;
        //    } // back vertices
        //    count_tris += tris.Length;
        //    for (int i = 0; i < poly.Length; i++)
        //    {
        //        // triangles around the perimeter of the object
        //        int n = (i + 1) % poly.Length;
        //        triangles[count_tris] = i;
        //        triangles[count_tris + 1] = n;
        //        triangles[count_tris + 2] = i + poly.Length;
        //        triangles[count_tris + 3] = n;
        //        triangles[count_tris + 4] = n + poly.Length;
        //        triangles[count_tris + 5] = i + poly.Length;
        //        count_tris += 6;
        //    }
        //    m.vertices = vertices;
        //    m.triangles = triangles;

        //    m.RecalculateNormals();

        //    // flip normals

        //    if (amount < 0)
        //    {

        //        Vector3[] normals = m.normals;
        //        for (int i = 0; i < normals.Length; i++)
        //            normals[i] = -normals[i];
        //        m.normals = normals;

        //        for (int y = 0; y < m.subMeshCount; y++)
        //        {
        //            int[] tria = m.GetTriangles(y);
        //            for (int i = 0; i < tria.Length; i += 3)
        //            {
        //                int temp = tria[i + 0];
        //                tria[i + 0] = tria[i + 1];
        //                tria[i + 1] = temp;
        //            }
        //            m.SetTriangles(tria, y);
        //        }

        //    }

        //    ////

        //    m.RecalculateBounds();
        //    return m;
        //}

        //public Vector2[] getPolygon(char axis)
        //{
        //    List<Vector3> linePositions = LineDraw.Instance.linePositions;

        //    int numberOfPositions = linePositions.Count;
        //    List<Vector3> newPos = LineDraw.Instance.linePositions;

        //    int j = 0;
        //    for (int i = 0; i < numberOfPositions; i += 2)
        //    {
        //        if (i < numberOfPositions)
        //        {
        //            newPos[j] = linePositions[i];
        //            j++;
        //        }
        //    }

        //    Vector2[] polygon = new Vector2[numberOfPositions+1];

        //    int noNewPositions = (newPos.Count / 2) + 1;

        //    for (int i = 0; i<noNewPositions; i++)
        //    {
        //        if (axis == 'y')
        //        {
        //            polygon[i].x = newPos[i].x;
        //            polygon[i].y = newPos[i].z;
        //        }
        //        else if (axis  == 'x')
        //        {
        //            polygon[i].x = newPos[i].z;
        //            polygon[i].y = newPos[i].y;
        //        }
        //        else if (axis == 'z')
        //        {
        //            polygon[i].x = newPos[i].x;
        //            polygon[i].y = newPos[i].y;
        //        }

        //        if (i == noNewPositions - 1)
        //        {
        //            if (axis == 'y')
        //            {
        //                polygon[i].x = newPos[0].x;
        //                polygon[i].y = newPos[0].z;
        //            }
        //            else if (axis == 'x')
        //            {
        //                polygon[i].x = newPos[0].z;
        //                polygon[i].y = newPos[0].y;
        //            }
        //            else if (axis == 'z')
        //            {
        //                polygon[i].x = newPos[0].x;
        //                polygon[i].y = newPos[0].y;
        //            }
        //        }
        //    }

        //    int flipper = noNewPositions - 1;

        //    for (int i = 0; i < noNewPositions; i++)
        //    {
        //        if (i + noNewPositions - 1 != noNewPositions - 1)
        //        {
        //            if (axis == 'y')
        //            {
        //                polygon[i + noNewPositions - 1].x = newPos[flipper].x;
        //                polygon[i + noNewPositions - 1].y = newPos[flipper].z;
        //            }
        //            else if (axis == 'x')
        //            {
        //                polygon[i + noNewPositions - 1].x = newPos[flipper].z;
        //                polygon[i + noNewPositions - 1].y = newPos[flipper].y;
        //            }
        //            else if (axis == 'z')
        //            {
        //                polygon[i + noNewPositions - 1].x = newPos[flipper].x;
        //                polygon[i + noNewPositions - 1].y = newPos[flipper].y;
        //            }
        //        }
        //        flipper--;
        //    }
        //    return polygon;
        //}

        //    polygon = new Vector2[] {
        //    new Vector2(0,0),
        //    new Vector2(0.050f,0.050f),
        //    new Vector2(0,0.050f),
        //    new Vector2(0,0),
        //    new Vector2(0,0.050f),
        //    new Vector2(0.050f,0.050f),
        //    new Vector2(0,0),
        //      };


        public Vector2[] getPolygon(char axis)
        {
            List<Vector3> linePositions = LineDraw.Instance.linePositions;

            int numberOfPositions = linePositions.Count;
            List<Vector3> newPos = new List<Vector3>();

            int j = 0;
            for (int i = 0; i < numberOfPositions-1; i += 2)
            {
                if (i < numberOfPositions-1)
                {
                    newPos.Add(linePositions[i]);
                    j++;
                }
            }

            Vector2[] polygon = new Vector2[newPos.Count];

            for (int i = 0; i < newPos.Count; i++)
            {
                if (axis == 'y')
                {
                    polygon[i].x = newPos[i].x;
                    polygon[i].y = newPos[i].z;
                }
                else if (axis  == 'x')
                {
                    polygon[i].x = newPos[i].z;
                    polygon[i].y = newPos[i].y;
                }
                else if (axis == 'z')
                {
                    polygon[i].x = newPos[i].x;
                    polygon[i].y = newPos[i].y;
                }
            }
            return polygon;
        }

        public void extrude(char axis, float amount)
        {
            //polygon = new Vector2[] {
            //new Vector2(0,0),
            //new Vector2(0.050f,0.050f),
            //new Vector2(0,0.050f),
            //  };
            polygon = getPolygon(axis);
            extrudeMesh = createMesh(polygon, amount, axis);
            extrudedObject = new GameObject();
            Renderer renderer = extrudedObject.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
            MeshFilter filter = extrudedObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
            filter.mesh = extrudeMesh;
            renderer.material = block;
            renderer.material.color = Color.cyan;
        }

        public void extrudeUpdate(char axis, float amount)
        {
            extrudeMesh = createMesh(polygon, amount, axis);
            Renderer renderer = extrudedObject.GetComponent<MeshRenderer>();
            MeshFilter filter = extrudedObject.GetComponent<MeshFilter>();
            filter.mesh = extrudeMesh;
            renderer.material = block;
            renderer.material.color = Color.cyan;
        }

        public void extrudeComplete()
        {
            extrudedObject.AddComponent<Interactible>();
            extrudedObject.AddComponent<GestureAction>();
            extrudedObject.AddComponent<ExpandModel>();
        }

        public void deleteExtrudedObject()
        {
            Destroy(extrudedObject);
            Destroy(extrudeMesh);
            polygon = null;
        }

        private Color3f[] getColorArray(int length, Color c)
        {
            var ar = new Color3f[length];
            for (var i = 0; i < length; i++)
            {
                ar[i] = new Color3f(c.r, c.g, c.b);
            }
            return ar;
        }

        private Vector3[] point2Vec(Point3d[] p)
        {
            int l = p.Length;
            var v = new Vector3[l];

            int i = 0;
            foreach (var point in p)
            {
                v[i].Set((float)p[i].x, (float)p[i].y, (float)p[i].z);
                i++;
            }
            return v;
        }

        private Point3d[] vec2Point(Vector3[] v)
        {
            int l = v.Length;
            var p = new List<Point3d>();

            int i = 0;
            for (i = 0; i < l; i++)
            {
                p.Add(new Point3d(v[i].x, v[i].y, v[i].z));
            }
            var y = p.ToArray();

            return y;
        }

        private Solid mesh2Solid(Mesh m, GameObject obj)
        {

            Matrix4x4 localToWorld = obj.transform.localToWorldMatrix;

            int x = 0;
            Vector3[] worldPoints = new Vector3[m.vertices.Length];
            foreach (var vert in m.vertices)
            {
                worldPoints[x] = localToWorld.MultiplyPoint3x4(vert);
                x++;
            }
            var verts = vec2Point(worldPoints);
            
            //var verts = vec2Point(m.vertices);

            int y = 0;
            int indLength = m.triangles.Length;
            var ind = new int[indLength];
            int i = 0;
            foreach (var tri in m.triangles)
            {
                ind[i] = tri;
                i++;
            }
            var cols = getColorArray(verts.Length, Color.red);
            return new Solid(verts, ind, cols);
        }

        private Mesh solid2Mesh(Solid s)
        {
            Mesh m = new Mesh();
            m.name = "ScriptedMesh";
            //List<int> triangles = new List<int>();
            var verts = s.getVertices();
            int[] ind = s.getIndices();
            m.vertices = point2Vec(verts);
            //List<int> triangles = new List<int>(ind.Length);
            int[] triangles = new int[ind.Length];

            System.Diagnostics.Debug.WriteLine(ind.Length);
            int i = 0;
            for (i = 0; i < ind.Length; i++)
            {
                //triangles.Add(ind[i]);
                triangles[i] = ind[i];
            }
            //m.triangles = triangles.ToArray();
            m.triangles = triangles;
            m.RecalculateNormals();
            return m;
        }

        public void calculate(Solid s1, Solid s2)
        {
            b = new BooleanModeller(s1, s2);
            newSolid = b.getDifference();   
        }

        private async Task asyncDifference(GameObject obj1, GameObject obj2)
        {
            Mesh m1 = obj1.GetComponent<MeshFilter>().mesh;
            Mesh m2 = obj2.GetComponent<MeshFilter>().mesh;
            Solid s1 = mesh2Solid(m1, obj1);
            Solid s2 = mesh2Solid(m2, obj2);

            await Task.Run(() =>calculate(s1, s2));

            obj1.SetActive(false);
            obj2.SetActive(false);
            CreateSelection(solid2Mesh(newSolid));
            ModelManager.Instance.DeSelectCommmand();
            DestroyObject(obj1);
            DestroyObject(obj2);
        }

        public void objectDifference(GameObject obj1, GameObject obj2)
        {
            #pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            asyncDifference(obj1, obj2);
            #pragma warning restore CS4014
            //Mesh m1 = obj1.GetComponent<MeshFilter>().mesh;
            //Mesh m2 = obj2.GetComponent<MeshFilter>().mesh;
            //Solid s1 = mesh2Solid(m1, obj1);
            //Solid s2 = mesh2Solid(m2, obj2);
            //var modeller = new BooleanModeller(s1, s2);
            //var tmp = modeller.getDifference();
            //CreateSelection(solid2Mesh(tmp));
            //ModelManager.Instance.DeSelectCommmand();
            //DestroyObject(obj1);
            //DestroyObject(obj2);
        }

        public void createIntersectObj()
        {
            var box = new Net3dBool.Solid(Net3dBool.DefaultCoordinates.DEFAULT_BOX_VERTICES, Net3dBool.DefaultCoordinates.DEFAULT_BOX_COORDINATES, getColorArray(Net3dBool.DefaultCoordinates.DEFAULT_BOX_VERTICES.Length, Color.red));
            var sphere = new Net3dBool.Solid(Net3dBool.DefaultCoordinates.DEFAULT_SPHERE_VERTICES, Net3dBool.DefaultCoordinates.DEFAULT_SPHERE_COORDINATES, getColorArray(Net3dBool.DefaultCoordinates.DEFAULT_SPHERE_VERTICES.Length, Color.red));
            sphere.scale(0.68, 0.68, 0.68);
            var modeller = new Net3dBool.BooleanModeller(box, sphere);
            var tmp = modeller.getDifference();

            CreateSelection(solid2Mesh(tmp));
        }

        public void makechild(List<GameObject> objs)
        {
            GameObject parentObj = new GameObject();

            float x = 0;
            float y = 0;
            float z = 0;

            foreach (var obj in objs)
            {
                System.Diagnostics.Debug.WriteLine(obj.transform.position);
                System.Diagnostics.Debug.WriteLine(obj.transform.localPosition);
                x += obj.GetComponent<Renderer>().bounds.center.x;
                y += obj.GetComponent<Renderer>().bounds.center.y;
                z += obj.GetComponent<Renderer>().bounds.center.z;
            }

            Vector3 pos = new Vector3(x / objs.Count, y / objs.Count, z / objs.Count);

            parentObj.transform.position = pos;

            foreach (var obj in objs)
            {
                obj.transform.parent = parentObj.transform;
            }
        }
    }
}
