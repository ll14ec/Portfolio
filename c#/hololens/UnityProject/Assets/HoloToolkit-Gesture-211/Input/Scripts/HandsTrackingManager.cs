﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

namespace Academy.HoloToolkit.Unity
{
    /// <summary>
    /// HandsManager determines if the hand is currently detected or not.
    /// </summary>
    public partial class HandsTrackingManager : Singleton<HandsTrackingManager>
    {
        public Vector3 lastPos { get; private set; }
        /// <summary>
        /// HandDetected tracks the hand detected state.
        /// Returns true if the list of tracked hands is not empty.
        /// </summary>
        public bool HandDetected
        {
            get { return trackedHands.Count > 0; }
        }

        public GameObject TrackingObject;
        Vector3 normalisedPosition = new Vector3();
        float GridSize;
        char GridType;
        Vector3 GridConstant;

        private HashSet<uint> trackedHands = new HashSet<uint>();
        private Dictionary<uint, GameObject> trackingObject = new Dictionary<uint, GameObject>();

        void Awake()
        {
            trackedHands = new HashSet<uint>();
            InteractionManager.SourceDetected += InteractionManager_SourceDetected;
            InteractionManager.SourceLost += InteractionManager_SourceLost;
            InteractionManager.SourceUpdated += InteractionManager_SourceUpdated;
        }

        private Vector3 normalisePosition(Vector3 rawPosition, float gridSize, char dir, Vector3 constant, string interactOption)
        {
            Vector3 newPosition = new Vector3();
            Boolean extruding = false;

            if (interactOption == "extruding")
            {
                extruding = true;
            }

            // 3d normalisation
            if ((dir == 'n') || (extruding))
            {
                newPosition.x = (float)(Math.Floor(rawPosition.x / gridSize) * gridSize);
                newPosition.y = (float)(Math.Floor((rawPosition.y + 0.05f) / gridSize) * gridSize);
                newPosition.z = (float)(Math.Floor(rawPosition.z / gridSize) * gridSize);
            }

            // y normalisation
            else if (dir == 'y')
            {
                newPosition.x = (float)(Math.Floor(rawPosition.x / gridSize) * gridSize);
                newPosition.y = constant.y;
                newPosition.z = (float)(Math.Floor(rawPosition.z / gridSize) * gridSize);
            }

            // x normalisation
            else if (dir == 'x')
            {
                newPosition.x = constant.x;
                newPosition.y = (float)(Math.Floor((rawPosition.y + 0.05f) / gridSize) * gridSize);
                newPosition.z = (float)(Math.Floor(rawPosition.z / gridSize) * gridSize);
            }

            // z normalisation
            else if (dir == 'z')
            {
                newPosition.x = (float)(Math.Floor(rawPosition.x / gridSize) * gridSize);
                newPosition.y = (float)(Math.Floor((rawPosition.y + 0.05f) / gridSize) * gridSize);
                newPosition.z = constant.z;
            }

            return newPosition;
        }

        private void InteractionManager_SourceDetected(InteractionSourceState state)
        {

            // Check to see that the source is a hand.
            if (state.source.kind != InteractionSourceKind.Hand)
            {
                return;
            }
            
            trackedHands.Add(state.source.id);
            var obj = Instantiate(TrackingObject) as GameObject;

            Vector3 pos;

            if (state.properties.location.TryGetPosition(out pos))
            {
                obj.transform.position = pos;
            }

            trackingObject.Add(state.source.id, obj);
            lastPos = pos;
            GridSize = GestureManager.Instance.gridSize;
            GridType = GestureManager.Instance.gridType;
            GridConstant = GestureManager.Instance.gridConstant;
            string interactOption = ModelManager.Instance.interactOption;

            normalisedPosition = normalisePosition(pos, GridSize, GridType, GridConstant, interactOption);

            lastPos = normalisedPosition;
            obj.transform.position = normalisedPosition;

            if (interactOption == "draw")
            {
                LineDraw.Instance.DrawGrid(GridSize, normalisedPosition);
            }

            
            else if (interactOption != "extrude")
            {
                LineDraw.Instance.DeleteGrid();
            }
            
        }

        private void InteractionManager_SourceUpdated(InteractionSourceState state)
        {
            uint id = state.source.id;
            Vector3 pos;

            if (state.source.kind == InteractionSourceKind.Hand)
            {
                if (trackingObject.ContainsKey(state.source.id))
                {
                    if (state.properties.location.TryGetPosition(out pos))
                    {
                        trackingObject[state.source.id].transform.position = pos;
                        System.Diagnostics.Debug.WriteLine(pos.x + " " + pos.y + " " + pos.z);
                        lastPos = pos;
                        GridSize = GestureManager.Instance.gridSize;
                        GridType = GestureManager.Instance.gridType;
                        GridConstant = GestureManager.Instance.gridConstant;
                        string interactOption = ModelManager.Instance.interactOption;
                        normalisedPosition = normalisePosition(pos, GridSize, GridType, GridConstant, interactOption);
                        lastPos = normalisedPosition;
                        trackingObject[state.source.id].transform.position = normalisedPosition;

                    }
                }
            }
            System.Diagnostics.Debug.WriteLine("updated");


            if (ModelManager.Instance.interactOption == "draw")
            {
                LineDraw.Instance.DrawGrid(0.05f, normalisedPosition);
            }

            else if (ModelManager.Instance.interactOption != "extrude")
            {
                LineDraw.Instance.DeleteGrid();
            }
        }

       
        private void InteractionManager_SourceLost(InteractionSourceState state)
        {
            // Check to see that the source is a hand.
            if (state.source.kind != InteractionSourceKind.Hand)
            {
                return;
            }

            if (trackedHands.Contains(state.source.id))
            {
                trackedHands.Remove(state.source.id);
            }

            if (trackingObject.ContainsKey(state.source.id))
            {
                var obj = trackingObject[state.source.id];
                trackingObject.Remove(state.source.id);
                Destroy(obj);
            }
        }

        void OnDestroy()
        {
            InteractionManager.SourceDetected -= InteractionManager_SourceDetected;
            InteractionManager.SourceLost -= InteractionManager_SourceLost;
            InteractionManager.SourceUpdated -= InteractionManager_SourceUpdated;
        }
    }
}