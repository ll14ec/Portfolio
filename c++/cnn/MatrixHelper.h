#ifndef CNN_MATRIXHELPER_H
#define CNN_MATRIXHELPER_H

using vf  = std::vector<float>;
using v2f = std::vector<std::vector<float>>;
using v3f = std::vector<std::vector<std::vector<float>>>;
using v4f = std::vector<std::vector<std::vector<std::vector<float>>>>;

v2f createV2f(int xSize, int ySize);
v3f createV3f(unsigned int xSize, unsigned int ySize, unsigned int channels);
v2f conv2d(v2f x, v2f k);
v2f conv2d(v2f img, v2f filter, int ySize, int xSize);
vf vfAdd(vf a, vf b);
vf vfSubtract(float a, vf b);
vf vfSubtract(vf a, vf b);
vf vfScale(vf v, float s);
v2f v2fAdd(v2f a, v2f b);
v2f v2fSubtract(v2f a, v2f b);
v2f v2fMultiply(v2f a, v2f b);
v2f v2fSquare(v2f a);
v2f v2fScale(v2f v, float s);
float v2fSum(v2f v);
v3f v3fReluMask(v3f v);
v3f v3fMultiply(v3f& a, v3f& b);
v3f v3fAdd(v3f& a, v3f& b);
v3f v3fSubtract(v3f a, v3f b);
v3f v3fScale(v3f v, float s);
v2f v2fRotate180(v2f v);
vf vfSigmoid(vf v);
vf vfDSigmoid(vf v);
vf vfMultiply(vf a, vf b);
v3f generateRandomConvolutionInput(int channels, int size);
vf generateRandomVectorInput(unsigned int length);
void printV(vf& v);
void printV2(v2f& v);
void printV3 (v3f& V);
void testConv2d();
void testV2fSum();
vf dot(vf v, v2f m);
vf dotTransposed(vf v, v2f m);
v2f outer(vf v1, vf v2);
v2f enforceSize(v2f v, v2f sv);
vf v3fFlatten(v3f v);
v3f vfToV3f(vf v, unsigned int channels);
vf toOneHot(char c, unsigned int categories);
v4f mnistDataToV(unsigned char** data, unsigned int numImages);
v2f mnistLabelToV(unsigned char* data, unsigned int numLabels);
v3f testDlDo(int channels, v3f target, v3f prediction);
vf mseDerivative(vf target, vf prediction);

#endif //CNN_MATRIXHELPER_H
