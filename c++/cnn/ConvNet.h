#ifndef CNN_CONVNET_H
#define CNN_CONVNET_H

#include "ConvolutionLayer.h"
#include "DenseLayer.h"
#include "MaxPool2D.h"
#include <vector>
#include <cmath>

class ConvNet {
public:
    ConvNet(unsigned int inputXSize, unsigned int inputYSize, unsigned int layerCount, float lr, unsigned int outputLayerSize=10, unsigned int channels=6, unsigned int kernelSize=3);
    vf forward(v3f &x);
    void backward(vf &dldo);

private:
    int _layerCount;
    float _lr;
    std::vector<ConvolutionLayer> _convLayers;
    std::vector<MaxPool2D> _poolLayers;
    DenseLayer _outputLayer;
};


#endif //CNN_CONVNET_H
