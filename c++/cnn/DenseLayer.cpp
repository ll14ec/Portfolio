#include <random>
#include <iostream>
#include "DenseLayer.h"

DenseLayer::DenseLayer(unsigned int inUnits, unsigned int outUnits, bool activation) :
        _inUnits(inUnits), _outUnits(outUnits), _activation(activation)
{
  init();
}

void DenseLayer::init() {
  for (auto i = 0; i < _inUnits; ++i)
    _weights.push_back(vf(_outUnits));
  _bias = vf(_outUnits);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::normal_distribution<float> dis(0, .5);
  for (auto &r: _weights)
    for (auto &v: r)
      v = dis(gen);
}

vf DenseLayer::forward(vf x) {
  _inputs = x;
  _outputs = dot(x, _weights);
  _outputs = vfAdd(_outputs, _bias);
  if (_activation)
    _outputs = vfSigmoid(_outputs);
  return _outputs;
}

vf DenseLayer::backward(vf dldo, float lr) {
  if (_activation)
    dldo = vfMultiply(vfDSigmoid(dldo), dldo);
  auto dldi = dotTransposed(dldo, _weights);
  auto grad = outer(_inputs, dldo);
  _weights = v2fSubtract(_weights, v2fScale(grad, lr));
  _bias = vfSubtract(_bias, vfScale(dldo, lr));
  return dldi;
}