#include <iostream>
#include "DenseNet.h"

DenseNet::DenseNet(unsigned int layerCount, float lr, unsigned int layerSize) :
        _layerCount(layerCount), _lr(lr), _layerSize(layerSize)
{
  for (auto i = 0; i < _layerCount; ++i)
    _layers.emplace_back(_layerSize, _layerSize, true);
}

vf DenseNet::forward(vf x) {
  auto res = x;
  for (auto i = 0; i < _layerCount; ++i) {
    res = _layers[i].forward(res);
//    std::cout << "fwd " << i << std::endl;
  }
  return res;
}

void DenseNet::backward(vf dldo) {
  auto grad = dldo;
//  auto d = _layerCount - 1;
  for (auto i = _layerCount - 1; i > -1; --i) {
    grad = _layers[i].backward(grad, _lr);
//    std::cout << "bck " << i << std::endl;
  }
}