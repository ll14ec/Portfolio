#ifndef CNN_READDATA_H
#define CNN_READDATA_H

#include <string>
#include <fstream>

using uchar = unsigned char;
using string = std::string;

uchar** read_mnist_images(string full_path, int number_of_images, int image_size);
uchar* read_mnist_labels(string full_path, int number_of_labels);

#endif
