#ifndef CNN_MAXPOOL2D_H
#define CNN_MAXPOOL2D_H
#include <vector>
#include "MatrixHelper.h"

class MaxPool2D {
public:
    MaxPool2D();
    v3f forward(v3f &x);
    v3f backward (v3f& dldo);

private:
    v3f _mask;
    int _channels;
    int _inYSize;
    int _inXSize;
    int _poolSize = 2;
};


#endif //CNN_MAXPOOL2D_H
