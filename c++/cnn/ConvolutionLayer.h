#ifndef CNN_CONVOLUTIONLAYER_H
#define CNN_CONVOLUTIONLAYER_H
#include <vector>
#include "MatrixHelper.h"


class ConvolutionLayer
{
public:
  ConvolutionLayer(unsigned int inputXSize, unsigned int inputYSize, unsigned int inChannels, unsigned int outChannels, unsigned int kernelSize, bool activation=true);
  v3f forward(v3f& x);
  v3f backward(v3f& dldo, float lr, bool updateWeights=true);
  unsigned int outChannels();

private:
    void init();
    void printWeights();
    unsigned int _inChannels;
    unsigned int _outChannels;
    unsigned int _kernelSize;
    unsigned int _inputXSize;
    unsigned int _inputYSize;
    bool _activation;
    v4f _weights;
    v3f _bias;
    v3f _inputs;
    v3f _outputs;
    v3f _reluMask;
};


#endif
