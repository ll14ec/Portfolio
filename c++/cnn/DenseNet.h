#ifndef CNN_DENSENET_H
#define CNN_DENSENET_H

#include <vector>
#include "DenseLayer.h"


class DenseNet {
public:
    DenseNet(unsigned int layerCount, float lr, unsigned int layerSize);
    vf forward(vf x);
    void backward(vf dldo);
private:
    int _layerCount;
    unsigned int _layerSize;
    float _lr;
    std::vector<DenseLayer> _layers;
};


#endif //CNN_DENSENET_H
