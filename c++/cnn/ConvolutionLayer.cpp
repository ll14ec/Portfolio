#include <iostream>
#include <random>

#include "ConvolutionLayer.h"
#include "MatrixHelper.h"

ConvolutionLayer::ConvolutionLayer(unsigned int inputXSize, unsigned int inputYSize, unsigned int inChannels, unsigned int outChannels, unsigned int kernelSize,bool activation) :
        _inChannels(inChannels), _outChannels(outChannels), _kernelSize(kernelSize), _activation(activation), _inputXSize(inputXSize), _inputYSize(inputYSize)
{
  init();
}

void ConvolutionLayer::init() {
  for (auto i = 0; i < _inChannels; ++i) {
    _weights.emplace_back();
    for (auto o = 0; o < _outChannels; ++o) {
      _weights[i].emplace_back();
      for (auto k = 0; k < _kernelSize; ++k)
        _weights[i][o].push_back(vf(_kernelSize));
    }
  }

  for (auto o = 0; o < _outChannels; ++o) {
    _bias.emplace_back();
    for (auto b = 0; b < _inputYSize; ++b)
      _bias[o].push_back(vf(_inputXSize));
  }

  std::random_device rd;
  std::mt19937 gen(rd());
  std::normal_distribution<float> dis(0, .1);
  for (auto &w: _weights)
    for (auto &k: w)
      for (auto &r: k)
        for (auto &v: r) {
          v = dis(gen);
        }
}

void ConvolutionLayer::printWeights() {
  for (auto &w: _weights) {
    for (auto &k: w) {
      for (auto &r: k) {
        for (auto &v: r) {
          std::cout << v << " ";
        }
        std::cout << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
}

v3f ConvolutionLayer::backward(v3f &dldo, float lr, bool updateWeights) {
  // apply relu mask to input grad
  if (_activation) {
    for (auto o = 0; o < _outChannels; ++o) {
      dldo[o] = v2fMultiply(dldo[o], _reluMask[o]);
    }
  }
  // calculate dldi as dldo * dodi
  v3f dldi;
  auto dldiYShape = (int) _inputs[0].size();
  auto dldiXShape = (int) _inputs[0][0].size();
  for (auto i = 0; i < _inChannels; ++i) {
    dldi.emplace_back();
    for (auto o = 0; o < _outChannels; ++o) {
      auto partialResult = conv2d(_weights[i][o], dldo[o], dldiYShape, dldiXShape);
      if (o == 0) { dldi[i] = partialResult; }
      else { dldi[i] = v2fAdd(dldi[i], partialResult); }
    }
  }
  // update weights from dldw
  auto dldwYShape = (int) _weights[0][0].size();
  auto dldwXShape = (int) _weights[0][0][0].size();
  for (auto i = 0; i < _inChannels; ++i) {
    for (auto o = 0; o < _outChannels; ++o) {
      auto partialResult = conv2d(_inputs[i], dldo[o], dldwYShape, dldwXShape);
      if (updateWeights)
        _weights[i][o] = v2fSubtract(_weights[i][o], v2fScale(partialResult, lr));
    }
  }
  // update bias from dldo
  if (updateWeights)
    _bias = v3fSubtract(_bias, v3fScale(dldo, lr));
  return dldi;
}

v3f ConvolutionLayer::forward(v3f &x) {
  _inputs = x;
  v3f res;
  // calculate multi channel conv
  auto outYShape = x[0].size();
  auto outXShape = x[0][0].size();
  for (auto o = 0; o < _outChannels; ++o) {
    res.emplace_back();
    for (auto i = 0; i < _inChannels; ++i) {
      auto partialResult = conv2d(x[i], _weights[i][o], outYShape, outXShape);

      if (i == 0)
        res[o] = partialResult;
      else
        res[o] = v2fAdd(res[o], partialResult);
    }
  }
  _outputs = res;
  //  add bias
  _outputs = v3fAdd(_bias, _outputs);
  if (_activation) {
    _reluMask = v3fReluMask(_outputs);
    _outputs = v3fMultiply(_reluMask, _outputs);
  }
  return _outputs;
}

unsigned int ConvolutionLayer::outChannels() { return _outChannels; }
