#ifndef CNN_DENSELAYER_H
#define CNN_DENSELAYER_H
#include "MatrixHelper.h"

class DenseLayer
{
public:
    DenseLayer(unsigned int inUnits, unsigned int outUnits, bool activation=true);
    vf forward(vf x);
    vf backward(vf dldo, float lr);
private:
    void init();
    unsigned int _inUnits;
    unsigned int _outUnits;
    bool _activation;
    v2f _weights;
    vf _bias;
    vf _inputs;
    vf _outputs;
};

#endif //CNN_DENSELAYER_H
