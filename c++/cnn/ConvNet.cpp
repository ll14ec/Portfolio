#include <iostream>
#include "ConvNet.h"

unsigned int pooledSize(unsigned int inputSize, unsigned int numPools)
{
  return (unsigned int)(inputSize / pow(2, numPools));
}

ConvNet::ConvNet(unsigned int inputXSize, unsigned int inputYSize, unsigned int layerCount, float lr, unsigned int outputLayerSize, unsigned int channels, unsigned int kernelSize) :
        _layerCount(layerCount), _lr(lr),
        _outputLayer(DenseLayer(
                channels * pooledSize(inputXSize, _layerCount) * pooledSize(inputYSize, _layerCount),
                outputLayerSize,true))
{
  _convLayers.emplace_back(inputXSize, inputYSize, 1, channels, kernelSize, true);
  _poolLayers.emplace_back();
  for (auto i = 0; i < _layerCount - 1; ++i) {
    _convLayers.emplace_back(pooledSize(inputXSize, i+1), pooledSize(inputYSize, i+1), channels, channels, kernelSize, true);
    _poolLayers.emplace_back();
  }
  auto f = 0;
}

vf ConvNet::forward(v3f &x) {
  auto res = x;
  for (auto i = 0; i < _layerCount; ++i) {
    res = _convLayers[i].forward(res);
    res = _poolLayers[i].forward(res);
  }
  auto output = _outputLayer.forward(v3fFlatten(res));
  return output;
}

void ConvNet::backward(vf &dldo) {
  auto denseGrad = _outputLayer.backward(dldo, _lr);
  auto grad = vfToV3f(denseGrad, _convLayers[_layerCount - 1].outChannels());
  for (auto i = _layerCount - 1; i > -1; --i) {
    grad = _poolLayers[i].backward(grad);
    grad = _convLayers[i].backward(grad, _lr);
  }
}
