#include <cassert>
#include <random>
#include <iostream>
#include "MatrixHelper.h"

v2f createV2f(int xSize, int ySize) {
  v2f res;
  for (auto y = 0; y < ySize; ++y)
    res.push_back(std::vector<float>(xSize));
  return res;
}

v3f createV3f(unsigned int xSize, unsigned int ySize, unsigned int channels) {
  v3f res;
  for (auto c = 0; c < channels; ++c)
    res.push_back(createV2f(xSize, ySize));
  return res;
}

v2f enforceSize(v2f v, v2f sv) {
  auto targetSize = (int) sv.size();
  auto inputSize = (int) v.size();
  if (inputSize == targetSize)
    return v;
  auto diff = targetSize - inputSize;
  v2f res = createV2f(targetSize, targetSize);
  // make input bigger
  if (diff > 0) {
    for (auto y = 0; y < inputSize; ++y) {
      for (auto x = 0; x < inputSize; ++x) {
        res[y + round(diff / 2)][x + round(diff / 2)] = v[y][x];
      }
    }
  }
    // make input smaller
  else {
    for (auto y = 0; y < targetSize; ++y) {
      for (auto x = 0; x < targetSize; ++x) {
        res[y][x] = v[y][x];
      }
    }
  }
  return res;
}

v2f conv2d(v2f x, v2f k) {
  int inputYSize = x.size();
  int inputXSize = x[0].size();
  int kernelYSize = k.size();
  int kernelXSize = k[0].size();

  // init result matrix
  v2f res = createV2f(inputXSize, inputYSize);

  // calculate pixels
  for (auto iy = 0; iy < inputYSize; ++iy) {
    for (auto ix = 0; ix < inputXSize; ++ix) {
      auto pixelVal = 0.f;
      for (auto ky = 0; ky < kernelYSize; ++ky) {
        for (auto kx = 0; kx < kernelXSize; ++kx) {
          int iYIndex = iy - (kernelYSize / 2) + ky;
          int iXIndex = ix - (kernelXSize / 2) + kx;
          if (iYIndex < 0) iYIndex = inputYSize + iYIndex;
          if (iXIndex < 0) iXIndex = inputXSize + iXIndex;
          if (iYIndex >= inputYSize) iYIndex = iYIndex - inputYSize;
          if (iXIndex >= inputXSize) iXIndex = iXIndex - inputXSize;
          auto kernelVal = k[ky][kx];
          auto inputVal = x[iYIndex][iXIndex];
          pixelVal += kernelVal * inputVal;
        }
      }
      res[iy][ix] = pixelVal;
    }
  }
  return res;
}

v2f centralShift(v2f v) {
  auto xSize = (int) v[0].size();
  auto ySize = (int) v.size();
  // create result matrix
  auto res = createV2f(xSize, ySize);

  // q1 (X, 0)
  //    (0, Y)
  auto q1XSize = (int) (xSize / 2.0);
  auto q1YSize = (int) (ySize / 2.0);
  auto q1XOffset = xSize - q1XSize;
  auto q1YOffset = ySize - q1YSize;

  for (auto x = 0; x < q1XSize; ++x)
    for (auto y = 0; y < q1YSize; ++y)
      res[y + q1YOffset][x + q1XOffset] = v[y][x];

//   q2 (0, X)
//      (Y, 0)
  auto q2XStart = q1XSize;
  auto q2XSize = xSize - q1XSize;
  auto q2YStart = 0;
  auto q2YSize = q1YSize;
  auto q2XOffset = 0;
  auto q2YOffset = q1YSize - (ySize % 2);

  for (auto x = q2XStart; x < (q2XStart + q2XSize); ++x)
    for (auto y = q2YStart; y < (q2YStart + q2YSize); ++y)
      res[y + q2YOffset][x + q2XOffset - q2XStart] = v[y][x];

  // q3 (0, Y)
  //    (X, 0)
  auto q3XStart = 0;
  auto q3XSize = q1XSize;
  auto q3YStart = q1YSize;
  auto q3YSize = ySize - q1YSize;
  auto q3XOffset = q1XSize - (xSize % 2);
  auto q3YOffset = 0;

  for (auto x = q3XStart; x < (q3XStart + q3XSize); ++x)
    for (auto y = q3YStart; y < (q3YStart + q3YSize); ++y)
      res[y + q3YOffset - q3YStart][x + q3XOffset] = v[y][x];

  // q3 (Y, 0)
  //    (0, X)
  auto q4XStart = q1XSize;
  auto q4YStart = q1YSize;
  auto q4XOffset = 0;
  auto q4YOffset = 0;
  auto q4XSize = xSize - q1XSize;
  auto q4YSize = ySize - q1YSize;

  for (auto x = q4XStart; x < (q4XStart + q4XSize); ++x)
    for (auto y = q4YStart; y < (q4YStart + q4YSize); ++y)
      res[y + q4YOffset - q4YStart][x + q4XOffset - q4XStart] = v[y][x];

  return res;
}

v2f conv2d(v2f img, v2f filter, int ySize, int xSize) {
  // create result matrix
  auto res = createV2f(xSize, ySize);
  for (auto x = 0; x < xSize; ++x)
    for (auto y = 0; y < xSize; ++y)
      for (auto kx = 0; kx < filter[0].size(); ++kx)
        for (auto ky = 0; ky < filter.size(); ++ky)
          if ((y - ky >= 0) && (y - ky < img.size()))
            if ((x - kx >= 0) && (x - kx < img[0].size()))
              res[y][x] += img[y - ky][x - kx] * filter[ky][kx];
  return centralShift(res);
}

v2f v2fAdd(v2f a, v2f b) {
  int aYSize = a.size();
  int aXSize = a[0].size();
  int bYSize = b.size();
  int bXSize = b[0].size();
  assert(aYSize == bYSize);
  assert(aXSize == bXSize);

  // init result matrix
  v2f res = createV2f(aXSize, aYSize);

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res[y][x] = a[y][x] + b[y][x];
    }
  }
  return res;
}

float v2fSum(v2f v) {
  int aYSize = v.size();
  int aXSize = v[0].size();

  float res = 0.f;

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res += v[y][x];
    }
  }
  return res;
}

v2f v2fSubtract(v2f a, v2f b) {
  int aYSize = a.size();
  int aXSize = a[0].size();
  int bYSize = b.size();
  int bXSize = b[0].size();
  assert(aYSize == bYSize);
  assert(aXSize == bXSize);

  // init result matrix
  v2f res = createV2f(aXSize, aYSize);

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res[y][x] = a[y][x] - b[y][x];
    }
  }
  return res;
}

vf vfSubtract(vf a, vf b) {
  int aSize = a.size();
  int bSize = b.size();
  assert(aSize == bSize);

  vf res = vf(aSize);
  for (auto i = 0; i < aSize; ++i)
    res[i] = a[i] - b[i];
  return res;
}

vf vfSquare(vf a) {
  vf res = vf(a.size());
  for (auto i = 0; i < a.size(); ++i)
    res[i] = a[i] * a[i];
  return res;
}

v2f v2fMultiply(v2f a, v2f b) {
  int aYSize = a.size();
  int aXSize = a[0].size();
  int bYSize = b.size();
  int bXSize = b[0].size();
  assert(aYSize == bYSize);
  assert(aXSize == bXSize);

  // init result matrix
  v2f res = createV2f(aXSize, aYSize);

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res[y][x] = a[y][x] * b[y][x];
    }
  }
  return res;
}

v2f v2fSquare(v2f a) {
  int aYSize = a.size();
  int aXSize = a[0].size();

  // init result matrix
  v2f res = createV2f(aXSize, aYSize);

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res[y][x] = a[y][x] * a[y][x];
    }
  }
  return res;
}

v2f v2fScale(v2f v, float s) {
  int aYSize = v.size();
  int aXSize = v[0].size();

  // init result matrix
  v2f res = createV2f(aXSize, aYSize);

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res[y][x] = v[y][x] * s;
    }
  }
  return res;
}

v2f v2fRotate180(v2f v) {
  int aYSize = v.size();
  int aXSize = v[0].size();

  // init result matrix
  v2f res = createV2f(aXSize, aYSize);

  for (auto y = 0; y < aYSize; ++y) {
    for (auto x = 0; x < aXSize; ++x) {
      res[y][x] = v[aYSize - y - 1][aXSize - x - 1];
    }
  }
  return res;
}

v3f v3fReluMask(v3f v) {
  v3f ans = v;
  for (auto i = 0; i < v.size(); ++i)
    for (auto y = 0; y < v[0].size(); ++y)
      for (auto x = 0; x < v[0][0].size(); ++x)
        ans[i][y][x] = v[i][y][x] > 0;
  return ans;
}

v3f v3fMultiply(v3f& a, v3f& b){
  auto aChannels = a.size();
  auto bChannels = b.size();
  auto aYSize = a[0].size();
  auto bYSize = b[0].size();
  auto aXSize = a[0][0].size();
  auto bXSize = b[0][0].size();
  assert(aChannels == bChannels);
  auto res = createV3f(aXSize, aYSize, aChannels);
  for (auto i = 0; i < aChannels; ++i)
    res[i] = v2fMultiply(a[i], b[i]);
  return res;
}

v3f v3fAdd(v3f& a, v3f& b){
  auto aChannels = a.size();
  auto bChannels = b.size();
  auto aYSize = a[0].size();
  auto bYSize = b[0].size();
  auto aXSize = a[0][0].size();
  auto bXSize = b[0][0].size();
  assert(aChannels == bChannels);
  auto res = createV3f(aXSize, aYSize, aChannels);
  for (auto i = 0; i < aChannels; ++i)
    res[i] = v2fAdd(a[i], b[i]);
  return res;
}

v3f v3fSubtract(v3f a, v3f b){
  auto aChannels = a.size();
  auto bChannels = b.size();
  auto aYSize = a[0].size();
  auto bYSize = b[0].size();
  auto aXSize = a[0][0].size();
  auto bXSize = b[0][0].size();
  assert(aChannels == bChannels);
  auto res = createV3f(aXSize, aYSize, aChannels);
  for (auto i = 0; i < aChannels; ++i)
    res[i] = v2fSubtract(a[i], b[i]);
  return res;
}

v3f v3fScale(v3f v, float s){
  auto aChannels = v.size();
  auto aYSize = v[0].size();
  auto aXSize = v[0][0].size();
  assert(aChannels == bChannels);
  auto res = createV3f(aXSize, aYSize, aChannels);
  for (auto i = 0; i < aChannels; ++i)
    res[i] = v2fScale(v[i], s);
  return res;
}

vf vfSigmoid(vf v) {
  auto ans = vf(v.size());
  for (auto i = 0; i < v.size(); ++i)
    ans[i] = 1 / (1 + exp(-v[i]));
  return ans;
}

vf vfMultiply(vf a, vf b) {
  auto aLength = a.size();
  auto bLength = b.size();
  assert(aLength == bLength);
  auto ans = a;
  for (auto i = 0; i < aLength; ++i)
    ans[i] = a[i] * b[i];
  return ans;
}

vf vfAdd(vf a, vf b) {
  auto aLength = a.size();
  auto bLength = b.size();
  assert(aLength == bLength);
  auto ans = a;
  for (auto i = 0; i < aLength; ++i)
    ans[i] = a[i] + b[i];
  return ans;
}

vf vfSubtract(float a, vf b) {
  auto ans = b;
  for (auto i = 0; i < b.size(); ++i)
    ans[i] = a - b[i];
  return ans;
}

vf vfScale(vf v, float s) {
  auto ans = v;
  for (auto i = 0; i < v.size(); ++i)
    ans[i] = v[i] * s;
  return ans;
}

vf vfDSigmoid(vf v) {
  auto sigV = vfSigmoid(v);
  auto dv = vfMultiply(sigV, (vfSubtract(1, sigV)));
  return dv;
}

v3f generateRandomConvolutionInput(int channels, int size) {
  v3f ans;
  for (int i = 0; i < channels; ++i) {
    ans.emplace_back();
    ans[i] = createV2f(size, size);
  }
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.0, 1.0);
  for (auto &i: ans)
    for (auto &r: i)
      for (auto &v: r) {
        v = dis(gen);
      }
  return ans;
}

vf generateRandomVectorInput(unsigned int length) {
  auto ans = vf(length);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1.0, 1.0);
  for (auto &v: ans)
    v = dis(gen);
  return ans;
}

void printV(vf &v) {
  for (auto &val: v) {
    std::cout << val << " ";
  }
  std::cout << std::endl;
}

void printV2(v2f &v) {
  for (auto &row: v) {
    printV(row);
  }
  std::cout << std::endl;
}

void printV3(v3f &v) {
  for (auto &v2 : v)
    printV2(v2);
}

void testConv2d() {
  v2f x = {{1, 1, 1},
           {1, 1, 1},
           {1, 1, 1}};
  v2f k = {{21, 2, 23},
           {24, 5, 26},
           {27, 8, 29}};
  auto convResult = conv2d(x, k, 3, 3);
  printV2(convResult);
}

void testV2fSum() {
  v2f a = {{10, 20, 30},
           {40, 50, 60},
           {70, 80, 90}};
  v2f b = {{1,  22, 33},
           {43, 5,  6},
           {7,  8,  9}};
  printV2(a);
  printV2(b);
  auto convResult = v2fAdd(a, b);
  printV2(convResult);
}

vf dot(vf v, v2f m) {
  auto vLength = v.size();
  auto mLength = m[0].size();
  auto mWidth = m.size();
  assert(vLength == mWidth);

  auto outVec = vf(mLength);
  for (auto o = 0; o < mLength; ++o) {
    float outVal = 0.f;
    for (auto i = 0; i < mWidth; ++i) {
      outVal += v[i] * m[i][o];
    }
    outVec[o] = outVal;
  }
  return outVec;
}

vf dotTransposed(vf v, v2f m) {
  auto vLength = v.size();
  auto mWidth = m[0].size();
  auto mLength = m.size();
  assert(vLength == mWidth);

  auto outVec = vf(mLength);
  for (auto o = 0; o < mLength; ++o) {
    float outVal = 0.f;
    for (auto i = 0; i < mWidth; ++i) {
      outVal += v[i] * m[o][i];
    }
    outVec[o] = outVal;
  }
  return outVec;
}

v2f outer(vf v1, vf v2) {
  auto v1Length = v1.size();
  auto v2Length = v2.size();
  auto outMat = createV2f(v2Length, v1Length);
  for (auto y = 0; y < v1Length; ++y) {
    for (auto x = 0; x < v2Length; ++x) {
      outMat[y][x] = v1[y] * v2[x];
    }
  }
  return outMat;
}

vf v3fFlatten(v3f v) {
  vf ans;
  for (auto &c: v)
    for (auto &r: c)
      for (auto &val: r)
        ans.push_back(val);
  return ans;
}

v3f vfToV3f(vf v, unsigned int channels) {
  auto vLength = v.size();
  auto matSize = double(vLength) / double(channels);
  auto xSize = sqrt(matSize);
  auto ySize = xSize;
  assert((int(xSize) * int(ySize) * channels) == vLength);

  auto res = createV3f(int(xSize), int(ySize), channels);
  auto channelSize = int(xSize) * int(ySize);
  for (auto i = 0; i < vLength; ++i) {
    unsigned int channelIndex = i / channelSize;
    unsigned int yIndex = (i - (channelIndex * channelSize)) / int(xSize);
    unsigned int xIndex = i - (channelIndex * channelSize) - (yIndex * ySize);
    res[channelIndex][yIndex][xIndex] = v[i];
  }
  return res;
}

v3f testDlDo(int channels, v3f target, v3f prediction) {
  v3f loss;
  v3f diff;

  for (auto i = 0; i < channels; ++i)
    diff.push_back(v2fSubtract(prediction[i], target[i]));

  for (auto i = 0; i < channels; ++i)
    loss.push_back(v2fSquare(diff[i]));

  float totalLoss = 0.f;
  for (auto i = 0; i < channels; ++i)
    totalLoss += v2fSum(loss[i]);

  std::cout << "Loss: " << totalLoss << std::endl;

  return diff;
}

vf mseDerivative(vf target, vf prediction) {
  vf diff = vfSubtract(prediction, target);
//  vf loss = vfSquare(diff);
//  float totalLoss = 0.f;
//  for (auto i = 0; i < diff.size(); ++i)
//    totalLoss += loss[i];
  return diff;
}

vf toOneHot(char c, unsigned int categories) {
  vf ans = vf(categories);
  ans[c] = 1;
  return ans;
}

v4f mnistDataToV(unsigned char **data, unsigned int numImages) {
  v4f res;
  for (auto i = 0; i < numImages; ++i) {
    res.push_back(createV3f(28, 28, 1));
    for (auto y = 0; y < 28; ++y) {
      vf row = vf(28);
      for (auto x = 0; x < 28; ++x) {
        row[x] = (data[i][x + y * 28]) > 0;
      }
      res[i][0][y] = row;
    }
  }
  return res;
}

v2f mnistLabelToV(unsigned char *data, unsigned int numLabels) {
  v2f res;
  for (auto i = 0; i < numLabels; ++i)
    res.push_back(toOneHot(data[i], 10));
  return res;
}