#include <iostream>
#include "ConvolutionLayer.h"
#include "DenseLayer.h"
#include "ConvNet.h"
#include "DenseNet.h"
#include "MatrixHelper.h"
#include "ReadData.h"

void testConvLayerTraining()
{
  auto testInChannels = 10;
  auto testOutChannels = 10;
  auto inputSize = 8;
  auto targetLayer = ConvolutionLayer(inputSize, inputSize, testInChannels, testOutChannels, 3);
  auto trainLayer = ConvolutionLayer(inputSize, inputSize, testInChannels, testOutChannels, 3);
  auto steps = 1000;
  for (int i = 0; i < steps; ++i){
    v3f x = generateRandomConvolutionInput(testInChannels, inputSize);
    v3f oTarget = targetLayer.forward(x);
    v3f oNetwork = trainLayer.forward(x);
    v3f dldo = testDlDo(testInChannels, oTarget, oNetwork);
    trainLayer.backward(dldo, 0.01);
  }
}

void testDenseNetTraining()
{
  auto testUnits = 10;
  auto targetNet = DenseNet(3, 0.001, testUnits);
  auto trainNet = DenseNet(3, 0.001, testUnits);
  auto steps = 100000;
  for (int i = 0; i < steps; ++i){
    vf x = generateRandomVectorInput(testUnits);
    vf oTarget = targetNet.forward(x);
    vf oNetwork = trainNet.forward(x);
    vf dldo = mseDerivative(oTarget, oNetwork);
    trainNet.backward(dldo);
  }
}

int correct(vf pred, vf label)
{
  float maxPred = 0.f;
  int predIndex = 0;
  for (auto i = 0; i < label.size(); ++i)
  {
    if (pred[i] > maxPred){
      maxPred = pred[i];
      predIndex = i;
    }
  }
  return (label[predIndex] == 1);
}

void testConvNetTraining(float lr, int layers)
{
  auto imXSize = 28;
  auto imYSize = 28;
  auto net = ConvNet(imXSize, imYSize, layers, lr);
  auto epochs = 100;

  auto imLoadCount = 60000;
  auto imSize = imXSize * imYSize;
  auto data = read_mnist_images("/home/ed/CLionProjects/cnn/mnist/train-images-idx3-ubyte", imLoadCount, imSize);
  auto labels = read_mnist_labels("/home/ed/CLionProjects/cnn/mnist/train-labels-idx1-ubyte", imLoadCount);

  auto imCount = 1000;
  auto xs = mnistDataToV(data, imCount);
  auto ys = mnistLabelToV(labels, imCount);

  for (int e = 0; e < epochs; ++e){
    float epochCorrect = 0.f;
    int imagesSeen = 0;
    for (int i = 0; i < imCount; ++i) {
      imagesSeen++;
      auto prediction = net.forward(xs[i]);
      epochCorrect += correct(prediction, ys[i]);
      auto dldo = mseDerivative(ys[i], prediction);
      net.backward(dldo);
    }
    std::cout << "Epoch acc: " << epochCorrect/imagesSeen << std::endl;
  }

  for (auto i = 0; i<imLoadCount; ++i)
    delete[] data[i];
  delete[] data;
  delete[] labels;
}

void testDenseLayerTraining()
{
  auto testInUnits = 10;
  auto testOutUnits = 2;
  auto targetLayer = DenseLayer(testInUnits, testOutUnits);
  auto trainLayer = DenseLayer(testInUnits, testOutUnits);
  auto steps = 10000;
  for (int i = 0; i < steps; ++i){
    vf x = generateRandomVectorInput(testInUnits);
    vf oTarget = targetLayer.forward(x);
    vf oNetwork = trainLayer.forward(x);
    vf dldo = mseDerivative(oTarget, oNetwork);
    trainLayer.backward(dldo, 0.1);
  }
}

void testReshaping()
{
  v3f _v3 = generateRandomConvolutionInput(10, 4);
  vf _v = v3fFlatten(_v3);
  printV3(_v3);
  for (auto& el: _v)
    std::cout << el << " ";
  std::cout << std::endl;
  v3f reshaped = vfToV3f(_v, 10);
  printV3(reshaped);
}