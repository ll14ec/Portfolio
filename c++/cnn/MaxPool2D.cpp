#include "MaxPool2D.h"
#include <limits>
#include <cassert>

MaxPool2D::MaxPool2D()
{
  _channels = 0;
  _inYSize = 0;
  _inXSize = 0;
}

v3f MaxPool2D::forward(v3f &v){
  auto y = (int) v.size();
  _channels = y;
  _inYSize = (int) v[0].size();
  _inXSize = (int) v[0][0].size();
  _mask = createV3f(_inXSize, _inYSize, _channels);
  int outYSize = _inYSize / _poolSize;
  int outXSize = _inXSize / _poolSize;

  v3f res = createV3f(outXSize, outYSize, _channels);
  for (auto ch = 0; ch < _channels; ++ch) {
    for (auto y = 0; y < outYSize; ++y) {
      for (auto x = 0; x < outXSize; ++x) {
        float maxVal = -std::numeric_limits<float>::max();
        int xIndex = -1;
        int yIndex = -1;
        for (auto py = 0; py < _poolSize; ++py) {
          for (auto px = 0; px < _poolSize; ++px) {
            auto inVal = v[ch][(y * _poolSize) + py][(x * _poolSize) + px];
            auto fg = 0;
            if (inVal > maxVal) {
              maxVal = inVal;
              yIndex = (y * _poolSize) + py;
              xIndex = (x * _poolSize) + px;
            }
          }
        }
        assert (yIndex != -1);
        assert (xIndex != -1);
        res[ch][y][x] = maxVal;
        _mask[ch][yIndex][xIndex] = 1;
      }
    }
  }
  return res;
}

v3f MaxPool2D::backward(v3f &dldo){
  v3f res = createV3f(_inXSize, _inYSize, _channels);
  for (auto ch = 0; ch < _channels; ++ch) {
    for (auto y = 0; y < _inYSize; ++y) {
      for (auto x = 0; x < _inXSize; ++x) {
        if (_mask[ch][y][x]) {
          res[ch][y][x] = dldo[ch][y / _poolSize][x / _poolSize];
        }
      }
    }
  }
  return res;
}