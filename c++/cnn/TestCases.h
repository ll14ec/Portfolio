#ifndef CNN_TESTCASES_H
#define CNN_TESTCASES_H

void testConvLayerTraining();
void testDenseNetTraining();
void testConvNetTraining(float lr, int layers);
void testDenseLayerTraining();
void testReshaping();

#endif //CNN_TESTCASES_H
