/*      Ed Cottle's Game o' Death - 2016
 *
 *      Cellular automatia simulation using the rules for the game of life.
 *
 *      Programmed for an MBED LPC1768 to use a nokia NS5110 lcd display.
 *
 *      The game has a main menu, and two different modes to select. 
 *
 *      1) Standard game of life with random object/glider generation/speed controlled by 
 *
 *      analogue ins.
 *
 *      2) Game of Death - two randomly generated armies fight each other with similar
 *
 *      rules to the standard game, however the rules of engagement can be changed
 *
 *      with analogue ins providing many different outcomes from the same starting point.
 *
 */

#include "mbed.h"   //mbed 
#include "N5110.h"  //lcd library
#include "time.h"   //time 
#include "math.h"   //maths functions
#include "tone.h"   //note constatnts

//  CONSTANTS

#define SWIDTH 84
#define SHEIGHT 48
#define FALSE 0
#define TRUE 1
#define FIXEDBORDER 0
#define WRAPBORDER 1
#define PI 3.14159265359
#define PLAY 0
#define PAUSE 1

// FUNCTION PROTOTYPES

void initArray();
void drawLineX(int,int,int,int,int);
void drawRectX(int,int,int,int,int);
void  drawCircleX(int,int,int,int);
void tone(float,float);
void setBrightness();
void refreshArray();
void getPixels();
void clear();
void pauseGame();
void intro();
void writeMenu();
void displayWelcome();
void switchBounds();
void invert();
void cellTick(char, char, char);
void updateArray(char);
void crazyFunction();
void object(char, char);
void glider(char, char, int);
void pattern(char, char);
void square();
void line();
void circle();
void create();
void randomiseScreen();
void updateScreen();
void updateScreen2();
void secondArray();
void flicker();
char getFriends(char, char, char);
char getEnemies(char, char, char);
char scan(int, int, int);
void war();
void chaos();

//  PIN ASSIGNMENT

N5110 lcd (p7,p8,p9,p10,p11,p13,p21); //VCC SCE RSTDC10 MOSI SCLOCK LED
DigitalIn randomise(p30);
DigitalIn pause(p29);
DigitalIn reset(p28);
DigitalIn boundary(p27);
DigitalOut tick(p26);
AnalogIn pot (p17);
AnalogIn user(p19);
AnalogIn ldr (p20);
AnalogOut aout(p18);



// AUDIO VARIABLES

int n = 32;  // number of samples
float y[32]; // array to store samples
float BPM = 73.0;  // beats per minute
Timer noteTimer;

// PIXEL ARRAY

char pixels [SWIDTH][SHEIGHT];
char pixels2 [SWIDTH][SHEIGHT];
char currentBorder;

//  TIMING CONSTANTS

float waitPeriod = 0.1;
char state;
int flip = 0;

void initArray()
{
    // create LUT - loop through array and calculate sine wave samples
    for (int i = 0; i < n ; i++) {
        y[i] = 0.5 + 0.5*sin(i*2*PI/n);
    }
}

// DRAW FUNCTIONS --->

// Re-written from the library so that they affect the arrays and not the pixels of the screen directly.

void drawLineX(int x0,int y0,int x1,int y1,int type)
{
    int y_range = y1-y0;  // calc range of y and x
    int x_range = x1-x0;
    int start,stop,step;

    // if dotted line, set step to 2, else step is 1
    step = (type==2) ? 2:1;

    // make sure we loop over the largest range to get the most pixels on the display
    // for instance, if drawing a vertical line (x_range = 0), we need to loop down the y pixels
    // or else we'll only end up with 1 pixel in the x column
    if ( abs(x_range) > abs(y_range) ) {

        // ensure we loop from smallest to largest or else for-loop won't run as expected
        start = x1>x0 ? x0:x1;
        stop =  x1>x0 ? x1:x0;

        // loop between x pixels
        for (int x = start; x<= stop ; x+=step) {
            // do linear interpolation
            int y = y0 + (y1-y0)*(x-x0)/(x1-x0);

            if (type == 0) {  // if 'white' line, turn off pixel
                if ((x>-1)&&(x<SWIDTH)&&(y-1)&&(x<SHEIGHT)) {
                    pixels[x][y] = 0;
                }
            } else if ((x>-1)&&(x<SWIDTH)&&(y-1)&&(x<SHEIGHT)) {
                pixels[x][y] = 0;  // else if 'black' or 'dotted' turn on pixel
            }
        }
    } else {

        // ensure we loop from smallest to largest or else for-loop won't run as expected
        start = y1>y0 ? y0:y1;
        stop =  y1>y0 ? y1:y0;

        for (int y = start; y<= stop ; y+=step) {
            // do linear interpolation
            int x = x0 + (x1-x0)*(y-y0)/(y1-y0);

            if (type == 0) {  // if 'white' line, turn off pixel
                if ((x>-1)&&(x<SWIDTH)&&(y-1)&&(x<SHEIGHT)) {
                    pixels[x][y] = 0;
                }
            } else if ((x>-1)&&(x<SWIDTH)&&(y-1)&&(x<SHEIGHT)) {
                pixels[x][y] = 1;
            } // else if 'black' or 'dotted' turn on pixel

        }
    }
}

void drawRectX(int x0,int y0,int width,int height,int fill)
{

    if (fill == 0) { // transparent, just outline
        drawLineX(x0,y0,x0+width,y0,1);  // top
        drawLineX(x0,y0+height,x0+width,y0+height,1);  // bottom
        drawLineX(x0,y0,x0,y0+height,1);  // left
        drawLineX(x0+width,y0,x0+width,y0+height,1);  // right
    } else { // filled rectangle
        int type = (fill==1) ? 1:0;  // black or white fill
        for (int y = y0; y<= y0+height; y++) {  // loop through rows of rectangle
            drawLineX(x0,y,x0+width,y,type);  // draw line across screen
        }
    }

}

void  drawCircleX(int x0,int y0,int radius,int fill)
{
    // from http://en.wikipedia.org/wiki/Midpoint_circle_algorithm
    int x = radius;
    int y = 0;
    int radiusError = 1-x;
    while(x >= y) {
        // if transparent, just draw outline
        if (fill == 0) {
            signed int pxx0 = x+x0 ;
            signed int nxx0 = -x+x0;
            signed int pyx0 = y+x0;
            signed int nyx0 = -y+x0;
            signed int pxy0 = x+y0 ;
            signed int nxy0 = -x+y0;
            signed int pyy0 = y+y0;
            signed int nyy0 = -y+y0;

            if (pxx0<0 || pxx0>=SWIDTH || nxx0<0 || nxx0>=SWIDTH || pyx0<0 || pxx0>=SWIDTH || nyx0<0 || nxx0>=SWIDTH) {
                return;
            }

            if (pxy0<0 || pxy0>=SHEIGHT|| nxy0<0 || nxy0>=SHEIGHT || pyy0<0 || pxy0>=SHEIGHT || nyy0<0 || nxy0>=SHEIGHT) {
                return;
            }

            pixels[x+x0][y+y0] = 1;
            pixels[-x+x0][y+y0] = 1;
            pixels[y+x0][x+y0] = 1;
            pixels[-y+x0][x+y0] = 1;
            pixels[-y+x0][-x+y0] = 1;
            pixels[y+x0][-x+y0] = 1;
            pixels[x+x0][-y+y0] = 1;
            pixels[-x+x0][-y+y0] = 1;
        } else {  // drawing filled circle, so draw lines between points at same y value

            int type = (fill==1) ? 1:0;  // black or white fill

            drawLineX(x+x0,y+y0,-x+x0,y+y0,type);
            drawLineX(y+x0,x+y0,-y+x0,x+y0,type);
            drawLineX(y+x0,-x+y0,-y+x0,-x+y0,type);
            drawLineX(x+x0,-y+y0,-x+x0,-y+y0,type);
        }
        y++;
        if (radiusError<0) {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError += 2 * (y - x) + 1;
        }
    }

}

void tone(float frequency,float duration)
{

    if (frequency > 0) {  // a frequency of 0 indicates no note played so only play a note if frequency is not 0

        float dt = 1.0/(frequency*n) - (1.34e-6 + 1e-6);  // calculate time step - take into account DAC time and wait() offset

        noteTimer.start(); // start timer

        while(noteTimer.read() < duration) { // keep looping while timer less than duration

            for (int i = 0; i < n ; i++) {  // loop through samples and output analog waveform
                aout = y[i];
                wait(dt); // leave appropriate delay for frequency
            }
        }
        noteTimer.stop();  // stop the timer
        noteTimer.reset(); // reset to 0

    } else { // if no note played, have a simple delay
        wait(duration);
    }


}

// DECIDES THE BRIGHNESS TO SET THE SCREEN

void setBrightness()
{
    lcd.setBrightness(ldr.read()*2);
}

//  RESETS THE PIXAL ARRAY TO 0

void refreshArray()
{
    char i = 0;
    char j = 0;
    for (i=0; i<SWIDTH; i++) {
        for (j=0; j<SHEIGHT; j++) {
            pixels[i][j] = FALSE;
        }
    }
}

void getPixels()
{
    int i = 0;
    int j = 0;
    for(i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            pixels[i][j] = lcd.getPixel(i,j);
        }
    }
}

//  RESETS ALL PIXELS ON THE SCREEN

//  VISIBLE ON SCREEN AS A TRANSITION L->R

void clear()
{
    char i = 0;
    char k = 0;
    for (i=0; i<SWIDTH; i++) {
        for (k=0; k<SHEIGHT; k++) {
            pixels[i][k] = 0;
            //update screen as pixels are being reset
            if (!k%3) {
                updateScreen();
            }
        }
        updateScreen();
        //increase the transition speed as it runs
        wait(0.18/(i+1));
    }
}

// PAUSES THE GAMNE WHEN THE PAUSE BUTTON IS PRESSED

void pauseGame()
{
    //wait for user to un-press pause
    while(pause) {
        wait(0.1);
    }
    //wait until the user un-pauses the game
    while(!pause) {
        wait(0.1);
    }
    //wait until the pause button is released for a second time
    while(pause) {
        wait(0.1);
    }

}

// DISTPLAYS THE INTRO ON SCREEN

void intro()
{
    char i = 0;
    //create 'static' effect by randomising the pixels
    for (i=0; i<50; i++) {
        randomiseScreen();
        updateScreen();
        wait((rand() % 1)/5);
    }
    lcd.clear();
    lcd.refresh();
    //print first message
    lcd.printString("Welcome to" , 15, 1);
    wait(0.5);
    //static
    for (i=0; i<50; i++) {
        randomiseScreen();
        updateScreen();
        wait((rand() % 1)/5);
    }
    lcd.clear();
    lcd.refresh();
    //print second message
    lcd.printString("Cottle's" , 20, 3);
    wait(0.5);
    //static
    for (i=0; i<50; i++) {
        randomiseScreen();
        updateScreen();
        wait((rand() % 1)/5);
    }
    lcd.clear();
    lcd.refresh();
    //print third message
    lcd.printString("Game o Death" , 6, 4);
    wait(0.5);
    //static
    for (i=0; i<50; i++) {
        randomiseScreen();
        updateScreen();
        wait((rand() % 1)/5);
    }
}

void writeMenu()
{
    lcd.printString("Select Game", 9,2);
}

//  SIMPLE TEXT INTRO

void displayWelcome()
{
    lcd.printString("Welcome to" , 15, 1);
    lcd.refresh();
    lcd.printString("Cottle's" , 20, 3);
    lcd.refresh();
    lcd.printString("Game o Death" , 6, 4);
    lcd.refresh();
    wait(2);
    lcd.clear();
}

// SWITCHES THE BOUNDARY TYPE WHEN THE BUTTON IS PRESSED

void switchBounds()
{
    while(boundary) {
        wait(0.01);
    }

    tone(NOTE_G2,60.0/(BPM*4));
    if (currentBorder == WRAPBORDER) {
        currentBorder = FIXEDBORDER;
    } else {
        currentBorder = WRAPBORDER;
    }
}

// INVERTS THE DISPLAY MODE

void invert()
{
    if (flip%2) {
        lcd.normalMode();
    } else {
        lcd.inverseMode();
    }
    flip++;
}

// APPLIES THE GAME OF LIFE TO THE CELLS

void cellTick(char neighbours, char i, char j)
{
    if ((pixels[i][j] == TRUE) && (neighbours <2)) {
        pixels[i][j] = FALSE;
        neighbours = 0;
    }

    else if (((pixels[i][j] == TRUE) && (neighbours == 2)) || ((pixels[i][j] == TRUE) && (neighbours == 3)) ) {
        pixels[i][j] = TRUE;
        neighbours = 0;
    }

    else if ((pixels[i][j] == TRUE) && (neighbours > 3)) {
        pixels[i][j] =  FALSE;
        neighbours = 0;
    }

    else if ((pixels[i][j] == FALSE) && (neighbours == 3)) {
        pixels[i][j] = TRUE;
        neighbours = 0;
    }
}

// UPDATES THE SCREEN ACCORDING TO THE GAME OF LIFE RULES

// FIRST CALCULATES THE NEIGHBOURS OF THE CELL

// THEN CALLS cellTick() ON THE CELL WITH THE CALCULATED NEIGHBOURS

// ARGUMENT OF THE FUNCTION DICTATES BORDER TYPE

void updateArray(char border)
{
    //looping variables, neghbours variable, variables for the i+1, i-1, j+1 and j-1 values
    char i =0;
    char j =0;
    char k =0;
    char neighbours = 0;
    signed char ip1=0;
    signed char  im1=0;
    signed char  jp1=0;
    signed char  jm1=0;

    //  FIXED   BORDER      --->

    if (border==FIXEDBORDER) {
        i = j = 1;
        for (i=1; (i+1)<SWIDTH; i++) {
            for (j=1; (j+1)<SHEIGHT; j++) {

                neighbours = 0;

                if (lcd.getPixel(i+1,j)) {
                    neighbours++;
                }
                if (lcd.getPixel(i,j+1)) {
                    neighbours++;
                }
                if (lcd.getPixel(i+1,j+1)) {
                    neighbours++;
                }
                if (lcd.getPixel(i-1,j)) {
                    neighbours++;
                }
                if (lcd.getPixel(i,j-1)) {
                    neighbours++;
                }

                if (lcd.getPixel(i-1,j-1)) {
                    neighbours++;
                }
                if (lcd.getPixel(i-1,j+1)) {
                    neighbours++;
                }
                if (lcd.getPixel(i+1,j-1)) {
                    neighbours++;
                }

                cellTick(neighbours, i, j);
                //set all border cells to 1
                for (k=0; k<SWIDTH; k++) {
                    pixels[k][0] = 0;
                    pixels[k][SHEIGHT-1] = 0;
                }

                for (k=0; k<SHEIGHT; k++) {
                    pixels[0][k] = 0;
                    pixels[SWIDTH-1][k] = 0;
                }
            }
        }
    }

    //  WRAP    BORDER      --->

    else if (border == WRAPBORDER) {
        i = j = 0;
        for (i=0; i<SWIDTH; i++) {
            for (j=0; j<SHEIGHT; j++) {

                jp1 = j + 1;
                if (jp1==SHEIGHT) {
                    jp1=0;
                }

                jm1 = j - 1;
                if (jm1 == -1) {
                    jm1 = SHEIGHT-1;
                }

                ip1 = i + 1;
                if (ip1==SWIDTH) {
                    ip1=0;
                }

                im1 = i - 1;
                if (im1 == -1) {
                    im1 = SWIDTH-1;
                }

                neighbours = 0;

                if (lcd.getPixel(ip1,j)) {
                    neighbours++;
                }
                if (lcd.getPixel(i,jp1)) {
                    neighbours++;
                }
                if (lcd.getPixel(ip1,jp1)) {
                    neighbours++;
                }
                if (lcd.getPixel(im1,j)) {
                    neighbours++;
                }
                if (lcd.getPixel(i,jm1)) {
                    neighbours++;
                }

                if (lcd.getPixel(im1,jm1)) {
                    neighbours++;
                }
                if (lcd.getPixel(im1,jp1)) {
                    neighbours++;
                }
                if (lcd.getPixel(ip1,jm1)) {
                    neighbours++;
                }

                cellTick(neighbours, i, j);
            }
        }
    }
}

// CREATES A LIVING OBJECT

void object(char x, char y)
{
    pixels[x][y] = 1;
    pixels[x][y+1] = 1;
    pixels[x][y+2] = 1;
    pixels[x-1][y+3] = 1;
    pixels[x-2][y+1] = 1;
}

//  CREATES A CLASSC 'GLIDER' IN 4 DIFFERENT DIRECTIONS

void glider(char x, char y, int type)
{
    if (type == 0) {
        if ((x>2)&&(x<SWIDTH-2)&&(y>2)&&(y<SHEIGHT-2)) {
            pixels[x+1][y] = 1;
            pixels[x+2][y+1] = 1;
            pixels[x][y+2] = 1;
            pixels[x+1][y+2] = 1;
            pixels[x+2][y+2] = 1;
        }
    }
    if (type == 1) {
        if ((x>2)&&(x<SWIDTH-2)&&(y>2)&&(y<SHEIGHT-2)) {
            pixels[x+1][y] = 1;
            pixels[x][y+1] = 1;
            pixels[x][y+2] = 1;
            pixels[x+1][y+2] = 1;
            pixels[x+2][y+2] = 1;
        }
    }
    if (type == 2) {
        if ((x>2)&&(x<SWIDTH-2)&&(y>2)&&(y<SHEIGHT-2)) {
            pixels[x+1][y+2] = 1;
            pixels[x+2][y+1] = 1;
            pixels[x][y] = 1;
            pixels[x+1][y] = 1;
            pixels[x+2][y] = 1;
        }
    }
    if (type == 3) {
        if ((x>2)&&(x<SWIDTH-2)&&(y>2)&&(y<SHEIGHT-2)) {
            pixels[x+1][y+2] = 1;
            pixels[x][y+1] = 1;
            pixels[x][y] = 1;
            pixels[x+1][y] = 1;
            pixels[x+2][y] = 1;
        }
    }
}

//  CREATES A LIVING OBJECT THAT EVOLES IF UNDISTURBED

void pattern(char x, char y)
{
    if ((x>3)&&(x<SWIDTH-3)&&(y>5)&&(y<SHEIGHT-5)) {
        pixels[x][y] = 1;
        pixels[x+1][y] = 1;
        pixels[x+2][y] = 1;
        pixels[x+1][y+2] = 1;
        pixels[x][y+4] = 1;
        pixels[x+1][y+4] = 1;
        pixels[x+2][y+4] = 1;
    }
}

//CREATES A SQUARE

void square()
{
    char x = (int)rand()%(SWIDTH-1);
    char  y = (int)rand()%(SHEIGHT-1);
    char w = ((int)rand()%(SWIDTH-1)/2.0);
    char h = ((int)rand()%(SHEIGHT-1)/2.0);
    char f = (int)rand()%2;
    drawRectX(x, y, w, h, 0);
}

//CREATES A LINE

void line()
{
    char x = (int)rand()%(SWIDTH-1);
    char  y = (int)rand()%(SHEIGHT-1);
    char w = ((int)rand()%(SWIDTH-1)/2.0);
    char h = ((int)rand()%(SHEIGHT-1)/2.0);
    char t = (int)rand()%3;
    drawLineX(x,y,w,h,t);
}
//CREATES A CIRCLE
void circle()
{
    char x = (int)rand()%(SWIDTH-1);
    char  y = (int)rand()%(SHEIGHT-1);
    char r = (int)rand()%50;
    char f = (int)rand()%2;
    drawCircleX(x,y,r,0);
}

//GENERATES SEMI-RANDOM SCENERY FOR THE MAIN GAME

void create()
{
    if ((rand() % 100)>93) {

        float decider = (user.read()*100);
        decider += (rand() % 15) ;
        decider -= (rand() % 15);

        int x =  rand() % (SWIDTH-1);
        int y =  rand() % (SHEIGHT-1);

        if (decider<0) {
            decider =0;
        }

        if (!decider % 15) {
            object(x,y);
        }

        if (decider < 20) {
            pattern(x,y);
        }

        else if (decider < 40) {
            glider(x,y,((unsigned int)rand() % 4));

        }

        else if (decider <60) {
            circle();

        } else if (decider <80) {
            square();
        } else if (decider <100) {
            line();
        }
    }
}

//  ASSIGNS THE PIXELS RANDOMLY GENERATED VALUES OF 1 OR 0

void randomiseScreen()
{
    char i = 0;
    char j = 0;
    float decider = 0;
    for (i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            decider = rand() % 10;
            if (decider > 1) {
                pixels[i][j] = 0;
            } else {
                pixels[i][j] = 1;
            }
        }
    }
}

//  UPDATES THE LCD SCREEN ACCORDING TO THE PIXEL ARRAY

void updateScreen()
{
    char i = 0;
    char j = 0;
    for (i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            if (pixels[i][j] ==  TRUE) {
                lcd.setPixel(i,j);
            } else if (pixels[i][j] == FALSE) {
                lcd.clearPixel(i,j);
            }
        }
    }
    lcd.refresh();
}

//UPDATES THE SCREEN FOR GAME 2

void updateScreen2()
{
    char i = 0;
    char j = 0;
    for (i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            if (pixels[i][j] ==  1) {
                lcd.setPixel(i,j);
            } else if (pixels[i][j] == 0) {
                lcd.clearPixel(i,j);
            }
        }
    }

    for (i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            if (pixels[i][j] ==  2) {
                lcd.setPixel(i,j);
            } else if (pixels[i][j] == 0) {
                lcd.clearPixel(i,j);
            }
        }
    }
    lcd.refresh();
}

//UPDATES THE ARRAY PIXELS2 TO == PIXELS

void secondArray()
{
    int i;
    int j;
    for (i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            pixels2[i][j] = pixels[i][j];
        }
    }
}

//TURNS OFF THE CELLS ON THE SECOND 'TEAM' TO DIFFERENTIATE THEM ONSCREEN

void flicker()
{
    int i=0;
    int j=0;
    for (i=0; i<SWIDTH; i++) {
        for(j=0; j<SHEIGHT; j++) {
            if (pixels2[i][j] ==  2) {
                lcd.clearPixel(i,j);
            }
        }
    }
}

//CALCULATES THE NUMBER OF FRIENDLY CELLLS

char getFriends(char i, char j, char team)
{
    char friends=0;
    if (pixels2[i+1][j+1]==team) {
        friends++;
    }
    if (pixels2[i][j+1]==team) {
        friends++;
    }
    if (pixels2[i+1][j]==team) {
        friends++;
    }
    if (pixels2[i-1][j-1]==team) {
        friends++;
    }
    if (pixels2[i][j-1]==team) {
        friends++;
    }
    if (pixels2[i-1][j]==team) {
        friends++;
    }
    if (pixels2[i+1][j-1]==team) {
        friends++;
    }
    if (pixels2[i-1][j+1]==team) {
        friends++;
    }
    return friends;
}

//CALCULATES THE NUMBER OF ENEMY CELLS

char getEnemies(char i, char j, char team)
{
    char enemies=0;
    if ((pixels2[i+1][j+1]!=team)&&(pixels2[i+1][j+1]!=0)) {
        enemies++;
    }
    if ((pixels2[i][j+1]!=team)&&(pixels2[i][j+1]!=0)) {
        enemies++;
    }
    if ((pixels2[i+1][j]!=team)&&(pixels2[i+1][j]!=0)) {
        enemies++;
    }
    if ((pixels2[i-1][j-1]!=team)&&(pixels2[i-1][j-1]!=0)) {
        enemies++;
    }
    if ((pixels2[i][j-1]!=team)&&(pixels2[i][j-1]!=0)) {
        enemies++;
    }
    if ((pixels2[i-1][j]!=team) &&(pixels2[i-1][j]!=0)) {
        enemies++;
    }
    if ((pixels2[i+1][j-1]!=team)&&(pixels2[i+1][j-1]!=0)) {
        enemies++;
    }
    if ((pixels2[i-1][j+1]!=team)&&(pixels2[i-1][j+1]!=0)) {
        enemies++;
    }
    return enemies;
}

//GENERATES A NEW CELL OF TYPE 1/2 IF THE CODITIONS ARE RIGHT

char scan(int state, int i, int j)
{
    char ones;
    char twos;

    if (pixels2[i+1][j+1]==1) {
        ones++;
    }
    if (pixels2[i][j+1]==1) {
        ones++;
    }
    if (pixels2[i+1][j]==1) {
        ones++;
    }
    if (pixels2[i-1][j-1]==1) {
        ones++;
    }
    if (pixels2[i][j-1]==1) {
        ones++;
    }
    if (pixels2[i-1][j]==1) {
        ones++;
    }
    if (pixels2[i+1][j-1]==1) {
        ones++;
    }
    if (pixels2[i-1][j+1]==1) {
        ones++;
    }

    if (pixels2[i+1][j+1]==2) {
        twos++;
    }
    if (pixels2[i][j+1]==2) {
        twos++;
    }
    if (pixels2[i+1][j]==2) {
        twos++;
    }
    if (pixels2[i-1][j-1]==2) {
        twos++;
    }
    if (pixels2[i][j-1]==2) {
        twos++;
    }
    if (pixels2[i-1][j]==2) {
        twos++;
    }
    if (pixels2[i+1][j-1]==2) {
        twos++;
    }
    if (pixels2[i-1][j+1]==2) {
        twos++;
    }

    //switch a cell's loyalty based on conditions, can be affected by the user potentiometer

    if (twos >(5*user.read())&& twos<6 && ones<1) {
        return 2;
    }

    if (ones >(5*user.read()) && ones<6 && twos<1) {
        return 1;
    }
    return 0;
}

//UPDATES THE GAME FOR THE TWO-TEAM GAME OF DEATH RULLES

//THESE RULES WERE WRITTEN BY ME

void war()
{
    char friends=0;
    char enemies=0;
    char i;
    char j;

    for (i=1; i<SWIDTH-1; i++) {
        for(j=1; j<SHEIGHT-1; j++) {
            friends = getFriends(i,j, pixels2[i][j]);
            enemies = getEnemies(i,j, pixels2[i][j]);

            if (pixels[i][j] !=0) {
                //if less than 3 friendly neighbours, the cell dies
                if (friends<3) {
                    pixels[i][j]=0;
                }
                //if there are more enemies than friends around the cell, switch the cell's loyalty
                if (enemies>friends) {
                    if (pixels2[i][j] == 1) {
                        pixels[i][j]=2;
                    }
                    if (pixels2[i][j] == 2) {
                        pixels[i][j]=1;
                    }
                }
                //if there are too many cells, around a cell, kill it (rule is affected by user input)
                if (friends+enemies > (12*user.read())) {
                    pixels[i][j]=0;
                }
            }
            if (pixels2[i][j]!=1 && pixels2[i][j]!=2) {
                pixels[i][j] = scan(pixels2[i][j], i ,j);

            }
        }
    }
}

//RANOMISES THE ARRAYS WITH CELLS OF TYPE 0,1,2

void chaos()
{
    char i=1;
    char j=1;
    for (i=1; i<SWIDTH-1; i++) {
        for (j=1; j<SHEIGHT-1; j++) {
            pixels[i][j] = rand()%3;
        }
    }
}

//MAIN FUNCTION

int main()
{
    //seed rng
    srand(time(NULL));
    currentBorder = WRAPBORDER;
    state = PLAY;
    //set up the array for sound
    initArray();
    //set up display
    lcd.normalMode();
    //set the screen brightness based on the ldr
    setBrightness();
    //zero the array
    refreshArray();
    //initialise the display
    lcd.init();
    //invert the pixels
    invert();
    //show welcome intro
    intro();
    //clear the curtain of pixels
    clear();

    // play intro leitmotif
    tone(NOTE_A4,60.0/(BPM*4));
    wait(15.0/(BPM*2));
    tone(NOTE_E5,60.0/(BPM*4));
    wait(15.0/(BPM*2));
    tone(NOTE_A5,60.0/(BPM*4));
    wait(15.0/(BPM*2));
    tone(NOTE_E6,120.0/(BPM*4));

    int i = 0;
    char gameType=0;
    char end = 0;

    // SELECT GAME MENU

    while (!end) {
        wait(0.1);
        writeMenu();
        if (pause) {
            while (pause) {
                wait(0.01);
            }
            end=1;
        }
        if (reset) {
            while (reset) {
                wait(0.01);
            }
            end=1;
            gameType=1;
        }
    }

    //main GAME OF LIFE

    if (gameType==1) {

        while(1) {

            tone(NOTE_A1,0.01);
            tick = 1;
            updateScreen();
            updateArray(currentBorder);
            i++;
            if (i%5) {
                setBrightness();
            }

            create();
            if (boundary) {
                switchBounds();
            }

            if (pause) {
                tone(NOTE_G2,60.0/(BPM*4));
                pauseGame();
            }

            if (reset && randomise) {
                main();
            }
            if (randomise) {
                tone(NOTE_G2,60.0/(BPM*4));
                randomiseScreen();
            }
            if (reset) {
                tone(NOTE_G2,60.0/(BPM*4));
                refreshArray();
            }
            tick = 0;
            waitPeriod= pot.read() * 0.5;
            wait(waitPeriod);
        }
    }

    //second GAME OF DEATH
    if (gameType==0) {

        chaos();

        while(1) {

            i++;
            secondArray();
            updateScreen2();
            wait (0.1);
            flicker();
            lcd.refresh();
            wait(0.03);
            war();
            tone(NOTE_A1,0.01);

            if (i%5) {
                setBrightness();
            }

            if (pause) {
                tone(NOTE_G2,60.0/(BPM*4));
                pauseGame();
            }

            if (reset && randomise) {
                main();
            }
            if (randomise) {
                tone(NOTE_G2,60.0/(BPM*4));
                chaos();
            }
            if (reset) {
                tone(NOTE_G2,60.0/(BPM*4));
                refreshArray();
            }
            tick = 0;
            waitPeriod= pot.read() * 0.5;
            wait(waitPeriod);
        }
    }
}
