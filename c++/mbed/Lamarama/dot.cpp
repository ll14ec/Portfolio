#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <math.h>

#define TRUE 1
#define FALSE 0

// g++ dot.cpp -lSDL2 -lSDL2_image -lm -o dot

struct Player
{
  char x;
  char y;
  float xv;
  float yv;
  char dir;
  char level;
  char image[12][8];
};

struct Bird
{
  char x;
  char y;
  float xv;
  float yv;
  char image[3][4][7];
  char frame;
};

struct Platform
{
  char x;
  char y;
  float xv;
  float yv;
  char length;
};

Player* p;
Bird* b;
Platform* pl;

char screen[48][84];
int jumpPhase;
int lastPlatform[2];

// Screen dimension constants
const int SCREEN_WIDTH = 778;
const int SCREEN_HEIGHT = 450;
SDL_Texture* t;
SDL_Texture* t0;
SDL_Surface* s;

// The window we'll be rendering to
SDL_Window* gWindow = NULL;

// The window renderer
SDL_Renderer* gRenderer = NULL;

void*
createPlatform(Platform* p)
{

  char x = 0;
  char y = 0;
  char i = 0;
  char j = 0;
  x = rand() % 40;
  x += 20;
  p->x = 0;
  p->y = 0;
  p->xv = 0;
  p->yv = 0;
  p->length = 0;

  while ((screen[i][x] != 2) && (x < 80)) {
    for (i = 0; i < 48; i++) {
      if (screen[i][x] == 2) {
        break;
      }
    }
    x++;
  }

  if (screen[i][x] != 2) {
    printf("returning ");
    return NULL;
  }

  if (i > 47) {
    return NULL;
  }

  if (screen[i][x] == 2) {
    p->y = i;

    printf("%d: y pos \n", p->y);

    while (screen[i][x] == 2) {
      p->x = x;
      x -= 1;
    }
    printf("%d: x pos \n", p->x);
    x += 1;

    while (screen[i][x] == 2) {
      p->length++;
      x++;
    }
    p->x += p->length - 1;
  }
}

void
updatePlatform(Platform* p, char flip)
{
  char length = p->length;

  if (length == 0) {
    return;
  }

  char x = p->x;
  char y = p->y;
  char i = 0;

  if (p->yv == 0) {
    p->yv = 1;
  }

  if (y > 41) {
    p->yv = -1;
  }

  if (y < 14) {
    p->yv = 1;
  }

  p->x += p->xv;
  p->y += p->yv;

  length = p->length;

  for (i = 0; i < length; i++) {
    screen[p->y][p->x - i] = 2;
  }

  i = 0;
  for (i = 0; i < length; i++) {
    screen[y][x - i] = 0;
  }
}

Bird*
createBird(Bird* b)
{
  b = (Bird*)malloc(sizeof(Bird));
  b->x = 10;
  b->y = 10;
  b->xv = 0;
  b->yv = 0;

  b->image[0][0][0] = 1;
  b->image[0][0][1] = 0;
  b->image[0][0][2] = 0;
  b->image[0][0][3] = 0;
  b->image[0][0][4] = 0;
  b->image[0][0][5] = 0;
  b->image[0][0][6] = 1;

  b->image[0][1][0] = 1;
  b->image[0][1][1] = 1;
  b->image[0][1][2] = 0;
  b->image[0][1][3] = 1;
  b->image[0][1][4] = 0;
  b->image[0][1][5] = 1;
  b->image[0][1][6] = 1;

  b->image[0][2][0] = 0;
  b->image[0][2][1] = 1;
  b->image[0][2][2] = 1;
  b->image[0][2][3] = 1;
  b->image[0][2][4] = 1;
  b->image[0][2][5] = 1;
  b->image[0][2][6] = 0;

  b->image[0][3][0] = 0;
  b->image[0][3][1] = 0;
  b->image[0][3][2] = 1;
  b->image[0][3][3] = 0;
  b->image[0][3][4] = 1;
  b->image[0][3][5] = 0;
  b->image[0][3][6] = 0;

  b->image[1][0][0] = 0;
  b->image[1][0][1] = 0;
  b->image[1][0][2] = 0;
  b->image[1][0][3] = 0;
  b->image[1][0][4] = 0;
  b->image[1][0][5] = 0;
  b->image[1][0][6] = 0;

  b->image[1][1][0] = 1;
  b->image[1][1][1] = 1;
  b->image[1][1][2] = 0;
  b->image[1][1][3] = 1;
  b->image[1][1][4] = 0;
  b->image[1][1][5] = 1;
  b->image[1][1][6] = 1;

  b->image[1][2][0] = 0;
  b->image[1][2][1] = 1;
  b->image[1][2][2] = 1;
  b->image[1][2][3] = 1;
  b->image[1][2][4] = 1;
  b->image[1][2][5] = 1;
  b->image[1][2][6] = 0;

  b->image[1][3][0] = 0;
  b->image[1][3][1] = 0;
  b->image[1][3][2] = 1;
  b->image[1][3][3] = 0;
  b->image[1][3][4] = 1;
  b->image[1][3][5] = 0;
  b->image[1][3][6] = 0;

  b->image[2][0][0] = 0;
  b->image[2][0][1] = 0;
  b->image[2][0][2] = 0;
  b->image[2][0][3] = 0;
  b->image[2][0][4] = 0;
  b->image[2][0][5] = 0;
  b->image[2][0][6] = 0;

  b->image[2][1][0] = 0;
  b->image[2][1][1] = 0;
  b->image[2][1][2] = 0;
  b->image[2][1][3] = 1;
  b->image[2][1][4] = 0;
  b->image[2][1][5] = 0;
  b->image[2][1][6] = 0;

  b->image[2][2][0] = 0;
  b->image[2][2][1] = 1;
  b->image[2][2][2] = 1;
  b->image[2][2][3] = 1;
  b->image[2][2][4] = 1;
  b->image[2][2][5] = 1;
  b->image[2][2][6] = 0;

  b->image[2][3][0] = 1;
  b->image[2][3][1] = 0;
  b->image[2][3][2] = 1;
  b->image[2][3][3] = 0;
  b->image[2][3][4] = 1;
  b->image[2][3][5] = 0;
  b->image[2][3][6] = 1;
}

Player*
createPlayer(Player* p)
{
  p = (Player*)malloc(sizeof(Player));
  p->x = 10;
  p->y = 0;
  p->xv = 1;
  p->yv = 0;
  p->dir = 'r';

  p->level = 0;

  p->image[0][0] = 0;
  p->image[0][1] = 0;
  p->image[0][2] = 0;
  p->image[0][3] = 0;
  p->image[0][4] = 0;
  p->image[0][5] = 1;
  p->image[0][6] = 0;
  p->image[0][7] = 0;

  p->image[1][0] = 0;
  p->image[1][1] = 0;
  p->image[1][2] = 0;
  p->image[1][3] = 0;
  p->image[1][4] = 1;
  p->image[1][5] = 0;
  p->image[1][6] = 1;
  p->image[1][7] = 0;

  p->image[2][0] = 0;
  p->image[2][1] = 0;
  p->image[2][2] = 0;
  p->image[2][3] = 0;
  p->image[2][4] = 1;
  p->image[2][5] = 0;
  p->image[2][6] = 0;
  p->image[2][7] = 1;

  p->image[3][0] = 0;
  p->image[3][1] = 0;
  p->image[3][2] = 0;
  p->image[3][3] = 0;
  p->image[3][4] = 1;
  p->image[3][5] = 0;
  p->image[3][6] = 1;
  p->image[3][7] = 0;

  p->image[4][0] = 0;
  p->image[4][1] = 1;
  p->image[4][2] = 1;
  p->image[4][3] = 1;
  p->image[4][4] = 1;
  p->image[4][5] = 0;
  p->image[4][6] = 1;
  p->image[4][7] = 0;

  p->image[5][0] = 1;
  p->image[5][1] = 0;
  p->image[5][2] = 0;
  p->image[5][3] = 0;
  p->image[5][4] = 0;
  p->image[5][5] = 0;
  p->image[5][6] = 1;
  p->image[5][7] = 0;

  p->image[6][0] = 1;
  p->image[6][1] = 0;
  p->image[6][2] = 0;
  p->image[6][3] = 0;
  p->image[6][4] = 0;
  p->image[6][5] = 0;
  p->image[6][6] = 1;
  p->image[6][7] = 0;

  p->image[7][0] = 1;
  p->image[7][1] = 0;
  p->image[7][2] = 0;
  p->image[7][3] = 0;
  p->image[7][4] = 0;
  p->image[7][5] = 0;
  p->image[7][6] = 1;
  p->image[7][7] = 0;

  p->image[8][0] = 1;
  p->image[8][1] = 1;
  p->image[8][2] = 1;
  p->image[8][3] = 1;
  p->image[8][4] = 1;
  p->image[8][5] = 1;
  p->image[8][6] = 1;
  p->image[8][7] = 0;

  p->image[9][0] = 1;
  p->image[9][1] = 1;
  p->image[9][2] = 1;
  p->image[9][3] = 0;
  p->image[9][4] = 0;
  p->image[9][5] = 1;
  p->image[9][6] = 1;
  p->image[9][7] = 0;

  p->image[10][0] = 0;
  p->image[10][1] = 1;
  p->image[10][2] = 1;
  p->image[10][3] = 0;
  p->image[10][4] = 0;
  p->image[10][5] = 1;
  p->image[10][6] = 1;
  p->image[10][7] = 0;

  p->image[11][0] = 0;
  p->image[11][1] = 1;
  p->image[11][2] = 0;
  p->image[11][3] = 1;
  p->image[11][4] = 0;
  p->image[11][5] = 1;
  p->image[11][6] = 0;
  p->image[11][7] = 1;

  return p;
}

void
updateBird()
{

  char i = 0;
  char j = 0;
  char frame = 0;

  if (b->frame < 10) {
    frame = 0;
  }

  else if (b->frame < 20) {
    frame = 1;
  }

  else if (b->frame < 30) {
    frame = 2;
  }

  else {
    b->frame = 0;
  }

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 7; j++) {
      screen[i + b->x][j + b->y] = b->image[frame][i][j];
    }
  }
  b->frame++;
}

void
updatePlayer()
{

  char i = 0;
  char j = 0;
  char supported = FALSE;
  char jZero = -1;
  char firstJ = 0;
  /*
          if (p->dir == 'r'){
                  for (j=0;j<100;j++){
                          for(i=8;i>=0;i--){
                                  if ((p->x +j)>36){break;}
                                  if (p->y+i < 84){
                                          if ((screen[(p->x+12+j)][p->y+i] ==
     2)){
                                          supported = TRUE;
                                          if (j<5 && firstJ ==0
     ){jZero=j;firstJ=1;}
                                          }
                                  }
                          }
                   }
          }

          else if (p->dir == 'l'){
                  for (j=0;j<100;j++){
                          for(i=0;i<8;i++){
                                  if ((p->x +j)>36){break;}
                                  if (p->y+i < 77){
                                          if ((screen[(p->x+12+j)][p->y+i] ==
     2)){
                                          supported = TRUE;
                                          if (j<5 && firstJ ==0
     ){jZero=j;firstJ=1;}
                                          }
                                  }
                          }
                   }
          }

          if (jZero>-1&&jumpPhase==0){p->xv++;}
          if (supported && p->xv!=0 && jZero>-1){p->x+=jZero;
     p->xv=0;jumpPhase=0;}
          if (!supported){p->xv++;}
          if (jumpPhase==1){p->xv=-6; jumpPhase++;}
          if (jumpPhase!=0){p->xv++;}

          */

  p->xv = 1;
  supported = FALSE;
  char connected = FALSE;

  for (j = 0; j < 8; j++) {
    for (i = 0; i < 2; i++) {
      if (screen[(p->x + 12 + i)][p->y + j] == 2) {
        supported = p->x + i + 12;
        if ((pl->y == p->x + i + 12) && (pl->x == p->y - j)) {
          p->xv = pl->yv;
          connected = 1;
          if (pl->yv > 0) {
            p->x++;
          }
          supported = 0;
          break;
        }
      }
    }
  }

  char z = 0;
  for (z = 0; z < pl->length; z++) {
    for (j = 0; j < 8; j++) {
      if ((pl->y == p->x + 12) && (pl->x - z == p->y + (8 - j))) {
        p->xv = pl->yv;
        supported = 0;
        if (pl->yv > 0) {
          p->xv--;
        }
      }
    }
  }

  if (supported) {
    p->xv = 0;
  }

  if (jumpPhase > 0) {
    if (jumpPhase < 10) {
      p->xv = -1;
      jumpPhase++;
    }

    else if (jumpPhase < 20) {
      jumpPhase++;
    } else {
      jumpPhase = 0;
    }
  }

  if ((p->x + p->xv > 0) && (p->x + p->xv < 48 - 12)) {
    p->x = p->x + p->xv;
  }
  if ((p->y + p->yv > 0) && (p->y + p->yv < 85 - 8)) {
    p->y = p->y + p->yv;
  }

  int mod = p->x + p->xv - 23;
  if (mod <= 7) {
    mod = 0;
  }

  i = 0;
  j = 0;

  if (p->dir == 'r') {
    for (i = 0; i < 12 - mod; i++) {
      for (j = 0; j < 8; j++) {
        if (screen[((p->x) + i + mod)][((p->y) + j)] == 0) {
          screen[((p->x) + i + mod)][((p->y) + j)] = p->image[i][j];
        }
      }
    }
  }

  if (p->dir == 'l') {
    for (i = 0; i < 12 - mod; i++) {
      for (j = 0; j < 8; j++) {
        if (screen[((p->x) + i + mod)][((p->y) + j)] == 0) {
          screen[((p->x) + i + mod)][((p->y) + j)] = p->image[i][7 - j];
        }
      }
    }
  }
}

void
lG()
{
  char x = 0;
  char y = 0;
  char lastcolumn = 0;
  char platform = 0;
  char gap = 0;
  char drop = 0;
  char lastrow = 0;

  lastrow = p->x + 12;

  screen[lastrow][lastcolumn] = 2;
  lastcolumn = 1;

  while (lastcolumn < 84) {

    gap = (p->level + 1) / 1.6;
    gap += rand() % 5;
    if (gap < 5) {
      gap = 5;
    }
    if (gap > 10) {
      gap = 10;
    }

    printf("%d ", gap);

    lastcolumn += gap;
    if (lastcolumn > 83) {
      lastcolumn = 83;
    }

    if (p->level < 18) {
      platform = 18 - p->level;
    } else {
      platform = 1;
    }

    drop = rand() % 5;
    drop += -(rand() % 5);
    if (drop == 1 || drop == -1) {
      drop *= 2;
    }

    // if (drop>8){drop=8;}
    // if (drop<-8){drop=-8;}

    lastrow += drop;
    if (lastrow < 19) {
      lastrow = 19;
    }
    if (lastrow > 43) {
      lastrow = 43;
    }

    // if (lastrow>25){
    //	lastrow=25;
    //}

    // if (lastrow<0){
    //		lastrow=0;
    //	}

    if (lastcolumn > 83) {
      lastrow = 83;
    }

    if (lastcolumn < 0) {
      lastcolumn = 0;
    }

    while ((platform > 0) && (lastcolumn < 84)) {
      screen[lastrow][lastcolumn] = 2;
      lastcolumn++;
      platform--;
    }
  }
}

void
removeFloor()
{
  int i = 0;
  int j = 0;

  for (i = 0; i < 48; i++) {
    for (j = 0; j < 84; j++) {
      if (screen[i][j] == 2) {
        screen[i][j] = 0;
      }
    }
  }
}

void
newLevel()
{
  if (p->y > 75) {
    p->y = 0;
    p->yv = 0;
    p->xv = -1;
    p->x = rand() % 34;
    p->level++;
    removeFloor();
    p->y = 0;
    p->yv = 0;
    lG();
  }
}

SDL_Texture*
loadTexture(const char* path, SDL_Texture* newTexture,
            SDL_Surface* loadedSurface)
{

  newTexture = NULL;
  // load image into surface
  loadedSurface = IMG_Load(path);
  if (loadedSurface == NULL) {
    printf("Image %s failed to load! \nError: %s\n", path, IMG_GetError());
  } else {
    // Colour key image
    // SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(
    // loadedSurface->format, 0, 0xFF, 0xFF ) );

    // Create texture from surface pixels
    newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
    if (newTexture == NULL) {
      printf("Texture creation from %s failed! \nError: \n %s\n", path,
             SDL_GetError());
    }
    // free the loaded surface
    SDL_FreeSurface(loadedSurface);
  }
  return newTexture;
}

int
handleEvent(SDL_Event& e)
{
  // If a key was pressed
  if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
    // Adjust the velocity
    switch (e.key.keysym.sym) {
      case SDLK_UP:
        if (jumpPhase == 0) {
          jumpPhase = 1;
        };
        break;
      case SDLK_DOWN:
        return 2;
        break;
      case SDLK_LEFT:
        p->yv = -1;
        p->dir = 'l';
        break;
      case SDLK_RIGHT:
        p->yv = 1;
        p->dir = 'r';
        break;
    }
  }
  // If a key was released
  else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
    // Adjust the velocity
    switch (e.key.keysym.sym) {
      case SDLK_UP:
        return 0;
        break;
      case SDLK_DOWN:
        return 6;
        break;
      case SDLK_LEFT:
        p->yv = 0;
        break;
      case SDLK_RIGHT:
        p->yv = 0;
        break;
    }
  }
}

bool
init()
{
  // Initialization flag
  bool success = true;

  // Initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
    success = false;
  } else {
    // Set texture filtering to linear
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
      printf("Warning: Linear texture filtering not enabled!");
    }

    // Create window
    gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                               SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (gWindow == NULL) {
      printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
      success = false;
    } else {
      // Create vsynced renderer for window
      gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED |
                                                    SDL_RENDERER_PRESENTVSYNC);
      if (gRenderer == NULL) {
        printf("Renderer could not be created! SDL Error: %s\n",
               SDL_GetError());
        success = false;
      } else {
        // Initialize renderer color
        SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

        // Initialize PNG loading
        int imgFlags = IMG_INIT_PNG;
        if (!(IMG_Init(imgFlags) & imgFlags)) {
          printf("SDL_image could not initialize! SDL_image Error: %s\n",
                 IMG_GetError());
          success = false;
        }
      }
    }
  }

  return success;
}

void
render(SDL_Rect* destRect)
{
  destRect->h = 8;
  destRect->w = 8;
  int i = 0;
  int j = 0;
  while (i < 48) {
    while (j < 84) {
      if (screen[i][j] != 0) {
        destRect->x = 11 + (9 * j);
        destRect->y = 11 + (9 * i);
        if ((t != NULL) && (destRect != NULL) && (gRenderer != NULL)) {
          SDL_RenderCopy(gRenderer, t, NULL, destRect);
        }
      }
      j++;
    }
    j = 0;
    i++;
  }
}

void
randomise()
{
  int i = 0;
  int j = 0;
  while (i < 48) {
    while (j < 84) {
      screen[i][j] = rand() % 2;
      j++;
    }
    j = 0;
    i++;
  }
}

void
clearArray()
{
  int i = 0;
  int j = 0;
  while (i < 48) {
    while (j < 84) {
      if (screen[i][j] == 1) {
        screen[i][j] = 0;
      };
      j++;
    }
    j = 0;
    i++;
  }
}

void
zeroArray()
{
  int i = 0;
  int j = 0;
  while (i < 48) {
    while (j < 84) {
      screen[i][j] = 0;
      j++;
    }
    j = 0;
    i++;
  }
}

void
close()
{
  // Destroy window
  SDL_DestroyRenderer(gRenderer);
  SDL_DestroyWindow(gWindow);
  gWindow = NULL;
  gRenderer = NULL;
  free(p);
  // Quit SDL subsystems
  IMG_Quit();
  SDL_Quit();
}

int
main(int argc, char* args[])
{
  zeroArray();
  time_t ti;
  srand((unsigned)time(&ti));
  pl = NULL;
  pl = (Platform*)malloc(sizeof(Platform));
  pl->length = 0;
  p = createPlayer(p);
  b = createBird(b);
  char flip = 0;
  jumpPhase = 0;
  clearArray();
  lG();
  // p->level=18;
  SDL_Rect* destRect = new SDL_Rect();
  // Start up SDL and create window
  if (!init()) {
    printf("Failed to initialize!\n");
  }

  t = loadTexture("sq.bmp", t0, s);
  if (t == NULL) {
    printf("texture failed to load\n");
  } else {
    {
      // Main loop flag
      bool quit = false;

      // Event handler
      SDL_Event e;

      // While application is running
      while (!quit) {
        // Handle events on queue
        while (SDL_PollEvent(&e) != 0) {
          // User requests quit
          if (e.type == SDL_QUIT) {
            quit = true;
          }
          handleEvent(e);
        }

        // printf("xv = %d", p->xv);
        // randomise();
        clearArray();
        newLevel();
        SDL_RenderClear(gRenderer);

        if (pl->length == 0) {
          createPlatform(pl);
        }
        if (flip == 0) {
          flip = 1;
        } else if (flip == 1) {
          flip = 0;
        }

        updatePlatform(pl, flip);

        updateBird();
        updatePlayer();

        render(destRect);

        SDL_RenderPresent(gRenderer);
      }
    }
  }

  // Free resources and close SDL
  close();

  return 0;
}
