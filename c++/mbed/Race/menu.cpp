/** @file */ 

#include "structs.h"

/**
 * @fn void printOptions(char * opt)
 * @ingroup Menu Functions
 * @brief Prints the text for the select controls menu.
 * @param opt The selected options for the controls.
 */

void printOptions(char * opt)
{
    lcd.clear();
    lcd.printString("Choose Controls:",0,0);
    if (opt[0] == 'A') {
        lcd.printString("*Gyro      [A]",0,1);
        lcd.printString(" Joystick   B",0,2);
    }

    else if (opt[0] == 'B') {
        lcd.printString(" Gyro       A",0,1);
        lcd.printString("*Joystick  [B]",0,2);
    }

    if (opt[1] == 'X') {
        lcd.printString("*L & R     [X]",0,3);
        lcd.printString(" X & B      Y",0,4);
    }

    else if (opt[1] == 'Y') {
        lcd.printString(" L & R      X",0,3);
        lcd.printString("*X & B     [Y]",0,4);
    }

    lcd.printString("Press Start",0,5);
    lcd.refresh();
}