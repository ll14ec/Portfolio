/** @file */ 

#include "structs.h"

char repeater  = 0;
signed char flipFlop = 1;
float curveMod = 0;
float curve = 0;
 
 /**
 * @fn void initGlobals(Game * game)
 * @ingroup Game Engine Functions
 * @brief Initialises the game state, control options and frame rate
 * @param game The game's data structure
 */
 
void initGlobals(struct Game * game){
    game->state = 0;
    game->controls[0] = 'A';
    game->controls[1] = 'X';
    game->frameRate = 1/50.0f;  
    //printf("State = %d", state); 
}

/**
 * @fn void initRoad(Road* road())
 * @ingroup Game Engine Functions
 * @brief Allocates memory to and initialises the values of the road struct.
 * @param road[] the array of road structures for the game.
 */

void initRoad(Road * road[])
{
    char i = 0;
    for (i=0; i<10; i++) {
        road[i] = (Road*)malloc(sizeof(Road));
        road[i]->y = 0;
        road[i]->x = 0;
        road[i]->z = (i);
        road[i]->item = 0;
        road[i]->item = rand () % 16;
        road[i]->circle = 'n';
    }
    //printf("Road created"); 
    return;
}

/**
 * @fn g(Camera * Camera)
 * @ingroup Game Engine Functions
 * @brief Moves the player left or right based on the curve of the road.
 * @param camera The game's camera.
 */

void g(Camera * camera)
{
    camera->x -=curve/10;
}

/**
 * @fn void updateCurve()
 * @ingroup Game Engine Functions
 * @brief Changes the curve of the road in a random, seamless way.
 */


void updateCurve()
{
    //repeater ensures consistent curving direction for at least 30 frames.
    repeater +=1;                       
    if (repeater >30) {
        repeater = 0;
        //after 30 frames, provide the option to change curve direction via rand()
        if ((rand() % 2) == 0) {        
            flipFlop = 1;
        } else {
            flipFlop = -1;
        }
    }
    curveMod = rand() % 10;
    curveMod /= 200;        
    curveMod*=flipFlop;  
    //if the new curve is less than +- 1.5 then allow it               
    if ((curve+curveMod < 1.5f) && (curve+curveMod > -1.5f)) {
        curve+=curveMod;
    }
    //printf("CurveMod = %f", curvemod); 
}

/**
 * @fn Camera * initCamera(Camera * camera)
 * @ingroup Game Engine Functions
 * @brief Allocates memory to and initialises the values of the camera struct.
 * @param road[] the array of road structures for the game.
 */


Camera* initCamera(Camera * camera)
{
    camera = (Camera*)malloc(sizeof(Camera));
    camera->x = camera->y = camera->z = 0;
    //set position to be slightly above the road/player
    camera->y=-10;                              
    return camera;
}

/**
 * @fn Road * findLastSegment(Road* road())
 * @ingroup Game Engine Functions
 * @brief Returns the road segment in the first position
 * @param road[] the array of road structures for the game.
 */

Road * findLastSegment(Road * road[])
{

    //first get the highest z value
    char i = 0;
    char index = 0;
    float farthest = 0;
    for (i=0; i<10; i++) {
        if (road[i]->z > farthest) {
            farthest = road[i]->z;
        }
    }
    i = 0;
    //then return the segment with that value
    for (i=0; i<10; i++) {
        if (road[i]->z == farthest) {
            index = i;
        }
    }
    return road[index];
}

/**
 * @fn void updateRoad(Camera * camera, Road * road[], char screen[][84])
 * @ingroup Game Engine Functions
 * @brief Updates the positions of the road structures as they go out of the scope of the camera.
 * @param camera The game's camera
 * @param road[] The array of road structures for the game.
 * @param screen The pixel array written to the screen.
 */ 

void updateRoad(Camera * camera, Road * road[], char screen[][84])
{
    char i = 0;
    for (i=0; i<10; i++) {
        //printf("Updating road %d", i); 
        //if the road segment is behind the field of view of the camera
        if (road[9-i]->z < camera->z) {
            // reposition the segment as 1 further than the furthest current segment
            road[9-i]->z = findLastSegment(road)->z + 1;
            //randomly add in hills/dips
            if (rand() % 2) {
                road[9-i]->y = findLastSegment(road)->y + 0.1f;
                road[9-i]->item = rand()% 16;
            } else {
                road[9-i]->y = findLastSegment(road)->y - 0.1f;
                road[9-i]->item = rand()% 16;
            }
            //spawn circles randomly on newly generated segments
            if (rand() % 100 < 97) {
                road[9-i]->circle = 'n';
            } else if (rand() % 2) {
                road[9-i]->circle = 'l';
            } else {
                road[9-i]->circle = 'r';
            }
        }
    }
}

/**
 * @fn void drawCircle(float r, char y, char x)
 * @ingroup Game Engine Functions
 * @brief Draws circles on the screen, and increase the hp of the player if they intersect.
 * @param r The radius of the circle
 * @param y The Y Coordinate
 * @param x The X Coordinate
 */

void drawCircle(float r, char y, char x)
{
    //printf("Drawing circle at %d[x] %d[y]", x, y); 
    int x0 = int(rint(x - r));
    int y0 = int(rint(y - r));
    lcd.drawCircle(x0,y0,int(rint(r)),0);
    if ((y>40) && ((x>43)&&(x<53))) {
        //if the circle is being drawn over the player (player has gone through it)
        increaseHp();
        pad->tone(500,0.01);
    }
}

/**
 * void drawRoadItem(Road * r, float dZ, char y, char x, char screen[][84])
 * @ingroup Game Engine Functions
 * @brief Calls the drawCircle function on the appropriate road segment.
 * @param r A segment of the road.
 * @param dZ The distance on the Z axis from the camera to the road segment.
 * @param y The Y coordinate of the road segment.
 * @param x The X Coordimate of the road segment.
 * @param screen The pixel array written to the screen.
 */

void drawRoadItem(Road * r, float dZ, char y, char x, char screen[][84])
{
    float spread = 17.0f/dZ;
    float size = 8.0f/dZ;
    switch (r->circle) {
        case 'l': {          //empty circle left
            drawCircle(size, y, (x-char(rint(spread))));
            break;
        }

        case 'r': {          //empty circle left
            drawCircle(size, y, (x+char(rint(spread))));
            break;
        }

    }
}

/**
 * void drawBoard(float size, char y, char x)
 * @ingroup Game Engine Functions
 * @brief Draws a bilboard beside the road.
 * @param size The size of the board.
 * @param y The Y coordinate.
 * @param x The X Coordimate.
 */

void drawBoard(float size, char y, char x)
{
    float width = 2.0f*size;
    float length = size;
    //offset calculated xy from centre of rectangle to the corner to draw from
    int x0 = int(rint(x - (width/2.0f)));
    int y0 = int(rint(y - (length/2.0f)));
    lcd.drawRect(x0,y0,int(rint(width)),int(rint(length)),0);
}

/**
 * void drawTree(float size, char y, char x, char screen[][84])
 * @ingroup Game Engine Functions
 * @brief Draws a fractal tree beside the road.
 * @param size The size of the board.
 * @param y The Y coordinate.
 * @param x The X Coordimate.
 * @param screen The pixel array written to the screen.
 */

void drawTree(float size, char y, char x, char screen[][84])
{
    char h = int(rint(size*1.7f));
    char i = 0;
    //the height is set from the size
    for (i=0; i<h; i++) {
        lcd.setPixel(x,y+i);
        //every third pixel down, render a branch of increasing width
        if (!((y+i)%3)) {
            lcd.setPixel(x-i/2,y+i);
            lcd.setPixel(x+i/2,y+i);
        }
    }
}

/**
 * void drawOption(char one, char two, float * spread, float * size, float * heightOffset, char  screen[][84], char y, char x)
 * @ingroup Game Engine Functions
 * @brief Calls drawBoard and drawTree based on the one and two variables.
 * @param one The first option
 * @param two The second option.
 * @param spread The distance from the midpoint of the road segment.
 * @param size The size of the item to draw.
 * @param heightOffset The y-axis offset.
 * @param x The X Coordimate.
 * @param y The Y Coordimate.
 * @param screen The pixel array written to the screen.
 */

void drawOption(char one, char two, float * spread, float * size, float * heightOffset, char  screen[][84], char y, char x)
{
    //board on the right
    if (one == 0) {
        drawBoard(*size, y, (x+char(rint(*spread))));
    }
    //tree on the right
    else if (one == 1) {
        drawTree(*size, y-char(rint(*heightOffset)), (x+char(rint(*spread))), screen);
    }
    //board on the left 
    if (two == 0) {
        drawBoard(*size, y, (x-char(rint(*spread))));
    } 
    //tree on the left
    else if (two == 1) {
        drawTree(*size, y-char(rint(*heightOffset)), (x-char(rint(*spread))), screen);
    }
}

/**
 * void drawItems(Road * r, float dZ, char y, char x, char screen[][84])
 * @ingroup Game Engine Functions
 * @brief Calls the drawItem function with the appropriate values.
 * @param r The road segment of the item to draw.
 * @param dZ The distance from the camera to the road semgment on the z axis.
 * @param x The X Coordimate.
 * @param y The Y Coordimate.
 * @param screen The pixel array written to the screen.
 */

void drawItems(Road * r, float dZ, char y, char x, char screen[][84])
{
    //size, spread and heightOffset depend on the z distance from the camera
    float spread = 70.0f/dZ;
    float size = 20.0f/dZ;
    float heightOffset = 30.0f/dZ;
    switch (r->item) {
        //two boards
        case 0b0101: {          
            drawOption(0,0, &spread, &size, &heightOffset, screen, y, x);
            break;
        }
        //two trees
        case 0b1010: {          
            drawOption(1,1, &spread, &size, &heightOffset, screen, y, x);
            break;
        }
        //board-tree  
        case 0b0110: {         
            drawOption(0,1, &spread, &size, &heightOffset, screen, y, x);
            break;
        }
        //tree-board
        case 0b1001: {          
            drawOption(1,0, &spread, &size, &heightOffset, screen, y, x);
            break;
        }
    }
}

/**
 * void dCamera(Road* road, Camera* camera, float * dCameraX, float * dCameraY, float * dCameraZ)
 * @ingroup Game Engine Functions
 * @brief Calculates the distance from a road segment to the camera.
 * @param road The road segment.
 * @param camera The game's camera.
 * @param dCameraX The difference in the X axis.
 * @param dCameraY The difference in the Y axis.
 * @param dCameraZ The difference in the Z axis.
 */

void dCamera(Road* road, Camera* camera, float * dCameraX, float * dCameraY, float * dCameraZ)
{
    *dCameraX = road->x - camera->x;
    *dCameraY = road->y - camera->y;
    *dCameraZ = road->z - camera->z;
    //printf("dcY: %f", dCameraY);
    //printf("    dcX: %f\n", dCameraX);
}

/**
 * void project(float * view, float * projectX, float * projectY, float * dCameraX, float* dCameraY, float * dCameraZ, signed char * screenX, signed char* screenY)
 * @ingroup Game Engine Functions
 * @brief Projects a segment of road from the XYZ system onto the 2d screen.
 * @param view Scaling variable.
 * @param projectX The projected X Coordinate.
 * @param projectY The projected Y Coordinate.
 * @param screenX The scaled X Coordinate from projectX.
 * @param screenY The scaled Y Coordinate from projectY.
 * @param dCameraX The difference in the X axis.
 * @param dCameraY The difference in the Y axis.
 * @param dCameraZ The difference in the Z axis.
 */

void project(float * view, float * projectX, float * projectY, float * dCameraX, float* dCameraY, float * dCameraZ, signed char * screenX, signed char* screenY)
{
    //project the coordinates onto the scaled plane
    *projectX = *dCameraX /( *dCameraZ * *view);
    *projectY = *dCameraY / (*dCameraZ * *view);
    //position the coordinates on the screen
    *screenX = rint(*projectX) + 42;
    *screenY = rint(*projectY) + 10;
    //printf("screeny: %f", screenY);
    //printf("     screenx: %f\n", screenX);
    //printf("projecty: %f", projectY);
    //printf("     projectX: %f\n", projectX);
}

/**
 * void draw(Road * road[], Camera * camera, char screen[][84])
 * @ingroup Game Engine Functions
 * @brief Draws the road and other items on it.
 * @param road[] The array of road segments.
 * @param camrera The game's camera.
 * @param screen The pixel array written to the screen.
 */

void draw(Road * road[], Camera * camera, char screen[][84])
{
    char i = 0;
    float dCameraX, dCameraY, dCameraZ, projectX, projectY, width;
    signed char screenX, screenY;
    //view = perpective - bigger is more zoomed in on the road etc.
    float view = 0.25f;
    for (i=0; i<10; i++) {
        //calculate the Z distance from the road segment to the camera
        dCamera(road[i], camera, &dCameraX, &dCameraY, &dCameraZ);
        //if the road segment is in the field of view
        if (dCameraZ > 0) {
            //calculate the width of the segment
            width = 40/dCameraZ;
            //project the segment onto the screen
            project(&view, &projectX, &projectY, &dCameraX, &dCameraY, &dCameraZ, &screenX, &screenY);
            //do not render under the top 15 pixels (reserved for the background)
            if (screenY <15) {
                continue;
            }
            //if the projected XY coordinates (of the centre of the road) are within the screen boundaries
            if ((screenX>-1) && (screenX<84) && (screenY>-1) && (screenY<48)) {
                //render the centre line of the road (adding any curving)
                screen[screenY][screenX + char(rint(dCameraZ * curve))] = 1;
                //if the projected XY coordinates (of the edge of the road) are within the screen boundaries
                if ((screenX + char(rint(width)) < 84) && (screenX - rint(width) > -1)) {
                    //render the left and right sides of the road
                    screen[screenY][screenX + char(rint(width))+ char(rint(dCameraZ * curve))] = 1;
                    screen[screenY][screenX - char(rint(width))+ char(rint(dCameraZ * curve))] = 1;
                    //draw bilboards and tree beside the segment
                    drawItems(road[i], dCameraZ, screenY, (screenX + char(rint(dCameraZ * curve))), screen);
                    //draw powerups on the road segment
                    drawRoadItem(road[i], dCameraZ, screenY, (screenX + char(rint(dCameraZ * curve))), screen);
                }
            }
        }
    }
}