/** @file */ 

#include "structs.h"
char made = 0;

/**
 * @fn void makeDirectory()
 * @ingroup Master Functions
 * @brief Makes the directory for the highscores
 */


void makeDirectory(){
    mkdir("/sd/mydir", 0777);
    made = 1;
    
}

/**
 * @fn char * update(int score)
 * @ingroup SD Card
 * @brief Takes the player's score, saves onto the highscores if it is a high score. Returns the higher of the highscore or the new score as a string
 * @param score The player's score
 */

char * update(int score)
{
    if (!made){
        makeDirectory();
    }
    char str[100];
    char buf[100];
    //makes the directory
    mkdir("/sd/mydir", 0777);
    //opens a file on the SD card in read mode
    FILE * fp = fopen("/sd/mydir/sd.txt", "r");
    //gets the high score
    fgets(str, 100, fp);
    //convert to int
    int x = atoi(str);
    //close file
    fclose(fp);
    // if new high score
    if (x<score) {
        //open file in write mode
        fp = fopen("/sd/mydir/sd.txt", "w");
        //if file failed to open
        if(fp == NULL) {
            printf("Could not open file for write.");
            return "null";
        }
        //convert the score to a string
        sprintf(buf, "%d", score);
        //add the score to the file
        fprintf(fp, buf);
        //return the new highscore as a string
        fclose(fp);
        return buf;
    }
    else{       
        //return the old highscore as a string
        return str;
    }
}
