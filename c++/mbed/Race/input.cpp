/** @file */ 

#include "structs.h"

/**
 * @fn void getInput(Polar polar, Camera * camera)
 * @ingroup Input Functions
 * @brief Gets the input from the joystick and updates the camera position accordingly.
 * @param polar The direction of the joystick
 * @param camera The game's camera.
 */

void getInput(Polar polar, Camera * camera)
{
    //if the stick has been moved an appreciable amount, update the camera's position
    if (polar.mag>0.1f) {
        if (polar.angle < 180) {
            camera->x += ((polar.angle )/ 500.0f);
        } else if (polar.angle>180) {
            camera->x -= ((polar.angle )/ 500.0f);
        }
    } 
}

/**
 * @fn void turn(float acceleration, Camera * camera)
 * @ingroup Input Functions
 * @brief Updates the camera's position based on the accelerometer data.
 * @param acceleration The data from the accelerometer.
 * @param camera The game's camera.
 */
 
void turn(float acceleration, Camera* camera)
{
    float mag = acceleration * acceleration;
    //if the accelerometer has been moved an appreciable amount, update the camera's position
    if (mag>0.1f) {
        camera->x -= (acceleration);
    }
}

