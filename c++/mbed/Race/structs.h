/** @file */ 

#include "mbed.h"
#include "N5110.h"
#include "FXOS8700CQ.h"
#include "Gamepad/Gamepad.h"
#include "SDFileSystem.h"

 /**
  * A structure to represent segments of the road
  */

struct Road {
    /*@{*/
    float z;        /**< the z coordinate - forward /backward */
    float x;        /**< the x coordinate - left /right */
    float y;        /**< the y coordinate - up / down */
    char item;      /**< the item(s) beside the road - 0000 -> nothing | 1000 -> tree on left | 0010 -> tree on right | 0100 -> board on left | 0001 -> board on right */
    char circle;    /**< whether there is a circle on the road - 'l','r','n' */
    /*@}*/
};

 /**
  * A structure to represent the game's camera
  */

struct Camera {
    /*@{*/
    float z;        /**< the z coordinate - forward /backward */
    float x;        /**< the x coordinate - left /right */
    float y;        /**< the y coordinate - up / down */
    /*@}*/
};

 /**
  * A structure to hold the game's data
  */

struct Game {
    /*@{*/
    Camera * camera;        /**< the game camera */
    Road * road[10];        /**< the game's road */
    char screen[48][84];    /**< the array to be displayed on the lcd */
    char state;             /**< the y coordinate - up / down */
    char controls[3];       /**< joystick/accelerometer + LR/XB */
    float frameRate;        /**< the speed of the frames */
    int score ;             /**< the player's score */
    /*@}*/
};

//screen, device, and game controller, memory card

extern N5110::N5110 lcd;
extern FXOS8700CQ device;
extern Gamepad * pad;
extern SDFileSystem sd; // the pinout on the mbed Cool Components workshop board

//function prototypes for functions called in modules other than their own

void g(Camera *);
void updateCurve();
Camera * initCamera(Camera * camera);
void initGlobals(Game * game);
void initRoad(Road * road[]);
Road * findFarthest(Road * road[]);
Road * findLastSegment(Road * road[]);
void updateRoad(Camera * camera, Road * road[], char screen[][84]);
void draw(Road * road[], Camera * camera, char screen[][84]);
void enemies();
void player(char screen[][84], char * opt);
bool dead(Camera* camera, Road* road);
void render(char screen[][84]);
void renderMenuImage(char screen[][84]);
void turn(float acceleration, Camera* camera);
void getInput(Polar polar, Camera * camera);
void increaseHp();
void resetHealth();
void printOptions(char * opt); 
void sleepWait(float wait);
char * update(int score);
