/** @file */ 

#include "mbed.h"

Ticker ticker;
volatile int g_timer_flag = 0;

/** 
 *  @fn void timer_isr()
 *  @ingroup Timing Functions
 *  @brief Sets the timing flag
 */
 
void timer_isr()
{
    g_timer_flag = 1;   // set flag in ISR
}


/** 
 *  @fn void sleepWait(float wait)
 *  @ingroup Timing Functions
 *  @brief Sleeps untill the timing flag changes
 */

void sleepWait(float wait){
    ticker.attach(&timer_isr, wait);
    sleep();
    if (g_timer_flag){
            g_timer_flag = 0;
        }
}