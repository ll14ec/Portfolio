/**
 * @Author Edward Cottle
 * @date   May 2017
 * @brief  Pseudo 3d racer.
 * @file
 *
 * A pseudo 3d simple pixel racing game.
 * The game can be controlled by either the joystick or the accelerometer.
 * Shooting can be controlled by R and L or X and B.
 * Bad guys will spawn beside you and try to shoot you.
 * The game will end when the health bar on the left is at 0 or if the player deviates too far from the road.
 * The player can go through the circles that spawn to gain health back.
 */

#include "structs.h"
#include <stdio.h>
#include <string>
#include <math.h>

//the controller
Gamepad * pad;
//structure used to store accelerometer data
Data values;
//the device
FXOS8700CQ device(I2C_SDA,I2C_SCL);
//the screen
N5110::N5110 lcd(PTC9,PTC0,PTC7,PTD2,PTD1,PTC11);
//the memory card
SDFileSystem sd(PTE3, PTE1, PTE2, PTE4, "sd"); // the pinout on the mbed Cool Components workshop board
//the game's data
struct Game game;

char buffer[100];

void startScreen();
void displayHelp();
void options();
void zoom();
void gameLoop();
void hsMenu();

/**
 * @fn int main()
 * @ingroup Master Functions
 * @brief The main game function that calls menus and the gameloop.
 */

int main()
{
    while (1) {
        switch (game.state) {
            case 0 :
                startScreen();
                break;

            case 1 :
                displayHelp();

            case 2 :
                options();
                break;
                
            case 3:
                zoom();
                break;

            case 4 :
                gameLoop();
                break;

            case 5 :
                hsMenu();
                break;
        }
    }
}

/**
 * @fn void clearArray()
 * @ingroup Master Functions
 * @brief Clears the screen pixel array
 */

void clearArray()
{
    int i = 0;
    int j = 0;
    while (i < 48) {
        while (j < 84) {
            if (game.screen[i][j] == 1) {
                game.screen[i][j] = 0;
            };
            j++;
        }
        j = 0;
        i++;
    }
}

/**
 * @fn void init()
 * @ingroup Master Functions
 * @brief Initialises all game data.
 */

void init()
{
    device.init();
    pad = new Gamepad();
    pad->init();
    lcd.init();
    lcd.normalMode();
    lcd.setBrightness(0.5);
    time_t ti;
    srand((unsigned)time(&ti));
    clearArray();
    initGlobals(&game);
    initRoad(game.road);
    game.camera = initCamera(game.camera);
    resetHealth();
}

/**
 * @fn void startScreen()
 * @ingroup Master Functions
 * @brief Displays the first menu with the game's title.
 */

void startScreen()
{
    init();
    pad->tone(1000, 0.1);
    lcd.printString("THE GHETTAWAY",0,0);
    lcd.printString(" Press A",0,4);
    lcd.printString(" To Begin...",0,5);
    renderMenuImage(game.screen);
    lcd.refresh();
    while(!pad->check_event(pad->A_PRESSED)) {
        lcd.setBrightness(pad->read_pot());
        sleepWait(0.03);
    }
    game.state = 1;
    sleepWait(0.1);
}

/**
 * @fn void displayHelp()
 * @ingroup Master Functions
 * @brief Displays the game instructions menu.
 */

void displayHelp()
{
    pad->tone(1000, 0.1);
    lcd.clear();
    lcd.printString("Stay on road,",0,0);
    lcd.printString("Loops regen",0,1);
    lcd.printString("health bar.",0,2);
    lcd.printString("Press Y...",0,5);
    lcd.refresh();
    while(!pad->check_event(pad->Y_PRESSED)) {
        sleepWait(0.03);
        lcd.setBrightness(pad->read_pot());
    }
    game.state = 2;
}

/**
 * @fn void options()
 * @ingroup Master Functions
 * @brief Displays the control options menu.
 */

void options()
{
    pad->tone(1000, 0.1);
    pad->init();
    sleepWait(0.1);
    //show menu untill start is selected
    while(!pad->check_event(pad->START_PRESSED)) {
        //change options if selected
        if (pad->check_event(pad->A_PRESSED)) {
            game.controls[0] = 'A';
        }
        if (pad->check_event(pad->B_PRESSED)) {
            game.controls[0] = 'B';
        }
        if (pad->check_event(pad->X_PRESSED)) {
            game.controls[1] = 'X';
        }
        if (pad->check_event(pad->Y_PRESSED)) {
            game.controls[1] = 'Y';
        }
        //show selected options on screen
        printOptions(game.controls);
        sleepWait(0.03);
        lcd.setBrightness(pad->read_pot());
    }
    game.state = 3;
    game.score = 0;
}

/**
 * @fn void zoom()
 * @ingroup Master Functions
 * @brief Zooms into the generated world
 */

void zoom(){
    int y = 0;
    //zoom into start position
    for (y=0; y<30; y++) {
        clearArray();
        lcd.clear();
        game.camera->y-=(y/10.0f);
        game.camera->z-=(y/100.0f);
        sleepWait(0.01);
    }
    y=0;
        
    //zoom into game position
    for (y=0; y<30; y++) {
        lcd.clear();
        clearArray();
        game.camera->y+=(y/10.0f);
        game.camera->z+=(y/100.0f);
        updateRoad(game.camera, game.road, game.screen);
        draw(game.road, game.camera, game.screen);
        render(game.screen);
        lcd.refresh();
        sleepWait(0.1);
    }   
    game.state = 4;
}

/**
 * @fn void gameLoop()
 * @ingroup Master Functions
 * @brief The main game loop.
 */

void gameLoop()
{
    game.score +=1;
    //clear the screen
    lcd.clear();
    lcd.refresh();
    //get input from joystick or accelerometer
    if (game.controls[0] == 'B') {
        getInput(pad->get_polar(), game.camera);
    } else {
        values = device.get_values();
        turn(values.ay, game.camera);
    }
    //incrment the z value (so that the camera is moving forwards)
    game.camera->z+=0.1f;
    //add new segments for those behind the camera
    updateRoad(game.camera, game.road, game.screen);
    //clear the previous screen[][] values
    clearArray();
    //uupdate the road's curving
    updateCurve();
    //apply the g force to the player going round the curve
    g(game.camera);
    //handle enemmies
    enemies();
    //handle the player
    player(game.screen, game.controls);
    //draw the road and other 3d rendered items
    draw(game.road, game.camera, game.screen);
    //render the pixel array
    render(game.screen);
    lcd.refresh();
    sleepWait(game.frameRate);
    if (dead(game.camera, game.road[9])) {
        game.state = 5;
    }    
}

/**
 * @fn void hsMenu()
 * @ingroup Master Functions
 * @brief Displays the highscore menu
 */


void hsMenu()
{
    pad->tone(1000, 0.1);
    sleepWait(0.1);
    pad->tone(1500, 0.1);
    sleepWait(0.1);
    pad->tone(2000, 0.1);
    lcd.clear();
    sprintf(buffer, "%d", game.score);
    lcd.printString("SCORE: ",0,0);
    lcd.printString(buffer,0,1);
    lcd.printString("HI SCORE: ",0,3);
    lcd.printString(update(game.score),0,4);
    lcd.printString("Press BACK... ",0,5);
    lcd.refresh();
    while(!pad->check_event(pad->BACK_PRESSED)) {
        wait(0.1);
    }
    game.state = 0;
}