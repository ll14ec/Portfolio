/** @file */ 

#include "structs.h"
#define SCREEN_WIDTH 84
#define SCREEN_HEIGHT 48
extern char background[15][84];

/**
 * @fn void renderMenuImage(char screen[][84])
 * @ingroup Rendering Functions
 * @brief Renders the mountainous background on the home menu.
 * @param screen The array to be written the screen.
 */

void renderMenuImage(char screen[][84]){
    char i = 0;
    char j = 0;
    for (i=0; i<15; i++) {
        for(j=0; j<84; j++) {
            if (background[i][j]) {
                lcd.setPixel(j,i+10);
            }
        }
    }
}

/**
 * @fn void renderBackground(char screen[][84])
 * @ingroup Rendering Functions
 * @brief Renders the mountainous background.
 * @param screen The array to be written the screen.
 */

void renderBackground(char screen[][84])
{
    char i = 0;
    char j = 0;
    for (i=0; i<15; i++) {
        for(j=0; j<84; j++) {
            if (background[i][j]) {
                screen[i][j] = background[i][j];
            }
            if (i==14) {
                screen[i][j] = 1;
            }
        }
    }
}

/**
 * @fn void render(char screen[][84])
 * @ingroup Rendering Functions
 * @brief Renders the screen array to the lcd.
 * @param screen The array to be written the screen.
 */

void render(char screen [][84])
{
    renderBackground(screen);
    char i = 0;
    char j = 0;
    for (i=0; i<SCREEN_HEIGHT; i++) {
        for (j=0; j<SCREEN_WIDTH; j++) {
            if (screen[i][j]!=0) {
                lcd.setPixel(j,i);
            }
        }
    }
}
