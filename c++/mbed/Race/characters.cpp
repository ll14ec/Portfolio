/** @file */ 

#include "structs.h"
extern char playerImage[6][9];
extern char blast[5][3];
char enemy=0;
char shotsRight=0;
char shotsLeft=0;
char health;

/** 
 *  @fn void resetHealth()
 *  @ingroup Character Functions
 *  @brief Resets the health of the player
 */
 
void resetHealth()
{
    health = 100;
}

/**
 * @fn void badGuy(char c, char screen[][84])
 * @ingroup Character Functions
 * @brief Renders an enemy to the screen
 * @param c Enemy on the left, right or both.
 * @param screen The pixel array written to the screen.
 */
void badGuy(char c, char screen[][84])
{
    //printf("Generating enemies: %d", c); 
    char i = 0;
    char j = 0;
    //no enemy
    if (c==0) {
        return;
    }
    //enemy on left
    if (c == 1) {
        for (i=0; i<6; i++) {
            for (j=0; j<9; j++) {
                if (playerImage[i][j] != 0) {
                    screen[j+36][i+20] = 1;
                }
            }
        }
    }
    //enemy on right
    else if (c == 2) {
        for (i=0; i<6; i++) {
            for (j=0; j<9; j++) {
                if (playerImage[i][j] != 0) {
                    screen[j+36][i+60] = 1;
                }
            }
        }
    }
    //enemies on left and right
    else if (c == 3) {
        for (i=0; i<6; i++) {
            for (j=0; j<9; j++) {
                if (playerImage[i][j] != 0) {
                    screen[j+36][i+20] = 1;
                }
            }
        }
        i=0;
        j=0;
        for (i=0; i<6; i++) {
            for (j=0; j<9; j++) {
                if (playerImage[i][j] != 0) {
                    screen[j+36][i+60] = 1;
                }
            }
        }
    }
}

/**
 * @fn void renderShot(char x, char y, char dir, char screen[][84])
 * @ingroup Character Functions
 * @brief Renders ashot to the screen
 * @param x X co-ordinate
 * @param y Y co-ordinate
 * @param dir The direction of the shot
 * @param screen The pixel array written to the screen.
 */

void renderShot(char x, char y, char dir, char screen[][84])
{
    char i = 0;
    char j = 0;
    //one direction
    if (dir==0) {
        for (i=0; i<5; i++) {
            for (j=0; j<3; j++) {
                if (blast[i][j]) {
                    screen[i+x][j+y] = 1;
                }
            }
        }
    } 
    //other direction
    else {
        for (i=0; i<5; i++) {
            for (j=0; j<3; j++) {
                if (blast[i][j]) {
                    screen[x+i][y-j] = 1;
                }
            }
        }
    }
}

/**
 * @fn void enemyFire(char screen[][84])
 * @ingroup Character Functions
 * @brief Shoots from an enemy at random times.
 * @param screen The pixel array written to the screen.
 */

void enemyFire(char screen[][84])
{
    //no enemies
    if (enemy == 0) {
        return;
    }

    //shoot randomly from generated enemies
    int probability = rand() % 100;
    
    //printf("Probability: %d", probability); 

    if (probability > 80) {
        if (enemy == 1) {
            renderShot(37,26,0,screen);
            pad->tone(2000,0.01);
            if (health>0)health-=1;
        }
        if (enemy == 2) {
            renderShot(37,59,1, screen);
            pad->tone(2000,0.01);
            if (health>0)health-=1;
        }
        if (enemy == 3) {
            renderShot(37,59,1, screen);
            pad->tone(2000,0.01);
            if (health>0)health-=1;
        }
    } else if (probability<20) {
        if (enemy == 3) {
            renderShot(37,26,0, screen);
            pad->tone(2000,0.01);
            if (health>0)health-=1;
        }
    }
}

/**
 * @fn void enemies()
 * @ingroup Character Functions
 * @brief Spawns/Despawns enemies from the screen.
 */

void enemies()
{
    //remove enemies that have been shot
    if ((enemy == 1) && (shotsLeft>2)) {
        enemy = 0;
    }
    if ((enemy == 2) && (shotsRight>2)) {
        enemy = 0;
    }
    if ((enemy == 3) && (shotsLeft>2)) {
        enemy = 2;
    }
    if ((enemy == 3) && (shotsRight>2)) {
        enemy = 1;
    }
    if (enemy != 0) {
        return;
    }
    //if no enemies, spawn them at random
    int probability = rand() % 1000;
    if (probability>998) {
        enemy = 3;
        shotsRight = 0;
        shotsLeft = 0;
    } else if (probability > 990) {
        if (probability % 2) {
            enemy = 2;
            shotsRight  = 0;
            shotsLeft = 0;
        } else {
            enemy = 1;
            shotsRight  = 0;
            shotsLeft = 0;
        }
    }
}

/**
 * @fn void hpBar()
 * @ingroup Character Functions
 * @brief Renders the Hp Bar to the screen.
 */

void hpBar()
{
    int h = health/3.5f;
    int y = 44-h;
    int w = 3;
    if (h<2)h=1;
    lcd.drawRect(2,y,w,h,0);
}

/**
 * @fn void enemyFire(char screen[][84], char * opt)
 * @ingroup Character Functions
 * @brief Shoots if the player pressed the button.
 * @param screen The pixel array written to the screen.
 * @param opt The option of input the user has selected.
 */

void shoot(char screen[][84], char * opt)
{
    //if player is using R - L configuration
    if (opt[1] == 'X') {
        if (pad->check_event(pad->R_PRESSED)) {
            renderShot(37,46,0, screen);
            shotsRight++;
            pad->tone(1000,0.01);
        }
        if (pad->check_event(pad->L_PRESSED)) {
            renderShot(37,39,1, screen);
            shotsLeft++;
            pad->tone(1000,0.01);
        }
    } 
    //if player is using B - X confguration
    else if (opt[1] == 'Y') {
        if (pad->check_event(pad->B_PRESSED)) {
            renderShot(37,46,0, screen);
            shotsRight++;
            pad->tone(1000,0.01);
        }
        if (pad->check_event(pad->X_PRESSED)) {
            renderShot(37,39,1, screen);
            shotsLeft++;
            pad->tone(1000,0.01);
        }
    }
}

/**
 * @fn void player(char screen[][84], char * opt)
 * @ingroup Character Functions
 * @brief Controls the players actions
 * @param screen The pixel array written to the screen.
 * @param opt The option of input the user has selected
 */

void player(char screen[][84], char * opt)
{
    //handle enemies, hp bar and shots
    badGuy(enemy, screen);
    enemyFire(screen);
    hpBar();
    shoot(screen, opt);
    //render the player image
    char i = 0;
    char j = 0;
    for (i=0; i<6; i++) {
        for (j=0; j<9; j++) {
            if (playerImage[i][j] != 0) {
                screen[j+36][i+40] = 1;
            }
        }
    }
}


/**
 * @fn void increaseHp()
 * @ingroup Character Functions
 * @brief Increases the hp of the player.
 */
 
void increaseHp()
{
    //printf("Health Increased"); 
    health+=25;
    if (health>100) {
        health = 100;
    }
}

/**
 * @fn void dead(Camera * camera, Road * road)
 * @ingroup Character Functions
 * @brief Camera The game's camera.
 */

bool dead(Camera* camera, Road* road)
{
    //if the player is too far off the road
    if (camera->x > (road->x + 15)) {
        return true;
    }

    if (camera->x < (road->x - 15)) {
        return true;
    }
    //if the palyer has no more health remaining
    if (health < 1) {
        return true;
    }
    return false;
}