#include <thread>
#include <atomic>
#include <queue>
#include <mutex>
#include <iostream>
#include <chrono>
typedef int chessboard;

// Popcount - returns the amount of 1s bits in x
#ifdef __GNUC__
int popCount(int &x, const int &maxN){
  return __builtin_popcount(x);
}
#else
int popCount(int &x, const int &maxN){
  return std::bitset<maxN>(x);
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////
//						
// --- Thread Holder Class ---
//
// Contains an array of threads, and their active status.
// 
// [ Thread saturation = true ] if all threads are active.
// [ Threads done = number of threads that have finished execution ] 
// [ Threads created = number of threads that have been started ] 
//
////////////////////////////////////////////////////////////////////////////////////////////////

class threadHolder{

  private:
    std::thread *threads;
    int *active;
    int length;
    std::atomic<bool> threadSaturation;
    std::atomic<int> threadsDone;
    int threadsCreated;
    std::mutex m;

  public:

    int sols = 0;

    // Constructor
    threadHolder(int size) : length(size)
    {
      // Zero initial variables
      threadsCreated = threadSaturation = threadsDone = 0;
      // Declare thread array / active status array of the right size
      threads = new std::thread[size];
      active = new int[size];
      // Set all threads to inactive
      for (int i = 0; i<length; i++){
        active[i] = 0;
      }
    }

    // Destructor - RAII - Handle threads going out of scope without being joined
    ~threadHolder()
    {
      joinAll();
      delete active;
    }

    // Join All -> Joins all joinable threads
    void joinAll()
    {
      for (int i = 0; i<length; i++){
        if (threads[i].joinable())
          threads[i].join();
      }
    }

    // Get Inactive Thread -> Returns the index of the first inactive thread, or -1 if all active (thread safe)
    int getInactiveThread()
    {
      if (length == 0){
        return -1;
      }
      std::lock_guard<std::mutex> lock(m);
      for (int i = 0; i<length; i++){
        if (!active[i]){
           active[i] = 1;
           return i;
        }
      }
      return -1;
    }

    // Attach Process -> Takes a reference to an active thread and moves it to the threads[index]
    void attachProcess(std::thread &&t, int index)
    {
      threads[index] = std::move(t);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////
//
// --- Param Struct ---
//
// Contains three ints per element to allow the ld, cols and rd parameters to be enqueued
//
////////////////////////////////////////////////////////////////////////////////////////////////

struct params{
  int p[3];
  params(int ld, int cols,int rd): p{ ld, cols, rd } {}
};

////////////////////////////////////////////////////////////////////////////////////////////////
//
// --- Param Queue Class --- 
// 
// Contains a queue of params that can be pushed to and popped from (thread safe or fast)
//
////////////////////////////////////////////////////////////////////////////////////////////////

class paramQueue{

  private:
    std::queue<params>  q;
    std::mutex m;

  public:

    // Thread safe push params to queue
    void threadSafePush(params p)
    {
      std::lock_guard<std::mutex> lock(m);
      q.push(p);
    }

    // Thread safe pop params to queue, if queue empty return non workable params (0,0,0)
    params threadSafePop()
    {
      std::lock_guard<std::mutex> lock(m);
      if (q.empty()){
        return params(0,0,0);
      }
      // Get first value
      params temp = q.front();
      // Delete from queue
      q.pop();
      return temp;
    }

    // Fast pop operation, not thread safe
    params fastPop()
    {
      if (q.empty()){
        return params(0,0,0);
      }
      params temp = q.front();
      q.pop();
      return temp;
    }

    // Check if the queue has any work
    int hasElement()
    {
      if (q.empty()){return 0; }
      else{return 1;}
    }

    // Return : params != (0,0,0)
    int workable(params p)
    {
      if (p.p[0] | p.p[1] | p.p[2]){
        return 1;
      }
      return 0;
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////
//
// ---Parallel N-Queen Solver Class---
//
// When constructing, takes parameters:
//   -  Sequential limit -> the N*N size at which the algorithm will switch to sequential
//   -  qn -> the N*N chessboard problem size
//   -  tc -> the max number of additional threads to be created alongside the main thread
//   -  t -> A threadHolder object with total number of threads == tc
//
// To solve problem:
//   - Create a threadHolder object
//   - Create a parNQueenSolver object, passing all args including the threadHolder
//   - Call solve() on the parNQueenSolver object
//   - Number of solutions to the problem is accessible in threadHolder->sols
//
////////////////////////////////////////////////////////////////////////////////////////////////

class parNQueenSolver{

  public:

    // Constructor
    parNQueenSolver(int sl, int qn, int tc, threadHolder *t) : n{qn}, seqLimit{sl}, threadCount{tc}, threads{t}
    {
      // Get id of calling (main) thread
      mainID = std::this_thread::get_id();
      // Initialise two paramQueues
      pq = new paramQueue();
      wpq = new paramQueue();
      multi = false;
      // Calculate 'all' value, used in solving the problem
      all = (1 << n) - 1;            // set N bits on, representing number of columns
      sols = 0;
    }

    // Destructor
    ~parNQueenSolver()
    {
      delete pq;
      delete wpq;
    }	

    // Exposed solve method
    void solve()
    {
      if (threadCount>=n) multi = true;
      nqueen_solver();
    }

  private:
    // Two work queues :
    // pq holds params for serial evaluation after parallel processing
    // wpq holds params for parallel processing if more initial params than max thread count
    paramQueue *pq;
    paramQueue *wpq;
    // Thread holder controls threads
    threadHolder *threads;
    // Constants - n = size of chessboard, seqLimit = when to switch from parallel to serial execution
    //           - threadCount = max number of threads requested, all = line of 1s of length n
    int n, seqLimit, threadCount, all;
    bool multi;
    // ID of main thread
    std::thread::id mainID;
    // Solutions found in parallel added to atomic
    std::atomic<int> sols;

    // Solves the sequential n-queen problem
    int seq_nqueen(chessboard ld, chessboard cols, chessboard rd)
    {
      int sol = 0;

      if (cols == all)                            // A solution is found
        return 1;

      chessboard pos = ~(ld | cols | rd) & all;  // Possible posstions for the queen on the current row
      chessboard next;
      while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
        next = pos  & (-pos);                    // next possible position
        pos -= next;                             // update the possible position
        sol += seq_nqueen((ld|next) << 1, cols|next, (rd|next)>>1); // recursive call for the `next' position
      }
      return sol;
    }

    // Unfolds the nqueen problem to the seq limit then pushes the parameters at this limit
    void unfold(chessboard ld, chessboard cols, chessboard rd)
    {
      // Caluate the number of columns and rows remaining at this stage
      int colsRemaining = n - popCount(cols,n);
      int pos = ~(ld | cols | rd) & all;
      int rowsRemaining = popCount(pos,n);
      // If below seq limit, push the parameters to sequential queue 'pq'
      if ( (rowsRemaining < seqLimit) && (colsRemaining < seqLimit) ) {
        pq->threadSafePush(params(ld,cols,rd));
        return;
      }
      // Else keep unfolding
      chessboard next;
      while (pos != 0){                          // Iterate over all possible positions and solve the (N-1)-queen in each case
        next = pos  & (-pos);                    // next possible position
        pos -= next;                             // update the possible position
        unfold((ld|next) << 1, cols|next, (rd|next)>>1); // recursive call for the `next' position
      }
    }

    // Parallel NQueen Solver - With non-zero sequential limit - Called by both the main thread and all the parallel threads
    //
    // Overall:
    //   - If there are more threads requested than the size of the problem, then each thread
    //     will recursively generate new threads untill saturation.
    //   - Else the threads will be allocated from the main thread and the extra tasks cached.  
    //   - Threads will unfold problems to the depth specified by sequential limit, then cache 
    //     the current parameters in a param queue.
    //   - Main thread will sequentially solve this queue after the threads are done.
    //
    // If main thread :
    //   - Assigns parameters from the first layer of the problem to new threads.
    //   - If there are no more threads, it then caches the parameters at this level in wpq.
    //   - After assigning/caching the parameters, unfolds jobs from the wpq queue untill empty.
    //   - Waits for other threads to complete and joins the threads.
    //
    // If parallel thread :
    //   - If multi mode, will assign parameters from its layers to new threads.
    //   - Once saturated, unfolds the current parameter set until the seqLimit has been reached.
    //   - Unfolds jobs from wpq untill empty
    // 
    int par_nqueen(chessboard ld, chessboard cols, chessboard rd)
    {
      // Find out if main thread
      bool isMainThread = (std::this_thread::get_id() == mainID);
      // Caluate the number of columns and rows remaining at this stage
      int colsRemaining = n - popCount(cols,n);
      int pos = ~(ld | cols | rd) & all;
      int rowsRemaining = popCount(pos,n);
      // If below seq limit, push the parameters to sequential queue [pq]
      if ( (rowsRemaining < seqLimit) && (colsRemaining < seqLimit) ){
        pq->threadSafePush(params(ld,cols,rd));
        return 0;
      }
      else{
        int pos = ~(ld | cols | rd) & all;
        int next;
        while (pos != 0){
          next = pos  & (-pos);                
          pos -= next;                       
          int inactiveThread;
          // If there are inactive threads and on main thread/multi mode, get an inactive thread's index
          if ( isMainThread || multi )
            inactiveThread = threads->getInactiveThread();
          // Else switch inactive thread to -1
          else
            inactiveThread = -1;
          // If inactive thread != -1 then allocate a new parallel thread on [index] = [inactive thread]
          if (inactiveThread > -1){
            threads->attachProcess(std::thread(&parNQueenSolver::par_nqueen, this, (ld|next) << 1, cols|next, (rd|next) >> 1), inactiveThread);
          }
          else {
            // If on main thread, push the parameters to the parallel queue [wpq]
    	    if (isMainThread){
              wpq->threadSafePush(params((ld|next) << 1, cols|next, (rd|next)>>1));
            }
            // If not main thread, unfold current parameters
            else
              unfold((ld|next) << 1, cols|next, (rd|next)>>1);
          }
        }
        // Get a job from the parallel queue [wpq]
        params job = wpq->threadSafePop();
        // If the job is non-zero, unfold it and get a new job
        while (wpq->workable(job)){
          unfold(job.p[0],job.p[1],job.p[2]);
          job = wpq->threadSafePop();
        }
        // If no more workable jobs, the thread is finished
        // If on main thread, join others
        if (isMainThread){
          threads->joinAll();
        }
      }
    }

    // Parallel NQueen Solver - No Sequntial - Called by both the main thread and all the parallel threads
    //
    // Overall:
    //   - If there are more threads requested than the size of the problem, then each thread
    //     will recursively generate new threads untill saturation.
    //   - Else the threads will be allocated from the main thread and the extra tasks cached.
    //   - Threads will solve sub-problems and store solutions the atomic variable 'sols'.
    //
    // If main thread :
    //   - Assigns parameters from the first layer of the problem to new threads.
    //   - If there are no more threads, it then caches the parameters at this level in wpq.
    //   - After assigning/caching the parameters, solves jobs from the wpq queue untill empty.
    //   - Waits for other threads to complete and joins the threads.
    //
    // If parallel thread :
    //   - If multi mode, will assign parameters from its layers to new threads.
    //   - Once saturated, solves the current parameter set.
    //   - Solves jobs from wpq untill empty
    // 
    int par_nqueen_noseq(chessboard ld, chessboard cols, chessboard rd)
    {
      // Find out if main thread
      bool isMainThread = (std::this_thread::get_id() == mainID);
      // Caluate the number of columns and rows remaining at this stage
      int colsRemaining = n - popCount(cols,n);
      int pos = ~(ld | cols | rd) & all;
      int rowsRemaining = popCount(pos,n);
      // If below seq limit, push the parameters to sequential queue [pq]
      if ( (rowsRemaining < seqLimit) && (colsRemaining < seqLimit) ){
        pq->threadSafePush(params(ld,cols,rd));
        return 0;
      }
      else{
        int pos = ~(ld | cols | rd) & all;
        int next;
        while (pos != 0){
          next = pos  & (-pos);                
          pos -= next;                       
          int inactiveThread;
          // If there are inactive threads and on main thread/multi mode, get an inactive thread's index
          if ( isMainThread || multi )
            inactiveThread = threads->getInactiveThread();
          // Else switch inactive thread to -1
          else
            inactiveThread = -1;
          // If inactive thread != -1 then allocate a new parallel thread on [index] = [inactive thread]
          if (inactiveThread > -1){
            threads->attachProcess(std::thread(&parNQueenSolver::par_nqueen_noseq, this, (ld|next) << 1, cols|next, (rd|next) >> 1), inactiveThread);
          }
          else {
            // If on main thread, push the parameters to the parallel queue [wpq]
    	    if (isMainThread){
              wpq->threadSafePush(params((ld|next) << 1, cols|next, (rd|next)>>1));
            }
            // If not main thread, unfold current parameters
            else
              sols += seq_nqueen((ld|next) << 1, cols|next, (rd|next)>>1);
          }
        }
        // Get a job from the parallel queue [wpq]
        params job = wpq->threadSafePop();
        // If the job is non-zero, unfold it and get a new job
        while (wpq->workable(job)){
          sols += seq_nqueen(job.p[0],job.p[1],job.p[2]);
          job = wpq->threadSafePop();
        }
        // If no more workable jobs, the thread is finished
        // If on main thread, join others
        if (isMainThread){
          threads->joinAll();
        }
      }
    }

    // Solves the nqueen problem 
    // If seqLimit is >= size of the problem, solve all sequential
    // Else solve parallel until limit reached then complete in sequential
    void nqueen_solver()
    {
      // Solve sequential only
      if (n <= seqLimit){
        threads->sols = seq_nqueen(0,0,0);
        return;
      }
      // Check how to allocate threads
      if (multi)
        std::cout << "<PNQueen Solver> Thread count > n, switching to recursive thread creation." << std::endl;
      else
        std::cout << "<PNQueen Solver> Thread count <= n, main thread will allocate." << std::endl;
      // Else solve parallel
      // If no sequential section desired
      if (seqLimit == 0){
        std::cout << "<PNQueen Solver> Sequential limit = 0, running par_nqueen_noseq()." << std::endl;
        par_nqueen_noseq(0,0,0);
        threads->sols = sols;
      }
      // Else if non zero seqLimit
      else{
        std::cout << "<PNQueen Solver> Sequential limit = " << seqLimit	 << " , running par_nqueen_noseq()." << std::endl;
        par_nqueen(0,0,0);
        // Finish the problem in sequential by popping params from the sequenal queue
        params tmp(0,0,0);
        while (pq->hasElement()){
          tmp = pq->fastPop();
          threads->sols+=seq_nqueen(tmp.p[0],tmp.p[1],tmp.p[2]);
        }
      }
      return;
    }
};


int main (int argc, char** argv){
 
  std::cout << "\n<PNQueen Solver> Welcome to the Parallel NQueen Solver" << std::endl;
  
  if (argc < 4){
     std::cout << "<PNQueen Solver> You have to provide : \n 1) Number of Queens \n 2) Size to revert to sequential execution \n 3) Max threads to launch" << std::endl;
     return 0;
  }
  // Get params from input command line args
  int qn = std::stoi(argv[1]);
  int seqLimit = std::stoi(argv[2]);
  int threadCount = std::stoi(argv[3]);

  if (seqLimit<0) seqLimit = 0;
  std::cout << "<PNQueen Solver> Sequential Limit = " << seqLimit  << ", N = " << qn << ", Running" << ((seqLimit<qn-1) ?  " parralel." : " sequential.") << std::endl;

  threadCount--;
  if (threadCount<0) {
    std::cout << "<PNQueen Solver> Thread count must be at least 1 (main thread). -> Switching to 1." << std::endl;
    threadCount = 0;
  }
  else{
    if (seqLimit<qn-1)
     std::cout << "<PNQueen Solver> Running on " << threadCount+1 << " threads." << std::endl;
  }
  // Create threadHolder object
  threadHolder threads(threadCount);
  // Create parNQueenSolver object
  parNQueenSolver solver(seqLimit, qn, threadCount, &threads);
  // Start wall clock timer
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  // Solve the problem
  solver.solve();
  // Stop wall clock
  std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
  // Output program execution information
  std::cout << "<PNQueen Solver> Numer of Solution to " << qn << "-Queen problem is : " << threads.sols << std::endl;
  std::cout << "<PNQueen Solver> Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() / 1000000.0 << "s\n" <<std::endl;
  return 0;
}
