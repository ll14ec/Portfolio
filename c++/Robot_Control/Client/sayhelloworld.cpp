/*

C. Edward Cottle & Zhiqiang Zhang & Leeds University 08/09/16

CLIENT SIDE OF NAO ROBOT CONTROL PROGRAM

RECEIVES JOINT ROTATIONAL DATA AS NORMALISED QUATERNIONS THENS CONVERTS TO EULER ANGLES

SENDS ANGLE DATA TO THE JOINTS OF THE ROBOT TO MIMIC THE HUMAN MOVEMENT

DATA:

Q[0] = HEAD                  - DIF BTWN Q[0] - Q[1] = NECK
Q[1] = TORSO				 - TORSO ANGLE
Q[2] = LEFT ARM LOWER		 - DIF BTWN Q[3] - Q[2] = LEFT ELBOW
Q[3] = LEFT ARM UPPER		 - DIF BTWN Q[1] - Q[3] = LEFT SHOULDER
Q[4] = RIGHT ARM UPPER		 - DIF BTWN Q[1] - Q[4] = RIGHT SHOULDER
Q[5] = RIGHT ARM LOWER		 - DIF BTWN Q[4] - Q[5] = RIGHT ELBOW
Q[6] = LEFT LEG UPPER		 - DIF BTWN Q[1] - Q[6] = LEFT HIP
Q[7] = LEFT LEG LOWER		 - DIF BTWN Q[6] - Q[7] = LEFT KNEE
Q[8] = RIGHT LEG UPPER		 - DIF BTWN Q[1] - Q[8] = RIGHT HIP
Q[9] = RIGHT LEG LOWER		 - DIF BTWN Q[8] - Q[9] = RIGHT KNEE

*/

#include "windows.h"
#include <iostream>
#include <fstream>
#include <alerror/alerror.h>
#include <alproxies/almotionproxy.h>
#include "quaternion.h"
#include "matrix/MatrixMath.h"
#include "matrix/MatrixMath.c"
#include <cmath>
#include <fstream>
#include "tchar.h"
#include "strsafe.h"
#include <stdlib.h>
#include <stdio.h>
#pragma comment(lib, "Ws2_32.lib")

/*                       AXIS SYSTEM USED
 *
 *                              Y
 *                              |
 *                              |
 *                              |
 *                              --------X
 *                             /
 *                            /
 *                           Z
 */

// THE FOLLOWING VECTORS ARE USED FOR CALCULATING THE END-EFFECTOR CURRENT POSITIONS
// THEY ARE NOT USED TO MOVE THE ROBOT OR CALCULATE ANGLES

//VECTOR DATA FOR ROBOT'S LIMBS (ONLY NEEDED TO CALCULATE POSITIONS OF END-EFFECTORS)
irr::core::vector3d <float> v_Trunk(0.0f, 0.1265f, 0.0f);
irr::core::vector3d <float> v_LeftH(-0.05f, 0.0f, 0.0f);
irr::core::vector3d <float> v_RightH(0.05f, 0.0f, 0.0f);
irr::core::vector3d <float> v_Thigh(0.0f,-0.1f, 0.0f);
irr::core::vector3d <float> v_Shank(0.0f, -0.105f, 0.0f);
irr::core::vector3d <float> v_Foot(0.0f, 0.0f, 0.045f);
irr::core::vector3d <float> v_LeftSh(-0.098f, 0.0f, 0.0f);
irr::core::vector3d <float> v_RightSh(0.098f, 0.0f, 0.0f);
irr::core::vector3d <float> v_Humerous(0.0f, -0.105f, 0.0f);
irr::core::vector3d <float> v_Radius(0.0f, -0.110f, 0.0f);
irr::core::vector3d <float> v_Hand(0.0f, -0.005f, 0.0f);

//POSITIONAL DATA FOR JOINTS (ONLY NEEDED TO CALCULATE POSITIONS OF END-EFFECTORS)
irr::core::vector3d <float> position[18];
irr::core::vector3d <float> p_Ori(0.0f, 0.0f, 0.250f);
irr::core::vector3d <float> p_Trunk(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LHip(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LKnee(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LAnkle(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LToe(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RHip(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RKnee(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RAnkle(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RToe(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LShoulder(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RShoulder(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LElbow(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RElbow(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LWrist(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RWrist(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_LFinger(0.0f, 0.0f, 0.0f);
irr::core::vector3d <float> p_RFinger(0.0f, 0.0f, 0.0f);

// QUATERNIONS FOR CALCULATIONS
irr::core::quaternion q[10];
irr::core::quaternion temp;

std::string effectors[10];
bool isOn;
bool isOff;
char balanceState;              // 0 = ON THE RIGHT FOOT || 1 = ON THE LEFT FOOT || 2 = ON BOTH FEET
double values[100];             // STORES THE DATA RECEIVED FROM THE PIPE

using namespace std;

void assignNames()
{
        effectors[1] = "Torso";
        effectors[2] = "LArm";
        effectors[3] = "RArm";
        effectors[4] = "LLeg";
        effectors[5] = "RLeg";
        effectors[6] = "Head";
}

enum RotSeq
{
        zyx, zyz, zxy, zxz, yxz, yxy, yzx, yzy, xyz, xyx, xzy,xzx
};

Tuple3 twoAxisRot(float r11, float r12, float r21, float r31, float r32)
{
        Tuple3 result;
        result.x = std::atan2( r11, r12 );
        result.y = std::acos( r21 );
        result.z = std::atan2( r31, r32 );
        return result;
}

Tuple3 threeAxisRot(float r11, float r12, float r21, float r31, float r32)
{
        Tuple3 result;
        result.x = std::atan2( r31, r32 );
        result.y = std::asin( r21 );
        result.z = std::atan2( r11, r12 );
        return result;
}

// CONVERTS QUATERNION TO EULER ANGLES TAKING AXIS ORDER AS ARGUMENT
Tuple3 quatToEuler(irr::core::quaternion q, RotSeq rotSeq)
{
        Tuple3 result;
        result.x = 0;
        result.y = 0;
        result.z = 0;

        switch (rotSeq){
        case zyx:
        return threeAxisRot( 2*(q.X*q.Y + q.W*q.Z),
                             q.W*q.W + q.X*q.X - q.Y*q.Y - q.Z*q.Z,
                             -2*(q.X*q.Z - q.W*q.Y),
                             2*(q.Y*q.Z + q.W*q.X),
                             q.W*q.W - q.X*q.X - q.Y*q.Y + q.Z*q.Z);
        case zyz:
        return twoAxisRot( 2*(q.Y*q.Z - q.W*q.X),
                           2*(q.X*q.Z + q.W*q.Y),
                           q.W*q.W - q.X*q.X - q.Y*q.Y + q.Z*q.Z,
                           2*(q.Y*q.Z + q.W*q.X),
                           -2*(q.X*q.Z - q.W*q.Y));

        case zxy:
        return threeAxisRot( -2*(q.X*q.Y - q.W*q.Z),
                             q.W*q.W - q.X*q.X + q.Y*q.Y - q.Z*q.Z,
                             2*(q.Y*q.Z + q.W*q.X),
                             -2*(q.X*q.Z - q.W*q.Y),
                             q.W*q.W - q.X*q.X - q.Y*q.Y + q.Z*q.Z);

        case zxz:
        return twoAxisRot( 2*(q.X*q.Z + q.W*q.Z),
                           -2*(q.Y*q.Z - q.W*q.X),
                           q.W*q.W - q.X*q.X - q.Y*q.Y + q.Z*q.Z,
                           2*(q.X*q.Z - q.W*q.Y),
                           2*(q.Y*q.Z + q.W*q.X));

        case yxz:
        return threeAxisRot( 2*(q.X*q.Z + q.W*q.Y),
                             q.W*q.W - q.X*q.X - q.Y*q.Y + q.Z*q.Z,
                             -2*(q.Y*q.Z - q.W*q.X),
                             2*(q.X*q.Y + q.W*q.Z),
                             q.W*q.W - q.X*q.X + q.Y*q.Y - q.Z*q.Z);

        case yxy:
        return twoAxisRot( 2*(q.X*q.Y - q.W*q.Z),
                           2*(q.Y*q.Z + q.W*q.X),
                           q.W*q.W - q.X*q.X + q.Y*q.Y - q.Z*q.Z,
                           2*(q.X*q.Y + q.W*q.Z),
                           -2*(q.Y*q.Z - q.W*q.X));

        case yzx:
        return threeAxisRot( -2*(q.X*q.Z - q.W*q.Y),
                             q.W*q.W + q.X*q.X - q.Y*q.Y - q.Z*q.Z,
                             2*(q.X*q.Y + q.W*q.Z),
                             -2*(q.Y*q.Z - q.W*q.X),
                             q.W*q.W - q.X*q.X + q.Y*q.Y - q.Z*q.Z);

        case yzy:
        return twoAxisRot( 2*(q.Y*q.Z + q.W*q.X),
                           -2*(q.X*q.Y - q.W*q.Z),
                           q.W*q.W - q.X*q.X + q.Y*q.Y - q.Z*q.Z,
                           2*(q.Y*q.Z - q.W*q.X),
                           2*(q.X*q.Y + q.W*q.Z));

        case xyz:
        return threeAxisRot( -2*(q.Y*q.Z - q.W*q.X),
                             q.W*q.W - q.X*q.X - q.Y*q.Y + q.Z*q.Z,
                             2*(q.X*q.Z + q.W*q.Y),
                             -2*(q.X*q.Y - q.W*q.Z),
                             q.W*q.W + q.X*q.X - q.Y*q.Y - q.Z*q.Z);

        case xyx:
        return twoAxisRot( 2*(q.X*q.Y + q.W*q.Z),
                           -2*(q.X*q.Z - q.W*q.Y),
                           q.W*q.W + q.X*q.X - q.Y*q.Y - q.Z*q.Z,
                           2*(q.X*q.Y - q.W*q.Z),
                           2*(q.X*q.Z + q.W*q.Y));

        case xzy:
        return threeAxisRot( 2*(q.Y*q.Z + q.W*q.X),
                             q.W*q.W - q.X*q.X + q.Y*q.Y - q.Z*q.Z,
                             -2*(q.X*q.Y - q.W*q.Z),
                             2*(q.X*q.Z + q.W*q.Y),
                             q.W*q.W + q.X*q.X - q.Y*q.Y - q.Z*q.Z);

        case xzx:
        return twoAxisRot( 2*(q.X*q.Z - q.W*q.Y),
                           2*(q.X*q.Y + q.W*q.Z),
                           q.W*q.W + q.X*q.X - q.Y*q.Y - q.Z*q.Z,
                           2*(q.X*q.Z + q.W*q.Y),
                           -2*(q.X*q.Y - q.W*q.Z));
        default:
        std::cout << "incorrect sequence!!" << std::endl;
        return result;
    }
}

// FINDS THE DIFFERENCE BETWEEN TWO QUATERNIONS
irr::core::quaternion findQr(irr::core::quaternion qParent, irr::core::quaternion qChild)
{
        return (qChild * qParent.makeInverse());
}

void moveUpperArmNew(AL::ALMotionProxy motion, Tuple3 t, irr::core::quaternion q, AL::ALValue name, float angle, float fracMaxSpeed, int side, int id)
{
	angle = 2.0f * acos(q.W);
	t.x = (q.X / sqrt((q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z)) * angle);
	t.y = (q.Y / sqrt((q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z)) * angle);
	t.z = (q.Z / sqrt((q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z)) * angle);

	if (side == 0){ name = "LShoulderRoll"; }
        else if (side == 1){ name = "RShoulderRoll"; }
	angle = t.z * (-1.0f);

	id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);

	if (side == 0){ name = "LShoulderPitch";}
        else if (side == 1){name = "RShoulderPitch";}
	angle =(3.14159/2.0f) - t.x ;

	id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);
}

void moveLowerArmNew(AL::ALMotionProxy motion, Tuple3 t, irr::core::quaternion q, AL::ALValue name, float angle, float fracMaxSpeed, int side, int id)
{
	angle = 2.0f * acos(q.W);
	t.x = (q.X / sqrt((q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z)) * angle);
	t.y = (q.Y / sqrt((q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z)) * angle);
	t.z = (q.Z / sqrt((q.X * q.X) + (q.Y * q.Y) + (q.Z * q.Z)) * angle);

	if (side == 0){ name = "LElbowRoll"; }
        else if (side == 1){ name = "RElbowRoll"; }
	angle = t.x;

	if (side == 0){ angle *= -1.0f;	}

	id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);

	if (side == 0){ name = "LElbowYaw";}
        else if (side == 1){ name = "RElbowYaw"; }
	angle =t.x ;
}

void moveUpperArm(AL::ALMotionProxy motion, Tuple3 t, irr::core::quaternion q, AL::ALValue name, float angle, float fracMaxSpeed, int side, int id)
{
	t = quatToEuler(q, zyx);
	if (side == 0){ name = "LShoulderRoll"; }
        else if (side == 1){ name = "RShoulderRoll"; }
	angle = t.z * (-1.0f);

        id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);

        if (side == 0){ name = "LShoulderPitch"; }
        else if (side == 1){ name = "RShoulderPitch"; }
	angle =(3.14159/2.0f) - t.x ;

	id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);
}

void moveLowerArm(AL::ALMotionProxy motion,Tuple3 t, irr::core::quaternion q,  AL::ALValue name, float angle, float fracMaxSpeed, int side, int id)
{
	t = quatToEuler(q,zxy);
        if (side == 0){ name = "LElbowRoll"; }
        else if (side == 1){ name = "RElbowRoll" }
        angle = t.z * (-1.0f);

        id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);

        if (side == 0){ name = "LElbowYaw"; }
        else if (side == 1){ name = "RElbowYaw"; }
        angle = t.x * (1.0f);

        id = motion.post.setAngles(name,angle,fracMaxSpeed);
        motion.wait(id,0);
}

void moveHead(AL::ALMotionProxy motion,Tuple3 t, irr::core::quaternion q,  AL::ALValue name, float angle, float fracMaxSpeed, int id)
{
        t = quatToEuler(q,zyx);
        name = "HeadYaw";
        angle = t.y;

        id = motion.post.setAngles(name,angle,fracMaxSpeed/3.0f);
        motion.wait(id,0);

        name = "HeadPitch";
        angle = t.x  * (-1.0f);
        id = motion.post.setAngles(name,angle,fracMaxSpeed/10.0f);
        motion.wait(id,0);
}

void moveTorso(AL::ALMotionProxy motion,Tuple3 t, irr::core::quaternion q,  AL::ALValue name, float angle, float fracMaxSpeed, int id)
{
        t = quatToEuler(q,yzx);
        name = "LHipYawPitch";
        angle = t.y * (-1.0f);
        id = motion.post.setAngles(name,angle,fracMaxSpeed/10.0f);
        motion.wait(id,0);
}

void moveUpperLeg(AL::ALMotionProxy motion,Tuple3 t, irr::core::quaternion q,  AL::ALValue name, float angle, float fracMaxSpeed, int side, int id)
{
        t = quatToEuler(q,xyz);
        if (side == 0){ name = "LHipRoll"; }
        else if (side == 1){ name = "RHipRoll"; }
        angle = t.x * (-1.0f);
        id  = motion.post.setAngles(name,angle,fracMaxSpeed/3.0f);
        motion.wait(id,0);

        if (side == 0){ name = "LHipPitch"; angle = t.y * (-1.0f); }
        else if (side == 1){ name = "RHipPitch"; angle = t.y; }
        id = motion.post.setAngles(name,angle,fracMaxSpeed/9.5f);
        motion.wait(id,0);
}

void moveLowerLeg(AL::ALMotionProxy motion,Tuple3 t, irr::core::quaternion q,  AL::ALValue name, float angle, float fracMaxSpeed, int side, int id)
{
        t = quatToEuler(q,zyx);
        if (side == 0){ name = "LKneePitch"; angle = t.y; }
        else if (side == 1){ name = "RKneePitch"; angle = t.y; }
        id  = motion.post.setAngles(name,angle,fracMaxSpeed/9.0f);
        motion.wait(id,0);
}

// SHIFTS THE WEIGHT OF THE ROBOT TO ONE LEG
void shiftWeight(AL::ALMotionProxy motion, std::string supportLeg, int id)
{
        id = motion.post.wbGoToBalance(supportLeg, 3.0f);
        motion.wait(id,0);
        std::cout << "shifting weight: " << supportLeg << std::endl;

        if (supportLeg == "RLeg"){
		motion.wbEnable(isOff);
		id = motion.post.wbFootState("Fixed", "RLeg");
                id = motion.post.wbFootState("Free", "LLeg");
		motion.wbEnable(isOn);
		balanceState = 0;
        }
        else if (supportLeg == "LLeg"){
		motion.wbEnable(isOff);
		id = motion.post.wbFootState("Fixed", "LLeg");
                id = motion.post.wbFootState("Free",  "RLeg");
		motion.wbEnable(isOn);
		balanceState = 1;
        }
        else if (supportLeg == "Legs"){
		motion.wbEnable(isOff);
                id = motion.post.wbFootState("Fixed", "LLeg");
                id = motion.post.wbFootState("Fixed", "RLeg");
		motion.wbEnable(isOn);
		balanceState = 2;
        }
}

// CALCUALTES REAL WORLD POSITION OF THE END-EFFECTORS
irr::core::vector3d <float> * updateRobotPosition(irr::core::quaternion q[10], AL::ALMotionProxy motion, irr::core::vector3d <float> * result)
{
        p_Trunk = p_Ori + (q[1] * v_Trunk);

        p_LShoulder = p_Trunk + (q[3] * v_LeftSh);
        p_LElbow = p_LShoulder + (q[3] * v_Humerous);
        p_LWrist = p_LElbow + (q[2] * v_Radius);
        p_LFinger = p_LWrist + (q[2] * v_Hand);

        p_RShoulder = p_Trunk + (q[4] * v_RightSh);
        p_RElbow = p_RShoulder + (q[4] * v_Humerous);
        p_RWrist = p_RElbow + (q[5] * v_Radius);
        p_RFinger = p_RWrist + (q[5] * v_Hand);

        p_LHip = p_Ori + (q[6] * v_LeftH);
        p_LKnee = p_LHip + (q[6] * v_Thigh);
        p_LAnkle = p_LKnee + (q[7] * v_Shank);
        p_LToe = p_LAnkle + (q[7] * v_Foot);

        p_RHip = p_Ori + (q[8] * v_RightH);
        p_RKnee = p_RHip + (q[8] * v_Thigh);
        p_RAnkle = p_RKnee + (q[9] * v_Shank);
        p_RToe = p_RAnkle + (q[9] * v_Foot);

        result[0] = p_Trunk;
        result[1] = p_LShoulder;
        result[2] = p_LElbow;
        result[3] = p_LWrist;
        result[4] = p_LFinger;
        result[5] = p_RShoulder;
        result[6] = p_RElbow;
        result[7] = p_RWrist;
        result[8] = p_RFinger;
        result[9] = p_LHip;
        result[10] = p_LKnee;
        result[11] = p_LAnkle;
        result[12] = p_LToe;
        result[13] = p_RHip;
        result[14] = p_RKnee;
        result[15] = p_RAnkle;
        result[16] = p_RToe;
        return (result);
}

// CHECKS WHETHER ONE FOOT IS RAISED
void checkFeet (irr::core::quaternion q[10], AL::ALMotionProxy motion, irr::core::vector3d <float> * result, float a ,float b)
{
	a = updateRobotPosition(q,motion,result)[11].Z;      // L FOOT Z AXIS
	b = updateRobotPosition(q,motion,result)[15].Z;      // R FOOT Z AXIS

	if (balanceState == 2){
	        if (a > b + 0.06f){                                  //L FOOT IN THE AIR
		        shiftWeight(motion,"RLeg", 1);
	        }

	        if (b > a + 0.06f){                                  //R FOOT IN THE AIR
		        shiftWeight(motion,"LLeg", 1);
	        }
	}

	else if (balanceState == 0){                                 // R FOOT DOWN
		if (a < b + 0.03f){
			shiftWeight(motion,"Legs", 1);
		}
	}

	else if (balanceState == 1){                                 // L FOOT DOWN
		if (b < a + 0.03f){
			shiftWeight(motion,"Legs", 1);
		}
	}
}

// GETS DATA FROM THE PIPE
void getData()
{
	// OPEN THE NAMED PIPE
	// MOST OF THE PARAMETERS ARE NOT RELEVANT
	HANDLE pipe = CreateFile(
	_T("\\\\.\\pipe\\my_pipe"),
	GENERIC_READ, // ONLY NEED READ ACCESS
	FILE_SHARE_READ | FILE_SHARE_WRITE,
	NULL,
	OPEN_EXISTING,
	FILE_ATTRIBUTE_NORMAL,
	NULL
	);

	if (pipe == INVALID_HANDLE_VALUE) {/*CHECK FOR PIPE FAILURE HERE*/}

	// THE READ OPERATION WILL BLOCK UNTIL THERE IS DATA TO READ
	DWORD numBytesRead = 0;

	BOOL result = ReadFile(
	pipe,
	&values[0], // THE DATA FROM THE PIPE WILL BE PUT HERE
	800, // NO. OF BYTES ALLOCATED
	&numBytesRead, // NUMBER OF BYTES READ
	NULL
	);
	CloseHandle(pipe);
}

// CHECKS FOR A NORMALISED QUATERNION
int check(irr::core::quaternion q)
{
	if (((q.W*q.W)+(q.Z*q.Z)+(q.X*q.X)+(q.Y*q.Y))>0.94){
		if (((q.W*q.W)+(q.Z*q.Z)+(q.X*q.X)+(q.Y*q.Y))<1.06){
			return 1;
		}
	}
	cout << "quaternion not normalised" << endl;
	cout << "[" << q.W << ", " << q.X << ", " << q.Y << ", " << q.Z << "]" << endl;
	return 0;
}

// WRITES A FRAME OF DATA FROM VALUES[] TO FILE
void writeFile()
{
	fstream outputFile(".\\rawdatahere", std::fstream::in | std::fstream::out | std::fstream::app | std::fstream::ate);
	int i=0;
	for (i=0;i<6;i++){
		outputFile << q[i].X << " ";
		outputFile << q[i].Y << " ";
		outputFile << q[i].Z << " ";
		outputFile << q[i].W << " ";
	}
	outputFile << '\n' ;
	outputFile.close();
}

// SETS QUATERNIONS FROM DATA RECEIVED FROM THE PIPE
void setQ()
{
	q[0].set(values[0], values[1], values[2], values[3]);
	q[1].set(values[4], values[5], values[6], values[7]);
	q[2].set(values[8], values[9], values[10], values[11]);
	q[3].set(values[12], values[13], values[14], values[15]);
	q[4].set(values[16], values[17], values[18], values[19]);
	q[5].set(values[20], values[21], values[22], values[23]);
	q[6].set(values[24], values[25], values[26], values[27]);
	q[7].set(values[28], values[29], values[30], values[31]);
	q[8].set(values[32], values[33], values[34], values[35]);
	q[9].set(values[36], values[37], values[38], values[39]);
}

int main(int argc, char* argv[])
{
	// CHECK FOR CORRECT INPUT ARGUMENTS
	if(argc != 2){
		std::cerr << "Wrong number of arguments!" << std::endl;
		std::cerr << "Usage: movehead NAO_IP" << std::endl;
		system("pause");
		exit(2);
	}

	balanceState = 2;	// BOTH FEET AS SUPPORT
	assignNames();
	isOff = false;
	isOn = true;

	try {
		// SET UP MOTION PROXY
		AL::ALMotionProxy motion(argv[1], 9559);
		// WAKE ROBOT FROM INTIAL POSITION, SET BALANCE CONSTRAINT AND COLLISION PROTECT
		motion.wakeUp();
		motion.wbEnable(isOn);
		std::string stateName = "Fixed";
		std::string supportLeg = "Legs";
		motion.wbFootState(stateName, supportLeg);
		motion.wbEnableBalanceConstraint(isOn, supportLeg);
		motion.wbFootState(stateName, supportLeg);
		stateName = "Arms";
		motion.setCollisionProtectionEnabled(stateName, isOn);

		// ZERO INITIAL VALUES
		int i=0;
		for (i=0;i<10;i++){
			q[i].set(0.0f,0.0f,0.0f,1.0f);
		}
		Tuple3 t;
		t.x = 0.0f;
		t.y = 0.0f;
		t.z = 0.0f;
	        AL::ALValue name = " ";
	        float x = 1.0f;

		// OVERWRITE PREVIOUS DATA FILE
		fstream outputFile(".\\rawdatahere", std::fstream::in | std::fstream::out | std::fstream::trunc);
		outputFile<< '\n';
		outputFile.close();

		// MAIN LOOP        ------------------->
		while(1){

			Sleep(150);
			getData();
			setQ();
			//writeFile();    // OPTIONAL WRITE SEQUENTIAL DATA FRAMES TO FILE HERE

			moveHead( motion, t, findQr(q[1],q[0]), name, x, x ,0.001f );
			moveUpperLeg( motion, t, findQr(q[1], q[6]), name, x , x , 0 , 0.001f ); //L
			moveUpperLeg( motion, t, findQr(q[1], q[8]), name, x , x , 1 , 0.001f ); //R
			//moveTorso( motion, t, q[1], name, x, x ,0.1f );
			moveLowerArmNew( motion, t, findQr(q[3], q[2]), name, x , x , 0 , 0.001f ); //L
			moveLowerArmNew( motion, t, findQr(q[4], q[5]), name, x , x , 1 , 0.001f ); //R
			moveUpperArmNew( motion, t, findQr(q[1], q[3]), name, x , x , 0 , 0.001f ); //L
			moveUpperArmNew( motion, t, findQr(q[1], q[4]), name, x , x , 1 , 0.001f ); //R
			//moveLowerArm( motion, t, findQr(q[3], q[2]), name, x , x , 0 , 0.1f ); //L
			//moveLowerArm( motion, t, findQr(q[4], q[5]), name, x , x , 1 , 0.1f ); //R
			//moveUpperArm( motion, t, findQr(q[1], q[3]), name, x , x , 0 , 0.1f ); //L
			//moveUpperArm( motion, t, findQr(q[1], q[4]), name, x , x , 1 , 0.1f ); //R
			//moveLowerLeg( motion, t, findQr(q[6], q[7]), name, x , x , 0 , 0.1f ); //L
			//moveLowerLeg( motion, t, findQr(q[8], q[9]), name, x , x , 1 , 0.1f ); //R
			//checkFeet(q,motion, position, x, x);
		}
    }
	catch (const AL::ALError& e) {
       		std::cerr << "Caught exception: " << e.what() << std::endl;
		exit(1);
	}
	exit(0);
}
