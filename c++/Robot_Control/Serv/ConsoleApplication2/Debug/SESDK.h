#pragma comment(lib, "Ws2_32.lib")
#include <string>
// Copyright � Tudor Pascu. SESDK v1.2 developed on the SE6.0 architecture.

extern "C"
{
	// Generates a skeleton from a .se file. The actor MUST be generated BEFORE any connection attempts.
	// @param path: filePath of the actor file (e.g. "c:\\skeleton.se")
	__declspec(dllexport) bool SEGenerateSkeleton(char* path);

	// Begins self-calibration gesture
	// @param ip: ip to connect to
	// @param port: port to register on the system's broadcaster (usually 30001)
	// &result: true if successfully connected, false otherwise
	__declspec(dllexport) bool SEConnect(char* ip, int port);

	// Begins self-calibration gesture
	// &result: true if successfully connected, false otherwise
	__declspec(dllexport) bool SEIsConnected();

	// Disconnects the SDK from the Suit
	__declspec(dllexport) void SEDisconnect();

	// Returns a JSON packet of all the current frames available in the suit buffer as const char*
	// @param lastFrameOnly: true if you only want to return the last frame and not the whole frame buffer.
	__declspec(dllexport) std::string SEPollString(bool lastFrameOnly);

	// Returns a JSON packet of all the current frames available in the suit buffer as std::string
	// @param lastFrameOnly: true if you only want to return the last frame and not the whole frame buffer.
	__declspec(dllexport) const char* SEPollChar(bool lastFrameOnly);

	__declspec(dllexport) int* SEPollInt(bool lastFrameOnly);

	// Compensates all the rotations of the skeleton to equal zero
	__declspec(dllexport) void SEZero();

	// Begins self-calibration gesture
	// @param inverted: direction of movement for calibration (use false for suit and true for gloves)
	__declspec(dllexport) void SEInitialize(bool inverted);

	// Sets the walk mode of the kinematic skeleton
	// @param mode: 0 = meat hooked, 1 = walking, 2 = height tracking
	__declspec(dllexport) void SESetWalkMode(int mode);
};

