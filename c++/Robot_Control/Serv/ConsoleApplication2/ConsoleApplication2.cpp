#include "stdafx.h"
#include "C:/Users/Ed/Desktop/armadillo-7.400.2/include/armadillo"
#include "quaternion.h"
#include <iostream>
#include <string>
#include <fstream>
#include <codecvt>
#include <stdlib.h>
#include <string>
#include "windows.h"

#define INTRO_FRAMES 50.0
#define CALIB_FRAMES 11.0

double values[100];

using namespace std;
using namespace arma;

irr::core::quaternion q[10];
irr::core::quaternion avg[10];
irr::core::quaternion avg2[10];
irr::core::quaternion temp;
irr::core::quaternion axisSwitch;
irr::core::quaternion lu_g[10];

extern "C" typedef bool(__cdecl * Skeleton1) (char *);                                  //SEGenerateSkeleton
extern "C" typedef bool(__cdecl * Skeleton2)(char*, int);                               //SEConnect
extern "C" typedef bool(__cdecl * Skeleton3) (void);                                    //SEIsConnected
extern "C" typedef void(__cdecl * Skeleton4) (void);                                    //SEDisconnect
extern "C" typedef std::string(__cdecl * Skeleton5)(bool);                              //SEPollString
extern "C" typedef const char*(__cdecl * Skeleton6)(bool);                              //SEPollChar
extern "C" typedef int*(__cdecl * Skeleton7)(bool);                                             //SEPollInt
extern "C" typedef void(__cdecl * Skeleton8)(void);                                     //SESetZero
extern "C" typedef void(__cdecl * Skeleton9)(bool);                                     //SEInitialize
extern "C" typedef void(__cdecl * Skeleton10)(int);

void writeFile()
{

        fstream outputFile2(".\\rda2", std::fstream::in | std::fstream::out |
std::fstream::app | std::fstream::ate);
        int i = 0;

        for (i = 0; i<40; i++) {
                outputFile2 << values[i] << " ";
        }
        outputFile2 << '\n';

        outputFile2.close();
}

void zeroQuaternions()
{
        int z = 0;
        for (z = 0; z<10; z++) {
                q[z].set(0.0f, 0.0f, 0.0f, 0.0f);
        }
        z = 0;
        for (z = 0; z<10; z++) {
                avg[z].set(0.0f, 0.0f, 0.0f, 0.0f);
        }
}

int check(irr::core::quaternion q, int x)
{
        if (((q.W*q.W) + (q.Z*q.Z) + (q.X*q.X) + (q.Y*q.Y))>0.94) {
                if (((q.W*q.W) + (q.Z*q.Z) + (q.X*q.X) + (q.Y*q.Y))<1.06) {
                        return 1;
                }
        }
        cout << "quaternion " << x << " not normalised" << endl;
        cout << "[" << q.W << ", " << q.X << ", " << q.Y << ", " << q.Z << "]" << endl;
        return 0;
}

void getQuaternions()
{
        std::ifstream myfile(".\\test.txt", std::ifstream::in);
        std::ofstream outfile(".\\norm.txt", std::ofstream::out |
std::ofstream::trunc);

        char a;
        char b;
        int counter = 0;
        int start = 0;

        while (counter < 872) {
                counter++;
                while ((a != 44) && (start == 0)) {
                        myfile >> a;
                }
                start = 1;
                while (myfile >> a) {

                        if (a == 44) {
                                outfile << " ";
                                continue;
                        }
                        if (a == 58) {
                                outfile << " ";
                                continue;
                        }

                        if (a > 44) {
                                if (a < 58) {
                                        if (a == 45) {
                                                myfile >> b;
                                                if (b == '0') {
                                                        outfile << a;
                                                        outfile << b;
                                                        continue;
                                                }
                                                if (b != '0') {
                                                        continue;
                                                }
                                        }
                                        else {
                                                outfile << a;
                                        }
                                }
                        }
                }
        }
        myfile.close();
        outfile.close();

        FILE * afile = fopen(".\\norm.txt", "r");

        if (!afile) { cout << "failed to open file." << endl; }

        double test;
        int i = 0;

        while ((fscanf(afile, "%lf", &test)) > 0) {
                values[i] = test;
                i++;
        }
        fclose(afile);
}

int findItem(int i, int q, float des)
{
        i = 0;
        while ((values[i] != des) && (i<1000)) {
                i++;
        }
        i++;

        if (i>1000) {
                cout << "cant find data q[" << q << "]" << endl;
        }
        return i;
}

void updateQuats()
{
        int i = findItem(0, 0, 4);
        temp.set(values[i + 3], values[i + 2], values[i + 1], values[i]);
        if (check(temp, 0)) {
                q[0].set(values[i + 1], values[i + 2], values[i + 3], values[i]);       //head
        }

        i = findItem(19, 1, 1);
        i += 2;
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 1)) {
                q[1].set(values[i + 1], values[i + 2], values[i + 3], values[i]);       //hips
        }

        i = findItem(0, 2, 6);
        temp.set(values[i + 3], values[i + 2], values[i + 1], values[i]);
        if (check(temp, 2)) {
                q[2].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //left low arm
        }

        i = findItem(0, 3, 5);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 3)) {
                q[3].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //left up arm
        }

        i = findItem(0, 4, 8);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 4)) {
                q[4].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //right up arm
        }

        i = findItem(0, 5, 9);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 5)) {
                q[5].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //right low arm
        }

        i = findItem(0, 6, 11);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 6)) {
                q[6].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //left up leg
        }

        i = findItem(0, 7, 12);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 7)) {
                q[7].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //left low leg
        }

        i = findItem(0, 8, 15);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 8)) {
                q[8].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //right up leg
        }

        i = findItem(0, 9, 16);
        temp.set(values[i + 3], values[i + 1], values[i + 2], values[i]);
        if (check(temp, 9)) {
                q[9].set(values[i + 1], values[i + 2], values[i + 3],
values[i]);     //right low leg
        }
}

void updateAverage(int quat, double x, double y, double z, double w)
{
        avg[quat].set((avg[quat].X + x), (avg[quat].Y + y), (avg[quat].Z +
z), (avg[quat].W + w));
}

void normaliseAverages()
{
        int z = 0;
        for (z = 0; z < 10; z++) {
                avg[z].set((avg[z].X / INTRO_FRAMES), (avg[z].Y / INTRO_FRAMES),
(avg[z].Z / INTRO_FRAMES), (avg[z].W / INTRO_FRAMES));
                avg[z].normalize();
        }
}

void meanRef()
{
        int z = 0;
        for (z = 0; z < 10; z++) {
                q[z] = q[z] * avg[z];                   //      ref=qmult(qconj(quamean_p),ref); etc
        }
}

mat rightQ(irr::core::quaternion q)
{
        mat rQ(3, 3);
        mat i(3, 3);
        i.eye();
        mat x(3, 3);
        mat fbf(4, 4);

        rQ << 0.0f << (q.Z * -1.0f) << q.Y << endr
                << q.Z << 0.0f << (q.X * -1.0f) << endr
                << (q.Y * -1.0f) << q.X << 0.0f << endr;

        x = (i * q.W) - rQ;

        fbf << x(0, 0) << x(0, 1) << x(0, 2) << q.X << endr
                << x(1, 0) << x(1, 1) << x(1, 2) << q.Y << endr
                << x(2, 0) << x(2, 1) << x(2, 2) << q.Z << endr
                << (q.X * -1.0f) << (q.Y * -1.0f) << (q.Z * -1.0f) << q.W << endr;

        return fbf;
}

mat leftQ(irr::core::quaternion q)
{
        mat lQ;
        mat i(3, 3);
        i.eye();
        mat x(3, 3);
        mat fbf(4, 4);

        lQ << 0.0f << (q.Z * -1.0f) << q.Y << endr
                << q.Z << 0.0f << (q.X * -1.0f) << endr
                << (q.Y * -1.0f) << q.X << 0.0f << endr;

        x = (i * q.W) + lQ;

        fbf << x(0, 0) << x(0, 1) << x(0, 2) << q.X << endr
                << x(1, 0) << x(1, 1) << x(1, 2) << q.Y << endr
                << x(2, 0) << x(2, 1) << x(2, 2) << q.Z << endr
                << (q.X * -1.0f) << (q.Y * -1.0f) << (q.Z * -1.0f) << q.W << endr;

        return fbf;
}

mat globalAllign(irr::core::quaternion ref, irr::core::quaternion
lu_g, int size, mat growMatrix)
{
        mat M(4, 4);
        M = rightQ(ref) - leftQ(lu_g);

        growMatrix((size * 4) - 4, 0) = M(0, 0);
        growMatrix((size * 4) - 4, 1) = M(0, 1);
        growMatrix((size * 4) - 4, 2) = M(0, 2);
        growMatrix((size * 4) - 4, 3) = M(0, 3);

        growMatrix((size * 4) - 3, 0) = M(1, 0);
        growMatrix((size * 4) - 3, 1) = M(1, 1);
        growMatrix((size * 4) - 3, 2) = M(1, 2);
        growMatrix((size * 4) - 3, 3) = M(1, 3);

        growMatrix((size * 4) - 2, 0) = M(2, 0);
        growMatrix((size * 4) - 2, 1) = M(2, 1);
        growMatrix((size * 4) - 2, 2) = M(2, 2);
        growMatrix((size * 4) - 2, 3) = M(2, 3);

        growMatrix((size * 4) - 1, 0) = M(3, 0);
        growMatrix((size * 4) - 1, 1) = M(3, 1);
        growMatrix((size * 4) - 1, 2) = M(3, 2);
        growMatrix((size * 4) - 1, 3) = M(3, 3);

        return growMatrix;
}

irr::core::quaternion returnAllignedQuaternion(mat M)
{
        mat temp1((M.size() / 4.0f), (M.size() / 4.0f));
        mat temp2(4, 4);
        vec s(4);

        svd(temp1, s, temp2, M, "dc");

        temp.X = temp2(0, 3);
        temp.Y = temp2(1, 3);
        temp.Z = temp2(2, 3);
        temp.W = temp2(3, 3);
        return temp;
}

void getQuaternionsFromFile(int line)
{
        // WORKS ON A RAW FILE WITH ROOT, LL, LU, RU, RL QUATERNIONS (UPPER BODY ONLY)
        std::ifstream myfile(".\\rawdatahere", std::ifstream::in);
        int i = 0;
        int j = 0;
        float a = 0;
        for (i = 0; i < line; i++) {
                for (j = 0; j < 24; j++) {
                        myfile >> a;
                }
        }
        i = 0;
        for (i = 0; i < 6; i++) {
                myfile >> a;
                q[i].X = a;
                myfile >> a;
                q[i].Y = a;
                myfile >> a;
                q[i].Z = a;
                myfile >> a;
                q[i].W = a;
        }
}

void offsetQuaternions()
{
        int z = 0;
        for (z = 0; z<10; z++) {
                temp = q[z] * avg2[z].makeInverse();                    //convert using deltaq
                lu_g[z] = avg2[z].makeInverse() * temp;
        }
        z = 0;
        for (z = 0; z<10; z++) {
                temp = lu_g[z] * axisSwitch.makeInverse();              // switch axis
                lu_g[z] = axisSwitch.makeInverse() * temp;
        }
}


void setValues()
{
        int i = 0;
        int j = 0;
        for (i = 0; i < 10; i++) {
                values[j] = lu_g[i].X;
                values[j + 1] = lu_g[i].Y;
                values[j + 2] = lu_g[i].Z;
                values[j + 3] = lu_g[i].W;
                j += 4;
        }
}

void printAverages()
{
        int z = 0;
        cout << "averages" << endl;
        for (z = 0; z < 10; z++) {
                cout << avg[z].X << " " << avg[z].Y << " " << avg[z].Z << " " <<
avg[z].W << endl;
        }
}

void invertAverages()
{
        int z = 0;
        for (z = 0; z < 10; z++) {
                avg[z].makeInverse();
        }
}

void outputCalibData()
{
        int z = 0;
        for (z = 0; z < 10; z++) {
                cout << z << endl;
                cout << avg2[z].X << " " << avg2[z].Y << " " << avg2[z].Z << " " <<
avg2[z].W << endl;
        }
}

int main()
{
        int z = 0;
        axisSwitch.set(0.7071, 0, -0.7071, 0);
        axisSwitch.normalize();

        Skeleton1 SEGenerateSkeleton = NULL;
        Skeleton2 SEConnect = NULL;
        Skeleton3 SEIsConnected = NULL;
        Skeleton4 SEDisconnect = NULL;
        Skeleton5 SEPollString = NULL;
        Skeleton6 SEPollChar = NULL;
        Skeleton8 SEZero = NULL;
        Skeleton9 SEInitialize = NULL;
        Skeleton10 SESetWalkMode = NULL;

        HINSTANCE hDLL = NULL;
        HANDLE pipe;

        int i = 0;
        BOOL result;

        char* st;
        string sentData;
        string copier;
        ofstream outputFile;
        ifstream inputFile;
        DWORD numBytesWritten;

        hDLL = LoadLibrary(_T("SESDK.dll"));
        if (hDLL == NULL) {
                std::cout << "not loaded library" << std::endl;
                system("pause");
        }

        SEGenerateSkeleton = (Skeleton1)GetProcAddress(hDLL, "SEGenerateSkeleton");
        SEConnect = (Skeleton2)GetProcAddress(hDLL, "SEConnect");
        SEIsConnected = (Skeleton3)GetProcAddress(hDLL, "SEIsConnected");
        SEDisconnect = (Skeleton4)GetProcAddress(hDLL, "SEDisconnect");
        SEPollString = (Skeleton5)GetProcAddress(hDLL, "SEPollString");
        SEPollChar = (Skeleton6)GetProcAddress(hDLL, "SEPollChar");
        SEZero = (Skeleton8)GetProcAddress(hDLL, "SEZero");
        SEInitialize = (Skeleton9)GetProcAddress(hDLL, "SEInitialize");
        SESetWalkMode = (Skeleton10)GetProcAddress(hDLL, "SESetWalkMode");

        if (SEGenerateSkeleton == NULL) {
                std::cout << "not loaded function1" << std::endl;
                system("pause");
        }

        if (SEConnect == NULL) {
                std::cout << "not loaded function2" << std::endl;
                system("pause");
        }

        if (SEIsConnected == NULL) {
                std::cout << "not loaded function3" << std::endl;
                system("pause");
        }

        if (SEDisconnect == NULL) {
                std::cout << "not loaded function4" << std::endl;
                system("pause");
        }

        if (SEPollString == NULL) {
                std::cout << "not loaded function5" << std::endl;
                system("pause");
        }

        if (SEPollChar == NULL) {
                std::cout << "not loaded function6" << std::endl;
                system("pause");
        }

        if (SEZero == NULL) {
                std::cout << "not loaded function8" << std::endl;
                system("pause");
        }

        if (SEInitialize == NULL) {
                std::cout << "not loaded function9" << std::endl;
                system("pause");
        }

        if (SESetWalkMode == NULL) {
                std::cout << "not loaded function10" << std::endl;
                system("pause");
        }

        if (SEGenerateSkeleton("C:\\Users\\Ed\\Desktop\\animate 8.82\\animate
8.82\\actors\\01 - 18 Sensor Suit.se") == false) {
                std::cout << "skeleton not generated" << std::endl;
                system("pause");
        }

        if (SEConnect("192.168.11.101", 9559) == false) {
                std::cout << "connection failed" << std::endl;
                system("pause");
        }
        else {
                if (!SEIsConnected()) {
                        cout << "suit has disconnected";
                        system("pause");
                }
                else {
                        int intro = 0;

                        zeroQuaternions();

                        cout << "Begin taking average..." << endl;
                        system("pause");
                        Sleep(2000);

                        //// TAKES INTRO FRAMES THEN TAKES THE AVERAGE
                        while (intro < INTRO_FRAMES) {

                                // CALLS FUNCTION TO GET DATA FROM THE SUIT (FROM SE SDK)
                                // THEN WRITES THIS STRING DATA TO FILE, WHICH WILL BE PARSED
LATER FOR VALUES
                                st = " ";
                                st = (char*)SEPollChar(true);

                                outputFile.open(".\\test.txt", std::ofstream::out | std::ofstream::trunc);

                                i = 0;

                                for (i = 0; i < 10500; i++) {
                                        outputFile << st[i];
                                }

                                outputFile.close();

                                //// GETS NUMERICAL DATA FROM STRING RETURNED BY SEPollChar()
                                getQuaternions();
                                //// SAVES THE DATA INTO RELAVENT QUATERNION STRUCTURES (q[0] ---> q[9])
                                updateQuats();
                                intro++;
                                z = 0;
                                for (z = 0; z < 10; z++) {
                                        if (intro > 20) {
                                                //UPDATE POSITIONAL DATA FOR AVERAGES
                                                updateAverage(z, q[z].X, q[z].Y, q[z].Z, q[z].W);
                                        }
                                }
                        }

                        // CALCULATE NORMALISED QUATERNION VALUES FROM AVERAGES, THEN
INVERT THE QUATERNIONS
                        normaliseAverages();
                        invertAverages();
                        intro = 1;
                        //DECLARE SVD MATRICES FOR EACH LIMB SEGMENT
                        //LEFT UP ARM
                        mat growMatrixLu(4, 4);
                        growMatrixLu.zeros();
                        //LEFT FOREARM
                        mat growMatrixLf(4, 4);
                        growMatrixLf.zeros();
                        //RIGHT UP ARM
                        mat growMatrixRu(4, 4);
                        growMatrixRu.zeros();
                        //RIGHT FOREARM
                        mat growMatrixRf(4, 4);
                        growMatrixRf.zeros();
                        //LEFT THIGH
                        mat growMatrixLt(4, 4);
                        growMatrixLt.zeros();
                        //LEFT SHIN
                        mat growMatrixLs(4, 4);
                        growMatrixLs.zeros();
                        //RIGHT THIGH
                        mat growMatrixRt(4, 4);
                        growMatrixRt.zeros();
                        //RIGHT SHIN
                        mat growMatrixRs(4, 4);
                        growMatrixRs.zeros();
                        //HEAD
                        mat growMatrixHd(4, 4);
                        growMatrixHd.zeros();

                        int matrixSize = 1;

                        //GETS CALIBRATION DATA FROM 5 POINTS ACROSS ~600 FRAMES
                        while (intro < CALIB_FRAMES) {

                                // CALLS FUNCTION TO GET DATA FROM THE SUIT (FROM SE SDK)
                                // THEN WRITES THIS STRING DATA TO FILE, WHICH WILL BE PARSED
LATER FOR VALUES
                                st = (char*)SEPollChar(true);
                                outputFile.open(".\\test.txt", std::ofstream::out | std::ofstream::trunc);
                                i = 0;

                                for (i = 0; i < 10500; i++) {
                                        outputFile << st[i];
                                }

                                outputFile.close();

                                //// GETS NUMERICAL DATA FROM STRING RETURNED BY SEPollChar()
                                getQuaternions();
                                //// SAVES THE DATA INTO RELAVENT QUATERNION STRUCTURES (q[0] ---> q[9])
                                updateQuats();
                                // LOCALISES AVERAGE POSITION ROTATIONAL DATA
                                meanRef();

                                if ((intro == 1) || (intro == 3) || (intro == 5) || (intro == 7)
|| (intro == 9)) {
                                        cout << "Taking posture for calibration..." << endl;
                                        system("pause");
                                        Sleep(2000);
                                }
                                //GROW THE MATRIX EACH TIME CLIBRATION FRAMES ARE TAKEN, BEFORE S V D
                                if ((intro == 2) || (intro == 4) || (intro == 6) || (intro == 8)
|| (intro == 10)) {

                                        growMatrixHd.resize(matrixSize * 4, 4);
                                        growMatrixLu.resize(matrixSize * 4, 4);
                                        growMatrixLf.resize(matrixSize * 4, 4);
                                        growMatrixRu.resize(matrixSize * 4, 4);
                                        growMatrixRf.resize(matrixSize * 4, 4);
                                        growMatrixLt.resize(matrixSize * 4, 4);
                                        growMatrixLs.resize(matrixSize * 4, 4);
                                        growMatrixRt.resize(matrixSize * 4, 4);
                                        growMatrixRs.resize(matrixSize * 4, 4);

                                        growMatrixHd = globalAllign(q[1], q[0], matrixSize, growMatrixHd);
                                        growMatrixLu = globalAllign(q[1], q[3], matrixSize, growMatrixLu);
                                        growMatrixLf = globalAllign(q[1], q[2], matrixSize, growMatrixLf);
                                        growMatrixRu = globalAllign(q[1], q[4], matrixSize, growMatrixRu);
                                        growMatrixRf = globalAllign(q[1], q[5], matrixSize, growMatrixRf);
                                        growMatrixLt = globalAllign(q[1], q[6], matrixSize, growMatrixLt);
                                        growMatrixLs = globalAllign(q[1], q[7], matrixSize, growMatrixLs);
                                        growMatrixRt = globalAllign(q[1], q[8], matrixSize, growMatrixRt);
                                        growMatrixRs = globalAllign(q[1], q[9], matrixSize, growMatrixRs);

                                        matrixSize++;
                                }
                                intro++;
                        }
                        //RETURNS THE CALIBRATION OFFSETS
                        avg2[0] = returnAllignedQuaternion(growMatrixHd);
                        avg2[3] = returnAllignedQuaternion(growMatrixLu);
                        avg2[2] = returnAllignedQuaternion(growMatrixLf);
                        avg2[4] = returnAllignedQuaternion(growMatrixRu);
                        avg2[5] = returnAllignedQuaternion(growMatrixRf);
                        avg2[6] = returnAllignedQuaternion(growMatrixLt);               /// BREAKING
HERE DUE TO 'NAN' VALUES
                        avg2[7] = returnAllignedQuaternion(growMatrixLs);
                        avg2[8] = returnAllignedQuaternion(growMatrixRt);
                        avg2[9] = returnAllignedQuaternion(growMatrixRs);
                }
                cout << "Now start client..." << endl;

                //MAIN LOOP AFTER CALIBRATION
                //while (1 && frame<1500) {
                while (1) {

                        Sleep(100);
                        // CALLS FUNCTION TO GET DATA FROM THE SUIT (FROM SE SDK)
                        // THEN WRITES THIS STRING DATA TO FILE, WHICH WILL BE PARSED LATER
FOR VALUES
                        st = " ";
                        st = (char*)SEPollChar(true);

                        outputFile.open(".\\test.txt", std::ofstream::out | std::ofstream::trunc);
                        i = 0;

                        for (i = 0; i < 10500; i++) {
                                outputFile << st[i];
                        }
                        outputFile.close();

                        // GETS NUMERICAL DATA FROM STRING RETURNED BY SEPollChar()
                        getQuaternions();
                        // SAVES THE DATA INTO RELAVENT QUATERNION STRUCTURES (q[0] ---> q[9])
                        updateQuats();
                        // OFFSET VALUES AS PER CALIBRATION
                        meanRef();
                        offsetQuaternions();

                        // SET THE VALUES FROM ROTATIONAL DATA TO VALUES[] ARRAY
                        setValues();
                        // CREATE THE PIPE
                        pipe = CreateNamedPipe(
                                _T("\\\\.\\pipe\\my_pipe"), // NAME
                                PIPE_ACCESS_OUTBOUND, // 1-WAY (SEND ONLY)
                                PIPE_TYPE_BYTE, // SEND AS BITE STREAM
                                1, // ONLY 1 INSTANCE OF PIPE ALLOWED
                                0, // NO OUTBOOUND BUFFER
                                0, // NO INBOUND BUFFER
                                0, // DEFAULT WAIT
                                NULL // DEFAULT SECURITY
                        );

                        if (pipe == NULL || pipe == INVALID_HANDLE_VALUE) {
                                cout << "Pipe not created." << endl;
                                system("pause");
                                return 1;
                        }
                        // CHECK TO SEE CLIENT HAS CONNECTED TO PIPE
                        result = ConnectNamedPipe(pipe, NULL);
                        if (!result) {
                                CloseHandle(pipe); // close the pipe
                        }
                        numBytesWritten = 0;

                        //   ********** SEND DATA TO PIPE **********    ---------------->
                        result = WriteFile(pipe, &values[0], sizeof(values), &numBytesWritten, NULL);
                        // RESULT WILL BE NULL IF THE DATA HAS FAILED TO SEND OR NON ZERO
IF SUCCESFUL
                        CloseHandle(pipe);
                }
        }
        return 0;
}
