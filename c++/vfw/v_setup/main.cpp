#define GLFW_INCLUDE_VULKAN
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// os specific window management
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <optional>
#include <set>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>
#include <array>
#include <random>

#define VK_FLAGS_NONE 0
//max particles
#define PARTICLE_COUNT 100000
//stage particle counts
#define INITIAL_PARTICLE_COUNT 10000
#define STAGE_2_PARTICLE_COUNT 10000
//stage frame counts
#define FIRST_FIREWORK_FRAMES 6000
#define SECOND_FIREWORK_FRAMES 12000
#define THIRD_FIREWORK_FRAMES 18000
#define FOURTH_FIREWORK_FRAMES 24000
//firework types
#define TYPE_1 1.0
#define TYPE_2 2.0
#define TYPE_3 3.0
#define TYPE_4 4.0
#define TYPE_5 5.0

//default window size
const int WIDTH = 800;
const int HEIGHT = 600;
//max number of frames in swap chain
const int MAX_FRAMES_IN_FLIGHT = 2;
//count frames and active particles
int frameCounter = 0;
uint32_t activeParticleCount = 0;


//struct to hold the model, view and projection matrices
struct UniformBufferObject {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;
};


//structure to store particle data
struct Particle {
	glm::vec4 pos;	//particle position
	glm::vec4 col;	//particle colour
	glm::vec4 vel;	//particle velocity (w coord is used as a firework type flag)
	//glm::float32 mass;
	//populates the vertex data
	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Particle);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		return bindingDescription;
	}
	//fill vertex attribute structs (position, texture coord and colour)
	static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = {};
		//position data
		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributeDescriptions[0].offset = offsetof(Particle, pos);
		//colour data
		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributeDescriptions[1].offset = offsetof(Particle, col);
		//velocity data
		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attributeDescriptions[2].offset = offsetof(Particle, vel);
		return attributeDescriptions;
	}
};


//particle array
std::vector<Particle> particles(PARTICLE_COUNT);


//indices of vertices to use
const std::vector<uint16_t> indices = {
	0,1,2,3,4,5,6,7,8,9,10,11
};


//grab a large amount of preset validation layers
const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};


//if the application is compiled & run in debug mode then enable validation layer support
//else do not for faster performance when not debugging
#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif
//extensions for the vk khr swapchain
const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};


/*create debug utils messenger ext => debug callback function is extension fucntion therefore not automatically
loaded, this means that we must look up the address ourselves at runtime*/
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	//func will be null if the function could not be loaded, therefore there is some form of error
	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
	}
	else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}


/*destroy debut utils messenger ext => like the create function, we must also load this extension function*/
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr) {
		func(instance, debugMessenger, pAllocator);
	}
}


/*queue family indices => stores the indices of queue families*/
struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;
	std::optional<uint32_t> computeFamily;
	/*check that the requried families have values*/
	bool isComplete() {
		return graphicsFamily.has_value() && presentFamily.has_value() && computeFamily.has_value();
	}
};


/*usedto check compatibility between the required extensions and the window surface*/
struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};


class HelloTriangleApplication {
public:
	void run() {
		//generate the window
		initWindow();
		//configure vulkan
		initVulkan();
		//render loop
		mainLoop();
		//free resources
		cleanup();
	}

private:
	//the window
	GLFWwindow * mainWindow;
	//the instance of vulkan
	VkInstance vulkanInstance;
	//the debug messenger - used for handling validation layer callback debug messages
	VkDebugUtilsMessengerEXT debugMessenger;
	//the abstraction of the window
	VkSurfaceKHR surface;
	//the gpu
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	//the gpu interface
	VkDevice device;
	//the graphics and presentation queues
	VkQueue graphicsQueue;
	VkQueue presentQueue;
	//swap chain - used for handling buffer switching
	VkSwapchainKHR swapChain;
	//images in the swap chain 
	std::vector<VkImage> swapChainImages;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;
	std::vector<VkImageView> swapChainImageViews;
	std::vector<VkFramebuffer> swapChainFramebuffers;
	//render pass, pipeline layout and the graphics pipeline
	VkRenderPass renderPass;
	VkDescriptorSetLayout descriptorSetLayout;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;
	//commands that are used to outline calculations on the physical device
	VkCommandPool commandPool;
	std::vector<VkCommandBuffer> commandBuffers;
	//buffer for vertex positions and colours
	VkBuffer vertexBuffer;
	//memory for the vertex vuffer
	VkDeviceMemory vertexBufferMemory;
	//buffer for vertex indices
	VkBuffer indexBuffer;
	//memory for the index buffer
	VkDeviceMemory indexBufferMemory;
	//uniform buffers
	std::vector<VkBuffer> uniformBuffers;
	std::vector<VkDeviceMemory> uniformBuffersMemory;
	//comute uniform buffers
	std::vector<VkBuffer> computeUniformBuffers;
	std::vector<VkDeviceMemory> computeUniformBuffersMemory;
	//descriptor pool & sets
	VkDescriptorPool descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;
	//if an image in the swapchain is available
	std::vector<VkSemaphore> imageAvailableSemaphores;
	//if an image has finished being renderred
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inFlightFences;
	std::vector<VkSemaphore> graphicsSemaphores;
	size_t currentFrame = 0;
	//used as a fail safe to handle all cases where the window has been resized
	bool framebufferResized = false;
	//create texture related params
	VkImageView textureImageView;
	VkSampler textureSampler;
	VkImage textureImage;
	VkDeviceMemory textureImageMemory;
	//create depth params
	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView depthImageView;
	//compute pipeline stuff
	VkQueue computeQueue;		//queue for compute shader
	VkCommandPool computeCommandPool; //command pool for compute shader
	std::vector <VkCommandBuffer> computeCommandBuffers; //command buffer for compute shader
	VkDescriptorSetLayout computeDescriptorSetLayout; //compute shader binding layout
	std::vector <VkDescriptorSet> computeDescriptorSets;	//compute shader bindings
	VkPipelineLayout computePipelineLayout; //layout of the compute pipeline
	VkDescriptorPool computeDescriptorPool;
	VkPipeline computePipeline; //compute pipeline
	//queue family indices
	uint32_t graphicsFamilyQueueIndex;
	uint32_t computeFamilyQueueIndex;


	/*inits the application window*/
	void initWindow() {
		//inits the window using glfw
		glfwInit();
		//do not use opengl context
		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		//give parameters
		mainWindow = glfwCreateWindow(WIDTH, HEIGHT, "Ed's Window", nullptr, nullptr);
		glfwSetWindowUserPointer(mainWindow, this);
		glfwSetFramebufferSizeCallback(mainWindow, framebufferResizeCallback);
	}


	/*frame buffer resize callback => auto called on window resized event*/
	static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
		auto app = reinterpret_cast<HelloTriangleApplication*>(glfwGetWindowUserPointer(window));
		app->framebufferResized = true;
	}


	/*init vulkan => create and initialise instance of vulkan*/
	void initVulkan() {
		//initialise particles
		particleStage1();
		//create the vulkan instance
		createInstance();
		//setup the validation layer callback messages
		setupDebugMessenger();
		//create the window
		createSurface();
		//find suitable gpu
		pickPhysicalDevice();
		//create interface to interact with gpu
		createLogicalDevice();
		//create the swap chain
		createSwapChain();
		//create the image views
		createImageViews();
		//create the render pass
		createRenderPass();
		//descriptor sets for bindings
		createDescriptorSetLayout();
		createComputeDescriptorSetLayout();
		//create the pipelines
		createGraphicsPipeline();
		createComputePipeline();
		//create the command pools
		createCommandPool();
		createComputeCommandPool();
		//create the depth buffer resources
		createDepthResources();
		//create the frame buffers
		createFramebuffers();
		//create the texture image & image view
		createTextureImage();
		createTextureImageView();
		createTextureSampler();
		//create particle data buffers
		createParticleBuffer();
		createIndexBuffer();
		//create the uniform buffers
		createUniformBuffers();
		createComputeUniformBuffers();
		//create descriptor pools & sets
		createDescriptorPool();
		createComputeDescriptorPool();
		createDescriptorSets();
		createComputeDescriptorSets();
		//create the command buffers
		createCommandBuffers();
		createComputeCommandBuffers();
		//create semaphores and fences for internal synchronisation
		createSyncObjects();
	}


	/*main loop => waits for window to be closed, meanwhile draws frames*/
	void mainLoop() {
		//while window not closed
		while (!glfwWindowShouldClose(mainWindow)) {
			glfwPollEvents(); //get events
			drawFrame(); //draw images
			frameCounter++;
			if (frameCounter == FIRST_FIREWORK_FRAMES) {
				sendNewParticlesToDevice(1);
			}
			else if (frameCounter == SECOND_FIREWORK_FRAMES) {
				sendNewParticlesToDevice(2);
			}
			else if (frameCounter == THIRD_FIREWORK_FRAMES) {
				sendNewParticlesToDevice(3);
			}
			else if (frameCounter == FOURTH_FIREWORK_FRAMES) {
				sendNewParticlesToDevice(4);
			}
			else if (frameCounter > 30000) {
				frameCounter = 0;
				sendNewParticlesToDevice(5);
			}
		}
		//wait for the logical device to finish operations before executing
		vkDeviceWaitIdle(device);
	}


	/*send new particles to device => updates device particles for each stage of fireworks*/
	void sendNewParticlesToDevice(int stage){
		//generate new particles
		switch (stage) {
		case 1: {particleStage2(); break; }
		case 2: {particleStage3(); break; }
		case 3: {particleStage4(); break; }
		case 4: {particleStage5(); break; }
		case 5: {particleStage1(); break; }
		}
		//free the command buffers
		vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
		vkFreeCommandBuffers(device, computeCommandPool, static_cast<uint32_t>(computeCommandBuffers.size()), computeCommandBuffers.data());
		//cleanup vertex buffer
		vkDestroyBuffer(device, vertexBuffer, nullptr);
		vkFreeMemory(device, vertexBufferMemory, nullptr);
		//recreate vertex / compute and render command buffers
		createParticleBuffer();
		createCommandBuffers();
		createComputeCommandBuffers();
	}


	/*cleanup swap chain => contains all cleanup functions related to creation of a new swap chain*/
	void cleanupSwapChain() {
		//destroy the depth stuff
		vkDestroyImageView(device, depthImageView, nullptr);
		vkDestroyImage(device, depthImage, nullptr);
		vkFreeMemory(device, depthImageMemory, nullptr);
		//destroy all frame buffers
		for (auto framebuffer : swapChainFramebuffers) {
			vkDestroyFramebuffer(device, framebuffer, nullptr);
		}
		//free command buffers
		vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
		vkFreeCommandBuffers(device, computeCommandPool, static_cast<uint32_t>(computeCommandBuffers.size()), computeCommandBuffers.data());
		//destroy pipelines and layouts
		vkDestroyPipeline(device, graphicsPipeline, nullptr);
		vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
		vkDestroyPipeline(device, computePipeline, nullptr);
		vkDestroyPipelineLayout(device, computePipelineLayout, nullptr);
		//destroy render pass
		vkDestroyRenderPass(device, renderPass, nullptr);
		//destroy all image views
		for (auto imageView : swapChainImageViews) {
			vkDestroyImageView(device, imageView, nullptr);
		}
		vkDestroySwapchainKHR(device, swapChain, nullptr);
	}


	/*cleanup => frees memory ascociated with the program on exit*/
	void cleanup() {
		//first delete old swap chain
		cleanupSwapChain();
		//destroy the texture sampler and imageview
		vkDestroySampler(device, textureSampler, nullptr);
		vkDestroyImageView(device, textureImageView, nullptr);
		//destroy the texture image and free memory
		vkDestroyImage(device, textureImage, nullptr);
		vkFreeMemory(device, textureImageMemory, nullptr);
		//destroy the render descriptor pool
		vkDestroyDescriptorPool(device, descriptorPool, nullptr);
		//destroy mvp descriptor set layout
		vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
		//destroy the compute descriptor pool
		vkDestroyDescriptorPool(device, computeDescriptorPool, nullptr);
		//destroy compute descriptor set
		vkDestroyDescriptorSetLayout(device, computeDescriptorSetLayout, nullptr);
		//destroy the uniform buffers & memory
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			vkDestroyBuffer(device, uniformBuffers[i], nullptr);
			vkFreeMemory(device, uniformBuffersMemory[i], nullptr);
			vkDestroyBuffer(device, computeUniformBuffers[i], nullptr);
			vkFreeMemory(device, computeUniformBuffersMemory[i], nullptr);
		}
		//cleanup vertex buffer
		vkDestroyBuffer(device, vertexBuffer, nullptr);
		vkFreeMemory(device, vertexBufferMemory, nullptr);
		//cleanup vertex index buffer
		vkDestroyBuffer(device, indexBuffer, nullptr);
		vkFreeMemory(device, indexBufferMemory, nullptr);
		//detroy the sync objects
		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
			vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
			vkDestroySemaphore(device, graphicsSemaphores[i], nullptr);
			vkDestroyFence(device, inFlightFences[i], nullptr);
		}
		//destroy the command pools, device and surface
		vkDestroyCommandPool(device, commandPool, nullptr);
		vkDestroyCommandPool(device, computeCommandPool, nullptr);
		vkDestroyDevice(device, nullptr);
		vkDestroySurfaceKHR(vulkanInstance, surface, nullptr);
		//cleanup debug messenger callback if validation layers were requested
		if (enableValidationLayers) {
			DestroyDebugUtilsMessengerEXT(vulkanInstance, debugMessenger, nullptr);
		}
		//free the instance memory, window and finally terminate glfw handler
		vkDestroyInstance(vulkanInstance, nullptr);
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
	}


	/*recreate swap chain => recreates the swap chain (on window resize for example)*/
	void recreateSwapChain() {
		int width = 0, height = 0;
		//handle window minimisation
		while (width == 0 || height == 0) {
			glfwGetFramebufferSize(mainWindow, &width, &height);
			glfwWaitEvents();
		}
		//dont touch resources that are still in use
		vkDeviceWaitIdle(device);
		//delelte current chain info
		cleanupSwapChain();
		//make a new chain
		createSwapChain();
		createImageViews();
		createRenderPass();
		createGraphicsPipeline();
		createComputePipeline();
		createDepthResources();
		createFramebuffers();
		createCommandBuffers();
		createComputeCommandBuffers();
	}


	/*create instance => creates the instance of vulkan*/
	void createInstance() {
		//check if unavailable validation layers were requested
		if (enableValidationLayers && !checkValidationLayerSupport()) {
			throw std::runtime_error("validation layers requested, but not available!");
		}
		//fill application information struct
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "Hello Triangle";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "No Engine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;
		// add application information struct to instance creation information struct
		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		//get extenstions that are required and add them to the creation information struct
		auto extensions = getRequiredExtensions();
		createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		createInfo.ppEnabledExtensionNames = extensions.data();
		//if we do not require validation layers, then the enabled layer count should == 0
		//else, the enablerd layer count should be the number of layers within the validation layer
		//and we should add the requested layers to the creation information struct
		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else {
			createInfo.enabledLayerCount = 0;
		}
		//handle errors creating the instance with the given params
		if (vkCreateInstance(&createInfo, nullptr, &vulkanInstance) != VK_SUCCESS) {
			throw std::runtime_error("failed to create instance!");
		}
	}


	/*set up debug messnger => sets up the system that relays validation layer callbacks*/
	void setupDebugMessenger() {
		//if we do not require debug callbacks then return
		if (!enableValidationLayers) return;
		//structure that defines what specifics of debug callback messages we require
		VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		//define what level of messages we would like to receive
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		//define what types of messages we would like to receive
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		//pointer to the callback function
		createInfo.pfnUserCallback = debugCallback;
		createInfo.pUserData = nullptr; // Optional
		//create the extrension object
		if (CreateDebugUtilsMessengerEXT(vulkanInstance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
			throw std::runtime_error("failed to set up debug messenger!");
		}
	}


	/*create surface => create platform agnostic window with glfw*/
	void createSurface() {
		if (glfwCreateWindowSurface(vulkanInstance, mainWindow, nullptr, &surface) != VK_SUCCESS) {
			throw std::runtime_error("failed to create window surface!");
		}
	}


	/*pick physical device => picks the gpu to use*/
	void pickPhysicalDevice() {
		//get the number of available physical devices (GPUs etc.)
		uint32_t deviceCount = 0;
		vkEnumeratePhysicalDevices(vulkanInstance, &deviceCount, nullptr);
		//handle case that there are no devices with vulkan support
		if (deviceCount == 0) {
			throw std::runtime_error("failed to find GPUs with Vulkan support!");
		}
		//if there are available devices, get the data and save into devices struct
		std::vector<VkPhysicalDevice> devices(deviceCount);
		vkEnumeratePhysicalDevices(vulkanInstance, &deviceCount, devices.data());
		//select the first available & compatible device
		for (const auto& device : devices) {
			if (isDeviceSuitable(device)) {
				physicalDevice = device;
				break;
			}
		}
		//handle the case that there are no suitable devices
		if (physicalDevice == VK_NULL_HANDLE) {
			throw std::runtime_error("failed to find a suitable GPU!");
		}
	}


	/*create logical device => creates the interface to control the physical device (GPU)*/
	void createLogicalDevice() {
		//first define the queues for the device
		QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };
		//default queue priority to 1 (there is only one anyway)
		float queuePriority = 1.0f;
		for (uint32_t queueFamily : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo queueCreateInfo = {};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = queueFamily;
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			queueCreateInfos.push_back(queueCreateInfo);
		}
		//instantiate device feature struct and create info struct
		VkPhysicalDeviceFeatures deviceFeatures = {};
		deviceFeatures.samplerAnisotropy = VK_TRUE;

		VkDeviceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
		createInfo.pQueueCreateInfos = queueCreateInfos.data();
		createInfo.pEnabledFeatures = &deviceFeatures;
		createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
		createInfo.ppEnabledExtensionNames = deviceExtensions.data();
		//add validation layers if needed
		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else {
			createInfo.enabledLayerCount = 0;
		}
		//check for errors
		if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
			throw std::runtime_error("failed to create logical device!");
		}
		//get queue handles for graphics and presentation queues
		vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
		vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
		vkGetDeviceQueue(device, indices.computeFamily.value(), 0, &computeQueue);
	}


	/*create swap chain => creates the buffers used to hold images and their rules/parameters*/
	void createSwapChain() {
		//query whether the GPU supports the swapchain 
		SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);
		//choose the surface format, presentation mode and swap extent rules
		VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
		VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
		VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);
		//decide the number of images to be held in the swap chain
		//add one to the minimum to prevent having to wait for the driver to complete internal operations
		uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
		//make sure that this value is not above the maximum
		if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
			imageCount = swapChainSupport.capabilities.maxImageCount;
		}
		//create struct to hold the swapchain creation info
		VkSwapchainCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = surface;
		//populate the stuct
		createInfo.minImageCount = imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		//decide how to handle swap chain images across multiple queue families (graphics and presentation)
		QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
		uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };
		//if they are not the same allow concurrent operations
		if (indices.graphicsFamily != indices.presentFamily) {
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		//if they are the same then it can be exclusive (as there is only one)
		else {
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		}
		//create pre-transform on the image (not required in this case to just use the current transform)
		createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
		//should the window have transparency? no so set to opaque
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		//set clipping info
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;
		//attempt to create the swap chain
		if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
			//if there is an error, output message for debug
			throw std::runtime_error("failed to create swap chain!");
		}
		//get the amount of images in the swapchain
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
		//resize image vector
		swapChainImages.resize(imageCount);
		//get the handles to the swapchain images
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());
		//finally set the format to the surface format and the extent to the current extent
		swapChainImageFormat = surfaceFormat.format;
		swapChainExtent = extent;
	}


	/*create image views => creates views into the images...how to access and what part to access*/
	void createImageViews() {
		swapChainImageViews.resize(swapChainImages.size());

		for (uint32_t i = 0; i < swapChainImages.size(); i++) {
			swapChainImageViews[i] = createImageView(swapChainImages[i], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT);
		}
	}

	/*create image view => generic image creation function*/
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags) {
		//fill out view information struct for creation
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = format;
		viewInfo.subresourceRange.aspectMask = aspectFlags;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = 1;
		//create the view
		VkImageView imageView;
		if (vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS) {
			throw std::runtime_error("failed to create texture image view!");
		}
		return imageView;
	}


	/*create texture image view => for the head texture, not used in final code*/
	void createTextureImageView() {
		textureImageView = createImageView(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
	}

	/*create render pass => creates object that stores how many color and depth buffers there will be,
	how many samples to use for each of them and how their contents should be handled throughout the
	rendering operations*/
	void createRenderPass() {
		//create single colour buffer attachment based on the images in the swap chain
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.format = swapChainImageFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		//create single depth buffer attachment 
		VkAttachmentDescription depthAttachment = {};
		depthAttachment.format = findDepthFormat();
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		//colour attachment and depth attachment refs
		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		VkAttachmentReference depthAttachmentRef = {};
		depthAttachmentRef.attachment = 1;
		depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		//define the subpass & dependencies
		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;
		subpass.pDepthStencilAttachment = &depthAttachmentRef;
		//wait for the colour attachment output bit for the subpass
		VkSubpassDependency dependency = {};
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		std::array<VkAttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };
		//create the renderpass info struct to create the renderpass object
		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		renderPassInfo.pAttachments = attachments.data();
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;
		renderPassInfo.dependencyCount = 1;
		renderPassInfo.pDependencies = &dependency;
		//create the renderpass object
		if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS) {
			throw std::runtime_error("failed to create render pass!");
		}
	}


	/*create graphics pipeline => creates the pipeline for renderring graphics*/
	void createGraphicsPipeline() {
		//get the data for the vert/frag shaders from file
		auto vertShaderCode = readFile("shaders/vert.spv");
		auto fragShaderCode = readFile("shaders/frag.spv");
		//create modules from the data
		VkShaderModule vertShaderModule = createShaderModule(vertShaderCode);
		VkShaderModule fragShaderModule = createShaderModule(fragShaderCode);
		//fill in the vertex shader
		VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
		vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vertShaderStageInfo.module = vertShaderModule;
		vertShaderStageInfo.pName = "main";
		//fill in the fragment shader
		VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
		fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		fragShaderStageInfo.module = fragShaderModule;
		fragShaderStageInfo.pName = "main";
		//add both shaders to shader stages array
		VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };
		//set vertex input data params
		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		//specify that the vertices positions and attributes come in through buffer
		auto bindingDescription = Particle::getBindingDescription();
		auto attributeDescriptions = Particle::getAttributeDescriptions();
		vertexInputInfo.vertexBindingDescriptionCount = 1;
		vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
		vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
		//set input assembly data
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		//set to triangle list
		inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		inputAssembly.primitiveRestartEnable = VK_FALSE;
		//set viewport to default => { 0 -> 1 }
		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)swapChainExtent.width;
		viewport.height = (float)swapChainExtent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;
		//set scissor to entire swapchain extent (no area removed from whole image in swapchain)
		VkRect2D scissor = {};
		scissor.offset = { 0, 0 };
		scissor.extent = swapChainExtent;
		//set viewport count to 1
		VkPipelineViewportStateCreateInfo viewportState = {};
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;
		//set rasteriser params
		VkPipelineRasterizationStateCreateInfo rasterizer = {};
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizer.depthClampEnable = VK_FALSE;
		rasterizer.rasterizerDiscardEnable = VK_FALSE;
		rasterizer.polygonMode = VK_POLYGON_MODE_POINT;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = VK_CULL_MODE_NONE;
		rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterizer.depthBiasEnable = VK_FALSE;
		//multisampling used for anti aliasing, where multiple fragments map to the same pixel
		//for simplicity, keep disabled
		VkPipelineMultisampleStateCreateInfo multisampling = {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		//depth stencil create info
		VkPipelineDepthStencilStateCreateInfo depthStencil = {};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_TRUE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
		depthStencil.depthBoundsTestEnable = VK_FALSE;
		depthStencil.stencilTestEnable = VK_FALSE;
		//colour blending for attached framebuffer
		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.blendEnable = VK_FALSE;
		//how all frame buffer arrays should be blended
		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY;
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f;
		colorBlending.blendConstants[1] = 0.0f;
		colorBlending.blendConstants[2] = 0.0f;
		colorBlending.blendConstants[3] = 0.0f;
		//pipeline layout info
		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
		//create pipeline layout 
		if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
			throw std::runtime_error("failed to create pipeline layout!");
		}
		//fill in the info for the graphics pipeline
		VkGraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pDepthStencilState = &depthStencil;
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.layout = pipelineLayout;
		pipelineInfo.renderPass = renderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
		//create the graphics pipeline
		if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS) {
			throw std::runtime_error("failed to create graphics pipeline!");
		}
		//can now destroy these modules as they have been used to instantiate new memory in the pipeline
		vkDestroyShaderModule(device, fragShaderModule, nullptr);
		vkDestroyShaderModule(device, vertShaderModule, nullptr);
	}


	/*create compute pipeline => creates the compute stage pipeline for particle GPU calculations*/
	void createComputePipeline(){
		//get the data for the vert/frag shaders from file
		auto compShaderCode = readFile("shaders/comp.spv");
		//create modules from the data
		VkShaderModule compShaderModule = createShaderModule(compShaderCode);
		//fill in the vertex shader
		VkPipelineShaderStageCreateInfo compShaderStageInfo = {};
		compShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		compShaderStageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
		compShaderStageInfo.module = compShaderModule;
		compShaderStageInfo.pName = "main";
		//add compute shader to shader stages array
		VkPipelineShaderStageCreateInfo shaderStages[] = { compShaderStageInfo };
		//pipeline layout info
		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &computeDescriptorSetLayout;
		//create pipeline layout 
		if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &computePipelineLayout) != VK_SUCCESS) {
			throw std::runtime_error("failed to create pipeline layout!");
		}
		//fill in the info for the graphics pipeline
		VkComputePipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
		pipelineInfo.layout = computePipelineLayout;
		pipelineInfo.flags = 0;
		pipelineInfo.stage = shaderStages[0];
		//create the graphics pipeline
		if (vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &computePipeline) != VK_SUCCESS) {
			throw std::runtime_error("failed to create graphics pipeline!");
		}
		//can now destroy these modules as they have been used to instantiate new memory in the pipeline
		vkDestroyShaderModule(device, compShaderModule, nullptr);
	}


	/*create frame buffers => creates one frame buffer per swap chain image*/
	void createFramebuffers() {
		//resize vector to hold the frame buffers
		swapChainFramebuffers.resize(swapChainImageViews.size());
		//assign from the swap chain images
		for (size_t i = 0; i < swapChainImageViews.size(); i++) {
			std::array<VkImageView, 2> attachments = {
				swapChainImageViews[i],
				depthImageView
			};
			//set the info for the frame buffers
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			framebufferInfo.pAttachments = attachments.data();
			framebufferInfo.width = swapChainExtent.width;
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;
			//create the frame buffer
			if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create framebuffer!");
			}
		}
	}


	/*create image => creates image used for texturing*/
	void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory) {
		//fill image creation struct
		VkImageCreateInfo imageInfo = {};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = width;
		imageInfo.extent.height = height;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.format = format;
		imageInfo.tiling = tiling;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = usage;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		//create the image
		if (vkCreateImage(device, &imageInfo, nullptr, &image) != VK_SUCCESS) {
			throw std::runtime_error("failed to create image!");
		}
		//get mem requirements
		VkMemoryRequirements memRequirements;
		vkGetImageMemoryRequirements(device, image, &memRequirements);
		//allocate the memory
		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

		if (vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate image memory!");
		}
		//bind the image
		vkBindImageMemory(device, image, imageMemory, 0);
	}


	/*create texture image*/
	void createTextureImage() {
		//load the speicific texture image
		int texWidth, texHeight, texChannels;
		stbi_uc* pixels = stbi_load("textures/texture.jpg", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
		VkDeviceSize imageSize = texWidth * texHeight * 4;
		//if there has been an error, output to debug
		if (!pixels) {
			throw std::runtime_error("failed to load texture image!");
		}
		//create buffer for image
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
		//copy data into buffer
		void* data;
		vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
		memcpy(data, pixels, static_cast<size_t>(imageSize));
		vkUnmapMemory(device, stagingBufferMemory);
		//free the original array now it is in the buffer
		stbi_image_free(pixels);
		//create the image
		createImage(texWidth, texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, textureImage, textureImageMemory);

		transitionImageLayout(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyBufferToImage(stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
		transitionImageLayout(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		//destroy the buffer and free mem
		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}


	/*create texture sampler => creates a sampler for texture renderring*/
	void createTextureSampler() {
		//fill out texture sampler struct info
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		//create the sampler
		if (vkCreateSampler(device, &samplerInfo, nullptr, &textureSampler) != VK_SUCCESS) {
			throw std::runtime_error("failed to create texture sampler!");
		}
	}


	/*create buffer => generic buffer creation function*/
	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		//set size, usage and sharing mode
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		//now create the buffer
		if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
			throw std::runtime_error("failed to create buffer!");
		}
		//now allocate memory to the buffer
		//first get the requirements, size, allignment and type
		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(device, buffer, &memRequirements);
		//determine the memory type and allocate it 
		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);
		//try allocate
		if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate buffer memory!");
		}
		//if allocation was successful, bind to the device memory
		vkBindBufferMemory(device, buffer, bufferMemory, 0);
	}


	/*particle stage 1 => initialises the particles for the fireworks*/
	void particleStage1(){
		//set active particle count 
		activeParticleCount = INITIAL_PARTICLE_COUNT;
		//init uniform random distribution
		std::random_device rand_dev;
		std::mt19937 g(rand_dev());
		std::uniform_real_distribution<> x(-0.01, 0.01);
		//init particle data
		for (int i = 0; i < activeParticleCount; i++) {
			particles[i].pos = glm::vec4(x(g), x(g), -2.5 + x(g), 0);
			particles[i].vel = glm::vec4(0, 0, 0.001, TYPE_1);
			particles[i].col = glm::vec4(0.9 + x(g) * 20, 0.69 + x(g) * 20, 0.1 + x(g) * 20, 1.0);
		}
	}


	/*particle stage 2 => sets up the particle array for second firework stage*/
	void particleStage2(){
		//set active particle count 
		activeParticleCount = STAGE_2_PARTICLE_COUNT;
		//init uniform random distribution
		std::random_device rand_dev;
		std::mt19937 g(rand_dev());
		std::uniform_real_distribution<> x(-0.01, 0.01);
		//init particle data
		for (int i = 0; i < activeParticleCount; i++) {
			particles[i].pos = glm::vec4(x(g), x(g), -2.5 + x(g), 0);
			particles[i].vel = glm::vec4(0, 0, 0.001, TYPE_2);
			particles[i].col = glm::vec4(0.9 + x(g) * 20, 0.9 + x(g) * 20, 0.56 + x(g) * 20, 1.0);
		}
	}


	/*particle stage 3 => sets up the particle array for third firework stage*/
	void particleStage3(){
		//set active particle count 
		activeParticleCount = STAGE_2_PARTICLE_COUNT;
		//init uniform random distribution
		std::random_device rand_dev;
		std::mt19937 g(rand_dev());
		std::uniform_real_distribution<> x(-0.01, 0.01);
		//init particle data
		for (int i = 0; i < activeParticleCount; i++) {
			particles[i].pos = glm::vec4(x(g), x(g), -2.5 + x(g), 0);
			particles[i].vel = glm::vec4(0, 0, 0.001, TYPE_3);
			particles[i].col = glm::vec4(0.7 + x(g) * 20, 0.9 + x(g) * 20, 0.56 + x(g) * 20, 1.0);
		}
	}


	/*particle stage 4 => sets up the particle array for fourth firework stage*/
	void particleStage4(){
		//set active particle count 
		activeParticleCount = STAGE_2_PARTICLE_COUNT;
		//init uniform random distribution
		std::random_device rand_dev;
		std::mt19937 g(rand_dev());
		std::uniform_real_distribution<> x(-0.01, 0.01);
		//init particle data
		for (int i = 0; i < activeParticleCount; i++) {
			particles[i].pos = glm::vec4(x(g), x(g), -2.5 + x(g), 0);
			particles[i].vel = glm::vec4(0, 0, 0.001, TYPE_4);
			particles[i].col = glm::vec4(0.9 + x(g) * 20, 0.9 + x(g) * 20, 0.9 + x(g) * 20, 1.0);
		}
	}


	/*particle stage 5 => sets up the particle array for fith firework stage*/
	void particleStage5(){
		//set active particle count 
		activeParticleCount = STAGE_2_PARTICLE_COUNT;
		//init uniform random distribution
		std::random_device rand_dev;
		std::mt19937 g(rand_dev());
		std::uniform_real_distribution<> x(-0.01, 0.01);
		//init particle data
		for (int i = 0; i < activeParticleCount; i++) {
			particles[i].pos = glm::vec4(x(g), x(g), -2.5 + x(g), 0);
			particles[i].vel = glm::vec4(0, 0, 0.001, TYPE_5);
			particles[i].col = glm::vec4(0.9 + x(g) * 20, 0.9 + x(g) * 20, 0.9 + x(g) * 20, 1.0);
		}
	}


	/*create particle buffer => creates the buffer to pass particle data*/
	void createParticleBuffer() {
		//get vertex buffer size
		VkDeviceSize bufferSize = sizeof(particles[0]) * particles.size();
		//create the staging buffer
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
		//fill the staging buffer with data
		void* data;
		vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, particles.data(), (size_t)bufferSize);
		vkUnmapMemory(device, stagingBufferMemory);
		//create the actual buffer from the staging buffer
		createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);
		//copy the data over
		copyBuffer(stagingBuffer, vertexBuffer, bufferSize);
		//free up staging buffer memory
		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}


	/*create index buffer => creates the buffer for mesh vertex index data*/
	void createIndexBuffer() {
		//define buffer size
		VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		//create staging buffer
		createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
		//copy the data into the buffer
		void* data;
		vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, indices.data(), (size_t)bufferSize);
		vkUnmapMemory(device, stagingBufferMemory);
		//create index buffer
		createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);
		//copy staging to index buffer
		copyBuffer(stagingBuffer, indexBuffer, bufferSize);
		//destroy the staging buffer & free mem
		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}


	/*create uniform buffers => buffers that are passed each frame to the vertex/fragment shader*/
	void createUniformBuffers() {
		VkDeviceSize bufferSize = sizeof(UniformBufferObject);
		//set size for buffers and memory
		uniformBuffers.resize(swapChainImages.size());
		uniformBuffersMemory.resize(swapChainImages.size());
		//create the buffers
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffers[i], uniformBuffersMemory[i]);
		}
	}


	/*create compute stage uniform buffers => buffers that are passed each frame to the compute shader*/
	void createComputeUniformBuffers() {
		VkDeviceSize bufferSize = sizeof(UniformBufferObject);
		//set size for buffers and memory
		computeUniformBuffers.resize(swapChainImages.size());
		computeUniformBuffersMemory.resize(swapChainImages.size());
		//create the buffers
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, computeUniformBuffers[i], computeUniformBuffersMemory[i]);
		}
	}


	/*create descriptor pool*/
	void createDescriptorPool() {
		//set up desrciptor pools for uniform buffer and image sampler
		std::array<VkDescriptorPoolSize, 2> poolSizes = {};
		poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[0].descriptorCount = static_cast<uint32_t>(swapChainImages.size());
		poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[1].descriptorCount = static_cast<uint32_t>(swapChainImages.size());
		//set up the pool info
		VkDescriptorPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
		poolInfo.pPoolSizes = poolSizes.data();
		poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());
		//create the pool
		if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
			throw std::runtime_error("failed to create descriptor pool!");
		}
	}


	/*create compute descriptor pool*/
	void createComputeDescriptorPool() {
		//set up desrciptor pools for uniform buffer and image sampler
		std::array<VkDescriptorPoolSize, 2> poolSizes = {};
		poolSizes[0].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		poolSizes[0].descriptorCount = static_cast<uint32_t>(swapChainImages.size());
		poolSizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[1].descriptorCount = static_cast<uint32_t>(swapChainImages.size());

		//set up the pool info
		VkDescriptorPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
		poolInfo.pPoolSizes = poolSizes.data();
		poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());
		//create the pool
		if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &computeDescriptorPool) != VK_SUCCESS) {
			throw std::runtime_error("failed to create descriptor pool!");
		}
	}


	/*create descriptor sets*/
	void createDescriptorSets() {
		//one descriptor set for each swapchain image
		std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
		allocInfo.pSetLayouts = layouts.data();
		//allocate the sets
		descriptorSets.resize(swapChainImages.size());
		if (vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate descriptor sets!");
		}
		//populate the descriptors
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			//first uniform buffer
			VkDescriptorBufferInfo bufferInfo = {};
			bufferInfo.buffer = uniformBuffers[i];
			bufferInfo.offset = 0;
			bufferInfo.range = sizeof(UniformBufferObject);
			//then image buffer
			VkDescriptorImageInfo imageInfo = {};
			imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo.imageView = textureImageView;
			imageInfo.sampler = textureSampler;

			std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};
			//fill out the descriptor set info for both sets
			//uniform buffer
			descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[0].dstSet = descriptorSets[i];
			descriptorWrites[0].dstBinding = 0;
			descriptorWrites[0].dstArrayElement = 0;
			descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrites[0].descriptorCount = 1;
			descriptorWrites[0].pBufferInfo = &bufferInfo;
			//texture sampler
			descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[1].dstSet = descriptorSets[i];
			descriptorWrites[1].dstBinding = 1;
			descriptorWrites[1].dstArrayElement = 0;
			descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrites[1].descriptorCount = 1;
			descriptorWrites[1].pImageInfo = &imageInfo;
			//update the sets
			vkUpdateDescriptorSets(device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
		}
	}


	/*create compute descriptor sets*/
	void createComputeDescriptorSets() {
		//one descriptor set for each swapchain image
		std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), computeDescriptorSetLayout);
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = computeDescriptorPool;
		allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
		allocInfo.pSetLayouts = layouts.data();
		//allocate the sets
		computeDescriptorSets.resize(swapChainImages.size());
		if (vkAllocateDescriptorSets(device, &allocInfo, computeDescriptorSets.data()) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate descriptor sets!");
		}
		//populate the descriptors
		for (size_t i = 0; i < swapChainImages.size(); i++) {
			//shader storage buffer (particle stream)
			VkDescriptorBufferInfo bufferInfo = {};
			bufferInfo.buffer = vertexBuffer;
			bufferInfo.offset = 0;
			bufferInfo.range = VK_WHOLE_SIZE;
			//first uniform buffer
			VkDescriptorBufferInfo UBObufferInfo = {};
			UBObufferInfo.buffer = computeUniformBuffers[i];
			UBObufferInfo.offset = 0;
			UBObufferInfo.range = sizeof(UniformBufferObject);

			std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};
			//fill out the descriptor set info for both sets
			//particle buffer
			descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[0].dstSet = computeDescriptorSets[i];
			descriptorWrites[0].dstBinding = 0;
			descriptorWrites[0].dstArrayElement = 0;
			descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
			descriptorWrites[0].descriptorCount = 1;
			descriptorWrites[0].pBufferInfo = &bufferInfo;
			//uniform buffer
			descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrites[1].dstSet = computeDescriptorSets[i];
			descriptorWrites[1].dstBinding = 1;
			descriptorWrites[1].dstArrayElement = 0;
			descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrites[1].descriptorCount = 1;
			descriptorWrites[1].pBufferInfo = &UBObufferInfo;
			//update the sets
			vkUpdateDescriptorSets(device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
		}
	}


	/*begin single time commands*/
	VkCommandBuffer beginSingleTimeCommands() {
		//populate the command buffer allocate struct
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = commandPool;
		allocInfo.commandBufferCount = 1;
		//allocate the command buffers
		VkCommandBuffer commandBuffer;
		vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		//begin the command buffer
		vkBeginCommandBuffer(commandBuffer, &beginInfo);
		//return the buffer
		return commandBuffer;
	}


	/*end single time commands*/
	void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
		//end the command buffer
		vkEndCommandBuffer(commandBuffer);
		//fill submit info struct
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer;
		//submit info to the graphics queue
		vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
		vkQueueWaitIdle(graphicsQueue);
		//free the command buffers
		vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
	}


	/*copy buffer => helper function for buffer copying*/
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();
		//copy the buffer
		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
		endSingleTimeCommands(commandBuffer);
	}


	/*transition image layout => records and executes copy buffer to image function*/
	void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
		//begin the time commands
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();
		//fill the image memory barrier info struct
		VkImageMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout; //layout transition
		barrier.newLayout = newLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = image;

		if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

			if (hasStencilComponent(format)) {
				barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
			}
		}
		else {
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		}
		//the image that is affected and the specific part of the image
		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;

		VkPipelineStageFlags sourceStage;
		VkPipelineStageFlags destinationStage;
		//handle various states oy old layout
		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		}
		else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		}
		else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		}
		else {
			throw std::invalid_argument("unsupported layout transition!");
		}
		//create the pipeline barrier
		vkCmdPipelineBarrier(
			commandBuffer,
			sourceStage, destinationStage,
			0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);
		//end the time commands
		endSingleTimeCommands(commandBuffer);
	}


	/*copy buffer to image*/
	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
		//begin the time command
		VkCommandBuffer commandBuffer = beginSingleTimeCommands();
		//fill out region info struct
		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;
		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = {
			width,
			height,
			1
		};
		//call the copy buffer to image function
		vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
		//end the time command
		endSingleTimeCommands(commandBuffer);
	}


	/*find memory type => gets the details of the memory used in the vertex buffer*/
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
		//first query the types of memory available
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
		//find a memory type suitable for the buffer
		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
				return i;
			}
		}
		throw std::runtime_error("failed to find suitable memory type!");
	}


	/*create command pool => creates the commands that will be called atr runtime e.g. drawing commands*/
	void createCommandPool() {
		//command pools will submit work to the relevant queue family
		QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);
		//our pool contains drawing commands so we use the graphics queue family
		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
		//create the pool
		if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
			throw std::runtime_error("failed to create command pool!");
		}
	}


	/*create compute command pool => creates the commands that will be called atr runtime e.g. compute shader commands*/
	void createComputeCommandPool() {
		//command pools will submit work to the relevant queue family
		QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);
		//our pool contains drawing commands so we use the graphics queue family
		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = queueFamilyIndices.computeFamily.value();
		//create the pool
		if (vkCreateCommandPool(device, &poolInfo, nullptr, &computeCommandPool) != VK_SUCCESS) {
			throw std::runtime_error("failed to create command pool!");
		}
	}


	/*create depth resources => creates the resources asociated with the depth buffer*/
	void createDepthResources() {
		VkFormat depthFormat = findDepthFormat();

		createImage(swapChainExtent.width, swapChainExtent.height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory);
		depthImageView = createImageView(depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

		transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
	}


	/*find supported vkformat*/
	VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
		for (VkFormat format : candidates) {
			VkFormatProperties props;
			vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);
			//return one of the two supported formats
			if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
				return format;
			}
			else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
				return format;
			}
		}
		//if we reach here, there are none supported
		throw std::runtime_error("failed to find supported format!");
	}


	/*find depth format*/
	VkFormat findDepthFormat() {
		return findSupportedFormat(
			{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
			VK_IMAGE_TILING_OPTIMAL,
			VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
		);
	}


	/*determine if format has a stencil component*/
	bool hasStencilComponent(VkFormat format) {
		return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
	}


	/*create command buffers => allocates and records commands for each swap chain image*/
	void createCommandBuffers() {
		//resize the command buffer vector to the size of the swap chain frame buffer
		commandBuffers.resize(swapChainFramebuffers.size());
		//fill out command buffer info struct
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = commandPool;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();
		//allocate the command buffers
		if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate command buffers!");
		}
		//record the command buffers
		for (size_t i = 0; i < commandBuffers.size(); i++) {
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
			//begin recording command buffer
			if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS) {
				throw std::runtime_error("failed to begin recording command buffer!");
			}
			//start a render pass
			VkRenderPassBeginInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.renderPass = renderPass;
			renderPassInfo.framebuffer = swapChainFramebuffers[i];
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = swapChainExtent;

			std::array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
			clearValues[1].depthStencil = { 1.0f, 0 };

			renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
			renderPassInfo.pClearValues = clearValues.data();
			//begin the render pass
			vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
			//bind command buffers and pipeline
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

			VkBuffer vertexBuffers[] = { vertexBuffer };
			VkDeviceSize offsets[] = { 0 };
			//bind vertex buffer
			vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);
			//bind index buffer
			vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT16);
			//bind the descriptor set for each swapchain image
			vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[i], 0, nullptr);
			//draw the triangle from the vertex index buffer
			vkCmdDraw(commandBuffers[i], activeParticleCount, 1, 0, 0);
			//end render pass
			vkCmdEndRenderPass(commandBuffers[i]);
			//stop recording command buffer
			if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to record command buffer!");
			}
		}
	}


	/*create compute command buffers => allocates and records commands for each swap chain image*/
	void createComputeCommandBuffers() {
		//resize the command buffer vector to the size of the swap chain frame buffer
		computeCommandBuffers.resize(swapChainFramebuffers.size());
		//fill out command buffer info struct
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = computeCommandPool;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = (uint32_t)computeCommandBuffers.size();
		//allocate the command buffers
		if (vkAllocateCommandBuffers(device, &allocInfo, computeCommandBuffers.data()) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate command buffers!");
		}
		//record the command buffers
		for (size_t i = 0; i < computeCommandBuffers.size(); i++) {
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
			//begin recording command buffer
			if (vkBeginCommandBuffer(computeCommandBuffers[i], &beginInfo) != VK_SUCCESS) {
				throw std::runtime_error("failed to begin recording command buffer!");
			}
			//barrier to ensure that data is inacessible during buffer update
			VkBufferMemoryBarrier bufferBarrier{};
			bufferBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
			bufferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			bufferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			bufferBarrier.buffer = vertexBuffer;
			bufferBarrier.size = particles.size();
			bufferBarrier.srcAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
			bufferBarrier.dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
			//graphics and compute queues may be different families
			bufferBarrier.srcQueueFamilyIndex = graphicsFamilyQueueIndex;		
			bufferBarrier.dstQueueFamilyIndex = computeFamilyQueueIndex;			

			vkCmdPipelineBarrier(
				computeCommandBuffers[i],
				VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
				VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
				VK_FLAGS_NONE,
				0, nullptr,
				1, &bufferBarrier,
				0, nullptr);

			vkCmdBindPipeline(computeCommandBuffers[i], VK_PIPELINE_BIND_POINT_COMPUTE, computePipeline);
			vkCmdBindDescriptorSets(computeCommandBuffers[i], VK_PIPELINE_BIND_POINT_COMPUTE, computePipelineLayout, 0, 1, &computeDescriptorSets[i], 0, 0);
			// Dispatch the compute job
			vkCmdDispatch(computeCommandBuffers[i], PARTICLE_COUNT, 1, 1);

			bufferBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;								
			bufferBarrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;						
			bufferBarrier.buffer = vertexBuffer;
			bufferBarrier.size = particles.size();
			bufferBarrier.srcQueueFamilyIndex = computeFamilyQueueIndex;			
			bufferBarrier.dstQueueFamilyIndex = graphicsFamilyQueueIndex;

			vkCmdPipelineBarrier(
				computeCommandBuffers[i],
				VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
				VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
				VK_FLAGS_NONE,
				0, nullptr,
				1, &bufferBarrier,
				0, nullptr);

			//stop recording command buffer
			if (vkEndCommandBuffer(computeCommandBuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to record command buffer!");
			}
		}
	}


	/*create sync objects => creates the objects for synchronising operations: semaphores and fences*/
	void createSyncObjects() {
		imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);
		graphicsSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		//define the semaphores
		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		//define the fences
		VkFenceCreateInfo fenceInfo = {};
		fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			//create the semaphores and fences
			if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS ||
				vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS ||
				vkCreateFence(device, &fenceInfo, nullptr, &inFlightFences[i]) != VK_SUCCESS ||
				vkCreateSemaphore(device, &semaphoreInfo, nullptr, &graphicsSemaphores[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to create synchronization objects for a frame!");
			}
		}
	}


	/*create descriptor set layout => describes the layout of the bindings to be sent to the shader*/
	void createDescriptorSetLayout() {
		VkDescriptorSetLayoutBinding uboLayoutBinding = {};
		uboLayoutBinding.binding = 0;
		uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER; //uniform buffer object
		uboLayoutBinding.descriptorCount = 1; //one struct (3 mats inside)
		//in which shader is this referenced?
		uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		uboLayoutBinding.pImmutableSamplers = nullptr; // Optional
		 //fill out the information struct before creation
		VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
		samplerLayoutBinding.binding = 1;
		samplerLayoutBinding.descriptorCount = 1;
		samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplerLayoutBinding.pImmutableSamplers = nullptr;
		samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		//create array of bindings
		std::array<VkDescriptorSetLayoutBinding, 2> bindings = { uboLayoutBinding, samplerLayoutBinding };
		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
		layoutInfo.pBindings = bindings.data();
		//create the descriptor set layout
		if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
			throw std::runtime_error("failed to create descriptor set layout!");
		}
	}


	/*create compute descriptor set layout => describes the layout of the bindings to be sent to the shader*/
	void createComputeDescriptorSetLayout() {
		VkDescriptorSetLayoutBinding particleLayoutBinding = {};
		particleLayoutBinding.binding = 0;
		particleLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER; //storage buffer object
		particleLayoutBinding.descriptorCount = 1; //one struct 
		//in which shader is this referenced?
		particleLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
		particleLayoutBinding.pImmutableSamplers = 0;

		VkDescriptorSetLayoutBinding uboLayoutBinding = {};
		uboLayoutBinding.binding = 1;
		uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER; //uniform buffer object
		uboLayoutBinding.descriptorCount = 1; //one struct (3 mats inside)
		//in which shader is this referenced?
		uboLayoutBinding.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
		uboLayoutBinding.pImmutableSamplers = nullptr; // Optional

		//create array of bindings
		std::array<VkDescriptorSetLayoutBinding, 2> bindings = { particleLayoutBinding, uboLayoutBinding };
		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
		layoutInfo.pBindings = bindings.data();
		//create the descriptor set layout
		if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &computeDescriptorSetLayout) != VK_SUCCESS) {
			throw std::runtime_error("failed to create descriptor set layout!");
		}
	}


	/*draw frame => called from the main loop to put the triangle on screen*/
	//first acquires image from swap chain
	//then execute the command buffer with image attachment 
	//return the image to swap chain for presentation
	void drawFrame() {
		//wait for the current frame to finish
		vkWaitForFences(device, 1, &inFlightFences[currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());
		//acquire image from the swap chain
		uint32_t imageIndex;
		//check to see if the swapchain is adequate
		VkResult result = vkAcquireNextImageKHR(device, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);
		//if out of date, recreate the swap chain
		if (result == VK_ERROR_OUT_OF_DATE_KHR) {
			recreateSwapChain();
			return;
		}
		else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
			throw std::runtime_error("failed to acquire swap chain image!");
		}
		//update the mvp buffers
		updateUniformBuffer(imageIndex);
		//update compute uniform buffers
		updateComputeUniformBuffer(imageIndex);
		//info for submitting the command buffers
		VkSubmitInfo graphicsSubmitInfo = {};
		graphicsSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		//wait on the image available semaphore
		VkSemaphore waitSemaphores[] = { imageAvailableSemaphores[currentFrame] };
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		graphicsSubmitInfo.waitSemaphoreCount = 1;
		graphicsSubmitInfo.pWaitSemaphores = waitSemaphores;
		graphicsSubmitInfo.pWaitDstStageMask = waitStages;
		//submit the command buffer that binds the swap chain image we just acquired as color attachment
		graphicsSubmitInfo.commandBufferCount = 1;
		graphicsSubmitInfo.pCommandBuffers = &commandBuffers[imageIndex];
		//which semaphores to signal once the command buffers have finished executing? the renderFinishedSemaphores...
		VkSemaphore signalSemaphores[] = { graphicsSemaphores[currentFrame] };
		graphicsSubmitInfo.signalSemaphoreCount = 1;
		graphicsSubmitInfo.pSignalSemaphores = signalSemaphores;
		//submit graphics render to queue
		if (vkQueueSubmit(graphicsQueue, 1, &graphicsSubmitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
			throw std::runtime_error("failed to submit draw command buffer!");
		}
		VkSubmitInfo computeSubmitInfo = {};
		computeSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		//wait on the graphics completed semaphore bercause we dont want both executing at same time
		VkSemaphore computeWaitSemaphores[] = { graphicsSemaphores[currentFrame] };
		VkPipelineStageFlags computeWaitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		computeSubmitInfo.waitSemaphoreCount = 1;
		computeSubmitInfo.pWaitSemaphores = computeWaitSemaphores;
		computeSubmitInfo.pWaitDstStageMask = computeWaitStages;
		//submit the command buffer that binds the swap chain image we just acquired as color attachment
		computeSubmitInfo.commandBufferCount = 1;
		computeSubmitInfo.pCommandBuffers = &computeCommandBuffers[imageIndex];
		//which semaphores to signal once the command buffers have finished executing? the renderFinishedSemaphores...
		VkSemaphore computeSignalSemaphores[] = { renderFinishedSemaphores[currentFrame] };
		computeSubmitInfo.signalSemaphoreCount = 1;
		computeSubmitInfo.pSignalSemaphores = computeSignalSemaphores;
		//reset the fences
		vkResetFences(device, 1, &inFlightFences[currentFrame]);
		//submit compute stage to compute queue
		if (vkQueueSubmit(computeQueue, 1, &computeSubmitInfo, inFlightFences[currentFrame]) != VK_SUCCESS) {
			throw std::runtime_error("failed to submit compute command buffer!");
		}
		//now, submit the image back to the swapchain to be displayed on screen
		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		//which semaphores to wait on before presentation can happen
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = computeSignalSemaphores;
		//what swap chain(s) to present the image to
		VkSwapchainKHR swapChains[] = { swapChain };
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = swapChains;
		presentInfo.pImageIndices = &imageIndex;
		//as we are only using one swap chain we can use the return value of the present function
		result = vkQueuePresentKHR(presentQueue, &presentInfo);
		//if the window has been resized, recreate the swapchain
		if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized) {
			framebufferResized = false;
			recreateSwapChain();
		}
		else if (result != VK_SUCCESS) {
			throw std::runtime_error("failed to present swap chain image!");
		}
		//advance to the next frame, % ensures that we loop round when we hitmax frames in flight
		currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	}


	/*update uniform buffers => updates the values of the mvp matrix*/
	void updateUniformBuffer(uint32_t currentImage) {
		//get the time
		static auto startTime = std::chrono::high_resolution_clock::now();
		auto currentTime = std::chrono::high_resolution_clock::now();
		float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();
		//define the mvp matrices using the uniform buffer object
		//model
		UniformBufferObject ubo = {};
		ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f) * 0, glm::vec3(0.0f, 0.0f, 1.0f));
		//view
		ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		//projection
		ubo.proj = glm::perspective(glm::radians(45.0f), swapChainExtent.width / (float)swapChainExtent.height, 0.1f, 10.0f);
		ubo.proj[1][1] *= -1;
		//copy the data into the uniform buffer object
		void* data;
		vkMapMemory(device, uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
		memcpy(data, &ubo, sizeof(ubo));
		vkUnmapMemory(device, uniformBuffersMemory[currentImage]);
	}


	/*update compute uniform buffers => updates the values of the matrix that holds timing progression data*/
	void updateComputeUniformBuffer(uint32_t currentImage) {
		//get the time
		static auto startTime = std::chrono::high_resolution_clock::now();
		auto currentTime = std::chrono::high_resolution_clock::now();
		float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();
		//define the mvp matrices using the uniform buffer object
		//model matrix stores frame
		UniformBufferObject ubo = {};
		ubo.model = glm::mat4(frameCounter);
		//copy the data into the uniform buffer object
		void* data;
		vkMapMemory(device, computeUniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
		memcpy(data, &ubo, sizeof(ubo));
		vkUnmapMemory(device, computeUniformBuffersMemory[currentImage]);
	}


	/*create shader module => wraps the shader data in a vkshadermodule object*/
	VkShaderModule createShaderModule(const std::vector<char>& code) {
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = code.size();
		//recast the pointer from bytes to uint32_t
		createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
		//create the shader module
		VkShaderModule shaderModule;
		if (vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
			throw std::runtime_error("failed to create shader module!");
		}
		return shaderModule;
	}


	/*choose swap surface format => selects the colour depth to use in the swap chain*/
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
		//check for the case that the surface has no preferred format
		if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
			return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
		}
		//if not then search for the preferred format in the list
		for (const auto& availableFormat : availableFormats) {
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return availableFormat;
			}
		}
		//if not found, then just return the first one from the list
		return availableFormats[0];
	}


	/*choose swap presentation mode => chooses the conditions for swapping images to the screen*/
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {
		//we would like to use triple buffering mode
		VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;
		//prefer presentr mode 'immediate khr'  if present mode 'mailbox khr' is not available
		for (const auto& availablePresentMode : availablePresentModes) {
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return availablePresentMode;
			}
			else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
				bestMode = availablePresentMode;
			}
		}
		//ideally, return fifo khr
		return bestMode;
	}


	/*choose swap extent => chooses the resolutions of imnage in the swap chain*/
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
		//if width not set to max, then use the value in the current extent
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			return capabilities.currentExtent;
		}
		//if set to max, then get the best matching resolution for the current window size
		else {
			int width, height;
			glfwGetFramebufferSize(mainWindow, &width, &height);
			//take the actual size of the window into account
			VkExtent2D actualExtent = {
				static_cast<uint32_t>(width),
				static_cast<uint32_t>(height)
			};

			actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}


	/*query swap chain support => populates the swacpchain support details struct*/
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
		SwapChainSupportDetails details;
		//first get the surface capabilities
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);
		//get the list of supported formats
		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
		if (formatCount != 0) {
			//resize the vector of formats
			details.formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
		}
		//get the list of supported presentation modes
		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);
		if (presentModeCount != 0) {
			//resize the vector of presentation modes
			details.presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
		}
		//return the details
		return details;
	}


	/*is device suitable => checks if a GPU is suitable for the application*/
	bool isDeviceSuitable(VkPhysicalDevice device) {
		VkPhysicalDeviceFeatures supportedFeatures;
		vkGetPhysicalDeviceFeatures(device, &supportedFeatures);
		//check that the device supports the desired queues
		QueueFamilyIndices indices = findQueueFamilies(device);
		//check that the devcice supports the desired extensions
		bool extensionsSupported = checkDeviceExtensionSupport(device);
		//check that the device has adequate swap chain support 
		bool swapChainAdequate = false;
		if (extensionsSupported) {
			SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
			swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
		}
		//true if the queue family supports graphics and presentaion && extensions supported && swapchain adequate
		return indices.isComplete() && extensionsSupported && swapChainAdequate && supportedFeatures.samplerAnisotropy;
	}


	/*check device extension support => checks that the device supports the required extensions*/
	bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
		//get number of extensions
		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
		//get extension properties
		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());
		//create a set of required extensions
		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
		//check that each required extension exists within the available extensions
		for (const auto& extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}
		//as each extension is found it is removed from the set, therefore an empty set means all are supported
		return requiredExtensions.empty();
	}


	/*find queue families => retrieves the queue families asociated with the physical device (GPU)*/
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
		//struct to fill & return
		QueueFamilyIndices indices;
		//amount of queue families
		uint32_t queueFamilyCount = 0;
		//get the number of families asociated with the device
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
		//get the information about the queue families
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

		int i = 0;
		for (const auto& queueFamily : queueFamilies) {
			//check queue family for graphics support by AND'ing it with the graphics bit
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				indices.graphicsFamily = i;
				graphicsFamilyQueueIndex = i;
			}
			//check queue family for compute support by AND'ing it with the compute bit
			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT) {
				indices.computeFamily = i;
				computeFamilyQueueIndex = i;
			}

			//check foir presentation support
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

			if (queueFamily.queueCount > 0 && presentSupport) {
				indices.presentFamily = i;
			}
			//if both properties are satisfied then we will use this queue family
			if (indices.isComplete()) {
				break;
			}

			i++;
		}

		return indices;
	}


	/*get required extensions => returns required extensions for the app*/
	std::vector<const char*> getRequiredExtensions() {
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
		//glfw extensions are always required
		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
		//add debug extensions if validation layer is requested
		if (enableValidationLayers) {
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		return extensions;
	}


	/*check validation layer support => makes sure that the requested validation layers are available*/
	bool checkValidationLayerSupport() {
		//request the number of required layers by passing null to enumerate function saves count into layerCount
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
		//allocate array to hold extension details
		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());
		//loop over all names of requested validation layers
		for (const char* layerName : validationLayers) {
			bool layerFound = false;
			//make sure each requested layer name exists within 'availableLayers' 
			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}
			//if any is not found then checking validation support has failed
			if (!layerFound) {
				return false;
			}
		}

		return true;
	}


	/*read file => used to load the pre-compiled shaders*/
	static std::vector<char> readFile(const std::string& filename) {
		//read in binary mode starting at the end of the file
		std::ifstream file(filename, std::ios::ate | std::ios::binary);
		//check file has been found succesfully
		if (!file.is_open()) {
			throw std::runtime_error("failed to open file!");
		}
		//allocate buffer the size of the file contents
		size_t fileSize = (size_t)file.tellg();
		std::vector<char> buffer(fileSize);
		//read all the file bytes
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		//close the file and return the data in the buffer
		file.close();
		return buffer;
	}


	/*debug callback => called when a message is sent from the validation layer*/
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, /*severity of the warning*/
		VkDebugUtilsMessageTypeFlagsEXT messageType, /*message type*/
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, /*message contents*/
		void* pUserData) {

		std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
		//always return false, because returning true would abort the call (used for testing the validation layer itself)
		return VK_FALSE;
	}
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                              //
//				               MAIN FUNCTION        =>=>=>=>=>=>=>=>=>=>=>=>=>=>=>                              //
//                                                                                                              //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main() {
	//instantiate vulkan app
	HelloTriangleApplication app;
	//run & catch any exception 
	try {
		app.run();
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}