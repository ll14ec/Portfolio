#include <iostream>
#include <fstream>	
#include <cstdlib>
#include <cmath>

/////////////////////////////////////////////////////////////////////
//
// A simple vector2 class providing methods:
//
//   - dot product - dot()
//   - get norm - norm() => sqrt ( x^2 + y^2 )
//   - get nomalised vector - normalise()
//   - get clockwise normal to vector - normal()
//   - get line intercept - getC()
//
// The '-' operator is overloaded so vectors can be subtracted.
//
/////////////////////////////////////////////////////////////////////

using namespace std;

class vector2{
  public:
    float x,y;
  
    // Constructor
    vector2 (float x, float y): x(x), y(y) {}

    // Dot product
    float dot(vector2 v2)
    {
      return x * v2.x + y * v2.y;
    }

    // Get norm
    float norm()
    {
      return sqrt(x*x + y*y);
    }
    // Get normalised vector
    vector2 normalise()
    {
      return vector2(x/norm(), y/norm());
    }

    // Get the clockwise normal
    vector2 normal()
    {
      return vector2(y,-x);
    }

    // Gets the intercept of the vector direction, if supplied a point on the line
    float getC(vector2 pointOnLine)
    {
      float gradient = y / x;
      float yVal = pointOnLine.y;
      float xVal = pointOnLine.x * gradient;
      return yVal-xVal;
    }

    // Subtract two vectors
    vector2 operator-(const vector2& other)
    {
      return vector2(x-other.x, y-other.y);
    }
};

/////////////////////////////////////////////////////////////////////
//
// Global Declarations
//
/////////////////////////////////////////////////////////////////////

// h and w of the triangle's surrounding square
int height = 128;
int width = 128;

// h and w of earth picture
int ew = 512;
int eh = 256;

// earth picture data
int data[512*256*3];

// abc of the triangle
vector2 a(10,20);
vector2 b(100,30);
vector2 c(40,100);	

// uv data for triangle, stored as alpha beta gamma owned vertices
vector2 aUV(0.5, 1.0);
vector2 bUV(0.6, 0.5);
vector2 gUV(0.44434, 0.87186);

// clockwise edge vectors
vector2 ba = a-b;
vector2 ac = c-a;
vector2 cb = b-c;

/////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////


float distanceToLine(vector2 arbitraryPoint, vector2 pointOnLine, vector2 normal)
// Takes a point to measure distance of, a point on the line, and a normal to the line
{
  vector2 lp2ap = arbitraryPoint - pointOnLine;
  return lp2ap.dot(normal.normalise());
}


float* getABG(vector2 point, vector2 ba, vector2 ac, vector2 cb, vector2 b, vector2 a, vector2 c, float * abg)
// Gets Alpha, Beta, Gamma for a point
// Takes the point, clockwise edge vectors, clockwise vertices, and a pointer to store the result in
{
  abg[0] = distanceToLine(point, b, cb.normal()) / distanceToLine(a, b, cb.normal()); // alpha
  abg[1] = distanceToLine(point, a, ac.normal()) / distanceToLine(b, a, ac.normal()); // beta
  abg[2] = distanceToLine(point, b, ba.normal()) / distanceToLine(c, b, ba.normal()); // gamma
  return &abg[0];
}


unsigned char clamp(int val)
// Clamp a value to 0,255
{
  if (val>255){
    return 255;
  }
  else if (val<0){
    return 0;
  }
  return val;
}


int halfPlane(float coord)
// Returns 0 or 255 based on +ve/-ve status of coord
{
  if (coord<0) return 0;
  else return 255;
}


int rasterize(float alpha, float beta, float gamma)
// Returns 0 or 255 based on whether we are in the triangle or not
{
  // If all of abg are +ve then we are in the triangle
  if ((alpha>0) && (beta > 0) && (gamma > 0))
    return 0;
  else 
    return 255;
}


vector2 getUV(float alpha, float beta, float gamma)
// Get uv coordinate from abg values based on vertex uv coords 
{
  return vector2(alpha * aUV.x + beta * bUV.x + gamma * gUV.x, alpha * aUV.y + beta * bUV.y + gamma * gUV.y);
}


void uvToPixel(vector2 uv, float* res)
// Takes uv coords and returns the pixel as float[2] x,y
{
  res[0] = uv.x * ew;
  res[1] = (1-uv.y) * eh;
}


void interpolatePixels(float * xy, int * rgb)
// Takes float[2] x,y pixel coords
// Returns int[3] rgb vals using bilenear interpolation
{
  // Determine x1 x2 y1 y2 eg if x=1.5 y=3.5 then x1 = 1, x2 = 2, y1 = 3, y2 = 4
  int x1 = (int)xy[0];
  int x2 = x1+1;
  int y1 = (int)xy[1];
  int y2 = y1+1;

  // Calculate the weighting between x1:x2 and y1:y2, { 0=x1|y1 -> 1=x2|y2 }
  float xWeight = xy[0] - x1;
  float yWeight = xy[1] - y1;

  // Get index in data array for all 4 pixels
  int x1y1 = (y1 * ew * 3) + x1 * 3;
  int x2y1 = (y1 * ew * 3) + x2 * 3;
  int x1y2 = (y2 * ew * 3) + x1 * 3;
  int x2y2 = (y2 * ew * 3) + x2 * 3;

  // Get the horizontal interpolation for (x1,y1) and (x2,y1)
  int r1 = (data[x1y1] * (1-xWeight)) + (data[x2y1] * (xWeight));
  int g1 = (data[x1y1+1] * (1-xWeight)) + (data[x2y1+1] * (xWeight));
  int b1 = (data[x1y1+2] * (1-xWeight)) + (data[x2y1+2] * (xWeight));

  // Get the horizontal interpolation for (x1,y2) and (x2,y2)
  int r2 = (data[x1y2] * (1-xWeight)) + (data[x2y2] * (xWeight));
  int g2 = (data[x1y2+1] * (1-xWeight)) + (data[x2y2+1] * (xWeight));
  int b2 = (data[x1y2+2] * (1-xWeight)) + (data[x2y2+2] * (xWeight));

  // Get the vertical interpolation between r1,g1,b1 and r2,g2,b2
  rgb[0] = (r1 * (1-yWeight) + r2 * yWeight);
  rgb[1] = (g1 * (1-yWeight) + g2 * yWeight);
  rgb[2] = (b1 * (1-yWeight) + b2 * yWeight);
}


unsigned char* generateABGSquare()
// Generates the data for image abg.ppm
{
  // Allocate memory for pixel rgb vals
  unsigned char* img = (unsigned char*)malloc(sizeof(unsigned char) * height * width * 3);
  int i, j = 0;
  // Set background to yellow colour
  for (i = 0; i<height*width*3; i+=3){
    img[i  ] = 255;
    img[i+1] = 255;
    img[i+2] = 192;
  }
  // Create abg array
  float abg[3];
  // Loop adds rbg vals to image array
  for (i=0; i<height ; i++){
    for (j=0; j<width*3; j+=3){
      // Calculate alpha, beta, gamma
      getABG(vector2(j/3,i), ba, ac, cb, b, a, c, &abg[0]);
      img[i*width*3 + j] = clamp((int)((abg[0] * 100) + 100)); // R
      img[i*width*3 + j + 1] = clamp((int)((abg[2] * 100) + 100)); // G
      img[i*width*3 + j + 2] = clamp((int)((abg[1] * 100) + 100)); // B  
    }
  }
  return img;
}


unsigned char* generateHalfPlaneSquare()
// Generates the data for halfplane.ppm
{
  // Allocate memory for pixel rgb vals
  unsigned char* img = (unsigned char*)malloc(sizeof(unsigned char) * height * width * 3);
  int i, j = 0;
  // Set background to yellow colour
  for (i = 0; i<height*width*3; i+=3){
    img[i  ] = 255;
    img[i+1] = 255;
    img[i+2] = 192;
  }
  // Create abg array
  float abg[3];
  // Will create halfplane
  for (i=0; i<height ; i++){
    for (j=0; j<width*3; j+=3){
      // Calculate alpha, beta, gamma
      getABG(vector2(j/3,i), ba, ac, cb, b, a, c, &abg[0]);
      img[i*width*3 + j] = clamp(halfPlane(abg[0])); // R
      img[i*width*3 + j + 1] = clamp(halfPlane(abg[2])); // G
      img[i*width*3 + j + 2] = clamp(halfPlane(abg[1])); // B
    }
  }
  return img;
}


unsigned char* rasterizeTriangle()
// Generates the data for triangle.ppm
{
  // Allocate memory for pixel rgb vals
  unsigned char* img = (unsigned char*)malloc(sizeof(unsigned char) * height * width * 3);
  int i, j = 0;
  // Set background to yellow colour
  for (i = 0; i<height*width*3; i+=3){
    img[i  ] = 255;
    img[i+1] = 255;
    img[i+2] = 192;
  }
  // Create abg array
  float abg[3];
  // Stores RGB val for each pixel coord
  int val = 0;
  // Rasterize triangle
  for (i=0; i<height ; i++){
    for (j=0; j<width*3; j+=3){
      // Calculate alpha, beta, gamma
      getABG(vector2(j/3,i), ba, ac, cb, b, a, c, &abg[0]);
      val = rasterize(abg[0],abg[1],abg[2]);
      // If inside the triangle
      if (!val){
        img[i*width*3 + j] = clamp(val);
        img[i*width*3 + j + 1] = clamp(val);
        img[i*width*3 + j + 2] = clamp(val);
      }
    }
  }
  return img;
}


unsigned char* rasterizeColouredTriangle()
// Generates the data for rgb.ppm
{
  // Allocate memory for pixel rgb vals
  unsigned char* img = (unsigned char*)malloc(sizeof(unsigned char) * height * width * 3);
  int i, j = 0;
  // Set background to yellow colour
  for (i = 0; i<height*width*3; i+=3){
    img[i  ] = 255;
    img[i+1] = 255;
    img[i+2] = 192;
  }
  // Create abg array
  float abg[3];
  // Create abg coloured triangle
  for (i=0; i<height ; i++){
    for (j=0; j<width*3; j+=3){
      getABG(vector2(j/3,i), ba, ac, cb, b, a, c, &abg[0]);
      if (!rasterize(abg[0],abg[1],abg[2])) {
        img[i*width*3 + j] = clamp((int)((abg[0] * 100) + 100));
        img[i*width*3 + j + 1] = clamp((int)((abg[2] * 100) + 100));
        img[i*width*3 + j + 2] = clamp((int)((abg[1] * 100) + 100));  
      }
    }
  }
  return img;
}


unsigned char* rasterizeTexturedTriangle()
// Generates the data for texture.ppm 
{ 
  // Allocate memory for pixel rgb vals
  unsigned char* img = (unsigned char*)malloc(sizeof(unsigned char) * height * width * 3);
  int i, j = 0;
  // Set background to yellow colour
  for (i = 0; i<height*width*3; i+=3){
    img[i  ] = 255;
    img[i+1] = 255;
    img[i+2] = 192;
  }
  // Create abg array
  float abg[3];
  // Create the uv and rgb arrays
  float uv[2];
  int rgb[3];
  // Create texture filled triangles
  for (i=0; i<height ; i++){
    for (j=0; j<width*3; j+=3){
      // Get alpha, beta, gamma
      getABG(vector2(j/3,i), ba, ac, cb, b, a, c, &abg[0]);
      // If rendering the pixel
      if (!rasterize(abg[0],abg[1],abg[2])) {
        // Get the uv coordinate
        vector2 UV = getUV(abg[0], abg[1], abg[2]);
        // Get the pixel from uv as a float[2]
        uvToPixel(UV, &uv[0]);
        // Interpolate from the float[2] to int[3] using bilinear interpolation
        interpolatePixels(uv, &rgb[0]);
        // Set rgb vals
        img[i*width*3 + j ] = rgb[0]; // R
        img[i*width*3 + j + 1] = rgb[1]; // G
        img[i*width*3 + j + 2] = rgb[2]; // B  
      }
    }
  }
  return img;
}


void readPPM()
// Reads the ppm file into data[]
{
  // Open earth.ppm
  std::fstream iFile("earth.ppm", std::ios_base::in);
  if (!iFile) cout << "Error opening file." << endl;
  // Skip header
  int i = 0;
  string temp;
  while (i<11){
    iFile >> temp;
    i++;
  }
  // Load ppm data into data[] 
  i = 0;
  while(iFile >> data[i]){ 
    i++;
  }
  // Close file
  iFile.close();
}


void writePPM(unsigned char *data, int height, int width, int max, const char * fileName)
// Writes the data in *data to a ppm file
{
  // Calcualte size
  int size = height * width * 3;
  // Open file
  ofstream oFile;
  oFile.open(fileName); 
  // Write header
  oFile << "P3" << endl;
  oFile << "# " << fileName << endl;
  oFile << "" << height << " " << width << endl;
  oFile << "" << max << endl;
  // Write data
  int i;
  for (i=0;i<size;i++)
  {
    //cout << "Writing data " << i << " " << size << "." << endl;
    oFile << int(data[i]) << " ";
    if (((i+1)%(width*3))==0)
      oFile << endl;
  }
  // Close file
  oFile.close();
}




int main(int argc, char** argv)
{
  // Generate data and output abg.ppm
  unsigned char* img = generateABGSquare();
  writePPM(img, 128, 128, 255, "abg.ppm");
  free(img);
  // Generate data and output halfplane.ppm
  img = generateHalfPlaneSquare();
  writePPM(img, 128, 128, 255, "halfplane.ppm");
  free(img);
  // Generate data and output triangle.ppm
  img = rasterizeTriangle();
  writePPM(img, 128, 128, 255, "triangle.ppm");
  free(img);
  // Generate data and output rgb.ppm
  img = rasterizeColouredTriangle();
  writePPM(img, 128, 128, 255, "rgb.ppm");
  free(img);
   // Read texture file, generate data and output texture.ppm
  readPPM();
  img = rasterizeTexturedTriangle();
  writePPM(img, 128, 128, 255, "texture.ppm");
  free(img);
  return 0;
}




