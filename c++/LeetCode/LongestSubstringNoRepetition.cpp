/*
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 

Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
			 
Solution:

Walk along the string, checking characters, if character is not in map, add to map.
If character is in map, check the length of the substring, save to max if > max.
Continue walk from the character after the duplicate character, using map index value.

Algorithmic complexity is O(n).
*/

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        int c = 0;
        int len = 0;
        int maxLen = 0;
        unordered_map<char, int> m;
        while (c<s.size())
        {
            if (m.find(s[c]) == m.end()){
                m.insert({s[c], c});
                len += 1;
                ++c;
            }
            else{                
                if (len > maxLen) maxLen = len;
                len = 0;
                c = m[s[c]] + 1;
                m.clear();
            }  
        }
        return len > maxLen? len: maxLen;
    }
};

