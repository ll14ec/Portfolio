/*
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321

Example 2:

Input: -123
Output: -321

Example 3:

Input: 120
Output: 21

Note:
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

Solution:

Get each digit starting with the least significant by taking number mod 10, then dividing number by 10.
Check for over-/underflow as we go.

Alorithmic complexity is O(n).
*/

class Solution {
public:
    int reverse(int x) 
    {
        int ans = 0;
        int i = 0;
        while (x != 0){
            auto digit = x % 10;
            x /= 10;
            if (ans > INT_MAX / 10) return 0;
            if ((ans == INT_MAX / 10) && (digit > 7)) return 0;
            if (ans < INT_MIN / 10) return 0;
            if ((ans == INT_MIN / 10) && (digit < -8)) return 0;            
            ans = ans * 10 + digit;
        }
        return ans;
    }
};