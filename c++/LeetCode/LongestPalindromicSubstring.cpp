/*
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.

Example 2:

Input: "cbbd"
Output: "bb"

NaiveIsh Solution: 

Start with the longest posssible substring, check if palindromic. If so, return it.
If not, keep reducing the length of the substring & trying all locations in the large string, return the first palindrome.

Algorithmic complexity is O(n^3)

Solution:

Add all length 1 characters and length 2 double characters to a queue with their position and length.
For all items in the queue, pop them, then try extending their length and check if the extended chars are equal.
If they are equal once extended, add the extended substring back to queue and if the longest so far, save into the problem answer.
Continue this until there are no more items in the queue.

Algorithmic complexity is O(n^2)

*/

class Solution {
public:
    string longestPalindrome(string s) {
        auto len = s.length();
        auto longest = 0;
        string answer = ("");
        if (s == answer) return s;  // handle empty string case
        if (len == 1) return s;     // handle length 1 case
        queue<pair<int, int>> pals; // where we will store palindromic string data (index, length)
        // get length 1 pals
        if (len > 0){
            for (auto i = 0; i<(len-1); ++i){
                pals.push(make_pair(i, 1));
                answer = s.substr(i, 1);
                longest = 1;
            }
        }
        // get length 2 pals
        if (len > 1){
            for (auto i = 0; i<(len-1); ++i){
                if (s[i] == s[i+1]){
                    pals.push(make_pair(i, 2));
                    answer = s.substr(i, 2);
                    longest = 2;
                }
            }
        }
        // keep extending pals in the queue until they are no longer palindromic
        while (!pals.empty()){
            // get item from queue and pop it
            auto currentPal = pals.front();
            pals.pop();
            // if we can extend the current string within the input string (index > 0 & not past the end point)
            if ((currentPal.first > 0) && ((currentPal.first + currentPal.second) < (len))){
                // extend the substring
                int startIndex = currentPal.first-1;
                int newLength = currentPal.second+2;
                // check if the new characters are still palindromic
                char c1 = s[startIndex];
                char c2 = s[startIndex + newLength - 1];
                if (c1 == c2){
                    // if they are, push palindromic string to queue
                    pals.push(make_pair(startIndex, newLength));
                    // if the length is the longest so far, save to answer & update longest string
                    if (newLength > longest){
                        longest = newLength;
                        answer = s.substr(startIndex, newLength);
                    }
                }
            }
        }
        return answer;
    }
        
//     Naive-ish solution
    
//     bool isPalindrome(const string& s){
//     auto len = s.length();
//     for (auto i = 0; i<( len / 2 ); ++i){
//         char c1 = s[i];
//         char c2 = s[len-1-i];
//         if (c1 != c2) return false;
//     }
//     return true;
// }
    
//     string longestPalindrome(string s) {
//         auto len = s.length();
//         string answer = ("");
//         if (s == answer) return s;
//         if (len == 1) return s;

//         for (auto longest = len; longest>0; longest--){
//             for (auto p = 0; p<(1 + len - longest); ++p){
//                 if (isPalindrome(s.substr(p, longest))){
//                     return s.substr(p, longest);
//                 }
//             }
//         }
//         return answer;
//     }
    
};