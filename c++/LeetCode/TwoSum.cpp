/*
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

Solution:

Add all items in the array into a map. For each item in the array find the required target val and check if it is in the map. 

If it is found in the map, return the indices.

Algorithmic complexity is O(n).

*/

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
      std::unordered_map<int, int> m;
      vector<int> ans;
      int key = 0;
      for (auto& e: nums){
       m[e] = key;
       ++key;
      }     
      int i = 0;
      for (auto& e: nums){
        int requiredVal = target - e;
        if (m.find(requiredVal)!=m.end()){
          if (i != m[requiredVal]){
            ans.push_back(i);
            ans.push_back(m[requiredVal]);
            return ans;
          }
        }
        i++;
      }
    return ans;
    }
};