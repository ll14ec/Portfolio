/*
Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:

Input: 121
Output: true

Example 2:

Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

Example 3:

Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Solution:

Push all digits to stack. After this loop over number digits and compare with stack digits. As the digits in the stack are in reverse order, if this comparison succeeds, the number is a palindrome.

Algorithmic complexity is O(n).

*/

class Solution {
public:
    bool isPalindrome(int x) {
        if (x<0)
            return false;
        stack<int> s;
        auto x2 = x;
        auto length = 0;
        while (x2 != 0){
            s.push(x2 % 10);
            x2 /= 10;
            length += 1;
        }
        for (auto i = length; i>0; --i){
            auto stackDigit = s.top();
            s.pop();
            auto inputDigit = x % 10;
            x /= 10;
            // cout << stackDigit << " " << inputDigit << endl;
            if (stackDigit != inputDigit)
                return false;
        } 
        return true;
    }
};