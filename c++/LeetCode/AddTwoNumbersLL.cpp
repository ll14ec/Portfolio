/*
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.

Solution: 	

Walk down each list adding as we go, creating a new list as we add. Save the carry for values > 10.
Add the carry to the new list node.

Algorithmic complexity is O(n).
		
*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* walk_1 = l1;
        ListNode* walk_2 = l2;
        ListNode* answer = new ListNode();
        ListNode* root = answer;
        auto v1 = 0;
        auto v2 = 0;
        auto carry = 0;
        auto sum = 0;
        
        while (walk_1->next || walk_2->next){
            v1 = walk_1->val;
            v2 = walk_2->val;
            sum = v1 + v2 + carry;
            carry = 0;
            if ((carry = (sum > 9))) sum -= 10;
            answer->val = sum;
            answer->next = new ListNode();
            answer = answer->next;
            if (walk_1->next) walk_1 = walk_1->next;
            else walk_1->val = 0;
            if (walk_2->next) walk_2 = walk_2->next;
            else walk_2->val = 0;
        }
        v1 = walk_1->val;
        v2 = walk_2->val;
        sum = v1 + v2 + carry;
        carry = 0;
        if ((carry = (sum > 9))) sum -= 10;
        answer->val = sum;
        if (carry) answer->next = new ListNode(carry);
        
        return root;
    }
};