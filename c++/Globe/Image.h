#include <QImage>
#include <GL/gl.h>

//class for reading images into a GLbyte array
class ImageReader{

public:
    //constructor
    ImageReader(QString);

    //image
    QImage * image;

    //GLbyte array
    GLbyte * imgBytes;

private:
    //destructor
    ~ImageReader();

};
