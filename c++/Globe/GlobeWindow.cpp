#include "GlobeWindow.h"
#include <stdio.h>


// constructor / destructor
GlobeWindow::GlobeWindow(QWidget *parent)
    : QWidget(parent)
{ // constructor

    // create menu bar
    menuBar = new QMenuBar(this);

    // create file menu
    fileMenu = menuBar->addMenu("&File");

    // create the action
    actionQuit = new QAction("&Quit", this);

    // leave signals & slots to the controller

    // add the item to the menu
    fileMenu->addAction(actionQuit);

    // create the window layout
    windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

    //create vertical layout
    stack = new QVBoxLayout();

    // create main widget
    globeWidget = new GlobeWidget(this);

    // add gl windown to vertical layout
    stack->addWidget(globeWidget);

    // create zoom slider
    zoomSlider = new QSlider(Qt::Horizontal);
    zoomSlider->setRange(30,200);
    connect(zoomSlider, SIGNAL(valueChanged(int)), this->globeWidget, SLOT(zoomUpdate(int)));
    zoomSlider->setMaximumHeight(20);
    globeWidget->zoomLevel = 30;

    //create label for zoom slider
    zoomLabel = new QLabel("Zoom", this);
    zoomLabel->setMaximumHeight(20);
    zoomLabel->setMinimumWidth(50);

    // add label and slider to zoom horizontal widget
    zoom = new QHBoxLayout();
    zoom->addWidget(zoomSlider);
    zoom->addWidget(zoomLabel);

    // add zoom slider to vertical layout
    stack->addLayout(zoom);

    // create rotation slider
    rotationSlider = new QSlider(Qt::Horizontal);
    rotationSlider->setRange(-180,180);
    rotationSlider->setValue(0);
    connect(rotationSlider, SIGNAL(valueChanged(int)), this->globeWidget, SLOT(rotationSlider(int)));
    rotationSlider->setMaximumHeight(20);

    // create rotation label
    rotationLabel = new QLabel("Rotation", this);
    rotationLabel->setMaximumHeight(20);
    rotationLabel->setMinimumWidth(50);

    // add label and slider to rotate horizontal widget
    rotate = new QHBoxLayout();
    rotate->addWidget(rotationSlider);
    rotate->addWidget(rotationLabel);

    // add rotation slider to vertical layout
    stack->addLayout(rotate);

    //create lamp sliders
    lampSliderX = new QSlider(Qt::Horizontal);
    lampSliderX->setRange(-180,180);
    lampSliderX->setValue(0);
    connect(lampSliderX, SIGNAL(valueChanged(int)), this->globeWidget, SLOT(lampRotationSliderX(int)));
    lampSliderX->setMaximumHeight(20);

    lampSliderY = new QSlider(Qt::Horizontal);
    lampSliderY->setRange(-180,180);
    lampSliderY->setValue(0);
    connect(lampSliderY, SIGNAL(valueChanged(int)), this->globeWidget, SLOT(lampRotationSliderY(int)));
    lampSliderY->setMaximumHeight(20);

    //create labels for lamp sliders
    lampLabelX = new QLabel("Lamp X", this);
    lampLabelX->setMaximumHeight(20);
    lampLabelX->setMinimumWidth(50);

    lampLabelY = new QLabel("Lamp Y", this);
    lampLabelY->setMaximumHeight(20);
    lampLabelY->setMinimumWidth(50);

    // add label and slider to rotate horizontal widget
    lamp = new QHBoxLayout();
    lamp->addWidget(lampSliderX);
    lamp->addWidget(lampLabelX);
    lamp->addWidget(lampSliderY);
    lamp->addWidget(lampLabelY);

    //set lamp angles
    globeWidget->lampRotationAngleX = globeWidget->lampRotationAngleY = 0.f;

    // add rotation slider to vertical layout
    stack->addLayout(lamp);

    // create boxes and button for time input
    hours = new QSpinBox();
    minutes = new QSpinBox();
    timeButton = new QPushButton("Set UK Time");
    currentTimeButton = new QPushButton("Set Current Time");

    //connect time button to set time slot
    connect (timeButton, SIGNAL(clicked(bool)), this, SLOT(timeSet()));

    // instanciate current time
    currentTime = new QTime();

    //connect current time button to set time slot
    connect (currentTimeButton, SIGNAL(clicked(bool)), this, SLOT(currentTimeSet()));

    // create lables for hours and minutes spinboxes
    hoursLabel = new QLabel("Hours");
    hoursLabel->setMaximumWidth(40);
    minutesLabel = new QLabel("Minutes");
    minutesLabel->setMaximumWidth(50);

    // add spinboxes and button to time horizontal layout
    time = new QHBoxLayout();
    time->addWidget(hours);
    time->addWidget(hoursLabel);
    time->addWidget(minutes);
    time->addWidget(minutesLabel);
    time->addWidget(timeButton);
    time->addWidget(currentTimeButton);

    // add time horizontal layout to window vertical layout
    stack->addLayout(time);

    // create timer and add to slot
    timer = new QTimer(this);
    connect (timer, SIGNAL(timeout()), this->globeWidget, SLOT(frameUpdate()));
    timer->start(0);

    //create new quadratic objects
    globeWidget->quadratic = gluNewQuadric();
    globeWidget->earthQuadric = gluNewQuadric();
    globeWidget->faceQuadric = gluNewQuadric();

    //create image readers
    globeWidget->earthImageReader = new ImageReader("earth.ppm");
    globeWidget->faceImageReader = new ImageReader("marc.ppm");

    // add stack vertical layout to window layout
    windowLayout->addLayout(stack);

    //set rotation angle
    globeWidget->rotationSliderAngle = 0;

} // constructor

//time setting
void GlobeWindow::timeSet()
{
    globeWidget->setTime(hours->value(), minutes->value());
}

void GlobeWindow::currentTimeSet()
{
    printf("%d", currentTime->currentTime().hour());
    globeWidget->setTime(currentTime->currentTime().hour(), currentTime->currentTime().minute());
}

//destructor
GlobeWindow::~GlobeWindow()
{ // destructor
    delete hours;
    delete minutes;
    delete timeButton;
    delete currentTimeButton;
    delete hoursLabel;
    delete minutesLabel;
    delete time;
    delete zoomSlider;
    delete rotationSlider;
    delete zoomLabel;
    delete zoom;
    delete rotate;
    delete stack;
    delete rotationLabel;
    delete globeWidget;
    delete windowLayout;
    delete actionQuit;
    delete fileMenu;
    delete menuBar;
    delete timer;
    delete currentTime;
    glDeleteTextures(2, globeWidget->tex);
    gluDeleteQuadric(globeWidget->quadratic);
    gluDeleteQuadric(globeWidget->earthQuadric);
    gluDeleteQuadric(globeWidget->faceQuadric);
} // destructor

//resets all the interface elements
void GlobeWindow::ResetInterface()
{
    globeWidget->update();
    update();
}
