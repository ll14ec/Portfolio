//// Setting up material properties
typedef struct materialStruct {
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat emissive[4];
    GLfloat shininess;
} materialStruct;

//materials

static materialStruct brassMaterials = {
    { 0.33, 0.22, 0.03, 1.},
    { 0.78, 0.57, 0.11, 1.},
    { 0.99, 0.91, 0.81, 1.},
    { 0,    0,    0,    0 },
    27.8
};

static materialStruct whiteShinyMaterials = {
    { 1.0, 1.0, 1.0, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    { 1.0, 1.0, 1.0, 1.0},
    { .0,    .0,    .0,    1.0 },
    100.0
};

static materialStruct glowingBulbMaterials = {
    { 0, 0, 0, 1.0},
    { 0, 0, 0, 1.0},
    { 0, 0, 0, 1.0},
    { 240.f/255.f,  230.f/255.f,    140.f/255.f,    1.0 },
    0.0
};


static materialStruct greenLeatherMaterials = {
    { .2/4.0f, 0.3/4.0f, .1/4.0f, 1},
    { .2/4.0f, 0.3/4.0f, .1/4.0f, 1},
    { .2/4.0f, 0.3/4.0f, .1/4.0f, 1},
    { 0,    0,    0,    0 },
    2
};

static materialStruct greenTranslucentMaterials = {
    { .2/1.0f, 0.3/1.0f, .1/1.0f, 0.9},
    { .2/1.0f, 0.3/1.0f, .1/1.0f, 0.9},
    { .2/1.0f, 0.3/1.0f, .1/1.0f, 0.9},
    { 0,    0,    0,    0 },
    1
};

static materialStruct woodMaterials = {
    { 0.33/4.0f, 0.22/4.0f, 0.03/4.0f, 1.},
    { 0.78/4.0f, 0.57/4.0f, 0.11/4.0f, 1.},
    { 0.1,       0.1,       0.1,       1.},
    { 0,    0,    0,    0 },
    0.1
};

static materialStruct darkWoodMaterials = {
    { 0.33/8.0f, 0.22/8.0f, 0.03/8.0f, 1.},
    { 0.78/8.0f, 0.57/8.0f, 0.11/8.0f, 1.},
    { 0.1,       0.1,       0.1,       1.},
    { 0,    0,    0,    0 },
    0.1
};

static materialStruct lightWoodMaterials = {
    { 0.33/2.0f, 0.22/2.0f, 0.03/2.0f, 1.},
    { 0.78/2.0f, 0.57/2.0f, 0.11/2.0f, 1.},
    { 0.1,       0.1,       0.1,       1.},
    { 0,    0,    0,    0 },
    0.2
};

static materialStruct redMaterials = {
    { 0.83/2.0f, 0.02/2.0f, 0.03/2.0f, 1.},
    { 0.78/2.0f, 0.07/2.0f, 0.01/2.0f, 1.},
    { 0.45, 0.1, 0.1, 1.},
    { 0,    0,    0,    0 },
    4.2
};

static materialStruct blueMaterials = {
    { 0.01f, 0.02/2.0f, 0.83/2.0f, 1.},
    { 0.02/2.0f, 0.07/2.0f, 0.78/2.0f, 1.},
    { 0.04, 0.1, 0.45, 1.},
    { 0,    0,    0,    0 },
    4.2
};

static materialStruct blueGreenMaterials = {
    { 0.01f, 0.92/2.0f, 0.83/2.0f, 1.},
    { 0.02/2.0f, 0.97/2.0f, 0.78/2.0f, 1.},
    { 0.04, 0.1, 0.45, 1.},
    { 0,    0,    0,    0 },
    4.2
};

static materialStruct yellowMaterials = {
    { 0.91f, 0.93/1.0f, 0.01/2.0f, 1.},
    { 0.92f, 0.93/1.0f, 0.01/2.0f, 1.},
    { 0.9f, 0.93, 0.05, 1.},
    { 0,    0,    0,    0 },
    4.2
};

static materialStruct brownMaterials = {
    { 166/255.f,84/255.f, 0.03/2.0f, 1.},
    { 166/255.f,84/255.f, 0.08/2.0f, 1.},
    { 166/255.f,86/255.f, 0.05, 1.},
    { 0,    0,    0,    0 },
    4.2
};

static materialStruct orangeMaterials = {
    { 0.91f, 0.23/2.0f, 0.01/2.0f, 1.},
    { 0.92f, 0.23/2.0f, 0.01/2.0f, 1.},
    { 0.9f, 0.34, 0.05, 1.},
    { 0,    0,    0,    0 },
    4.2
};

static materialStruct orangeDarkMaterials = {
    { 0.31f, 0.23/6.0f, 0.01/6.0f, 1.},
    { 0.32f, 0.23/6.0f, 0.01/6.0f, 1.},
    { 0.3f, 0.34, 0.05, 1.},
    { 0,    0,    0,    0 },
    0.2
};

static materialStruct redDarkMaterials = {
    { 0.83/6.0f, 0.02/6.0f, 0.03/6.0f, 1.},
    { 0.78/6.0f, 0.07/6.0f, 0.01/6.0f, 1.},
    { 0.45, 0.1, 0.1, 1.},
    { 0,    0,    0,    0 },
    0.2
};

//sets the material to the correct struct
void GlobeWidget::setMaterial(char colour)
{
    if (colour == 'w'){

        materialStruct* p_front = &whiteShinyMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == 'x'){
        materialStruct* p_front = &glowingBulbMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == 'b'){
        materialStruct* p_front = &brassMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == 'g'){
        materialStruct* p_front = &greenLeatherMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == 't'){
        materialStruct* p_front = &woodMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == 'l'){
        materialStruct* p_front = &lightWoodMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == 'r'){
        materialStruct* p_front = &redMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '1'){
        materialStruct* p_front = &blueMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '2'){
        materialStruct* p_front = &yellowMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '3'){
        materialStruct* p_front = &brownMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '4'){
        materialStruct* p_front = &orangeMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '5'){
        materialStruct* p_front = &blueGreenMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '6'){
        materialStruct* p_front = &greenTranslucentMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '7'){
        materialStruct* p_front = &darkWoodMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '8'){
        materialStruct* p_front = &orangeDarkMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }

    else if (colour == '9'){
        materialStruct* p_front = &redDarkMaterials;
        glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
        glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
        glMaterialfv(GL_FRONT, GL_EMISSION, p_front->emissive);
    }
}

