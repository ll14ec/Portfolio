//calculates the angle of the minute hand
void GlobeWidget::getMinuteAngle(float m){
    while (m>59){
        m-=60;
    }
    minuteAngle = m/60.f*360.f;

}

//calculates the angle of the hour hand for each country
void GlobeWidget::getHourAngle(float h, float m){

    while (m>59){
        m-=60;
    }

    while (h>11){
        h-=12;
    }

    int hNZ = h + 13;
    while (hNZ>11){
        hNZ-=12;
    }

    int hLA = h + 4;
    while (hLA>11){
        hLA-=12;
    }

    int hBK = h + 7;
    while (hBK>11){
        hBK-=12;
    }

    hourAngleUK = (h+(m/60.f))/12.f*360.f;
    hourAngleNZ = (hNZ+(m/60.f))/12.f*360.f;
    hourAngleLA = (hLA+(m/60.f))/12.f*360.f;
    hourAngleBK = (hBK+(m/60.f))/12.f*360.f;
}

//sets the angles of the two hands
void GlobeWidget::setTime(float hours, float minutes){
    this->getHourAngle(hours, minutes);
    this->getMinuteAngle(minutes);
}
