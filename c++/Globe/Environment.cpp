// draws a cube with dimensions and of a certain material
void GlobeWidget::cuboid(float length, float width, float depth, char colour)
{
    // Normals for the cuboid
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

    //Set the material of the cube
    setMaterial(colour);

    glPushMatrix();

    glRotatef(-90, 0., 0., 1.);

    // define the faces
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    glVertex3f( length, -width,  depth);
    glVertex3f( length, -width, -depth);
    glVertex3f( length,  width, -depth);
    glVertex3f( length,  width,  depth);
    glEnd();

    glNormal3fv(normals[3]);
    glBegin(GL_POLYGON);
    glVertex3f(-length, -width, -depth);
    glVertex3f( length, -width, -depth);
    glVertex3f( length,  width, -depth);
    glVertex3f(-length,  width, -depth);
    glEnd();

    glNormal3fv(normals[2]);
    glBegin(GL_POLYGON);
    glVertex3f(-length, -width, depth);
    glVertex3f( length, -width, depth);
    glVertex3f( length,  width, depth);
    glVertex3f(-length,  width, depth);
    glEnd();

    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
    glVertex3f( -length, -width,  depth);
    glVertex3f( -length, -width, -depth);
    glVertex3f( -length,  width, -depth);
    glVertex3f( -length,  width,  depth);
    glEnd();

    glNormal3fv(normals[4]);
    glBegin(GL_POLYGON);
    glVertex3f( -length,  width, -depth);
    glVertex3f(  length,  width, -depth);
    glVertex3f(  length,  width,  depth);
    glVertex3f( -length,  width,  depth);
    glEnd();

    glNormal3fv(normals[5]);
    glBegin(GL_POLYGON);
    glVertex3f( -length, -width, -depth);
    glVertex3f(  length, -width, -depth);
    glVertex3f(  length, -width,  depth);
    glVertex3f( -length, -width,  depth);
    glEnd();

    glPopMatrix();
}

//draws the dart board
void GlobeWidget::dartBoard()
{
    // normals for the dart board
    GLfloat normal[3] =  {1., 0. ,0.};

    glPushMatrix();
    glRotatef(180,0,1,0);
    glTranslatef(0,45,87);
    setMaterial('g');

    //draw the cylinder and the disk for the board
    gluCylinder(quadratic, 12.f, 12.f, 3.f, 20, 20);
    gluDisk(quadratic, 0.f, 12.f, 20, 20);

    glTranslatef(0,0,-0.1);

    //draw the triangular segments
    char i = 0;
    for (i=0;i<20;i++)
    {
        glRotatef(18.f*i,0,0,1);
        if (i%2){
            setMaterial('g');
        }
        else{
            setMaterial('w');
        }
        glNormal3fv(normal);
        glBegin(GL_POLYGON);
        glVertex3f( 0, 0,  0);
        glVertex3f( 0, -11.5,  0);
        glVertex3f(3.7, -11.,  0);
        glEnd();

        glRotatef(-18.f*i,0,0,1);
    }

    //draw the rings
    glTranslatef(0,0,-0.1);
    setMaterial('4');
    gluDisk(quadratic, 10.f, 11.f, 20, 20);
    setMaterial('g');
    gluDisk(quadratic, 5.5f, 6.5f, 20, 20);

    glTranslatef(0,0,-0.1);
    glNormal3fv(normal);

    //draw the square the with face on it
    setMaterial('w');
    enableFaceTexture();
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1); glVertex3f(-6, -6, -0);
    glTexCoord2f(0, 0); glVertex3f(-6,  6, -0);
    glTexCoord2f(1, 0); glVertex3f( 6,  6,  0);
    glTexCoord2f(1, 1); glVertex3f( 6, -6,  0);
    glEnd();
    disableTexture();

    //draw the dart shaft
    setMaterial('b');
    glRotatef(180,0,1,0);
    glTranslatef(0,2,0);
    gluCylinder(quadratic, 0.2f, 0.5f, 10.f, 4, 4);

    glTranslatef(0.f,0.f,10.f);

    //draw the dart flights
    glRotatef(90.f, 0,1,0);
    glNormal3fv(normal);
    glBegin(GL_POLYGON);
    glVertex3f( 1.5, -1.0,  0);
    glVertex3f(-1.5, -1.0, -0);
    glVertex3f(-1.5,  1.0, -0);
    glVertex3f( 1.5,  1.0,  0);
    glEnd();

    glRotatef(90.f, 1,0,0);
    glNormal3fv(normal);
    glBegin(GL_POLYGON);
    glVertex3f( 2, -1.0,  0);
    glVertex3f(-2, -1.0, -0);
    glVertex3f(-2,  1.0, -0);
    glVertex3f( 2,  1.0,  0);
    glEnd();

    glPopMatrix();
}

//draws the table
void GlobeWidget::table(){

    //main table
    glPushMatrix();
    glTranslatef(0.f,-1.7f,0.f);
    this->cuboid(1.5f,35,18,'t');

    glPopMatrix();

    //legs
    glPushMatrix();
    glTranslatef(30.f,-16.5f,15.f);
    this->cuboid(15.f,1.2f,1.2f,'t');
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-30.f,-16.5f,15.f);
    this->cuboid(15.f,1.2f,1.2f,'t');
    glPopMatrix();

    glPushMatrix();
    glTranslatef(30.f,-16.5f,-15.f);
    this->cuboid(15.f,1.2f,1.2f,'t');
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-30.f,-16.5f,-15.f);
    this->cuboid(15.f,1.2f,1.2f,'t');
    glPopMatrix();

    //table green bit
    glPushMatrix();
    glTranslatef(0.f,0.0005f,3.f);
    this->cuboid(0.001f,19,13,'g');
    glPopMatrix();

    //table veneer
    glPushMatrix();
    glTranslatef(0.f,-0.2f,0.f);
    this->cuboid(0.2f,33,16,'l');
    glPopMatrix();
}

//draws the green table lamp top
void GlobeWidget::greenLampTop()
{
    //normals for the segments of green
    GLfloat normals[][3] = { {0, 0. ,1.}, {0,0,-1}};
    GLfloat lightDir[] = { 0, -0, -1.0 };
    glPushMatrix();
    //rotate matrix for user lamp angle from ui
    glRotatef(this->lampRotationAngleX, 1,0,0);
    glRotatef(this->lampRotationAngleY, 0,1,0);

    //set light direction
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, lightDir);
    glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, lightDir);
    glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, lightDir);

    //draw elongated bulb
    glPushMatrix();
    glScalef(2,0.3,0.3);
    gluSphere(quadratic, 2.f, 20, 5);
    glPopMatrix();

    glTranslatef(0,0,2);

    //draw the green bits for the lamps
    setMaterial('6');
    glPushMatrix();
    glRotatef(60,1,0,0);
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    glVertex3f( -4, 0,  0);
    glVertex3f(  4, 0, 0);
    glVertex3f(  6, -3, 0);
    glVertex3f( -6, -3, 0);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    glVertex3f( -4, 0,  0);
    glVertex3f( -5.8, -1.5,  -2.4);
    glVertex3f( -5.8,  1.5,  -2.4);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glRotatef(-60,1,0,0);
    glRotatef(180,1,0,0);
    glNormal3fv(normals[1]);
    glBegin(GL_POLYGON);
    glVertex3f( -4, 0,  0);
    glVertex3f(  4, 0, 0);
    glVertex3f(  6, -3, 0);
    glVertex3f( -6, -3, 0);
    glEnd();
    glPopMatrix();

    glPushMatrix();
    glRotatef(180,0,0,1);
    glNormal3fv(normals[0]);
    glBegin(GL_POLYGON);
    glVertex3f( -4, 0,  0);
    glVertex3f( -5.8, -1.5,  -2.4);
    glVertex3f( -5.8,  1.5,  -2.4);
    glEnd();
    glPopMatrix();

    glPopMatrix();
}

//draws the three lights in the room
void GlobeWidget::lamp()
{
    //draw the standing lamp
    setMaterial('l');
    lampPos[0] = -60.f;
    lampPos[1] = 40.f;
    lampPos[2] = 20.f;
    glPushMatrix();
    glTranslatef(lampPos[0],lampPos[1],lampPos[2]);
    glRotatef(90,1,0,0);
    glRotatef(180,0,1,0);
    //draw the lamp shade
    gluCylinder(quadratic, 15.f, 8.f, 10.f, 20, 20);
    glTranslatef(0.f, 0.f, 10.f);
    glRotatef(180,0,1,0);
    setMaterial('x');
    //draw the bulb
    gluSphere(quadratic, 3.f, 20, 5);
    setMaterial('t');
    //draw the lamp shaft
    gluCylinder(quadratic, 2.f, 2.f, 81.5f, 6, 6);
    glTranslatef(0.f,0.f,81.5);
    glRotatef(180,0,1,0);
    //draw the lamp base
    gluCylinder(quadratic, 20.f, 2.f, 1.5f, 6, 6);
    glPopMatrix();

    //draw the main central light
    glPushMatrix();
    glTranslatef(0,120,0);
    glRotatef(90,1,0,0);
    glRotatef(180,0,1,0);
    //draw the lamp shade
    gluCylinder(quadratic, 15.f, 8.f, 10.f, 20, 20);
    glTranslatef(0,0,7);
    setMaterial('t');
    //draw the string from ceiling fitting
    gluCylinder(quadratic, .5f, .5f, 81.5f, 6, 6);
    setMaterial('x');
    gluSphere(quadratic, 3.f, 20, 5);
    glPopMatrix();

    //draw the desk lamp
    glPushMatrix();
    glTranslatef(0,10,10);
    glRotatef(90,1,0,0);
    glTranslatef(0,0,9);
    setMaterial('7');
    //draw the lamp base
    gluCylinder(quadratic, 0.f, 6.f, 1.f, 20, 2);
    glRotatef(180, 0,1,0);
    //draw the lamp shaft
    gluCylinder(quadratic, 0.5f, 0.5f, 6.f, 6, 6);
    glTranslatef(0,0,6);
    setMaterial('x');

    greenLampTop();
    glPopMatrix();
}

//draws the chair
void GlobeWidget::chair()
{

    glPushMatrix();

    glTranslatef(7.5,-23,-20);
    //legs
    cuboid(8.5,1,1,'t');
    glTranslatef(-15,0,0);
    cuboid(8.5,1,1,'t');
    glTranslatef(0,0,-15);
    cuboid(8.5,1,1,'t');
    glTranslatef(15,0,0);
    cuboid(8.5,1,1,'t');

    //seat
    glTranslatef(-7.5, 7.5, 7.5);
    cuboid(1,8,8,'t');
    glTranslatef(0,1,1);
    cuboid(0.1,5,5,'g');

    //back
    glTranslatef(0,8,-8.5);
    cuboid(16,8.5,1,'t');
    glTranslatef(0,4.5,1);
    cuboid(9,5,0.1,'g');

    glPopMatrix();

}

//gets the randomised book sizes and textures
void GlobeWidget::getBooks(){
    int i = 0;
    for (i=0;i<118;i++){
        books[i] = getRandomTexture();
    }
    this->getBookSizes();
}

//returns random textures
char GlobeWidget::getRandomTexture()
{
    int t = rand() % 8;

    switch (t){
    case 1: return 'g';
    case 2: return 't';
    case 3: return 'l';
    case 4: return '7';
    case 5: return '8';
    case 6: return '9';
    case 7: return 'g';
    }
    return 'g';
}

//gets the random book sizes
void GlobeWidget::getBookSizes(){
    int i = 0;
    for (i=0;i<118;i++){
        bookSize[i] = rand()%4;
    }
}

//draws the book case
void GlobeWidget::bookCase()
{
    glPushMatrix();
    //back
    glTranslatef(0,0,-87);
    cuboid(31.4, 80, 1, '7');

    //near side
    glTranslatef(80 ,0,12);
    cuboid(31.4,1,12, '7');
    glTranslatef(-80 ,0,-12);

    //far side
    glTranslatef(-80 ,0,12);
    cuboid(31.4,1,12, '7');
    glTranslatef(80 ,-5,-2);

    //middle shelf
    cuboid(1,80,8, '7');

    //bottom shelf
    glTranslatef(0 ,-25,0);
    cuboid(1,80,8, '7');

    //top
    glTranslatef(0,55,2);
    cuboid(1,80,12, '7');

    //top row of books
    glTranslatef(78,-22,0);
    cuboid(7,1,4,'g');

    int i = 0;

    for (i=0;i<57;i++){
        glTranslatef(-2.7,0,0);
        cuboid(7+bookSize[i],1,4,books[i]);
    }

    int z = i;
    i=0;

    //bottom row of books
    glTranslatef(0,-25.5,0);

    for (i=0;i<58;i++){
        cuboid(7+bookSize[i],1,4,books[z+i]);
        glTranslatef(2.7,0,0);
    }
    glPopMatrix();
}

//draws the walls and floor
void GlobeWidget::room()
{
    glPushMatrix();

    //floor
    glTranslatef(0.f,-31.4f,0.f);
    cuboid(0.2,90,90,'t');

    //left wall
    glTranslatef(-90.f,90.f,0.f);
    cuboid(90,0.2,90, 'l');

    //right wall
    glTranslatef(90.f,0.f,-90.f);
    cuboid(90,90,0.2, 't');

    glPopMatrix();
}
