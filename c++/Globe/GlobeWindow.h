#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QBoxLayout>
#include <QTime>
#include "GlobeWidget.h"
#include "QTimer"
#include <GL/glut.h>

class GlobeWindow: public QWidget
{
    Q_OBJECT

public slots:
    void timeSet();
    void currentTimeSet();

public:

    // constructor / destructor
    GlobeWindow(QWidget *parent);
    ~GlobeWindow();

    // visual hierarchy
    // menu bar
    QMenuBar *menuBar;
    // file menu
    QMenu *fileMenu;
    // quit action
    QAction *actionQuit;

    // window layout
    QBoxLayout *windowLayout;

    // beneath that, the main widget
    GlobeWidget *globeWidget;
    // add a slider for zooming
    QSlider *zoomSlider;
    // add a label for zooming
    QLabel *zoomLabel;

    // add a slider for rotation
    QSlider *rotationSlider;
    // add a label for rotation
    QLabel *rotationLabel;

    // add sliders for lamp orientation
    QSlider *lampSliderX;
    QSlider *lampSliderY;

    // add labels for lamp sliders
    QLabel * lampLabelX;
    QLabel * lampLabelY;

    // add a spin box for hours
    QSpinBox * hours;

    // add a spin box for minutes
    QSpinBox * minutes;

    // add a button for time entry
    QPushButton * timeButton;

    // add a button for current time entry
    QPushButton *currentTimeButton;
    // add a time member
    QTime * currentTime;

    // add lables for hour and minute spinboxes
    QLabel * hoursLabel;
    QLabel * minutesLabel;

    // vertical window layout
    QVBoxLayout * stack;

    //horizontal layouts for sliders and labels
    QHBoxLayout *zoom;
    QHBoxLayout *rotate;
    QHBoxLayout *lamp;

    //horizontal layout for time entry
    QHBoxLayout* time;

    // resets all the interface elements
    void ResetInterface();

private:

    QTimer * timer;
};

#endif
