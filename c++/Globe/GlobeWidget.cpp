﻿#include <GL/glu.h>
#include <QGLWidget>
#include "GlobeWidget.h"
#include "Globe.cpp"
#include "Environment.cpp"
#include "Materials.cpp"
#include "Time.cpp"
#include "stdio.h"
#include "Planets.cpp"
#include <GL/glut.h>

//light direction, positions and colour
GLfloat lightDir[] = { 0.0, -1.0, 0.0 };
GLfloat lightPos0[] = {0.f,120.f,0.f,1};
GLfloat lightPos1[] = {-60.f, 40.f+10, 20.f, 1};
GLfloat lightPos2[] = {0.f,15.f,10.f,1};
GLfloat lightPos3[] = {-4.f,15.f,10.f,1};
GLfloat lightPos4[] = {4.f,15.f,10.f,1};
GLfloat lightColor[] = {0.9f, 0.9f, 0.9f, 1.0f};

// constructor
GlobeWidget::GlobeWidget(QWidget *parent)
    : QGLWidget(parent)
{
    rotationAngle = 0;

}

//initialiser
void GlobeWidget::initializeGL()
{
    // set the widget background colour
    glClearColor(35/255.f, 0.0, 0.02, 0.0);

    //set up lighting
    this->lighting();
    //get book textures and sizes
    this->getBooks();

    //init alpha colours
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGBA);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_BLEND );

    //load textures
    glEnable(GL_TEXTURE_2D);

    //world texture
    glGenTextures(2, tex);
    glBindTexture(GL_TEXTURE_2D, tex[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2048, 1024, 0, GL_RGB, GL_UNSIGNED_BYTE, earthImageReader->imgBytes);
    gluQuadricDrawStyle(earthQuadric, GLU_FILL);
    gluQuadricTexture(earthQuadric, GL_TRUE);

    //face texture
    glBindTexture(GL_TEXTURE_2D, tex[1]);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128, 0, GL_RGB, GL_UNSIGNED_BYTE, faceImageReader->imgBytes);
    gluQuadricDrawStyle(faceQuadric, GLU_FILL);
    gluQuadricTexture(faceQuadric, GL_TRUE);

}

//called every time the widget is resized
void GlobeWidget::resizeGL(int w, int h)
{
    // set the viewport to the entire widget
    glViewport(0, 0, w, h);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    //sets near and far frame cutoff
    glOrtho(-4.0, 4.0, -4.0, 4.0, -6.0, 24.0);

}

//sets up the lighting
void GlobeWidget::lighting()
{
    //main room light
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightColor);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 80.0);
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
    glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.0);
    glEnable(GL_LIGHT0);

    //standing lamp
    glLightfv(GL_LIGHT1, GL_SPECULAR, lightColor);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 40.0);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, .2);
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 4.0);
    glEnable(GL_LIGHT1);

    //table lamp (3 lights in the lamp)
    glLightfv(GL_LIGHT2, GL_SPECULAR, lightColor);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, lightColor);
    glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 15.0);
    glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, .1);
    glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 180.0);
    glEnable(GL_LIGHT2);

    glLightfv(GL_LIGHT3, GL_SPECULAR, lightColor);
    glLightfv(GL_LIGHT3, GL_DIFFUSE, lightColor);
    glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, 15.0);
    glLightf(GL_LIGHT3, GL_CONSTANT_ATTENUATION, .1);
    glLightf(GL_LIGHT3, GL_SPOT_EXPONENT, 180.0);
    glEnable(GL_LIGHT3);

    glLightfv(GL_LIGHT4, GL_SPECULAR, lightColor);
    glLightfv(GL_LIGHT4, GL_DIFFUSE, lightColor);
    glLightf(GL_LIGHT4, GL_SPOT_CUTOFF, 15.0);
    glLightf(GL_LIGHT4, GL_CONSTANT_ATTENUATION, .1);
    glLightf(GL_LIGHT4, GL_SPOT_EXPONENT, 180.0);
    glEnable(GL_LIGHT4);

    //enble lighting
    glEnable(GL_LIGHTING);
}

//updates the lighting every frame
void GlobeWidget::lightingUpdate()
{
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightDir);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, lightDir);
    glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);
    glLightfv(GL_LIGHT2, GL_POSITION, lightPos2);
    glLightfv(GL_LIGHT3, GL_POSITION, lightPos3);
    glLightfv(GL_LIGHT4, GL_POSITION, lightPos4);
}

//loads the earth texture
void GlobeWidget::enableEarthTexture()
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex[0]);
}

//loads the face texture
void GlobeWidget::enableFaceTexture()
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex[1]);
}

//disables texturing
void GlobeWidget::disableTexture()
{
    glDisable(GL_TEXTURE_2D);
}

//update called by qtimer
void GlobeWidget::frameUpdate()
{
    rotationAngle+=1;
    glScalef(zoomLevel/100.f,zoomLevel/100.f,zoomLevel/100.f);
    this->repaint();
    // flush to screen
    glFlush();
}

//zoom slider update
void GlobeWidget::zoomUpdate(int value)
{
    this->zoomLevel = value;
}

//rotation slider update
void GlobeWidget::rotationSlider(int s)
{
    this->rotationSliderAngle = s;
}

//lamp x rotation slider update
void GlobeWidget::lampRotationSliderX(int angle)
{
    lampRotationAngleX = angle;
}

//lamp y rotation slider update
void GlobeWidget::lampRotationSliderY(int angle)
{
    lampRotationAngleY = angle;
}

// called every time the widget needs painting
void GlobeWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);
    glPushMatrix();
    glLoadIdentity();
    glPopMatrix();

    //rotate scene from slider
    glRotatef(rotationSliderAngle, 0.f, 1.f,0.f);

    //draw the objects
    this->dartBoard();
    this->lamp();
    this->lightingUpdate();
    this->globe();
    this->table();
    this->chair();
    this->bookCase();
    this->room();
    this->planetStand();

    //rescale
    glScalef(0.1,0.1,0.1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //gluLookAt(0.f,0.0f,1.0f, 0.0,0.0,0.0, 0.0,1.0,0.0);

    gluLookAt(1.f,1.f,1., 0,0,0, 0.0,1.0,0.0);

} // paintGL()
