//draws the earth
void GlobeWidget::earth()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,7.f);
    glPushMatrix();
    glTranslatef(0.f,0.f,-2.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle, 0.f, 1.f, 0.f);

    //draw the curved planet shaft
    char i =0;
    for (i=0;i<36;i++){
        glTranslatef(0,10,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-10,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the earth
    setMaterial('5');
    gluSphere(quadratic, 1.f, 20, 5);
    glPushMatrix();
    glRotatef(rotationAngle*10, 1.f, 0.f, 0.f);

    //draw the moon shaft
    for (i=0;i<8;i++){
        glTranslatef(0,0.2,0);
        this->cuboid(0.1f,0.1f,0.1f, 'b');
    }

    //draw the moon
    setMaterial('w');
    gluSphere(quadratic, 0.5f, 20, 5);
    glPopMatrix();
    glPopMatrix();
}

//draws venus
void GlobeWidget::venus()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-1.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*1.2, 0.f, 1.f, 0.f);

    //draw the curved planet shaft
    char i = 0;
    for (i=0;i<36;i++){
        glTranslatef(0,6.9,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-6.9,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<5;i++){
        glTranslatef(-0.4,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw venus
    setMaterial('t');
    gluSphere(quadratic, 1.f, 20, 5);
    glPopMatrix();
}

//draws mercury
void GlobeWidget::mercury()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-0.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*1.4, 0.f, 1.f, 0.f);

    //draw the curved planet shaft
    char i = 0;
    for (i=0;i<36;i++){
        glTranslatef(0,4.2,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-4.2,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<7;i++){
        glTranslatef(-0.51,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw mercury
    setMaterial('r');
    gluSphere(quadratic, 1.f, 20, 5);
    glPopMatrix();
}

//draws mars
void GlobeWidget::mars()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-3.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*0.8, 0.f, 1.f, 0.f);

    //draw the horizontal extension
    char i = 0;
    for (i=0;i<10;i++){
        glTranslatef(-0.52f,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the curved planet shaft
    i =0;
    for (i=0;i<36;i++){
        glTranslatef(0,8,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-8,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<5;i++){
        glTranslatef(-0.6,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw mars
    setMaterial('r');
    gluSphere(quadratic, 1.f, 20, 5);
    glPopMatrix();
}

//draws jupiter
void GlobeWidget::jupiter(){

    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-4.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*0.7, 0.f, 1.f, 0.f);

    //draw the horizontal extension
    char i = 0;
    for (i=0;i<16;i++){
        glTranslatef(-0.52f,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }
    i =0;

    //draw the curved planet shaft
    for (i=0;i<36;i++){
        glTranslatef(0,8,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-8,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<7;i++){
        glTranslatef(-0.55,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw jupiter
    setMaterial('3');
    gluSphere(quadratic, 1.5f, 20, 5);
    glPopMatrix();
}

//draws saturn
void GlobeWidget::saturn()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-5.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*0.6, 0.f, 1.f, 0.f);

    //draw the horizontal extension
    char i = 0;
    for (i=0;i<22;i++){
        glTranslatef(-0.52f,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the curved planet shaft
    i =0;
    for (i=0;i<36;i++){
        glTranslatef(0,8,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-8,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<9;i++){
        glTranslatef(-0.55,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw saturn
    setMaterial('2');
    gluSphere(quadratic, 1.3f, 20, 5);
    glPopMatrix();
}

//draws uranus
void GlobeWidget::uranus()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-6.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*0.5, 0.f, 1.f, 0.f);

    //draw the horizontal extension
    char i =0;
    for (i=0;i<28;i++){
        glTranslatef(-0.52f,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the curved planet shaft
    i =0;
    for (i=0;i<36;i++){
        glTranslatef(0,8,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-8,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<11;i++){
        glTranslatef(-0.55,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw uranus
    setMaterial('1');
    gluSphere(quadratic, 1.2f, 20, 5);
    glPopMatrix();

}

//draws neptune
void GlobeWidget::neptune()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-7.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*0.4, 0.f, 1.f, 0.f);

    //draw the horizontal extension
    char i = 0;
    for (i=0;i<33;i++){
        glTranslatef(-0.52f,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');;
    }
    i =0;

    //draw the curved planet shaft
    for (i=0;i<36;i++){
        glTranslatef(0,8,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-8,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<13;i++){
        glTranslatef(-0.55,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw neptune
    setMaterial('1');
    gluSphere(quadratic, 1.2f, 20, 5);
    glPopMatrix();
}

//draws pluto
void GlobeWidget::pluto()
{
    //orient the placement of the planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-8.2f);
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glRotatef(rotationAngle*0.3, 0.f, 1.f, 0.f);

    //draw the horizontal extension
    char i = 0;
    for (i=0;i<37;i++){
        glTranslatef(-0.52f,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }
    i =0;

    //draw the curved planet shaft
    for (i=0;i<36;i++){
        glTranslatef(0,8,0);
        glRotatef(-2.5, 0., 0., 1.);
        glTranslatef(0,-8,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw the vertical extension
    for (i=0;i<15;i++){
        glTranslatef(-0.55,0,0);
        this->cuboid(0.1f,0.3f,0.1f, 'b');
    }

    //draw pluto
    setMaterial('w');
    gluSphere(quadratic, 0.4f, 20, 5);
    glPopMatrix();
}

//draws the planet model
void GlobeWidget::planetStand()
{
    //planet base
    setMaterial('t');
    glPushMatrix();
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glTranslatef(-25.0f,0.f,-2.f);
    gluCylinder(quadratic, 2.f, 7.f, 2.f, 16, 16);
    glRotatef(180.f, 1.f, 0.f, 0.f);
    gluDisk(quadratic, 0.f, 5.f, 20, 20);

    //planet shaft
    glPushMatrix();
    glTranslatef(0.f,0.f,-.3f);
    gluCylinder(quadratic, .5f, .5f, 16.0f, 16, 16);
    glTranslatef(0.f,0.f,2.9f);
    gluDisk(quadratic, 0.f, 1.f, 20, 20);

    //sun
    enableFaceTexture();

    glPushMatrix();
    glTranslatef(0.f,0.f,14.5f);
    setMaterial('4');
    gluSphere(faceQuadric, 2.f, 20, 5);
    glPopMatrix();

    disableTexture();

    //draw the planets
    earth();
    venus();
    mercury();
    mars();
    jupiter();
    saturn();
    uranus();
    neptune();
    pluto();

    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
}
