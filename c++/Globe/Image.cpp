#include <QImage>
#include "Image.h"
#include <stdio.h>

//constructor
ImageReader::ImageReader(QString name)
{
    image = new QImage(name, "PPM");
    int i = 0;
    int j = 0;
    int index = 0;
    int width = image->width();
    int height = image->height();
    int imageSize = 3*(height*width);
    imgBytes = new GLbyte[imageSize];

    for (j=0;j<height;j++){
        for (i=width-1;i>=0;i--){
            imgBytes[index] = qRed(image->pixel(i,j));
            index+=1;
            imgBytes[index] = qGreen(image->pixel(i,j));
            index+=1;
            imgBytes[index] = qBlue(image->pixel(i,j));
            index+=1;
        }
    }

}

ImageReader::~ImageReader()
{
    image->~QImage();
    delete imgBytes;
}
