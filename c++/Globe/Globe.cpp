//draws the base of the globe
void GlobeWidget::globeBase()
{
    //globe bottom
    setMaterial('t');
    glPushMatrix();
    glRotatef(90.f, 1.f, 0.f, 0.f);
    glTranslatef(-6.0f,0.f,4.f);
    gluCylinder(quadratic, 5.f, 10.f, 3.f, 16, 16);

    glRotatef(180.f, 1.f, 0.f, 0.f);
    gluDisk(quadratic, 0.f, 5.f, 20, 20);

    glPushMatrix();
    glTranslatef(0.f,0.f,-.3f);

    //globe shaft
    gluCylinder(quadratic, 1.f, 1.f, 2.9f, 16, 16);

    glTranslatef(0.f,0.f,2.9f);
    gluDisk(quadratic, 0.f, 1.f, 20, 20);

    glPopMatrix();
    glPopMatrix();
}

//draws the curved frame and spindles of the globe
void GlobeWidget::arcAndSpindles()
{
    this->cuboid(1.f,1.f,0.5f, 'b');
    glPushMatrix();
    glRotatef(180.f, 1.f, 0.f, 0.f);
    glRotatef(90.f, 1.f, 0.f, 0.f);

    //bottom spindle
    gluCylinder(quadratic, 0.5f, 0.5f, 3.f, 16, 16);
    glRotatef(180.f, 1.f, 0.f, 0.f);
    gluDisk(quadratic, 0.f, 0.5f, 20, 20);
    glPopMatrix();
    glPushMatrix();

    //arc
    char i =0;
    for (i=0;i<18;i++){
        glTranslatef(0,10,0);
        glRotatef(-10, 0., 0., 1.);
        glTranslatef(0,-10,0);
        this->cuboid(1.f,1.f,0.5f, 'b');
    }

    glPushMatrix();
    glRotatef(180.f, 1.f, 0.f, 0.f);
    glRotatef(90.f, 1.f, 0.f, 0.f);

    // top spindle
    gluCylinder(quadratic, 0.5f, 0.5f, 3.f, 16, 16);
    glRotatef(180.f, 1.f, 0.f, 0.f);
    gluDisk(quadratic, 0.f, 0.5f, 20, 20);
    glPopMatrix();
}

//draws the clocks onto the globe
void GlobeWidget::clocks()
{
    char i = 0;
    for (i=0;i<4;i++){

        glPushMatrix();

        //rotate to the correct countries for the clock: UK, Thailand, New Zealand, Los Angeles
        switch (i){
        case 0:
            glRotatef(-180.f,0,1,0);
            glRotatef(-55.f, 1,0,0);
            break;
        case 1:
            glRotatef(-80.f,0,1,0);
            glRotatef(-20.f,1,0,0);
            break;
        case 2:
            glRotatef(0.f,0,1,0);
            glRotatef(45.f, 1,0,0);
            break;
        case 3:
            glRotatef(-310.f,0,1,0);
            glRotatef(-40.f, 1,0,0);
            break;
        }

        glTranslatef(0.f, 0.f, 8.f);
        setMaterial('l');

        // clock face
        gluDisk(quadratic, 0.f, 2.f, 20, 20);

        glPushMatrix();

        // divisions
        char j = 0;
        for (j=0;j<12;j++){

            glPushMatrix();
            glRotatef(180.f, 0.f, 0.f, -1.f);
            glTranslatef(0.f, -1.9f, 0.1f);
            glTranslatef(0.f, 1.9f, 0.f);
            glRotatef(j*30.f, 0.f, 0.f, -1.f);
            glTranslatef(-0.f, -1.9f, -0.f);
            //make division at 12 oclock green to stand out
            if (j == 0){
                this->cuboid(.1f, 0.05f, 0.01f, 'g');
            }
            //rest white
            else{
                this->cuboid(.1f, 0.05f, 0.01f, 'w');
            }
            glPopMatrix();
        }

        // minute hand
        glPushMatrix();
        glTranslatef(0.f, -.7f, 0.1f);
        glTranslatef(0.f, .7f, 0.f);
        glRotatef(180.f + minuteAngle, 0.f, 0.f, -1.f);
        glTranslatef(-0.f, -.7f, -0.f);
        this->cuboid(1.f, 0.1f, 0.1f, 'w');
        glPopMatrix();

        // hour hand
        glPushMatrix();
        glTranslatef(0.f, -0.4f, 0.1f);
        glTranslatef(0.f, 0.4f, 0.f);
        if (i==0)
            glRotatef(180.f + hourAngleUK, 0.f, 0.f, -1.f);

        else if (i==1)
            glRotatef(180.f + hourAngleBK, 0.f, 0.f, -1.f);

        else if (i==2)
            glRotatef(180.f + hourAngleNZ, 0.f, 0.f, -1.f);

        else if (i==3)
            glRotatef(180.f + hourAngleLA, 0.f, 0.f, -1.f);

        glTranslatef(-0.f, -0.4f, -0.f);
        this->cuboid(.6f, 0.1f, 0.1f, 'w');

        glPopMatrix();
        glPopMatrix();
        glPopMatrix();
    }
}

//draws the globe
void GlobeWidget::globe()
{
    glPushMatrix();
    glTranslatef(28.f,7.0f,0.f);

    // globe base
    this->globeBase();

    glPushMatrix();
    glTranslatef(0.f,-1.2f,0.f);

    // rotate for axial tilt
    glPushMatrix();
    glRotatef(23.43697f, 0.f, 0.f, 1.f);
    glPushMatrix();
    glTranslatef(0.f,10.f,0.f);
    glRotatef(rotationAngle/3.f, 0., 1., 0.);

    setMaterial('w');

    // globe sphere
    enableEarthTexture();
    glPushMatrix();
    glRotatef(90,1,0,0);
    gluSphere(earthQuadric, 8.f, 20, 20);
    glPopMatrix();
    disableTexture();

    setMaterial('b');

    // draw clocks
    this->clocks();

    glPopMatrix();

    // draw arc and spindles
    this->arcAndSpindles();

    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
}
