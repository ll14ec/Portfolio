#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <GL/glut.h>
#include "Image.h"

class GlobeWidget: public QGLWidget
{
    Q_OBJECT

public slots:
    //update from qtimer
    void frameUpdate();

    //update from zoom slider
    void zoomUpdate(int);

    //update from rotation slider
    void rotationSlider(int);

    //update from lamp sliders
    void lampRotationSliderX(int);
    void lampRotationSliderY(int);

public:
    //constructor
    GlobeWidget(QWidget *parent);

    //variables controlled by sliders
    float rotationAngle;
    int zoomLevel;
    int rotationSliderAngle;
    int lampRotationAngleX;
    int lampRotationAngleY;

    //clock angles
    float minuteAngle;
    float hourAngleUK;
    float hourAngleLA;
    float hourAngleBK;
    float hourAngleNZ;

    //quadrics
    GLUquadricObj * quadratic;
    GLUquadricObj * earthQuadric;
    GLUquadricObj * faceQuadric;

    //image readers
    ImageReader * earthImageReader;
    ImageReader * faceImageReader;

    //textures
    GLuint tex[2];

    //book textures and sizes
    char books[118];
    char bookSize[118];

    //time setting function
    void setTime(float, float);

protected:
    // called when OpenGL context is set up
    void initializeGL();
    // called every time the widget is resized
    void resizeGL(int, int);
    // called every time the widget needs painting
    void paintGL();

private:

    //position of the standing lamp
    float lampPos [3];

    //texture enabling functions
    void enableEarthTexture();
    void enableFaceTexture();
    void disableTexture();

    //lighting functions
    void lighting();
    void lightingUpdate();

    //object functions
    void planetStand();
    void mercury();
    void venus();
    void earth();
    void mars();
    void jupiter();
    void saturn();
    void uranus();
    void neptune();
    void pluto();

    void dartBoard();
    void table();
    void lamp();
    void greenLampTop();
    void chair();
    void bookCase();
    void room();

    void globe();
    void clocks();

    void globeBase();
    void arcAndSpindles();

    //book functions
    char getRandomTexture();
    void getBooks();
    void getBookSizes();

    //clock hand calculations
    void getMinuteAngle(float);
    void getHourAngle(float, float);

    //cuboid drawing function
    void cuboid(float,float,float, char);

    //material setting function
    void setMaterial(char);
};

#endif
