#include <QApplication>
#include <QVBoxLayout>
#include "GlobeWindow.h"
#include <GL/glut.h>

int main(int argc, char *argv[])
{ // main()
    // create the application
    QApplication app(argc, argv);

    // create model (polygon) as a triangle
    //	GLPolygon *polygon = new GLPolygon();

    // create a master widget
    GlobeWindow *window = new GlobeWindow(NULL);

    glutInit(&argc, argv);

    //glutInitWindowSize(666,861);

    glutInitWindowSize(512,672);

    // create a controller to hook things up
    //	GLPolygonController *controller = new GLPolygonController(window, polygon);

    // resize the window


    //window->resize(666,861);

    window->resize(512, 672);

    // show the label
    window->show();

    // start it running
    app.exec();

    // clean up
    //	delete controller;
    delete window;

    // return to caller
    return 0;
} // main()
