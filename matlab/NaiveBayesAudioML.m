addpath("/home/ed/Desktop/audio/");
addpath("/home/ed/Downloads/PeteRock/");
files1=dir('/home/ed/Desktop/audio/*.wav');
files2=dir('/home/ed/Downloads/PeteRock/*.wav');
features1=(1:100000);
features2=(1:100000);
column = zeros(size(features1));
features1 = [features1; column];
features2 = [features2; column];

for index = 1 : length(files2)
  fprintf('Processing %s\n', files2(index).name);
  data = wavread(files2(index).name);
  samplespace = size(data);
  for i = 1 : samplespace
    z = data(i);
    if z < 0
      z *= -1;
    end
    z *= 100000;
    z = round(z);
    if z != 0
      features2(2,z)++;
    end
  end
  result2{index} = data;
end

for index = 1 : length(files1)
  fprintf('Processing %s\n', files1(index).name);
  data = wavread(files1(index).name);
  samplespace = size(data);
  for i = 1 : samplespace
    z = data(i);
    if z < 0
      z *= -1;
    end
    z *= 100000;
    z = round(z);
    if z != 0
      features1(2,z)++;
    end
  end
  result1{index} = data;
end

save datafile.txt result1 result2

%for index = 1 : 100000
%  if features1(2,index) != 0
%    features1(1,index);
%    features1(2,index);
%  end
%end

