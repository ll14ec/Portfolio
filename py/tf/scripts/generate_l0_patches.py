import multiresolutionimageinterface as mir
import png
import numpy as np
import xml.etree.ElementTree as ET
import os
import random

# reader for wsi
reader = mir.MultiResolutionImageReader()
# directory to read tifs/write pngs
normal_read_directory = 'D:\\CAMELYON\\normal_tif\\'
tumor_read_directory = 'D:\\CAMELYON\\tumor_tif\\'
tumor_write_directory = 'C:\\Users\\philu\\tf\\l0_grids\\t\\tumor\\'
normal_write_directory = 'C:\\Users\\philu\\tf\\l0_grids\\t\\normal\\'

list_output_file = 'C:\\Users\\philu\\tf\\models\\research\\slim\\examine_list_l0.txt'

l1_out_file = 'C:\\Users\\philu\\tf\\layer_outputs\\l1_out.txt'
l0_grid_debug_dir = 'C:\\Users\\philu\\tf\\_debug\\'

pngs = 0
ds = 1.0

# init png writer
w = png.Writer(299, 299, greyscale=False)
# set downsample level
level = 0
xGridSize = 160 * 2
yGridSize = 356 * 2
files = 0

l1_list = []
l0_list = []

# open level 3 output and get list of squares
with open(l1_out_file, 'r') as l1_out:
    data = l1_out.read().split()
    for elem in data:
        try:
            l1_list.append(int(elem))
            # add 4 items to level 2 list per one item in l3 list
            l0_list.append(3)
            l0_list.append(3)
            l0_list.append(3)
            l0_list.append(3)
        except ValueError:
            pass

l1_list_counter = 0
current_image = -1

# convert level 3 list of squares to level 2 list of squares
for elem in l1_list:
    # calculate which image
    if l1_list_counter % (40 * 89 * 4 * 4) == 0:
        current_image += 1
    # get x and y for l3 list
    l1_x = int((l1_list_counter - (current_image * 40 * 89 * 4 * 4)) / (89*2*2))
    l1_y = int((l1_list_counter - (current_image * 40 * 89 * 4 * 4)) % (89*2*2))
    # get x1, x2, y1, y2 for l2 list
    l0_x1 = l1_x * 2
    l0_x2 = l0_x1 + 1
    l0_y1 = l1_y * 2
    l0_y2 = l0_y1 + 1
    # set squares
    l0_list[l0_x1 + (l0_y1 * 80 * 4) + (current_image * 80 * 178 * 4 * 4)] = elem
    l0_list[l0_x2 + (l0_y1 * 80 * 4) + (current_image * 80 * 178 * 4 * 4)] = elem
    l0_list[l0_x1 + (l0_y2 * 80 * 4) + (current_image * 80 * 178 * 4 * 4)] = elem
    l0_list[l0_x2 + (l0_y2 * 80 * 4) + (current_image * 80 * 178 * 4 * 4)] = elem
    l1_list_counter += 1

with open(list_output_file, 'w') as list_output:
    for element in l0_list:
        list_output.write('%s\n' % element)

# go through all filenames in the normal directory that end in tif
for filename in os.listdir(normal_read_directory):
    if filename.endswith(".tif"):
        files += 1
        # get wsi filename
        # open tif and set downsample level for png reading
        print("Opening " + filename + " whole slide image...")
        mr_image = reader.open(normal_read_directory + filename)

        for x in range(xGridSize):
            # print ("Col " + str(x+1) + " of " + str(xGridSize) + ". Files processed: " + str(files-1) + ".")
            for y in range(yGridSize):
                if l0_list[x + (y * 80 * 2 * 2) + ((files - 1) * 80 * 178 * 4 * 4)] == 1:
                    # print("writing " + filename + " pixel x:" + str(x) + " y: " + str(y))
                    image_patch = mr_image.getUCharPatch(int(x * 299 * ds), int(y * 299 * ds), 299, 299, level)
                    pixels = np.reshape(image_patch, (299, 299 * 3)).astype(np.uint8)
                    f = open(normal_write_directory + filename[:-4] + '_' + str(10000 + y + x * 1000) + '.png', 'wb')
                    w.write(f, pixels)
                    pngs += 1

# calculate offset for tumor files
l0_list_offset = files * xGridSize * yGridSize

print("Scanned " + str(files) + " normal files.")
print("Wrote " + str(pngs) + " pngs to " + normal_write_directory)
files = 0
pngs = 0

# go through all filenames in the tumor directory that end in tif
for filename in os.listdir(tumor_read_directory):
    if filename.endswith(".tif"):
        files += 1
        # get wsi filename
        # open tif and set downsample level for png reading
        print("Opening " + filename + " whole slide image...")
        mr_image = reader.open(tumor_read_directory + filename)

        for x in range(xGridSize):
            # print ("Col " + str(x+1) + " of " + str(xGridSize) + ". Files processed: " + str(files-1) + ".")
            for y in range(yGridSize):
                if l0_list[x + (y * 80 * 2 * 2) + ((files - 1) * 80 * 178 * 4 * 4) + l0_list_offset] == 1:
                    # print("writing " + filename + " pixel x:" + str(x) + " y: " + str(y))
                    image_patch = mr_image.getUCharPatch(int(x * 299 * ds), int(y * 299 * ds), 299, 299, level)
                    pixels = np.reshape(image_patch, (299, 299 * 3)).astype(np.uint8)
                    f = open(tumor_write_directory + filename[:-4] + '_' + str(10000 + y + x * 1000) + '.png', 'wb')
                    w.write(f, pixels)
                    pngs += 1


print("Scanned " + str(files) + " tumor files.")
print("Wrote " + str(pngs) + " pngs to " + tumor_write_directory)


# # DEBUG IMAGE
# # stores the l2 grid data
# tempPixelData = np.zeros((178, 80 * 3))
# # write l2 grid debug image
# debug_w = png.Writer(80, 178, greyscale=False)
#
# for y in range(178):
#     for x in range(80):
#         tempPixelData[y][x * 3] = l2_list[x + (y * 80)]
#         tempPixelData[y][(x * 3) + 1] = l2_list[x + (y * 80)]
#         tempPixelData[y][(x * 3) + 2] = l2_list[x + (y * 80)]
#
# tempPixelData *= 255.0
# png_file = open(l2_grid_debug_dir + 'debug.png', 'wb')
# debug_w.write(png_file, tempPixelData.astype(np.uint8))
