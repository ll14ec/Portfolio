import xml.etree.ElementTree as ET
import numpy as np
import os 

directory = 'C:\\Users\\philu\\Desktop\\'

pointList = np.array([-1.0,-1.0])
boxes = 0
files = 0
coords = 0
quadCounter = 0



for filename in os.listdir(directory):
  if filename.endswith(".xml"):
    print (("Scanning file: ") + (filename))
    files +=1

    e = ET.parse(directory + filename).getroot()
    annotations = e.find('Annotations')

    point = np.array([-1.0,-1.0])
    points = np.array([-1.0,-1.0])
    quad = np.array([-1.0,-1.0])

    for annotation in annotations.iter('Annotation'):
      boxes +=1
      for coord in annotation.iter('Coordinate'):
        #print ((" X: ") + (coord.get('X')) + ("  Y: ") + (coord.get('Y')))
        coords +=1
        quadCounter +=1
        point[0] = coord.get('X')
        point[1] = coord.get('Y')
        points = np.append(points, point,axis=0)
        if (quadCounter>4):
          quadCounter = 0
          print (quad[2:])
          quad = np.array([-1.0,-1.0])
        else:
          quad = np.append(quad, point)

    #print (points)
    print ((filename) + (": "))
    print (points[2:])
    pointList = np.append(pointList, points, axis=0)
	
print ("Scanned " + str(files) + " files, " + str(boxes) + " boxes, " + str(coords) + " points.")
#print ("Final Point list:")
#print (pointList)