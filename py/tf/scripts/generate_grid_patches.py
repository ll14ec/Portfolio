import multiresolutionimageinterface as mir
import png
import numpy as np
import xml.etree.ElementTree as ET
import os
import random

# reader for wsi
reader = mir.MultiResolutionImageReader()
# directory to read tifs/write pngs
read_directory = 'D:\\CAMELYON\\tumor_tif\\'
write_directory = 'C:\\Users\\philu\\tf\\test_grids\\t\\tumor\\'
list_output = 'C:\\Users\\philu\\tf\\models\\research\\slim\\examine_list_tumor.txt'

pngs = 0

# init png writer
w = png.Writer(299, 299, greyscale=False)
# set downsample level
level = 3
xGridSize = 40
yGridSize = 89
ds = 8
files = 0
element_counter = 0
examine_list = []

# go through all filenames in the directory that end in tif
for filename in os.listdir(read_directory):
    if filename.endswith(".tif"):
        files += 1
        # get wsi filename
        # open tif and set downsample level for png reading
        print("Opening " + filename + " whole slide image...")
        mr_image = reader.open(read_directory + filename)
        print("Generating tissue images...")

        for x in range(xGridSize):
            # print ("Col " + str(x+1) + " of " + str(xGridSize) + ". Files processed: " + str(files-1) + ".")
            for y in range(yGridSize):
                image_patch = mr_image.getUCharPatch(x * 299 * ds, y * 299 * ds, 299, 299, level)
                pixels = np.reshape(image_patch, (299 * 3, 299)).astype(np.uint8)
                element_counter = 0
                red_pixels = 0

                for element in np.nditer(pixels):
                    element_counter += 1
                    if (element_counter % 3 == 1):
                        # print ('R: ' + str(element), end=' ')
                        r = element
                    if (element_counter % 3 == 2):
                        # print ('G: ' + str(element), end=' ')
                        g = element
                    if (element_counter % 3 == 0):
                        # print ('B: ' + str(element), end='\n')
                        b = element
                        if ((int(r) > (int(g) + int(b)))):
                            red_pixels += 1
                    if red_pixels > 10:
                        break

                if (red_pixels > 10):
                    pixels = np.reshape(image_patch, (299, 299 * 3)).astype(np.uint8)
                    f = open(write_directory + filename[:-4] + '_' + str(10000 + y + x * 89) + '.png', 'wb')
                    w.write(f, pixels)
                    pngs += 1
                    examine_list.append('1')
                else:
                    examine_list.append('0')

with open(list_output, 'w') as f:
    for element in examine_list:
        f.write('%s\n' % element)

print("Scanned " + str(files) + " files.")
print("Wrote " + str(pngs) + " pngs to " + write_directory)
