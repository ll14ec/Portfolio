import png
import os
import numpy as np
import sys

write_directory = 'C:\\Users\\philu\\tf\\eye_images\\filtered_l3\\'
inputFile = "outfile_filtered_l3_partial.txt"
l3_out_file = 'C:\\Users\\philu\\tf\\layer_outputs\\l3_out.txt'

imageSize = 40 * 89
imageCount = 0

tempPixelData = np.zeros((89, 40 * 3))
annotationSize = 299
ds = 8

w = png.Writer(40, 89, greyscale=False)

with open(inputFile) as f:
    data = f.read().split()
    floats = []
    for elem in data:
        try:
            floats.append(float(elem))
        except ValueError:
            pass

    print(str(int(len(floats) / 3)) + (" numbers in " + inputFile))
    imageCount = int((len(floats) / 3) / imageSize)
    print(str(imageCount) + " images to create.")

    # open layer output file
    layer_output_file = open(l3_out_file, 'w')

    for fileIndex in range(0, imageCount):
        sys.stdout.write('\r')
        sys.stdout.flush()
        sys.stdout.write("Processing [" + str(fileIndex + 1) + " of " + str(imageCount) + "] " + str(
            round(((fileIndex + 1) / imageCount) * 100, 1)) + "%")
        sys.stdout.flush()

        # write annotation header
        annotation_file = open(write_directory + 'annotation_' + str(fileIndex) + '.xml', 'w')
        annotation_file.write("<?xml version=\"1.0\"?>\n<ASAP_Annotations>\n<Annotations>\n")

        for i in range(0, 40):
            for j in range(0, 89):
                tempPixelData[j][i * 3] = 1 - floats[(j + i * 89 + (fileIndex * imageSize)) * 3]
                tempPixelData[j][(i * 3) + 1] = 1 - floats[(j + i * 89 + (fileIndex * imageSize)) * 3]
                tempPixelData[j][(i * 3) + 2] = 1 - floats[(j + i * 89 + (fileIndex * imageSize)) * 3]
                xCoord1 = (i * annotationSize * ds) - (annotationSize * ds / 2)
                yCoord1 = (j * annotationSize * ds) - (annotationSize * ds / 2)
                xCoord2 = (i * annotationSize * ds) + (annotationSize * ds / 2)
                yCoord2 = (j * annotationSize * ds) + (annotationSize * ds / 2)
                if floats[(j + i * 89 + (fileIndex * imageSize)) * 3] < 0.5:
                    # write annotation body
                    annotation_file.write("<Annotation Name=\"Annotation" + str(j + i * 90) + "\"  Type=\"Rectangle\" PartOfGroup=\"None\" Color=\"#F4FA58\">\n")
                    annotation_file.write("<Coordinates>\n")
                    annotation_file.write("<Coordinate Order=\"0\" X=\"" + str(xCoord1) + "\" Y=\"" + str(yCoord1) + "\" />\n")  # 00
                    annotation_file.write("<Coordinate Order=\"1\" X=\"" + str(xCoord2) + "\" Y=\"" + str(yCoord1) + "\" />\n")  # 10
                    annotation_file.write("<Coordinate Order=\"2\" X=\"" + str(xCoord2) + "\" Y=\"" + str(yCoord2) + "\" />\n")  # 11
                    annotation_file.write("<Coordinate Order=\"3\" X=\"" + str(xCoord1) + "\" Y=\"" + str(yCoord2) + "\" />\n")  # 01
                    annotation_file.write("</Coordinates>\n")
                    annotation_file.write("</Annotation>\n")
                    # write to layer output file
                    layer_output_file.write("1\n")
                else:
                    layer_output_file.write("0\n")

        # write annotation footer
        annotation_file.write("</Annotations>\n<AnnotationGroups />\n</ASAP_Annotations>\n")

        # write image
        tempPixelData *= 255.0
        png_file = open(write_directory + 'eye_' + str(fileIndex) + '.png', 'wb')
        w.write(png_file, tempPixelData.astype(np.uint8))
