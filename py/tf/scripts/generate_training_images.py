import multiresolutionimageinterface as mir
import png
import numpy as np
import xml.etree.ElementTree as ET
import os 
import random


numNormalTrain=70
numTumorTrain = 60

#reader for wsi
reader = mir.MultiResolutionImageReader()
#directory to read tifs/write pngs 
read_directory = 'D:\\CAMELYON\\tumor_tif\\'
write_directory = 'C:\\Users\\philu\\tf\\box0\\0\\tumor\\'
#numer of pngs to generate per tif
#samples = int(50.0 * numNormalTrain/numTumorTrain)
#samples = 0.3
samples = 6
#samples = 2
print (str(samples) + " samples per WSI.")

randomProb = 0.0
randThres = 10-(randomProb*10)
threshold = 300
boxes = 0
files = 0
coords = 0
quadCounter = 0
pngs = 0
box = np.zeros(4);
#init png writer
w = png.Writer(299,299,greyscale=False)
#set downsample level
level = 0


#go through all filenames in the directory that have a annotation box (xml file)
for filename in os.listdir(read_directory):
  if filename.endswith(".xml"):
    print (("Scanning file: ") + (filename))
    files +=1
    #parse xml
    e = ET.parse(read_directory + filename).getroot()
    annotations = e.find('Annotations')
    #reset points
    point = np.array([-1.0,-1.0])
    points = np.array([-1.0,-1.0])
    #traverse xml
    for annotation in annotations.iter('Annotation'):
      boxes +=1
      #look at box coords, append each xy pair to points
      for coord in annotation.iter('Coordinate'):
        coords +=1
        quadCounter +=1
        point[0] = coord.get('X')
        point[1] = coord.get('Y')
        points = np.append(points, point,axis=0)
    #print(str(points))
    #print(len(points))
    boxesInImage = int((len(points)-2)/8)
    boxIndex= -1
    for b in range (0,boxesInImage):
      boxIndex +=1
      #save the box vertices
      box[0] = points[2+(8*boxIndex)]
      box[1] = points[4+(8*boxIndex)]
      box[2] = points[3+(8*boxIndex)]
      box[3] = points[7+(8*boxIndex)] 
    
      #get wsi filename
      wsi = filename[:-4] + ".tif"
      #open tif and set downsample level for png reading
      print ("Opening " + wsi + " whole slide image...")
      mr_image = reader.open(read_directory + wsi)
      ds = mr_image.getLevelDownsample(level)
    
      #get box area
      xLength = (box[1] - box[0]) / (8.0*299)
      yLength = (box[3] - box[2]) / (8.0*299)
      boxArea = yLength*xLength
      attempts=0
      
      print ("Generating training patches at magnification = "+ str(ds) + "...")
      #generate a randomply placed png (in the box) for each sample
      samplesInBox = int(samples*boxArea)
      if samplesInBox < 7:
        samplesInBox = 7
      elif samplesInBox > 68:
        samplesInBox = 68
      
      for i in range(samplesInBox):
        while True:
          #generate x,y in the box (offset by -150 because this is the top left corner of the box)
          #also make sure no values < 0
          if (random.randint(0,10)>randThres):
            randX = random.randint(1,50000)
            randY = random.randint(1,50000)
          else:
            randX = max((random.randint(int(box[0]), int(box[1])) - 150),0)
            randY = max((random.randint(int(box[2]), int(box[3])) - 150),0)
          image_patch = mr_image.getUCharPatch(randX, randY, 299, 299, level)
          if np.average(image_patch)<threshold:
            break

        f = open(write_directory + filename[:-4] + '_' + str(boxIndex) + '_' + str(i) + '.png', 'wb')
        w.write(f, np.reshape(image_patch, (299,299*3)).astype(np.uint8))
        pngs+=1
        
print ("Scanned " + str(files) + " files, " + str(boxes) + " boxes, " + str(coords) + " points.")
print ("Wrote " + str(pngs) + " pngs to " + write_directory)