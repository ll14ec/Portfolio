#reinforcement learning, navigating an agent through a domain 
#using SARSA and Q learning

#define the domain

# |O|O|S|O|O|    S = snake = -10 reward
# |O|X|O|O|B|
# |S|O|S|O|O|    B = bitcoin = 100 reward
# |O|O|X|O|O|    X = death = -100 reward absorbing
# |O|O|O|X|O|    0 = empty state = 0 rweard

# prossible actions u, d, l, r
# possibe states 0,0 bottom left -> 4,4 top right

import random as rd
import numpy as np
import matplotlib.pyplot as plt

###############################################

#         ENVIRONMENT IMPLEMENTATION          #

###############################################

environment = [ [ 0,    0,     0,  -10,    0 ],
                [ 0,    0,  -100,    0,    0 ],
                [ 0,    0,     0,    0,    0 ],
                [ 0,  -10,     0,    0,    0 ],
                [ 0,    0,     0,  100,    0 ] ]

size  = [5,5]

def env_move_det(s,a):
    #any move action from absorb state returns the current state
    if env_reward(s,a,s)[1] == True:
        return s
    #if movement is up and not on the top edge
    if (a == "u"):
        if (s[0] < size[0]-1):
            return [s[0]+1, s[1]]
        # if on the top edge
        else:
            return s
    # if movement is down and not on the bottom edge
    elif (a == "d"):
        if (s[0] > 0):
            return [s[0]-1, s[1]]
        # if on the bottom edge
        else:
            return s
    # if movement is right and not on the right edge
    elif (a == "r"):
        if (s[1] < size[1]-1):
            return [s[0], s[1]+1]
        # if on the right edge
        else:
            return s
    # if movement is left and not on the left edge
    elif (a == "l"):
        if (s[1] > 0):
            return [s[0], s[1]-1]
        # if on the left edge
        else:
            return s


def env_move_sto(s,a):
    # stochistic movement
    probability = rd.randint(1,100)
    # if movement is up, 80% chance to move up, 10% for right and left
    if (a == "u"):
        if (probability < 81):
            return env_move_det(s,a)
        elif (probability < 91):
            return env_move_det(s,"r")
        else:
            return env_move_det(s,"l")
    # if movement is down, 80% chance to move down, 10% for right and lehft
    elif (a == "d"):
        if (probability < 81):
            return env_move_det(s,a)
        elif (probability < 91):
            return env_move_det(s,"r")
        else:
            return env_move_det(s,"l")
    # if movement is right, 80% chance to move right, 10% for up and down
    elif (a == "r"):
        if (probability < 81):
            return env_move_det(s,a)
        elif (probability < 91):
            return env_move_det(s,"u")
        else:
            return env_move_det(s,"d")
    # if movement is left, 80% chance to move left, 10% for up and down
    elif (a == "l"):
        if (probability < 81):
            return env_move_det(s,a)
        elif (probability < 91):
            return env_move_det(s,"u")
        else:
            return env_move_det(s,"d")


def env_reward(s, a, next_s):
    # assess absorb
    if (environment[next_s[0]][next_s[1]] == 100):
        absorb = True
    elif (environment[next_s[0]][next_s[1]] == -100):
        absorb = True
    else:
        absorb = False

    #return reward
    return environment[next_s[0]][next_s[1]], absorb


###############################################

#            AGENT IMPLEMENTATION            #

###############################################


def generate_policy():
    # generates the transitions for all states in the model
    for i in range (0,size[0]):
        for j in range (0,size[1]):
            # chose either 1, 2, 3 or 4
            option = rd.randint(1,4)
            # set transition to u, d, l or r
            if (option == 1):
                policy[i][j] = "u"
            elif (option == 2):
                policy[i][j] = "d"
            elif (option == 3):
                policy[i][j] = "l"
            elif (option == 4):
                policy[i][j] = "r"


def agt_choose(s, epsilon):
    # nomalise epsion from 0 < e < 1 to 0 -> 100
    epsilon_norm = epsilon * 100;
    threshold = rd.randint(0,100)
    # take a the policy action with probability 1 - epsilon
    if (threshold > epsilon_norm):
        return policy[s[0]][s[1]]
    # take a random action with probability epsilon
    else:
        option = rd.randint(1,4)
        if (option == 1):
            return "u"
        elif (option == 2):
            return "d"
        elif (option == 3):
            return "l"
        elif (option == 4):
            return "r"


def agt_learn_sarsa(alpha, s,a,r,next_s,next_a):
    u = 0
    d = 1
    l = 2
    ri = 3

    # update the value of the state action reward pair
    if (policy[next_s[0]][next_s[1]] == "u"):
        polInd = 0
    elif (policy[next_s[0]][next_s[1]] == "d"):
        polInd = 1
    elif (policy[next_s[0]][next_s[1]] == "l"):
        polInd = 2
    elif (policy[next_s[0]][next_s[1]] == "r"):
        polInd = 3

    if (a == 'u'):
        Qsa[s[0]][s[1]][u]  = (((1 - alpha) * Qsa[s[0]][s[1]][u])  + alpha * (r + gamma * (Qsa[next_s[0]][next_s[1]][polInd])))
    if (a == 'd'):
        Qsa[s[0]][s[1]][d]  = (((1 - alpha) * Qsa[s[0]][s[1]][d])  + alpha * (r + gamma * (Qsa[next_s[0]][next_s[1]][polInd])))
    if (a == 'l'):
        Qsa[s[0]][s[1]][l]  = (((1 - alpha) * Qsa[s[0]][s[1]][l])  + alpha * (r + gamma * (Qsa[next_s[0]][next_s[1]][polInd])))
    if (a == 'r'):
        Qsa[s[0]][s[1]][ri] = (((1 - alpha) * Qsa[s[0]][s[1]][ri]) + alpha * (r + gamma * (Qsa[next_s[0]][next_s[1]][polInd])))

    #update policy from the index of the maximum state action reward pair for each state
    for i in range (0,5):
        for j in range (0,5):
            action = np.argmax(Qsa[:][i][j])
            if (action == 0):	
                policy[i][j] = "u"
            elif (action == 1):
                policy[i][j] = "d"
            elif (action == 2):
                policy[i][j] = "l"
            elif (action == 3):
                policy[i][j] = "r"


def agt_learn_q(alpha,s,a,r,next_s):
    u = 0
    d = 1
    l = 2
    ri = 3

    # update the value of the state action reward pair
    if (a == 'u'):
        Qsa[s[0]][s[1]][u]  = (((1 - alpha) * Qsa[s[0]][s[1]][u])  + (alpha * (r + gamma * (max(Qsa[next_s[0]][next_s[1]])))))
    if (a == 'd'):
        Qsa[s[0]][s[1]][d]  = (((1 - alpha) * Qsa[s[0]][s[1]][d])  + (alpha * (r + gamma * (max(Qsa[next_s[0]][next_s[1]])))))
    if (a == 'l'):
        Qsa[s[0]][s[1]][l]  = (((1 - alpha) * Qsa[s[0]][s[1]][l])  + (alpha * (r + gamma * (max(Qsa[next_s[0]][next_s[1]])))))
    if (a == 'r'):
        Qsa[s[0]][s[1]][ri] = (((1 - alpha) * Qsa[s[0]][s[1]][ri]) + (alpha * (r + gamma * (max(Qsa[next_s[0]][next_s[1]])))))

    #update policy from the index of the maximum state action reward pair for each state
    for i in range (0,4):
        for j in range (0,4):
            action = np.argmax(Qsa[:][i][j])
            if (action == 0):
                policy[i][j] = "u"
            elif (action == 1):
                policy[i][j] = "d"
            elif (action == 2):
                policy[i][j] = "l"
            elif (action == 3):
                policy[i][j] = "r"


def agt_learn_final(alpha,s,a,r):

    u = 0
    d = 1
    l = 2
    r = 3
    # update the value of the state action reward pair
    if (a == 'u'):
        Qsa[s[0]][s[1]][u] = (((1 - alpha) * Qsa[s[0]][s[1]][u]) + alpha * (r + gamma * (0)))
    if (a == 'd'):
        Qsa[s[0]][s[1]][d] = (((1 - alpha) * Qsa[s[0]][s[1]][d]) + alpha * (r + gamma * (0)))
    if (a == 'l'):
        Qsa[s[0]][s[1]][l] = (((1 - alpha) * Qsa[s[0]][s[1]][l]) + alpha * (r + gamma * (0)))
    if (a == 'r'):
        Qsa[s[0]][s[1]][r] = (((1 - alpha) * Qsa[s[0]][s[1]][r]) + alpha * (r + gamma * (0)))

    #update policy from the index of the maximum state action reward pair for each state
    for i in range (0,5):
        for j in range (0,5):
            action = np.argmax(Qsa[:][i][j])
            if (action == 0):
                policy[i][j] = "u"
            elif (action == 1):
                policy[i][j] = "d"
            elif (action == 2):
                policy[i][j] = "l"
            elif (action == 3):
                policy[i][j] = "r"


def agt_reset_value():
    #reset all state action reqard pairs to 100
    for i in range (0,5):
        for j in range (0,5):
            for a in range (0,4):
                Qsa[i][j][a] = 100;


def rl(mode, method, alpha):
    r = 0
    # reset all rewards to zero and Qsa matrix
    rewards[:] = [0] * EPISODES
    agt_reset_value();	

    for epoch in range (0,EPOCHS):
        print epoch
        for episode in range(0,EPISODES):
            #if learning, take random actions goverened by epsilon
            learning = (episode < EPISODES - 50)
            if (learning):
                eps = epsilon
            else:
                eps = 0
            #reset gamma an agent starting point
            cumulative_gamma = 1
            s = initial_state
            a = agt_choose(s, eps)

            # STOCHASTIC
            if (mode == "stochastic"):
                for timestep in range (0, T):
                    #move to the next state
                    next_s = env_move_sto(s,a)
                    #calculate reward
                    r = env_reward(s,a,next_s)[0]
                    #reward is zero if in absorb state
                    if (env_reward(s, a, s))[1]:
                        r = 0

                    #calculate rewards, increment gamma, choose next action
                    rewards[episode] += (cumulative_gamma * r) / EPOCHS*1.0
                    cumulative_gamma *= gamma	
                    next_a = agt_choose(next_s, eps)
                    
                    #conduct sarsa
		    if (method == "sarsa"):
                        #check if learning
                        if (learning):
                            #test for absorb state / penultimate timestep
                            if ((env_reward(s, a, s)[1] == True) or (timestep == T-1)):
                                agt_learn_final(alpha, s, a, r)
                            #learn on normal movements
                            else:
                                agt_learn_sarsa(alpha, s, a, r, next_s, next_a)                      
                            
                    #conduct q learning                   
                    if (method == "q"):
                        if (learning):
                            if ((env_reward(s, a, s)[1] == True) or (timestep == T-1)):
                                agt_learn_final(alpha, s, a, r)
                            else:
                                agt_learn_q(alpha, s, a, r, next_s)

                    #move to next state
                    a = next_a
                    s = next_s

            # DETERMINISTIC
            if (mode == "deterministic"):
                for timestep in range (0, T):
                    #move to the next state
                    next_s = env_move_det(s,a)
                    #calculate reward
                    r = env_reward(s,a,next_s)[0]
                    #reward is zero if in absorb state
                    if (env_reward(s, a, s))[1]:
                        r = 0
                    
                    #calculate rewards, increment gamma, choose next action
                    rewards[episode] += (cumulative_gamma * r / EPOCHS*1.0)
                    cumulative_gamma *= gamma * 1.0
                    next_a = agt_choose(next_s, eps)
                    
                    #conduct sarsa
		    if (method == "sarsa"):
                        if (learning):
                            #test for absorb state / penultimate timestep
                            if ((env_reward(s, a, s)[1] == True) or (timestep == T-1)):
                                agt_learn_final(alpha, s, a, r)
                            #learn on normal movements
                            else:
                                agt_learn_sarsa(alpha, s, a, r, next_s, next_a)
                                  
                    #conduct q learning                           
                    if (method == "q"):
                        #check if learning
                        if (learning):
                            #test for absorb state / penultimate timestep
                            if ((env_reward(s, a, s)[1] == True) or (timestep == T-1)):
                                agt_learn_final(alpha, s, a, r)
                            #learn on normal movements
                            else:
                                agt_learn_q(alpha, s, a, r, next_s)

                    #move to next state
                    a = next_a
                    s = next_s


###############################################

#                    MAIN                     #

###############################################

#create Qsa
Qsa=np.empty((5,5,4))

#create an empty policy for the domain
policy = [[0 for x in range(size[0])] for y in range(size[1])]

#generate the initial policy
generate_policy()

#generate plot
fig = plt.figure()
plot = fig.add_subplot(1,1,1)

#open file
rewardFile = open('rewards.txt', 'w')

#set parameters
EPOCHS = 500
EPISODES = 500
rewards = [0 for x in range(EPISODES)]
initial_state = [0,0]
T = 72
epsilon = 0.1
gamma = 0.99

#call and plot reinforcement learning - stochastic q
rl("stochastic", "q", 0.1)
plot.plot(rewards, color = 'm', label = 'Q - Stochastic')

#write to file
i = 0
for reward in rewards:
  i+=1
  rewardFile.write("Q Stochastic  -  Episode " + str(i) + "  -  Reward = %s\n" % reward)

##call and plot reinforcement learning - stochastic sarsa
#rl("stochastic", "sarsa", 0.1)
#plot.plot(rewards, color = 'lawngreen', label = 'SARSA - Stochastic')

##write to file
#i = 0
#for reward in rewards:
#  i+=1
#  rewardFile.write("SARSA Stochastic  -  Episode " + str(i) + "  -  Reward = %s\n" % reward)
#
##call and plot reinforcement learning - deterministic sarsa
#rl("deterministic", "sarsa", 0.1)
#plot.plot(rewards, color = 'midnightblue', label = 'SARSA - Deterministic')

##write to file
#i = 0
#for reward in rewards:
#  i+=1
#  rewardFile.write("SARA Deterministic  -  Episode " + str(i) + "  -  Reward = %s\n" % reward)
#
##call and plot reinforcement learning - deterministic q
#rl("deterministic", "q", 0.1)
#plot.plot(rewards, color = 'darkorange', label = 'Q - Deterministic')
#
##write to file
#i = 0
#for reward in rewards:
#  i+=1
#  rewardFile.write("Q Deterministic  -  Episode " + str(i) + "  -  Reward = %s\n" % reward)
#
#


plot.legend()
plt.show()

print rewards



























#
