import numpy as np
import random as r

iterations = 100000

x = np.loadtxt("d", delimiter=",")

rows = x.shape[0]
cols = x.shape[1]
w = []
y = []

predictions = []
classified = []
rate = 0.0000002
errors = 1

for j in range(0,cols):
	w.append(r.uniform(-0.1,0.1))

for i in range(0, rows):
	classified.append(x[i][8])


for it in range(0, iterations):

	print "generation " + str(it)
	print "w " + str(w) 
	del predictions[:]
	print str(errors)
	if (errors==0):
		break

	errors = 0

	for i in range(0, rows):
		y=0
		for j in range(0, cols-1):

			y+=w[j]*x[i][j]

		y+=w[j+1]
		#print str(y)
		if (y>0):
			y=1
		else:
			y=0

		difference = y-classified[i]
		if (difference!=0):		
			errors+=1
		#print str(difference)

		for t in range (0, cols-1):
			w[t] = w[t] - ((rate * (difference) *  (x[i][t])))
			
		w[t+1] = w[t+1] - (rate * difference)





