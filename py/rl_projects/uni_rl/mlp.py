import matplotlib.pyplot as plt
import numpy as np
import math
import time

# This code is for the construction of a Multi-Layer Perceptron Neural Network using the numpy framework,
# the network is able to analyse the generated dataset, learn its parameters and predict the classes when run.

def generate_dataset():
	fig = plt.figure()

	# orange and blue rectangular distributions
	# generate
	oRect = np.random.rand(500,2)
	bRect = np.random.rand(500,2)
	#position
	oRect+=0.9
	bRect+=2.3
	bRect[:,1] -=3
	bRect[:,0] +=0.3
	oRect[:,1] -=1.4
	oRect*=5
	bRect*=5
	oRect[:,1] *=1.3
	oRect[:,0] *=2
	bRect[:,0] *=1.8
	bRect[:,1] *=0.7
	# rotate
	theta = 0.2
	rotMatrix = np.matrix([[math.cos(theta), math.sin(-theta)],
		      [math.sin(theta), math.cos(theta)]])

	for i in range(0,500):
		oRect[i] = oRect[i] * rotMatrix
		bRect[i] = bRect[i] * rotMatrix

	# green and red normal distributions
	# parameters
	gMean = np.array([7,7])
	rMean = np.array([2,2])
	gCov = np.matrix([[2,0],[0,5]])
	rCov = np.matrix([[0,2.5],[6,0]])
	# generate
	gDist = np.random.multivariate_normal(gMean, gCov, (500))
	rDist = np.random.multivariate_normal(rMean, rCov, (500))


	# add bias and class to dataset
	bias = np.ones([500,1])
	bias *=-1
	classVector = np.zeros([500,1])
	# bias and class 0 to orange rectangle
	oRect = np.append(oRect, bias, 1)
	oRect = np.append(oRect, classVector, 1)
	classVector.fill(1)
	# bias and class 1 to blue rectangle
	bRect = np.append(bRect, bias, 1)
	bRect = np.append(bRect, classVector, 1)
	classVector.fill(2)
	# bias and class 2 to green distribution
	gDist = np.append(gDist, bias, 1)
	gDist = np.append(gDist, classVector, 1)
	classVector.fill(3)
	# bias and class 3 to red distribution
	rDist = np.append(rDist, bias, 1)
	rDist = np.append(rDist, classVector, 1)

	# save the data to file in the form ' x y bias class '
	data = np.concatenate((oRect, bRect, gDist, rDist), 0)
	np.savetxt('data', data, '%6.10f')

	# plot the x y points of the data
	plot = fig.add_subplot(1,1,1)
	plot.scatter(oRect[:,0], oRect[:,1], marker=".", color='darkorange')
	plot.scatter(bRect[:,0], bRect[:,1], marker=".", color='midnightblue')
	plot.scatter(rDist[:,0], rDist[:,1], marker="." , color='darkred')
	plot.scatter(gDist[:,0], gDist[:,1], marker=".", color='forestgreen')
	plot.set_xlim(-10,40)
	plot.set_ylim(-10,15)
	plt.show()

	# randomise the order of the data
	np.random.shuffle(data)

	# split the data into training, test and validation
	training = data[0:1000,:]
	test = data [1000:1500,:]
	validation = data[1500:2000,:]

	return training, test, validation

def generateWeightMatrix(nodeDepth):
	# generate weight matrix with random values between -0.5 and 0.5
	matrix = np.random.uniform(-0.5,0.5,(3+nodeDepth+1, nodeDepth))	
	return matrix	

def activation(vector):
	# sigmoid function (activation) on each element in a vector
	i=0
	for element in np.nditer(vector):
		vector[i] =  1 / (1 + math.exp(-element))
		i+=1
	return vector

def classify(x,w,h):
	# calculates hidden out, appends -1, calculates output layer out, then takes highes output node value as class

	############### faster code ################	

	return activation(np.dot(np.append(activation(np.dot(x,w[0:3,:])),-1),w[3:3+h+1,0:categories])).argmax()


	############## readable code ###############

	# dots the input vector with the hidden layer weight matrix
	#hidOut = activation(np.dot(x, w[0:3,:]))

	# appends -1 for the hidden layer bias
	#hidOut = np.append(hidOut, -1)

	# dots the hidden layer output (with bias) with the output layer weight matrix
	#out = activation(np.dot(hidOut, w[3:3+h+1,0:categories]))

	# returns the highest value from the output nodes
	#return out.argmax()

def getOutputFromNodes(x,w,h):
	# gets the output of each node and returns as two vectors

	############### faster code ################	

	return np.array([np.append(activation(np.dot(x, w[0:3,:])),-1), activation(np.dot(np.append(activation(np.dot(x, w[0:3,:])),-1), w[3:3+h+1,0:categories]))])


	############## readable code ###############

	# dots the input vector with the hidden layer weight matrix
	#hidOut = activation(np.dot(x, w[0:3,:]))

	# appends -1 for the hidden layer bias
	#hidOut = np.append(hidOut, -1)	

	# dots the hidden layer output (with bias) with the output layer weight matrix
	#out = activation(np.dot(hidOut, w[3:3+h+1,0:categories]))

	# appends the two output vectors into nodesOut
	#nodesOut = np.array([hidOut,out])
	#return nodesOut

def getClassVector(dataClass):
	# returns a correct output vector based on an input's class
	if (dataClass<1): return np.array([1,0,0,0])		
	elif (dataClass<2): return np.array([0,1,0,0])
	elif (dataClass<3): return np.array([0,0,1,0])
	else: return np.array([0,0,0,1])

def getZ(x,w,h):
	# gets zi, sj and zk values (i = input layer, j = hidden layer, k = output layer)
	nodesOut = getOutputFromNodes(x, w, h)
	zj = nodesOut[0]				
	zk = nodesOut[1]				
	zi = x[0:3]					
	return zi, zj, zk	

def update(x,h,eta,w):
	# called on each value of the training set and updates the weights stochastically	
	
	#get z values and tk (class output vector)
	zi,zj,zk = getZ(x[0:3], w, h)					
	tk = getClassVector(x[3:4])	

	############### faster code ################							

	deltaK = ((zk-tk) * zk) * (1-zk)	
	w[0:3,:] -= eta * np.multiply.outer(zi, (zj * (1-zj) * (np.sum((w[3:3+h+1,0:categories]*deltaK),1))))[:,0:h]
	w[3:3+h+1,0:categories] -= eta * np.multiply.outer(zj, deltaK)
	

	############## readable code ###############

	#calculate deltaK which is a [4 x 1] vector
	#deltaK = ((zk-tk) * zk) * (1-zk)	

	#calculate deltaJ which is a [h+1 x 1] vector					
	#deltaJ = (zj * (1-zj) * (np.sum((w[3:3+h+1,0:categories]*deltaK),1)))	

	#calculate output layer update matrix which is a [h x 4] matrix		
	#dOuterLayer = eta * np.multiply.outer(zj, deltaK)

	#calculate hidden layer update matrix which is a [3 x h] matrix				
	#dHiddenLayer = eta * np.multiply.outer(zi, deltaJ)[:,0:h]				
					
	#update weight matrix
	#w[0:3,:] -= dHiddenLayer						
	#w[3:3+h+1,0:categories] -= dOuterLayer

def train(h,eta,D,w):
	# called on the whole training set to update the weights
	np.random.shuffle(D)
	np.apply_along_axis(update,1, D, h, eta, w)

def check(inp, h, w):
	# checks if a predicted value by the network is the correct classified value
	if (classify(inp[0:3], w, h) == inp[3:4]):
		return 1
	return 0

def evaluate(h,D,w):
	# checks for prediction accuracy on an input set D, returning  { correct predictions / total predictions }
	return np.sum(np.apply_along_axis(check, 1, D, h, w)/float(D.shape[0]))




# script that initilises data and weights, trains the network then asseses its accuracy



# initialise output categories, nodes in hidden layer and number of training epochs
categories = 4
nodes = 8
epochs = 100


# generate datasets and weight matrix
training, test, validation = generate_dataset()
w = generateWeightMatrix(nodes)

# generate matrix to store accuracy of network over each epoch
accuracyData = np.zeros([3, epochs])

#start = time.time()

for ep in range (0,epochs):

	# print epoch then train network
	print str(ep) + '/' + str(epochs)

	train(nodes, 0.05, training, w)

	# add accuracy data to plots
	# first column is epoch
	accuracyData[0][ep] = ep
	# second column for training data accuracy
	accuracyData[1][ep] = evaluate(nodes, training, w)
	# third column for validation data accuracy
	accuracyData[2][ep] = evaluate(nodes, validation, w)

	#print str(accuracyData[1][ep]) + ' Train'
	#print str(accuracyData[2][ep]) + ' Valid'	
	#print str(evaluate(nodes, test, w)) + ' Test'

#end = time.time()



fig = plt.figure()
plot = fig.add_subplot(1,1,1)
# training plot is purple
plot.plot(accuracyData[0,:], accuracyData[1,:], color = 'm', label = 'Training')
# validation plot is green
plot.plot(accuracyData[0,:], accuracyData[2,:], color = 'lawngreen', label = 'Validation')
plot.legend()
plt.show()

# plot test data

# split test data

# test set classifiers
testSetZeros = []
testSetOnes = []
testSetTwos = []
testSetThrees = []

# test set network predicted classifiers
predictedZeros = []
predictedOnes = []
predictedTwos = []
predictedThrees = []

# add data to testSet and predicted lists per class
for i in range (0, test.shape[0]):
	if (test[i,3] == 0):
		testSetZeros.append(test[i,:])
	if (test[i,3] == 1):
		testSetOnes.append(test[i,:])
	if (test[i,3] == 2):
		testSetTwos.append(test[i,:])
	if (test[i,3] == 3):
		testSetThrees.append(test[i,:])
	if (classify(test[i][0:3], w, nodes) == 0):
		predictedZeros.append(test[i,:])
	if (classify(test[i][0:3], w, nodes) == 1):
		predictedOnes.append(test[i,:])
	if (classify(test[i][0:3], w, nodes) == 2):
		predictedTwos.append(test[i,:])
	if (classify(test[i][0:3], w, nodes) == 3):
		predictedThrees.append(test[i,:])


# plot the test and predicted data classes
fig = plt.figure()
plot = fig.add_subplot(1,2,1)

for i in range (0, len(testSetZeros)):
	plot.scatter(testSetZeros[i][0], testSetZeros[i][1], marker=".", color='darkorange')

for i in range (0, len(testSetOnes)):
	plot.scatter(testSetOnes[i][0], testSetOnes[i][1], marker=".", color='midnightblue')

for i in range (0, len(testSetTwos)):
	plot.scatter(testSetTwos[i][0], testSetTwos[i][1], marker=".", color='darkred')

for i in range (0, len(testSetThrees)):
	plot.scatter(testSetThrees[i][0], testSetThrees[i][1], marker=".", color='forestgreen')

plot2 = fig.add_subplot(1,2,2)

for i in range (0, len(predictedZeros)):
	plot2.scatter(predictedZeros[i][0], predictedZeros[i][1], marker=".", color='chocolate')

for i in range (0, len(predictedOnes)):
	plot2.scatter(predictedOnes[i][0], predictedOnes[i][1], marker=".", color='slateblue')

for i in range (0, len(predictedTwos)):
	plot2.scatter(predictedTwos[i][0], predictedTwos[i][1], marker=".", color='indianred')

for i in range (0, len(predictedThrees)):
	plot2.scatter(predictedThrees[i][0], predictedThrees[i][1], marker=".", color='yellowgreen')


plt.show() 	









