import numpy as np
import chess
import chess.pgn
import copy
from alpha_z_chess_single_thread import Node, Policy_Value_Network
from numpy.random import choice, rand
from numpy import arange
import multiprocessing
import time

BATCH_SIZE = 64
BOARDS = [chess.Board() for _ in range(BATCH_SIZE)]
PLY = 0
REWARDS = np.zeros([BATCH_SIZE, 2])
MAX_GAME_MOVES = 250
# MAX_GAME_MOVES = 250
PLAYER = 0
NUM_ACTIONS = 4140
UCI_MOVES = []
GAMES_ENDED = [False for _ in range(BATCH_SIZE)]
PV_NET = Policy_Value_Network(BATCH_SIZE, (8, 8, 12), NUM_ACTIONS)
STATES_VISITED = []
IMPROVED_POLICIES = []
GAME_IDENTIFIERS = []
PLAYERS = []
LEARNING_NOISE = 0.02
TREE_SEARCH_DEPTH = 20


# generates a list of moves that is used to convert an action array into a chess move
def populate_uci_move_list():
    global UCI_MOVES
    file_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    rank_list = ['1', '2', '3', '4', '5', '6', '7', '8']
    promotions_list = ['a2a1q', 'b2b1q', 'c2c1q', 'd2d1q', 'e2e1q', 'f2f1q', 'g2g1q', 'h2h1q',
                       'a2b1q', 'b2c1q', 'c2d1q', 'd2e1q', 'e2f1q', 'f2g1q', 'g2h1q',
                       'b2a1q', 'c2b1q', 'd2c1q', 'e2d1q', 'f2e1q', 'g2f1q', 'h2g1q',

                       'a7b8q', 'b7c8q', 'c7d8q', 'd7e8q', 'e7f8q', 'f7g8q', 'g7h8q',
                       'b7a8q', 'c7b8q', 'd7c8q', 'e7d8q', 'f7e8q', 'g7f8q', 'h7g8q',
                       'a7a8q', 'b7b8q', 'c7c8q', 'd7d8q', 'e7e8q', 'f7f8q', 'g7g8q', 'h7h8q']

    for i in range(22):
        UCI_MOVES.append(promotions_list[i])

    for i in range(64):
        for j in range(64):
            file_1 = i % 8
            rank_1 = i // 8
            file_2 = j % 8
            rank_2 = j // 8
            UCI_MOVES.append(file_list[file_1] + rank_list[rank_1] + file_list[file_2] + rank_list[rank_2])

    for i in range(22):
        UCI_MOVES.append(promotions_list[i + 22])


# resets all boards to start state
def reset():
    global REWARDS, BOARDS, GAMES_ENDED, STATES_VISITED, IMPROVED_POLICIES, GAME_IDENTIFIERS, PLAYER, PLY, PLAYERS
    [b.reset() for b in BOARDS]
    REWARDS = np.zeros([BATCH_SIZE, 2])
    GAMES_ENDED = [False for _ in range(BATCH_SIZE)]
    STATES_VISITED = []
    IMPROVED_POLICIES = []
    GAME_IDENTIFIERS = []
    PLAYERS = []
    PLAYER = 0
    PLY = 0


# returns an array of games that have ended, updates rewards for finished games
def check_games_ended():
    global REWARDS, BOARDS, MAX_GAME_MOVES, PLYS
    results = [b.result() for b in BOARDS]
    finished_games = [False for _ in BOARDS]
    i = 0
    for result in results:
        if result == '1-0':
            REWARDS[i, 0] = 1
            REWARDS[i, 1] = -1
            finished_games[i] = True
        elif result == '0-1':
            REWARDS[i, 0] = -1
            REWARDS[i, 1] = 1
            finished_games[i] = True
        elif result == '*':
            REWARDS[i, 0] = 0
            REWARDS[i, 1] = 0
        else:
            REWARDS[i, 0] = 0
            REWARDS[i, 1] = 0
            finished_games[i] = True
        i += 1
    if PLY > MAX_GAME_MOVES * 2:
        finished_games = [True for _ in BOARDS]
    return finished_games


# get the legal moves in the current position on a given board
def get_legal_actions_from_board(board):
    global NUM_ACTIONS
    mask = np.zeros(NUM_ACTIONS)
    for m in board.legal_moves:
        try:
            mask[UCI_MOVES.index(str(m))] = 1
        except:
            continue
    return mask


# get the legal moves in the current position on all boards
def get_legal_actions():
    global BATCH_SIZE, NUM_ACTIONS, BOARDS, UCI_MOVES
    mask = np.zeros([BATCH_SIZE, NUM_ACTIONS]).astype(np.float32)
    i = 0
    for board in BOARDS:
        for m in board.legal_moves:
            try:
                mask[i, UCI_MOVES.index(str(m))] = 1
            except:
                continue
        i += 1
    return mask


# converts an output from the network into a legal move distribution
def convert_distribution_to_legal_distribution(pi):
    global PLAYER
    # get legal moves
    legal_actions = get_legal_actions()
    pi += (legal_actions * 1e-22)
    pi *= legal_actions
    # sum across the move distribution axis
    val = np.sum(pi, axis=1).reshape(pi.shape[0], 1)
    # normalise probs
    pi = np.divide(pi, val)
    return pi


# input to this function is np array of size {BATCH_SIZE, NUM_ACTIONS}
def make_move_from_policy_stochastic(pi):
    global NUM_ACTIONS, BATCH_SIZE, BOARDS, PLAYER, GAMES_ENDED, STATES_VISITED, IMPROVED_POLICIES, GAME_IDENTIFIERS, PLY
    PLY += 1
    # mask legal moves and re normalise
    pi = convert_distribution_to_legal_distribution(pi)
    # get starting board states
    starting_states = get_state_of_all_boards()
    # get chosen moves for each board
    for i in range(BATCH_SIZE):
        if not GAMES_ENDED[i]:
            # store policy pi and state of the boards
            IMPROVED_POLICIES.append(pi[i])
            STATES_VISITED.append(starting_states[i])
            GAME_IDENTIFIERS.append(i)
            PLAYERS.append(PLAYER)
            # get move and play it
            chosen_move = choice(arange(NUM_ACTIONS), p=pi[i])
            move_string = UCI_MOVES[chosen_move]
            BOARDS[i].push(chess.Move.from_uci(move_string))
    # change player
    PLAYER = (PLAYER + 1) % 2
    # get array of games that have ended and update rewards
    GAMES_ENDED = check_games_ended()


# gets the state from all the current boards {BATCH_SIZE, 8, 8, 12}
def get_state_of_all_boards():
    global BOARDS, PLAYER
    board_position = np.zeros([BATCH_SIZE, 8, 8, 12])

    for i in range(BATCH_SIZE):
        raw_state = np.asarray([ord(c) for c in list(str(BOARDS[i])[::2])], dtype=np.float32).reshape([8, 8])
        white_pawns = np.array(raw_state == 80, dtype=np.float32)
        white_rooks = np.array(raw_state == 82, dtype=np.float32)
        white_knights = np.array(raw_state == 78, dtype=np.float32)
        white_bishops = np.array(raw_state == 66, dtype=np.float32)
        white_queen = np.array(raw_state == 81, dtype=np.float32)
        white_king = np.array(raw_state == 75, dtype=np.float32)
        black_pawns = np.array(raw_state == 112, dtype=np.float32)
        black_rooks = np.array(raw_state == 114, dtype=np.float32)
        black_knights = np.array(raw_state == 110, dtype=np.float32)
        black_bishops = np.array(raw_state == 98, dtype=np.float32)
        black_queen = np.array(raw_state == 113, dtype=np.float32)
        black_king = np.array(raw_state == 107, dtype=np.float32)

        if PLAYER == 0:
            board_position[i, :, :, 0] = white_pawns
            board_position[i, :, :, 1] = white_rooks
            board_position[i, :, :, 2] = white_knights
            board_position[i, :, :, 3] = white_bishops
            board_position[i, :, :, 4] = white_queen
            board_position[i, :, :, 5] = white_king
            board_position[i, :, :, 6] = black_pawns
            board_position[i, :, :, 7] = black_rooks
            board_position[i, :, :, 8] = black_knights
            board_position[i, :, :, 9] = black_bishops
            board_position[i, :, :, 10] = black_queen
            board_position[i, :, :, 11] = black_king

        else:
            board_position[i, :, :, 0] = np.flip(black_pawns)
            board_position[i, :, :, 1] = np.flip(black_rooks)
            board_position[i, :, :, 2] = np.flip(black_knights)
            board_position[i, :, :, 3] = np.flip(black_bishops)
            board_position[i, :, :, 4] = np.flip(black_queen)
            board_position[i, :, :, 5] = np.flip(black_king)
            board_position[i, :, :, 6] = np.flip(white_pawns)
            board_position[i, :, :, 7] = np.flip(white_rooks)
            board_position[i, :, :, 8] = np.flip(white_knights)
            board_position[i, :, :, 9] = np.flip(white_bishops)
            board_position[i, :, :, 10] = np.flip(white_queen)
            board_position[i, :, :, 11] = np.flip(white_king)

    return board_position


# gets the state from a passed board
def get_state_from_board(board, player):
    board_position = np.zeros([1, 8, 8, 12])
    raw_state = np.asarray([ord(c) for c in list(str(board)[::2])], dtype=np.float32).reshape([8, 8])
    white_pawns = np.array(raw_state == 80, dtype=np.float32)
    white_rooks = np.array(raw_state == 82, dtype=np.float32)
    white_knights = np.array(raw_state == 78, dtype=np.float32)
    white_bishops = np.array(raw_state == 66, dtype=np.float32)
    white_queen = np.array(raw_state == 81, dtype=np.float32)
    white_king = np.array(raw_state == 75, dtype=np.float32)
    black_pawns = np.array(raw_state == 112, dtype=np.float32)
    black_rooks = np.array(raw_state == 114, dtype=np.float32)
    black_knights = np.array(raw_state == 110, dtype=np.float32)
    black_bishops = np.array(raw_state == 98, dtype=np.float32)
    black_queen = np.array(raw_state == 113, dtype=np.float32)
    black_king = np.array(raw_state == 107, dtype=np.float32)

    if player == 0:
        board_position[0, :, :, 0] = white_pawns
        board_position[0, :, :, 1] = white_rooks
        board_position[0, :, :, 2] = white_knights
        board_position[0, :, :, 3] = white_bishops
        board_position[0, :, :, 4] = white_queen
        board_position[0, :, :, 5] = white_king
        board_position[0, :, :, 6] = black_pawns
        board_position[0, :, :, 7] = black_rooks
        board_position[0, :, :, 8] = black_knights
        board_position[0, :, :, 9] = black_bishops
        board_position[0, :, :, 10] = black_queen
        board_position[0, :, :, 11] = black_king

    else:
        board_position[0, :, :, 0] = np.flip(black_pawns)
        board_position[0, :, :, 1] = np.flip(black_rooks)
        board_position[0, :, :, 2] = np.flip(black_knights)
        board_position[0, :, :, 3] = np.flip(black_bishops)
        board_position[0, :, :, 4] = np.flip(black_queen)
        board_position[0, :, :, 5] = np.flip(black_king)
        board_position[0, :, :, 6] = np.flip(white_pawns)
        board_position[0, :, :, 7] = np.flip(white_rooks)
        board_position[0, :, :, 8] = np.flip(white_knights)
        board_position[0, :, :, 9] = np.flip(white_bishops)
        board_position[0, :, :, 10] = np.flip(white_queen)
        board_position[0, :, :, 11] = np.flip(white_king)

    return board_position


# calculates the confidence of a move via the alpha zero confidence formula
def confidence(q, c_puct, p, n_total, n_action):
    explorative_term = c_puct * (p * (n_total / (1 + n_action)))
    return q + explorative_term


# gets the legal move with the highest confidence score
def upper_confidence_bound(node, legal_actions):
    global NUM_ACTIONS
    confidence_array = (node.policy * 0).reshape(NUM_ACTIONS)
    sqrt_parent_visits = np.sqrt(node.parent.visited_count)
    q_s_a = 0
    for i in range(NUM_ACTIONS):
        if legal_actions[i] == 0:
            confidence_array[i] = -1e22
        else:
            move_to_make = np.zeros([NUM_ACTIONS])
            move_to_make[i] = 1
            for c in range(len(node.children)):
                if np.array_equal(node.children[c].move, move_to_make):
                    q_s_a = node.children[c].value
            confidence_array[i] = confidence(q_s_a, 1, node.policy.reshape(NUM_ACTIONS)[i], sqrt_parent_visits,
                                             node.action_count.reshape(NUM_ACTIONS)[i])
    action_array = (node.policy * 0).reshape(NUM_ACTIONS)
    action_array[np.argmax(confidence_array)] = 1
    return action_array


# traverses the tree until it finds an appropriate leaf to examine
def expand(node, board, id, player):
    global NUM_ACTIONS
    # get best move from position
    action = upper_confidence_bound(node, get_legal_actions_from_board(board))
    # add to parent's actions
    node.action_count += action
    # make move, and get game result
    board.push(chess.Move.from_uci(UCI_MOVES[np.argmax(action)]))
    move = action
    new_player = (player + 1) % 2
    res = board.result()
    game_over = not res == '*'
    # if the game has ended get the value from the perspective of the player that made the move
    result_value = np.array([0])
    if game_over:
        if res == '1-0':
            if new_player == 0:
                result_value = np.array([1])
            else:
                result_value = np.array([-1])
        elif res == '0-1':
            if new_player == 0:
                result_value = np.array([-1])
            else:
                result_value = np.array([1])
        else:
            result_value = np.array([0])
    # count the number of children of the current node
    num_children = len(node.children)
    # get the current board position
    new_position = get_state_from_board(board, new_player)
    # initialise counter (used to check if any of the children are the new position)
    not_this_position = 0
    # loop over each of the node's children
    for i in range(num_children):
        # if the new position is found in a child node
        if np.array_equal(node.children[i].move, move):
            # if the position is the end of the game, the value is the result of the game (no need to eval with the nn)
            if game_over:
                return [result_value, 0, id, node, move, board]
            # if the position is not the end of the game
            else:
                # expand the child node fot the chosen move from the perspective of the new player
                return expand(node.children[i], board, id, new_player)
        else:
            not_this_position += 1
    # if the node has no children, or the new position has not been explored
    if num_children == 0 or not_this_position == num_children:
        if game_over:
            # if the position is the end of the game, the value is the result of the game (no need to eval with the nn)
            return [result_value, 0, id, node, move, board]
        else:
            # if the position is not the end of the game, request an evaluation from the nn
            return [new_position, 1, id, node, move, board]


# evaluates the leaves that need it with the neural network, else adds game result value and zero policy
def process_leaves(leaves, leaf_count):
    global NUM_ACTIONS, PV_NET
    values = [None for _ in range(leaf_count)]
    policies = [None for _ in range(leaf_count)]
    nodes = [None for _ in range(leaf_count)]
    moves = [None for _ in range(leaf_count)]
    boards = [None for _ in range(leaf_count)]
    positions_to_evaluate = []
    evaluated_position_ids = []
    zero_policy = np.zeros([4140])
    # go through all the leaves
    for i in range(leaf_count):
        # add the node, move and board for the current leaf
        nodes[i] = leaves[i][3]
        moves[i] = leaves[i][4]
        boards[i] = leaves[i][5]
        # if no evauation requested, add the passed value and a dummy policy to the lists
        if leaves[i][1] == 0:
            values[i] = leaves[i][0]
            policies[i] = zero_policy
        # else add the position and id to the list of positions to eval
        else:
            positions_to_evaluate.append(leaves[i][0])
            evaluated_position_ids.append(leaves[i][2])
    # convert the positions to evaluate to np array
    positions_to_evaluate = np.asarray(positions_to_evaluate).reshape(len(positions_to_evaluate), 8, 8, 12)
    try:
        # evaluate the positions
        eval_policies, eval_values = PV_NET.get_policy_and_value(positions_to_evaluate)
    except:
        # for b in range(BATCH_SIZE):
        #     print(str(chess.pgn.Game.from_board(BOARDS[b])))
        # print(positions_to_evaluate)
        # print(positions_to_evaluate.shape)
        print("nn returned nothing")

    # add the evaluated values and policies to the lists
    for i in range(len(evaluated_position_ids)):
        values[evaluated_position_ids[i]] = eval_values[i]
        policies[evaluated_position_ids[i]] = eval_policies[i]

    return policies, values, nodes, moves, boards


# adds a new node to the tree and propogates the value back up
def propogate_tree(pol, val, node, root_player, m, board):
    # get the current player from the depth of the tree
    if node.depth % 2 == 0:
        current_player = (root_player + 1) % 2
    else:
        current_player = root_player
    # flip policy if the player is black
    if current_player == 1:
        pol = np.flip(pol)
    # convert policy into legal move only policy
    board_legal_moves = get_legal_actions_from_board(board)
    if np.sum(board_legal_moves) != 0:
        pol += board_legal_moves * 1e-22
        pol *= board_legal_moves
        pol /= np.sum(pol)
    # create new child node
    child = Node(
        position=None,
        parent=node,
        policy=pol,
        value=-val,
        children=[],
        depth=node.depth + 1,
        action_count=pol * 0,
        move=m
    )
    # add child to node
    node.add_child(child)
    # propogate value back up the tree
    child_value = val
    walk_node = node

    while walk_node.depth != 0:
        temp_val = walk_node.value
        temp_val *= walk_node.visited_count
        walk_node.visited_count += 1
        temp_val += child_value
        temp_val /= walk_node.visited_count
        walk_node.value = temp_val
        walk_node = walk_node.parent
        child_value *= -1

        # while walk_node.depth != 0:
        #     total_value = 0
        #     for c in walk_node.children:
        #         total_value += float(c.value)
        #     total_value = total_value / float(walk_node.visited_count)
        #     walk_node.value = total_value
        #     walk_node = walk_node.parent

    return walk_node


# takes a list with some items that are none, returns a list with the none items removed
def remove_none_roots(roots):
    return [r for r in roots if r is not None]


# replaces dummy nodes into the full sized root structure for games that have ended
def replace_roots(roots, root_key):
    dummy_node = Node(
        position=None,
        parent=None,
        policy=None,
        value=None,
        children=None,
        depth=None,
        action_count=None,
        move=None
    )
    roots_to_return = []
    index = 0
    for i in range(len(root_key)):
        if root_key[i]:
            roots_to_return.append(roots[index])
            index += 1
        else:
            roots_to_return.append(dummy_node)
    return roots_to_return


# evaluate a tree of possible moves starting from a policy, returning an improved distribution
def mcts(depth, noise_power):
    global BATCH_SIZE, NUM_ACTIONS, GAMES_ENDED, PLAYER, PV_NET, POOL
    # get state, policy and value
    initial_state = get_state_of_all_boards()
    initial_policy, initial_value = PV_NET.get_policy_and_value(initial_state)
    # add noise to the policy, convert to legal policy
    noisy_policy = initial_policy + (rand(BATCH_SIZE, NUM_ACTIONS) * noise_power)
    noisy_legal_policy = convert_distribution_to_legal_distribution(noisy_policy)
    # initialise improved policy
    improved_policy = noisy_legal_policy * 0
    initial_action_count = noisy_legal_policy * 0
    # initialise roots
    roots = [None for _ in range(BATCH_SIZE)]
    for i in range(BATCH_SIZE):
        if not GAMES_ENDED[i]:
            roots[i] = Node(
                position=initial_state[i],
                parent=None,
                policy=noisy_legal_policy[i],
                value=initial_value[i],
                children=[],
                depth=0,
                action_count=initial_action_count[i],
                move=''
            )
    # remove roots that are of ended games
    sanitised_roots = remove_none_roots(roots)
    # remember which roots we have removed
    root_key = [False if r is None else True for r in roots]
    # count the games remaining
    root_count = len(sanitised_roots)
    # get the boards that pertain to the roots
    temp_boards = copy.deepcopy(BOARDS)
    # debug ->
    # if not len(temp_boards) == BATCH_SIZE == len(root_key):
    #     print(len(temp_boards), BATCH_SIZE, len(root_key))
    boards_to_pass = [temp_boards[b] for b in range(BATCH_SIZE) if root_key[b]]
    # init the thread ids, players and current player
    ids = [id for id in range(root_count)]
    root_players = [PLAYER for _ in range(root_count)]
    # find as many leaves as the input tree depth
    for i in range(depth):
        # traverse the tree to get the information for found leaves
        ans = POOL.starmap(expand, zip(sanitised_roots, boards_to_pass, ids, root_players))
        # process those nodes that need it
        policies, values, nodes, moves, boards = process_leaves(ans, root_count)
        # propogate the leaf values back up the tree
        sanitised_roots = POOL.starmap(propogate_tree,
                                       zip(policies, values, nodes, root_players, moves, boards))
        # every time we expand, each root's visited count must be incremented
        for r in range(root_count):
            sanitised_roots[r].visited_count += 1
    # replace the roots that have been calculated else add dummy roots for ended games
    roots = replace_roots(sanitised_roots, root_key)
    # initialise dummy policy for ended games
    dummy_policy = np.zeros([NUM_ACTIONS])
    # get the policy from the roots' visit counts
    for x in range(BATCH_SIZE):
        r = roots[x]
        total_visits = 0
        prob_list = []
        # if the root has been processed via MCTS, derive new policy from children's visit count
        if r.children is not None:
            for c in r.children:
                total_visits += c.visited_count
            for c in r.children:
                prob_list.append(c.visited_count / total_visits)
            for i in range(len(prob_list)):
                improved_policy[x] += r.children[i].move * prob_list[i]
        # else add dummy policy
        else:
            improved_policy[x] = dummy_policy

    return improved_policy


# plays games against itself the learns using the az algorithm
def play_and_learn():
    global GAMES_ENDED, LEARNING_NOISE, TREE_SEARCH_DEPTH, PV_NET
    # start_time = time.time()
    while not GAMES_ENDED == [True for _ in range(BATCH_SIZE)]:
        if not PLY % 100:
            print(PLY)
        policies = mcts(TREE_SEARCH_DEPTH, LEARNING_NOISE)
        make_move_from_policy_stochastic(policies)
    # pre_train_time = time.time()
    PV_NET.train_multithreaded(IMPROVED_POLICIES, STATES_VISITED, REWARDS, GAME_IDENTIFIERS, PLAYERS)
    # end_time = time.time()
    # print("Batch size (s)", BATCH_SIZE, "Game in ", pre_train_time - start_time)
    # print("Took", end_time - pre_train_time, "to train")
    # print("Total episode time", end_time - start_time)
    # print game pgns
    for b in range(BATCH_SIZE):
        print(str(chess.pgn.Game.from_board(BOARDS[b])))


# plays games against a random move generator to test strength
def play_random(player_using_nn):
    global GAMES_ENDED, LEARNING_NOISE, TREE_SEARCH_DEPTH, PV_NET
    while not GAMES_ENDED == [True for _ in range(BATCH_SIZE)]:
        if not PLY % 100:
            print(PLY)
        if PLAYER == player_using_nn:
            policies = mcts(TREE_SEARCH_DEPTH, LEARNING_NOISE)
        else:
            policies = rand(BATCH_SIZE, NUM_ACTIONS)
        make_move_from_policy_stochastic(policies)

    white_score = 0
    black_score = 0
    for b in range(BATCH_SIZE):
        print(str(chess.pgn.Game.from_board(BOARDS[b])))
        res = BOARDS[b].result()
        if res == '1-0':
            white_score += 1
        elif res == '0-1':
            black_score += 1
        else:
            white_score += 0.5
            black_score += 0.5

    print("White:", white_score, "Black:", black_score)


# save policy/value network checkpoints
def save(checkpoint_val):
    global PV_NET
    PV_NET.pv_model.save_weights('./checkpoints/chess_checkpoints/ckpt_' + str(checkpoint_val))

# generate a list of move strings
populate_uci_move_list()
# load pv net weights
checkpoint = PV_NET.load_weights()
# inititlise thread pool
POOL = multiprocessing.Pool(processes=multiprocessing.cpu_count())

# reset()
# play_random(0)
# reset()
# play_random(1)
# quit()

# run self play
for i in range(1000):
    reset()
    play_and_learn()
    # save model weights
    new_checkpoint = checkpoint + (i + 1) * BATCH_SIZE
    save(new_checkpoint)
