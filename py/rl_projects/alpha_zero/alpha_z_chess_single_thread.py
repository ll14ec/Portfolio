import numpy as np
from keras.layers import Input, Dense, Conv2D, Flatten, BatchNormalization, MaxPool2D, Activation, Add
from keras import Model
from keras.optimizers import adam
from keras.losses import categorical_crossentropy, mse
import keras.backend as K
from keras.regularizers import l2
import chess
import chess.pgn
import copy
import glob

np.set_printoptions(threshold=np.inf)


# Used to evaluate game positions via tree search
class Node:
    def __init__(self, position, parent, policy, value, children, depth, action_count, move):
        self.position = position
        if parent is None:
            self.parent = self
        else:
            self.parent = parent
        self.policy = policy
        self.value = value
        self.visited_count = 1
        self.children = children
        self.depth = depth
        self.action_count = action_count
        self.move = move

    def add_child(self, c):
        self.children.append(c)


# A class for a reinforcement learning agent that learns using the alpha zero algorithm
class RL_Agent:
    # constructor for the agent takes a policy/value network 'pi' and a state machine
    def __init__(self, p, sm, depth):
        self.pi = p
        self.state_machine = sm
        self.tree_search_depth = depth
        self.learning_noise = 0.2
        self.playing_noise = 0.00001

    # learn via self-play exploration
    def learn(self, games):
        self.state_machine.init_state()
        for i in range(games):
            print("Starting self-play game", i + 1)
            game_end = False
            while not game_end:
                state = self.state_machine.get_state()
                improved_policy = self.get_improved_policy(state, copy.deepcopy(self.state_machine), self.learning_noise)
                next_state, game_end, _ = self.state_machine.update(improved_policy)
            try:
                print(str(chess.pgn.Game.from_board(self.state_machine.markov_process.board)))
            except:
                print("Could not print pgn")
            self.pi.train(self.state_machine.actions, self.state_machine.visited_states, self.state_machine.rewards)
            self.state_machine.init_state()

    # self play using raw policy network (no learning)
    def play(self):
        game_end = False
        self.state_machine.init_state()
        while not game_end:
            state = self.state_machine.get_state()
            action = self.get_improved_policy(state, copy.deepcopy(self.state_machine), self.playing_noise)
            next_state, game_end, _ = self.state_machine.update(action)
            # print(self.state_machine.state)

    # play a trained network (white) against random moves (black)
    def play_random(self, games):
        p1_score = 0
        p2_score = 0
        for i in range(games):
            print("Starting random play game", i + 1)
            game_end = False
            self.state_machine.init_state()
            while not game_end:
                state = self.state_machine.get_state()
                if self.state_machine.markov_process.player == 0:
                    action = self.get_improved_policy(state, copy.deepcopy(self.state_machine), self.playing_noise)
                else:
                    action = np.random.rand(self.pi.batch_size, self.pi.action_count)
                next_state, game_end, _ = self.state_machine.update(action)
            for g in range(self.pi.batch_size):
                p1_score += self.state_machine.rewards[g, 0]
                p2_score += self.state_machine.rewards[g, 1]
            print(str(chess.pgn.Game.from_board(self.state_machine.markov_process.board)))
        p1_score /= self.pi.batch_size
        p2_score /= self.pi.batch_size
        print("Scored", p1_score, "in", games, "games against random play, random scored", p2_score)

    # play one network against another
    def compete(self, games, p2_net):
        p1_score = 0
        for i in range(games):
            game_end = False
            self.state_machine.init_state()
            while not game_end:
                state = self.state_machine.get_state()
                if self.state_machine.markov_process.player == 0:
                    improved_policy = self.get_improved_policy(state, copy.deepcopy(self.state_machine), self.playing_noise)
                else:
                    improved_policy = self.get_improved_policy(state, copy.deepcopy(self.state_machine), self.playing_noise)
                next_state, game_end, _ = self.state_machine.update(improved_policy)
            print(str(chess.pgn.Game.from_board(self.state_machine.markov_process.board)))
            for g in range(self.pi.batch_size):
                p1_score += self.state_machine.rewards[g, 0]
            print("Finished game", i)

        print("Scored", p1_score, "in", games * self.pi.batch_size, "games against second network")

    # save policy/value network checkpoints
    def save(self, id):
        self.pi.pv_model.save_weights('./checkpoints/ckpt_' + str(id))

    # cacluates the confidence of a move via the alpha zero confidence formula
    def confidence(self, q, c_puct, p, n_total, n_action):
        explorative_term = c_puct * (p * (n_total / (1 + n_action)))
        return q + explorative_term

    # gets the legal move with the highest confidence score
    def upper_confidence_bound(self, node):
        confidence_array = (node.policy * 0).reshape(self.pi.action_count)
        sqrt_parent_visits = np.sqrt(node.parent.visited_count)
        q_s_a = 0
        for i in range(self.pi.action_count):
            if node.policy.reshape(self.pi.action_count)[i] == 0:
                confidence_array[i] = -1e22
            else:
                move_to_make = np.zeros([1, self.pi.action_count])
                move_to_make[0, i] = 1
                for c in range(len(node.children)):
                    if np.array_equal(node.children[c].move, move_to_make):
                        q_s_a = node.children[c].value
                confidence_array[i] = self.confidence(q_s_a, 1, node.policy.reshape(self.pi.action_count)[i],
                                                      sqrt_parent_visits,
                                                      node.action_count.reshape(self.pi.action_count)[i])
        action_array = (node.policy * 0).reshape(self.pi.action_count)
        action_array[np.argmax(confidence_array)] = 1
        return action_array.reshape([1, self.pi.action_count])

    # expands the tree to evaluate an unvisited leaf
    def expand(self, node, local_state_machine, prev_depth):
        # get actions for each batch
        actions = self.upper_confidence_bound(node)
        # take actions
        next_state, episode_end, action = local_state_machine.update(actions)
        # add actions to action count
        node.action_count += action
        # traverse tree until either game end or new node
        num_children = len(node.children)
        new_position = next_state
        not_this_position = 0
        child_value = 0
        # loop over each of the node's children
        for i in range(num_children):
            # if the new position is found in a child node
            if np.array_equal(node.children[i].position, new_position):
                # if the position is the end of the game, the value is the result of the game
                if episode_end == 1:
                    child_value = -local_state_machine.rewards[0, local_state_machine.markov_process.player]
                # if the position is not the end of the game
                else:
                    # get the value of the next unvisted node below by expanding the current node
                    child_value = self.expand(node.children[i], copy.deepcopy(local_state_machine), prev_depth + 1)
                    # take the value of the newly expanded node into account to give a new value for this node
                    node.children[i].value *= node.children[i].visited_count
                    node.children[i].visited_count += 1
                    node.children[i].value += child_value
                    node.children[i].value /= node.children[i].visited_count
            else:
                not_this_position += 1
        # if the node has no children, or the new position has not been explored
        if num_children == 0 or not_this_position == num_children:
            # get the policy for the new position
            player_legal_actions = self.state_machine.markov_process.get_legal_actions()
            current_player = local_state_machine.markov_process.player
            if current_player == 1:
                player_legal_actions = np.flip(player_legal_actions)
            # get network output
            raw_policy, raw_value = self.pi.get_policy_and_value(next_state)
            raw_policy = (raw_policy + (player_legal_actions * 1e-22)) * player_legal_actions
            normalised_policy = raw_policy / np.sum(raw_policy)
            initial_action_count = normalised_policy * 0
            # get the child node value, either from game result or value network
            if episode_end:
                child_value = -local_state_machine.rewards[0, local_state_machine.markov_process.player]
            else:
                child_value = -raw_value
            # add a new child node
            child = Node(
                position=next_state,
                parent=node,
                policy=normalised_policy,
                value=child_value,
                children=[],
                depth=prev_depth + 1,
                action_count=initial_action_count,
                move=action)
            node.add_child(child)

        return child_value

    # uses the tree search to find an improved policy
    def get_improved_policy(self, state, local_state_machine, noise_power):
        # get the initial policy and value
        initial_policy, initial_value = self.pi.get_policy_and_value(state)
        # initial_policy = self.pi.get_policy(state)
        # initial_value = self.pi.get_value(state)

        # get the state and the player
        initial_state = state
        initial_player = local_state_machine.markov_process.player
        # add noise to the policy for exploration
        noisy_policy = initial_policy + np.random.dirichlet(alpha=np.ones(4140) * noise_power,
                                                           size=np.array([self.pi.batch_size])).reshape([1, 4140])
        # get legal actions
        player_legal_actions = local_state_machine.markov_process.get_legal_actions()
        if initial_player == 1:
            # flip if black, as predictions will be unflipped in the state machine
            player_legal_actions = np.flip(player_legal_actions)
        # make all legal moves non-zero probability for the case that the policy did not recommend any legal moves
        noisy_policy += 1e-22 * player_legal_actions
        # mask illegal moves from the policy
        noisy_policy *= player_legal_actions
        # re-normalise the policy
        normalised_noisy_policy = noisy_policy / np.sum(noisy_policy)
        initial_action_count = normalised_noisy_policy * 0
        improved_policy = initial_action_count * 0
        # initialise root node
        root = Node(
            position=state,
            parent=None,
            policy=normalised_noisy_policy,
            value=initial_value,
            children=[],
            depth=0,
            action_count=initial_action_count,
            move='')
        # expand nodes below the root to the specified depth
        for i in range(self.tree_search_depth):
            local_state_machine.set_state(initial_state, initial_player)
            root.visited_count += 1
            self.expand(root, copy.deepcopy(local_state_machine), 0)
        # initialise visits and list for improved policy probabilities
        total_visits = 0
        prob_list = []
        for c in root.children:
            total_visits += c.visited_count
        for c in root.children:
            prob_list.append(c.visited_count / total_visits)
        # use improved policy values for each of the root's children to create improved move ditribution
        for i in range(len(prob_list)):
            improved_policy += root.children[i].move * prob_list[i]
        # return the improved policy
        return improved_policy.reshape([self.pi.batch_size, self.pi.action_count])


# A general state machine class for high level interactions with a markov decision process backend
class State_Machine:

    def __init__(self, mdp):
        self.markov_process = mdp
        self.visited_states = []
        self.actions = []
        self.state = self.init_state()
        self.rewards = None
        self.end_state = False

    def init_state(self):
        state = self.markov_process.start_state()
        self.visited_states = []
        self.actions = []
        self.visited_states.append(state.copy())
        return state

    def get_state(self):
        return self.markov_process.get_state()

    def set_state(self, state, player):
        self.markov_process.state = state
        self.markov_process.player = player
        self.state = state

    def update(self, probabilities):
        self.state, self.rewards, self.end_state, action = self.markov_process.action(probabilities)
        self.actions.append(probabilities)
        if not self.end_state:
            self.visited_states.append(self.state.copy())
        return self.state, self.end_state, action


# A markov decision process class for the game of chess
class Chess_MDP:
    def __init__(self, batch_size):
        self.UCI_MOVES = []
        self.state = None
        self.board = chess.Board()
        self.player = 0
        self.ply = 0
        self.populate_uci_move_list()
        self.batch_size = batch_size
        self.start_state()
        self.rewards = np.zeros([self.batch_size, 2])
        self.games_running = np.ones([self.batch_size])

    # initialise the start state
    def start_state(self):
        self.board.reset()
        self.rewards = np.zeros([self.batch_size, 2])
        self.games_running = np.ones([self.batch_size])
        self.state = self.get_state()
        self.player = 0
        self.ply = 0
        return self.state

    # checks for the end of a game, assigns rewards for wins/losses
    def episode_ended(self):
        if self.ply > 600:
            self.games_running[0] = 0
            self.rewards[0, 0] = 0
            self.rewards[0, 1] = 0

        elif self.board.is_game_over():
            self.games_running[0] = 0
            res = self.board.result()
            if res == '1-0':
                self.rewards[0, 0] = 1
                self.rewards[0, 1] = -1
            elif res == '0-1':
                self.rewards[0, 0] = -1
                self.rewards[0, 1] = 1
            else:
                self.rewards[0, 0] = 0
                self.rewards[0, 1] = 0

    # takes a probability distribution accross actions and stochastically takes one of them
    def action(self, probabilities):
        self.ply += 1
        if self.player == 0:
            legal_moves = self.get_legal_actions()
            probabilities += (legal_moves * 1e-22)
            probabilities *= legal_moves
            probabilities /= np.sum(probabilities)
            chosen_move = np.random.choice(np.arange(4096 + 44), p=probabilities[0])
        else:
            legal_moves = self.get_legal_actions()
            probabilities = np.flip(probabilities)
            probabilities += (legal_moves * 1e-22)
            probabilities *= legal_moves
            probabilities /= np.sum(probabilities)
            chosen_move = np.random.choice(np.arange(4096 + 44), p=probabilities[0])

        move_string = self.UCI_MOVES[chosen_move]
        self.board.push(chess.Move.from_uci(move_string))

        agent_action = np.zeros(4096 + 44)
        if self.player == 1:
            agent_action[4095 + 44 - chosen_move] = 1
        else:
            agent_action[chosen_move] = 1

        if self.player == 0:
            self.player = 1
        else:
            self.player = 0

        self.episode_ended()
        self.state = self.get_state()

        return self.state, self.rewards, not np.any(self.games_running), agent_action.reshape([1, 4140])

    # get the legal moves in the current position
    def get_legal_actions(self):
        mask = np.zeros([4096 + 44]).astype(np.float32)
        for m in self.board.legal_moves:
            try:
                mask[self.UCI_MOVES.index(str(m))] = 1
            except:
                continue
        return mask.reshape([1, 4096 + 44])

    # gets the current state of the game from the perspecitve of the current player
    def get_state(self):
        raw_state = np.asarray([ord(c) for c in list(str(self.board)[::2])], dtype=np.float32).reshape([8, 8])
        white_pawns = np.array(raw_state == 80, dtype=np.float32)
        white_rooks = np.array(raw_state == 82, dtype=np.float32)
        white_knights = np.array(raw_state == 78, dtype=np.float32)
        white_bishops = np.array(raw_state == 66, dtype=np.float32)
        white_queen = np.array(raw_state == 81, dtype=np.float32)
        white_king = np.array(raw_state == 75, dtype=np.float32)
        black_pawns = np.array(raw_state == 112, dtype=np.float32)
        black_rooks = np.array(raw_state == 114, dtype=np.float32)
        black_knights = np.array(raw_state == 110, dtype=np.float32)
        black_bishops = np.array(raw_state == 98, dtype=np.float32)
        black_queen = np.array(raw_state == 113, dtype=np.float32)
        black_king = np.array(raw_state == 107, dtype=np.float32)
        board_position = np.zeros([1, 8, 8, 12])

        if self.player == 0:
            board_position[:, :, :, 0] = white_pawns
            board_position[:, :, :, 1] = white_rooks
            board_position[:, :, :, 2] = white_knights
            board_position[:, :, :, 3] = white_bishops
            board_position[:, :, :, 4] = white_queen
            board_position[:, :, :, 5] = white_king
            board_position[:, :, :, 6] = black_pawns
            board_position[:, :, :, 7] = black_rooks
            board_position[:, :, :, 8] = black_knights
            board_position[:, :, :, 9] = black_bishops
            board_position[:, :, :, 10] = black_queen
            board_position[:, :, :, 11] = black_king

        else:
            board_position[:, :, :, 0] = np.flip(black_pawns),
            board_position[:, :, :, 1] = np.flip(black_rooks),
            board_position[:, :, :, 2] = np.flip(black_knights),
            board_position[:, :, :, 3] = np.flip(black_bishops),
            board_position[:, :, :, 4] = np.flip(black_queen),
            board_position[:, :, :, 5] = np.flip(black_king),
            board_position[:, :, :, 6] = np.flip(white_pawns),
            board_position[:, :, :, 7] = np.flip(white_rooks),
            board_position[:, :, :, 8] = np.flip(white_knights),
            board_position[:, :, :, 9] = np.flip(white_bishops),
            board_position[:, :, :, 10] = np.flip(white_queen),
            board_position[:, :, :, 11] = np.flip(white_king)

        return board_position

    # generates a list of moves that is used to convert an action array into a chess move
    def populate_uci_move_list(self):
        file_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        rank_list = ['1', '2', '3', '4', '5', '6', '7', '8']
        promotions_list = ['a2a1q', 'b2b1q', 'c2c1q', 'd2d1q', 'e2e1q', 'f2f1q', 'g2g1q', 'h2h1q',
                           'a2b1q', 'b2c1q', 'c2d1q', 'd2e1q', 'e2f1q', 'f2g1q', 'g2h1q',
                           'b2a1q', 'c2b1q', 'd2c1q', 'e2d1q', 'f2e1q', 'g2f1q', 'h2g1q',

                           'a7b8q', 'b7c8q', 'c7d8q', 'd7e8q', 'e7f8q', 'f7g8q', 'g7h8q',
                           'b7a8q', 'c7b8q', 'd7c8q', 'e7d8q', 'f7e8q', 'g7f8q', 'h7g8q',
                           'a7a8q', 'b7b8q', 'c7c8q', 'd7d8q', 'e7e8q', 'f7f8q', 'g7g8q', 'h7h8q']

        for i in range(22):
            self.UCI_MOVES.append(promotions_list[i])

        for i in range(64):
            for j in range(64):
                file_1 = i % 8
                rank_1 = i // 8
                file_2 = j % 8
                rank_2 = j // 8
                self.UCI_MOVES.append(file_list[file_1] + rank_list[rank_1] + file_list[file_2] + rank_list[rank_2])

        for i in range(22):
            self.UCI_MOVES.append(promotions_list[i + 22])


# A class that holds a deep neural network for policy and value estimation
class Policy_Value_Network:

    def __init__(self, batch_size, input_state_size, action_space_size):
        self.dummy_policy = np.zeros([1, action_space_size])
        self.dummy_value = np.zeros([1, 1])
        self.pv_model = None
        self.create_pv_model(input_state_size, action_space_size)
        self.batch_size = batch_size
        self.input_state_size = input_state_size
        self.action_count = action_space_size

    def residual_block(self, input_tensor):
        conv_1 = Conv2D(kernel_size=(3, 3), filters=256, activation='linear', padding='same', kernel_regularizer=l2(0.01))(input_tensor)
        bn_1 = BatchNormalization()(conv_1)
        relu_1 = Activation('relu')(bn_1)
        conv_2 = Conv2D(kernel_size=(3, 3), filters=256, activation='linear', padding='same', kernel_regularizer=l2(0.01))(relu_1)
        bn_2 = BatchNormalization()(conv_2)
        add_1 = Add()([input_tensor, bn_2])
        relu_2 = Activation('relu')(add_1)
        return relu_2

    def custom_loss(self, p_in, p_pred, v_in, v_pred):
        def loss_fn(x, y):
            policy_loss = p_in + 1e-10 * K.log(p_pred + 1e-10)
            value_loss = mse(v_in, v_pred)
            return value_loss - policy_loss
            # return categorical_crossentropy(p_in, p_pred) + mse(v_in, v_pred)
        return loss_fn

    def create_pv_model(self, input_shape=(8, 8,), actions=4140):
        input_layer = Input(shape=input_shape)
        policy_input = Input(shape=(actions,))
        value_input = Input(shape=(1,))
        conv_1 = Conv2D(kernel_size=(3, 3), filters=256, activation='linear', padding='same', kernel_regularizer=l2(0.01))(input_layer)
        bn_1 = BatchNormalization()(conv_1)
        relu_1 = Activation('relu')(bn_1)
        res_in_layer = relu_1

        for i in range(20):
            res_out_layer = self.residual_block(res_in_layer)
            res_in_layer = res_out_layer

        policy_branch_1 = Conv2D(kernel_size=(1, 1), filters=2, activation='linear', padding='same', kernel_regularizer=l2(0.01))(res_out_layer)
        policy_branch_2 = BatchNormalization()(policy_branch_1)
        policy_branch_3 = Activation('relu')(policy_branch_2)
        policy_branch_4 = Flatten()(policy_branch_3)
        policy_branch_5 = Dense(units=actions, activation='softmax', kernel_regularizer=l2(0.01))(policy_branch_4)

        value_branch_1 = Conv2D(kernel_size=(1, 1), filters=1, activation='linear', padding='same', kernel_regularizer=l2(0.01))(res_out_layer)
        value_branch_2 = BatchNormalization()(value_branch_1)
        value_branch_3 = Activation('relu')(value_branch_2)
        value_branch_4 = Flatten()(value_branch_3)
        value_branch_5 = Dense(units=256, activation='relu', kernel_regularizer=l2(0.01))(value_branch_4)
        value_branch_6 = Dense(units=1, activation='tanh', kernel_regularizer=l2(0.01))(value_branch_5)

        opt = adam(0.0001)
        m = Model([input_layer, policy_input, value_input], [policy_branch_5, value_branch_6])
        self.pv_model = m
        self.pv_model.compile(optimizer=opt, loss=self.custom_loss(policy_input, policy_branch_5, value_input, value_branch_6))

    def get_policy_and_value(self, model_input):
        policy, value = self.pv_model.predict([model_input, self.dummy_policy, self.dummy_value], batch_size=self.batch_size)
        return policy.reshape([policy.shape[0], self.action_count]), value.reshape([value.shape[0], 1])

    def load_weights(self):
        highest_checkpoint = -1
        checkpoint_to_load = ''

        for f in glob.glob('./checkpoints/ckpt*'):
            temp_checkpoint = int(f[19:])
            if temp_checkpoint > highest_checkpoint:
                highest_checkpoint = temp_checkpoint
                checkpoint_to_load = f

        if checkpoint_to_load != '':
            print("Loading", checkpoint_to_load)
            self.pv_model.load_weights(checkpoint_to_load)
            return highest_checkpoint
        else:
            print("Could not find any checkpoints")
            quit()

    def train(self, actions, states, rewards):
        state_list = []
        action_list = []
        reward_list = []

        for i in range(len(states)):
            for b in range(self.batch_size):
                state_list.append(states[i][b])
                action_list.append(actions[i][b])
                reward_list.append(rewards[b, i % 2])

        self.pv_model.fit([state_list, action_list, reward_list], [action_list, reward_list], batch_size=len(state_list), verbose=0, validation_steps=0)

    def train_multithreaded(self, actions, states, rewards, identifiers, players):
        state_list = []
        action_list = []
        reward_list = []

        for i in range(len(states)):
            game = identifiers[i]
            player = players[i]
            state_list.append(states[i])
            action_list.append(actions[i])
            reward_list.append(rewards[game, player])
        print("Training")
        self.pv_model.fit([state_list, action_list, reward_list], [action_list, reward_list], batch_size=1, verbose=0, validation_steps=0)
        print("Done training")


# A function that creates an agent and learns/plays etc
def learn_fn():
    batch_size = 1
    total_epochs = 100000
    batches_per_save = 10
    tree_depth = 8

    c = Chess_MDP(batch_size)
    s = State_Machine(c)

    input_shape = (8, 8, 12)
    output_size = 4096 + 44
    pv_net = Policy_Value_Network(batch_size, input_shape, output_size)
    checkpoint = pv_net.load_weights()


    pi = pv_net
    state_machine = s
    a = RL_Agent(pi, state_machine, tree_depth)
    # a.play_random(10)

    for e in range(total_epochs):
        a.learn(batches_per_save)
        new_checkpoint = checkpoint + (e + 1) * batch_size * batches_per_save
        a.save(new_checkpoint)
        print("Completed batch", e)


if __name__ == "__main__":
    learn_fn()
