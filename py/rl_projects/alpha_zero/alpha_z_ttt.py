import numpy as np
from keras.layers import Input, Dense, Flatten
from keras import Model
from keras.optimizers import adam

np.set_printoptions(threshold=np.inf)
import copy


# Used to evaluate game positions via tree search
class Node:
    def __init__(self, position, parent, policy, value, children, depth, action_count, move):
        self.position = position
        if parent is None:
            self.parent = self
        else:
            self.parent = parent
        self.policy = policy
        self.value = value
        self.visited_count = 1
        self.children = children
        self.depth = depth
        self.action_count = action_count
        self.move = move

    def add_child(self, c):
        self.children.append(c)


# A class for a reinforcement learning agent that learns using the alpha zero algorithm
class RL_Agent:
    # constructor for the agent takes a policy/value network 'pi' and a state machine
    def __init__(self, p, sm):
        self.pi = p
        self.state_machine = sm

    # learn via self-play exploration
    def learn(self, epochs):
        self.state_machine.init_state()
        for i in range(epochs):
            episode_end = False
            while not episode_end:
                state = self.state_machine.get_state()
                improved_policy = self.get_improved_policy(state, copy.deepcopy(self.state_machine))
                next_state, episode_end = self.state_machine.update(improved_policy)

            self.pi.train(self.state_machine.actions, self.state_machine.visited_states, self.state_machine.rewards)
            self.state_machine.init_state()

    # self play using raw policy network (no learning)
    def play(self):
        game_end = False
        self.state_machine.init_state()
        while not game_end:
            state = self.state_machine.get_state()
            action = self.pi.get_policy(state)
            next_state, game_end = self.state_machine.update(action)

    # play a trained network (white) against random moves (black)
    def play_random(self, games):
        p1_score = 0
        p2_score = 0
        for i in range(games):
            game_end = False
            self.state_machine.init_state()
            while not game_end:
                state = self.state_machine.get_state()
                if self.state_machine.markov_process.player == 0:
                    action = self.pi.get_policy(state)
                else:
                    action = np.random.rand(self.pi.batch_size, 3, 3)
                next_state, game_end = self.state_machine.update(action)
            for g in range(self.pi.batch_size):
                p1_score += self.state_machine.rewards[g, 0]
                p2_score += self.state_machine.rewards[g, 1]

        p1_score /= self.pi.batch_size
        p2_score /= self.pi.batch_size
        print("Scored", p1_score, "in", games, "games against random play, random scored", p2_score)

    # play one network against another
    def compete(self, games, p2_net):
        p1_score = 0
        for i in range(games):
            game_end = False
            self.state_machine.init_state()
            while not game_end:
                state = self.state_machine.get_state()
                if self.state_machine.markov_process.player == 0:
                    action = self.pi.get_policy(state)
                else:
                    action = p2_net.get_policy(state)
                next_state, game_end = self.state_machine.update(action)
                # print(self.state_machine.state)
            for g in range(self.pi.batch_size):
                p1_score += self.state_machine.rewards[g, 0]
            print("Finished game", i)

        print("Scored", p1_score, "in", games * self.pi.batch_size, "games against second network")

    # save policy/value network checkpoints
    def save(self, id):
        self.pi.p_model.save_weights('./checkpoints/policy_' + str(id))
        self.pi.v_model.save_weights('./checkpoints/value_' + str(id))

    # cacluates the confidence of a move via the alpha zero confidence formula
    def confidence(self, q, c_puct, p, n_total, n_action):
        explorative_term = c_puct * (p * (n_total / (1 + n_action)))
        return q + explorative_term

    # gets the legal move with the highest confidence score
    def upper_confidence_bound(self, node, local_state_machine):
        confidence_array = (node.policy * 0).reshape(9)
        sqrt_parent_visits = np.sqrt(node.parent.visited_count)
        q_s_a = 0
        for i in range(9):
            if node.policy.reshape(9)[i] == 0:
                confidence_array[i] = -1e22
            else:
                temp_state_machine = copy.deepcopy(local_state_machine)
                next_state, episode_end = temp_state_machine.update(node.policy.reshape(9)[i])
                for c in range(len(node.children)):
                    if np.array_equal(node.children[c].position, next_state):
                        q_s_a = node.children[c].value
                confidence_array[i] = self.confidence(q_s_a, 1, node.policy.reshape(9)[i], sqrt_parent_visits,
                                                      node.action_count.reshape(9)[i])
        action_array = (node.policy * 0).reshape(9)
        action_array[np.argmax(confidence_array)] = 1
        return action_array.reshape([1, 3, 3])

    # expands the tree to evaluate an unvisited leaf
    def expand(self, node, local_state_machine, prev_depth):
        # get actions for each batch
        actions = self.upper_confidence_bound(node, local_state_machine)
        # add actions to action count
        node.action_count += actions
        # take actions
        next_state, episode_end = local_state_machine.update(actions)
        # traverse tree untill either game end or new node
        num_children = len(node.children)
        new_position = next_state
        not_this_position = 0
        child_value = 0

        for i in range(num_children):
            if np.array_equal(node.children[i].position, new_position):
                if episode_end == 1:
                    child_value = -local_state_machine.rewards[0, local_state_machine.markov_process.player]
                else:
                    child_value = self.expand(node.children[i], copy.deepcopy(local_state_machine), prev_depth + 1)
                    node.children[i].value *= node.children[i].visited_count
                    node.children[i].visited_count += 1
                    node.children[i].value += child_value
                    node.children[i].value /= node.children[i].visited_count
            else:
                not_this_position += 1

        if num_children == 0 or not_this_position == num_children:
            legal_actions = self.state_machine.markov_process.get_legal_actions()
            raw_policy = (self.pi.get_policy(next_state) + (legal_actions * 1e-22)) * legal_actions
            normalised_policy = raw_policy / np.sum(raw_policy)
            initial_action_count = normalised_policy * 0
            # get the child node value, either from game result or value network
            if episode_end:
                child_value = -local_state_machine.rewards[0, local_state_machine.markov_process.player]
            else:
                child_value = -self.pi.get_value(next_state)
            child = Node(
                position=next_state,
                parent=node,
                policy=normalised_policy,
                value=child_value,
                children=[],
                depth=prev_depth + 1,
                action_count=initial_action_count,
                move=actions)
            node.add_child(child)

        return child_value

    # uses the tree search to find an improved policy
    def get_improved_policy(self, state, local_state_machine):
        initial_policy = self.pi.get_policy(state)
        initial_value = self.pi.get_value(state)
        initial_state = state
        initial_player = local_state_machine.markov_process.player
        noisy_policy = initial_policy + np.random.dirichlet(alpha=np.ones(9) * 0.01,
                                                            size=np.array([self.pi.batch_size])).reshape([1, 3, 3])
        noisy_policy += 1e-22 * local_state_machine.markov_process.get_legal_actions()
        noisy_policy *= local_state_machine.markov_process.get_legal_actions()
        normalised_noisy_policy = noisy_policy / np.sum(noisy_policy)
        initial_action_count = normalised_noisy_policy * 0
        improved_policy = initial_action_count * 0
        depth = 3

        root = Node(
            position=state,
            parent=None,
            policy=normalised_noisy_policy,
            value=initial_value,
            children=[],
            depth=0,
            action_count=initial_action_count,
            move=''
        )

        for i in range(depth):
            local_state_machine.set_state(initial_state, initial_player)
            root.visited_count += 1
            self.expand(root, copy.deepcopy(local_state_machine), 0)

        total_visits = 0
        prob_list = []
        for c in root.children:
            total_visits += c.visited_count
        for c in root.children:
            prob_list.append(c.visited_count / total_visits)

        for i in range(len(prob_list)):
            improved_policy += root.children[i].move * prob_list[i]

        return improved_policy.reshape([self.pi.batch_size, 3, 3])


# A general state machine class for high level interactions with a markov decision process backend
class State_Machine:

    def __init__(self, mdp):
        self.markov_process = mdp
        self.visited_states = []
        self.actions = []
        self.state = self.init_state()
        self.rewards = None
        self.end_state = False

    def init_state(self):
        state = self.markov_process.start_state()
        self.visited_states = []
        self.actions = []
        self.visited_states.append(state.copy())
        return state

    def get_state(self):
        return self.markov_process.get_state()

    def set_state(self, state, player):
        self.markov_process.state = state
        self.markov_process.player = player
        self.state = state

    def update(self, probabilities):
        self.state, self.rewards, self.end_state, action = self.markov_process.action(probabilities)
        self.actions.append(action)
        if not self.end_state:
            self.visited_states.append(self.state.copy())
        return self.state, self.end_state


# A markov decision process class for the game of tic tac toe
class TicTacToe_MDP:
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.start_state()
        self.player = 0
        self.winning_states = self.init_winning_states()
        self.rewards = np.zeros([self.batch_size, 2])
        self.games_running = np.ones([self.batch_size])

    def start_state(self):
        self.rewards = np.zeros([self.batch_size, 2])
        self.games_running = np.ones([self.batch_size])
        self.state = np.zeros([int(self.batch_size), 3, 3])
        self.player = 0
        return self.state

    def get_legal_actions(self):
        return (self.state == 0).astype(np.int8)

    def get_random_legal_actions(self):
        legal_actions = self.get_legal_actions()
        for i in range(self.batch_size):
            temp_action = legal_actions[i].reshape(9)
            random_action = np.random.choice(np.arange(temp_action.shape[0]), p=temp_action / np.sum(temp_action))
            action_array = np.zeros(9)
            action_array[random_action] = 1
            legal_actions[i] = action_array.reshape((3, 3))
        return legal_actions

    def init_winning_states(self):
        winning_mask = np.array(
            [
                [[1, 1, 1],
                 [0, 0, 0],
                 [0, 0, 0]],
                [[0, 0, 0],
                 [1, 1, 1],
                 [0, 0, 0]],
                [[0, 0, 0],
                 [0, 0, 0],
                 [1, 1, 1]],
                [[1, 0, 0],
                 [1, 0, 0],
                 [1, 0, 0]],
                [[0, 1, 0],
                 [0, 1, 0],
                 [0, 1, 0]],
                [[0, 0, 1],
                 [0, 0, 1],
                 [0, 0, 1]],
                [[1, 0, 0],
                 [0, 1, 0],
                 [0, 0, 1]],
                [[0, 0, 1],
                 [0, 1, 0],
                 [1, 0, 0]]
            ]
        )
        return winning_mask

    def episode_ended(self):
        if self.player == 0:
            examine_state = self.state.clip(0, 1)
        else:
            examine_state = self.state.clip(-1, 0
                                            )
        potential_wins = [examine_state[state] * self.winning_states for state in range(examine_state.shape[0])]
        potential_wins = [potential_wins[pw].reshape(8, 9) for pw in range(len(potential_wins))]
        sums = [np.sum(potential_wins[pw], 1) for pw in range(len(potential_wins))]
        game = -1

        for s in sums:
            game += 1
            for i in range(len(s)):
                if s[i] == 3:
                    if self.games_running[game] == 1:
                        self.rewards[game, 0] = 1
                        self.rewards[game, 1] = -1
                        self.games_running[game] = 0
                if s[i] == -3:
                    if self.games_running[game] == 1:
                        self.rewards[game, 0] = -1
                        self.rewards[game, 1] = 1
                        self.games_running[game] = 0

        game = -1
        for s in self.state:
            game += 1
            if np.count_nonzero(s) == 9:
                self.games_running[game] = 0

    def action(self, probabilities):
        legal_actions = self.get_legal_actions()
        action = ((legal_actions * 1e-22) + probabilities) * legal_actions
        flat_actions = action.reshape(action.shape[0], 9) * 0
        for i in range(action.shape[0]):
            probs = action[i].reshape([9])
            val = np.random.choice(np.arange(probs.size), p=probs / np.sum(probs))
            flat_actions[i] = np.zeros([9])
            flat_actions[i, val] = 1
            action[i] = flat_actions[i].reshape([3, 3])
        if self.player == 1:
            action *= -1
        self.state += action
        self.player = (self.player + 1) % 2
        self.episode_ended()
        return self.state, self.rewards, not np.any(self.games_running), flat_actions

    def get_state(self):
        if self.player == 1:
            return self.state * -1
        return self.state


# A class that holds a deep neural network for policy and value estimation
class Policy_Value_Network:

    def __init__(self, batch_size, input_state_size, action_space_size):
        self.p_model = None
        self.v_model = None
        self.create_p_model(input_state_size, action_space_size)
        self.create_v_model(input_state_size, action_space_size)
        self.batch_size = batch_size
        self.input_state_size = input_state_size
        self.action_count = action_space_size

    def create_p_model(self, input_shape=(3, 3,), actions=9):
        input_layer = Input(shape=input_shape)
        dense_1 = Dense(units=32, activation='relu')(input_layer)
        dense_2 = Dense(units=32, activation='relu')(dense_1)
        flat = Flatten()(dense_2)
        dense_3 = Dense(units=actions, activation='softmax')(flat)
        m = Model(input_layer, dense_3)
        self.p_model = m
        opt = adam(0.001)
        self.p_model.compile(opt, loss='categorical_crossentropy')

    def create_v_model(self, input_shape=(3, 3,), actions=9):
        input_layer = Input(shape=input_shape)
        dense_1 = Dense(units=32, activation='relu')(input_layer)
        dense_2 = Dense(units=32, activation='relu')(dense_1)
        flat = Flatten()(dense_2)
        dense_3 = Dense(units=1, activation='tanh')(flat)
        m = Model(input_layer, dense_3)
        self.v_model = m
        opt = adam(0.001)
        self.v_model.compile(opt, loss='mse')

    def get_policy(self, model_input):
        prediction = self.p_model.predict([model_input], batch_size=self.batch_size)
        return prediction.reshape([prediction.shape[0], 3, 3])

    def get_value(self, model_input):
        prediction = self.v_model.predict([model_input], batch_size=self.batch_size)
        return prediction.reshape([prediction.shape[0], 1])

    def train(self, actions, states, rewards):
        state_list = []
        action_list = []
        reward_list = []

        for i in range(len(states)):
            for b in range(self.batch_size):
                if not i % 2:
                    if not rewards[b, 0] == 0:
                        state_list.append(states[i][b])
                        action_list.append(actions[i][b])
                        reward_list.append(rewards[b, 0])
                else:
                    if not rewards[b, 1] == 0:
                        state_list.append(states[i][b] * -1)
                        action_list.append(actions[i][b] * -1)
                        reward_list.append(rewards[b, 1])

        if len(reward_list):
            self.p_model.fit([state_list], [action_list], batch_size=len(state_list), verbose=0)
            self.v_model.fit([state_list], [reward_list], batch_size=len(state_list), verbose=0)


# A function that creates an agent and learns/plays etc
def learn_fn():
    batch_size = 1
    total_epochs = 100000
    batches_per_save = 100

    t = TicTacToe_MDP(batch_size)
    s = State_Machine(t)
    input_shape = (3, 3,)
    output_size = 9
    pv_net = Policy_Value_Network(batch_size, input_shape, output_size)
    pv_net.p_model.load_weights('checkpoints/policy_25100')
    pv_net.v_model.load_weights('checkpoints/value_25100')
    pi = pv_net
    state_machine = s
    a = RL_Agent(pi, state_machine)
    starting_checkpoint = 5600

    for e in range(total_epochs):
        a.play_random(100)
        a.learn(batches_per_save)
        new_checkpoint = starting_checkpoint + (e + 1) * batch_size * batches_per_save
        a.save(new_checkpoint)
        print("Completed batch", e)


if __name__ == "__main__":
    learn_fn()
