import numpy as np
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam, SGD
import gym
from tqdm import trange, tqdm
from tensorflow.keras.models import load_model
import time

tf.keras.backend.set_floatx('float32')
np.set_printoptions(precision=2)
env_name = "LunarLander-v2"
MAX_STEPS = 2000
MAX_SCORE = -np.inf

# GROUP LEARNING ALGORITHM
#
# This algorithm was inspired by the way rock climbers work together to solve
# 'problems' in the climbing gym, by imitating stronger climbers.
#
# Several random agents are initialised that all attempt to solve a problem, 
# e.g. act in an environment observing rewards. The best performing agent is 
# then used as a teacher for the other agents. This is achieved by going back
# over each agent's experience replay buffer and asking the best agent what it
# would have done in each state experienced. The weaker agents are trained 
# using the gradient obtained from the difference between the actions they 
# took in the episode and the actions taken by the best agent.
#
# My initial concern was that the agents would plateu at the level of the best
# initialised agent, however after running this algorithm on different 
# environments, it is clear that weaker agents can surpass the 'best' agent
# then becoming the new teacher agent. This leads to a piggy-back effect that
# results in all of the agents eventually solving the task.

class PolicyModel(tf.keras.Model):
    def __init__(self, output_nodes):
        super(Model, self).__init__()
        self.flatten_1 = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(units=20, activation='elu',
                                            kernel_regularizer=tf.keras.regularizers.l2(l=1e-22))
        self.dense2 = tf.keras.layers.Dense(units=20, activation='elu',
                                            kernel_regularizer=tf.keras.regularizers.l2(l=1e-22))
        self.dense3 = tf.keras.layers.Dense(units=20, activation='elu',
                                            kernel_regularizer=tf.keras.regularizers.l2(l=1e-22))
        self.dense4 = tf.keras.layers.Dense(units=output_nodes, activation='softmax',
                                            kernel_regularizer=tf.keras.regularizers.l2(l=1e-22))

    @tf.function
    def call(self, inputs):
        x = self.flatten_1(inputs)
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        return x

def mutate_model(mutate_index, best_model_index):
    print("Mutating model", mutate_index)
    best_model_weights = p_model_group[best_model_index].get_weights()
    p_model_group[mutate_index].set_weights(best_model_weights)
    states = np.asarray(states_encountered)
    labels = p_model_group[mutate_index](states).numpy()
    mutated_labels = (labels * (1 - MUTATE_AMOUNT)) + (np.random.rand(*labels.shape) * MUTATE_AMOUNT)
    normalised_mutated_labels = mutated_labels / np.sum(mutated_labels)
    train_model(mutate_index, best_model_index, states, normalised_mutated_labels)

def test_model(m, e, bar):
    global states_encountered
    state = e.reset()
    terminal = False
    value = 0
    step = 0
    while terminal is False:
        step += 1
        state = state.reshape(1, state_space)
        states_encountered.append(state)
        policy = m(state).numpy().reshape(action_space)
        action = np.random.choice(np.arange(action_space), p=policy)
        state, reward, terminal, _ = e.step(action)     # + noise)
        value += reward
        if step > MAX_STEPS:
            terminal = True
    bar.update()

    return value

def test_and_render_model(m, e):
    state = e.reset()
    terminal = False
    value = 0
    step = 0
    while terminal is False:
        # time.sleep(0.1)
        step += 1
        e.render()
        state = state.reshape(1, state_space)                                   
        policy = m(state).numpy().reshape(action_space)
        action = np.random.choice(np.arange(action_space), p=policy)
        state, reward, terminal, _ = e.step(action)
        value += reward
        # print(reward)
        if step > MAX_STEPS:
            terminal = True
    print("scored", value)
    return value

def test_models(m, e, n):
    with tqdm(total=(n * num_models), leave=False) as t:
        values = np.average([[test_model(m[i], e[i], t) for i in range(num_models)] for _ in range(n)], axis=0)
    return values

def get_best_and_worst_model(m, e, n):
    global MAX_SCORE
    model_values = test_models(m, e, n)
    print(model_values, "Av:", int(np.average(model_values)), "Max:", int(np.amax(model_values)))
    max_val = np.amax(model_values)
    all_time_high = False
    if max_val > MAX_SCORE:
        MAX_SCORE = max_val
        all_time_high = True
    return np.argmax(model_values), np.argmin(model_values), all_time_high

def train_models(best_model, worst_model):
    training_states = np.asarray(states_encountered)
    good_labels = p_model_group[best_model](training_states)
    [train_model(i, best_model, training_states, good_labels) for i in trange(num_models, desc='Training', leave=False)]

def train_model(model_index, best_model_index, states, labels):
    # if not model_index == best_model_index:
    with tf.GradientTape() as t:
        predictions = p_model_group[model_index](states)
        loss = tf.reduce_mean(tf.square(labels - predictions))
    v = p_model_group[model_index].trainable_variables
    grads = t.gradient(loss, v)
    optimizers[model_index].apply_gradients(zip(grads, v))

def save_model_from_index(model_index):
    p_model_group[model_index].save("./checkpoints/" + env_name)

def load_latest_checkpoint_to_model():
    m = load_model("./checkpoints/" + env_name)
    return m

def set_model_inputs(m):
    inputs = tf.keras.Input(shape=(state_space,))
    outputs = m(inputs)
    m = tf.keras.Model(inputs=inputs, outputs=outputs)
    return m

num_models = 10
num_simulations = 1000
num_train_cycles = 10000
mutate_iterations = 3

envs = [gym.make(env_name) for _ in range(num_models)]
# action_space = envs[0].action_space.shape[0]
# state_space = envs[0].observation_space.shape[0]

action_space = 4
state_space = 8
MUTATE_AMOUNT = 0.9

print(envs[0].action_space)
print(envs[0].observation_space)

optimizers = [Adam(learning_rate=1e-2) for _ in range(num_models)]
p_model_group = [PolicyModel(action_space) for _ in range(num_models)]
p_model_group = [set_model_inputs(p_model_group[i]) for i in range(num_models)]

# # LOAD AND RENDER BEST MODEL ===>
# model_to_test = load_latest_checkpoint_to_model()
# test_env = gym.make(env_name)
# for _ in range(100):
#     test_and_render_model(model_to_test, test_env)
# quit()

# states_encountered = []
consistently_worst = 0
last_worst_model = -1
for i in range(num_train_cycles):
    states_encountered = []
    best_model, worst_model, to_save = get_best_and_worst_model(p_model_group, envs, num_simulations)
    if worst_model == last_worst_model:
        consistently_worst += 1
    if consistently_worst >= mutate_iterations:
        mutate_model(worst_model, best_model)
        consistently_worst = 0
    last_worst_model = worst_model
    if to_save:
        save_model_from_index(best_model)
        print("Saved")
    train_models(best_model, worst_model)

















