import numpy as np
import tensorflow as tf
from tensorflow.keras import Model
from cartpole_config import CartPoleConfig

initializer = tf.keras.initializers.glorot_normal(seed=None)
config = None
# sets the config file for internal use in the module
def set_network_config(c):
    global config
    config = c

# class used to generate a probability distribution over actions to take, based on current state
class PolicyModel(tf.keras.Model):
    def __init__(self):
        super(Model, self).__init__()
        self.flatten_1 = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense2 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense3 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense4 = tf.keras.layers.Dense(units=config.action_space_size, activation='linear', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)

    def call(self, inputs):
        x = self.flatten_1(inputs)
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        return x

# class used to output a predicted value, based on current state
class ValueModel(tf.keras.Model):
    def __init__(self):
        super(Model, self).__init__()
        self.flatten_1 = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense2 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense3 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense4 = tf.keras.layers.Dense(units=1, activation='linear', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)

    def call(self, inputs):
        x = self.flatten_1(inputs)
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        return x

# class used to predict the value of taking a specific action in the current state
class RewardModel(tf.keras.Model):
    def __init__(self):
        super(Model, self).__init__()
        self.flatten_1 = tf.keras.layers.Flatten()
        self.flatten_2 = tf.keras.layers.Flatten()
        self.conc_1 = tf.keras.layers.Concatenate()
        self.dense1 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense2 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense3 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense4 = tf.keras.layers.Dense(units=1, activation='linear', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)

    def call(self, input1, input2):
        x1 = self.flatten_1(input1)
        x2 = self.flatten_2(input2)
        x = self.conc_1([x1, x2])
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        return x

# class used to encode the current observation (raw state) into an internal hidden state
class RepresentationModel(tf.keras.Model):
    def __init__(self):
        super(Model, self).__init__()
        # self.input_1 = tf.keras.layers.Input(input_shape=(4, 5))
        self.flatten_1 = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense2 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense3 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense4 = tf.keras.layers.Dense(units=config.hidden_state_neurons, activation='tanh', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.reshape1 = tf.keras.layers.Reshape(config.hidden_state_shape)

    def call(self, inputs):
        x = self.flatten_1(inputs)
        x = self.dense1(x)
        x = self.dense2(x)
        x = self.dense3(x)
        x = self.dense4(x)
        x = self.reshape1(x)
        return x

# class used to predict the next hidden state that will occur after a given action in a given hidden state
class DynamicsModel(tf.keras.Model):
    def __init__(self):
        super(Model, self).__init__()
        self.flatten_1 = tf.keras.layers.Flatten()
        self.flatten_2 = tf.keras.layers.Flatten()
        self.conc_1 = tf.keras.layers.Concatenate()
        self.dense1 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense2 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense3 = tf.keras.layers.Dense(units=config.default_node_count, activation='elu', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.dense4 = tf.keras.layers.Dense(units=config.hidden_state_neurons, activation='tanh', kernel_regularizer=tf.keras.regularizers.l2(l=config.l2_reg), kernel_initializer=initializer)
        self.reshape1 = tf.keras.layers.Reshape(config.hidden_state_shape)

    def call(self, input1, input2):
        x1 = self.flatten_1(input1)
        x2 = self.flatten_2(input2)
        x3 = self.conc_1([x1, x2])
        x3 = self.dense1(x3)
        x3 = self.dense2(x3)
        x3 = self.dense3(x3)
        x3 = self.dense4(x3)
        x3 = self.reshape1(x3)
        return x3

# main networks container, responsible for combining the networks for full/recurrent inference, and training
class Networks:
    def __init__(self):
        tf.keras.backend.set_floatx('float64')              # set backend to override flatten layer conversion to fp32
        self.games = []                                     # stores game statistics for training e.g. states, rewards, values, poliies, actions
        self.r_model = RepresentationModel()                # member models
        self.p_model = PolicyModel()
        self.v_model = ValueModel()
        self.d_model = DynamicsModel()
        self.reward_model = RewardModel()
        self.num_train_steps = 0                            # counter
        self.global_optimizer = tf.keras.optimizers.SGD(learning_rate=config.learning_rate)

        if tf.test.gpu_device_name():
            print('GPU found')
        else:
            print("No GPU found")

    # function to take the current state and output a policy, value and hidden state
    @tf.function  # annotate as tf function to force graph execution (optimisation)
    def initial_inference(self, state):
        hidden_state = self.r_model(state)
        policy = self.p_model(hidden_state)
        value = self.v_model(hidden_state)
        return policy, value, hidden_state

    # function to take a hidden state and action, outputting a policy, value, next hidden state and reward
    @tf.function
    def recurrent_inference(self, hidden_state, action):
        next_state = self.d_model(hidden_state, action)
        value = self.v_model(next_state)
        policy = self.p_model(next_state)
        reward = self.reward_model(hidden_state, action)
        return policy, value, next_state, reward

    # trick function to scale gradients in tensorflow (used because we have more unroll steps -> dyn network training examples)
    def scale_gradient(self, tensor, scale: float):
        return (1. - scale) * tf.stop_gradient(tensor) + scale * tensor

    # reshapes a game history into lists for use in the train function. Also adds absorbing states for bootstraping post-episode end
    def process_game(self, s, p, v, a, r):
        t_policies = [t_p.reshape(1, config.action_space_size) for t_p in p]
        t_values = [t_v.reshape(1, 1) for t_v in v]
        t_states = [t_s.reshape(1, config.state_space_size[0], config.state_space_size[1]) for t_s in s]
        t_actions = [t_a.reshape(1, config.action_space_size) for t_a in a]
        t_rewards = [np.array(t_r).reshape(1, 1) for t_r in r]

        last = len(t_policies) - 1

        for _ in range(config.boostrap_steps):
            t_values.append(t_values[last] * 0)
            t_actions.append(t_actions[last])
            t_rewards.append(t_rewards[last] * 0)

        return t_states, t_policies, t_values, t_actions, t_rewards

    @tf.function
    def mse_inner(self, num_samples):
        with tf.GradientTape(persistent=True) as t:
            loss = 0
            # get random game from storage
            random_game = self.games[np.random.randint(0, len(self.games))]
            # unpack game and reshape data
            t_states, t_policies, t_values, t_actions, t_rewards = random_game
            t_states, t_policies, t_values, t_actions, t_rewards = self.process_game(t_states, t_policies, t_values,
                                                                                     t_actions, t_rewards)
            # get timestep to start at in game
            state_index = np.random.randint(0, len(t_states) - 1)
            # get timestep from which we bootstrap the reward
            bootstrap_index = state_index + config.boostrap_steps
            # calculate temporal difference value target
            value_target = np.sum(np.asarray(t_rewards[state_index:bootstrap_index])) + (
                        (config.discount ** (state_index - bootstrap_index)) * t_values[bootstrap_index])
            # initial inference, direct observation -> policy, value, hidden state
            policy, value, hidden_state = self.initial_inference(t_states[state_index])
            # calculate policy and value loss
            loss += self.scale_gradient(tf.reduce_mean(tf.square(t_policies[state_index] - tf.nn.softmax(policy))),
                                        (1 / (num_samples * config.num_unroll_steps)))
            loss += self.scale_gradient(tf.reduce_mean(tf.square(value_target - value)),
                                        (1 / (num_samples * config.num_unroll_steps)))
            # unroll from the start state to get loss values of dynamics/reward networks
            for s in range(config.num_unroll_steps):
                # get the next state, policy, value and reward via recurrrent inference (from hidden state rather than observation)
                policy, value, next_state, reward = self.recurrent_inference(hidden_state, t_actions[state_index])
                # calculate reward loss
                loss += self.scale_gradient(tf.reduce_mean(tf.square(t_rewards[state_index] - reward)),
                                            (1 / (config.num_unroll_steps * num_samples)))
                state_index += 1
                # calculate temporal difference value target
                value_target = np.sum(np.asarray(t_rewards[state_index:bootstrap_index])) + (
                            (config.discount ** (state_index - bootstrap_index)) * t_values[bootstrap_index])
                # calculate policy and value loss
                if not state_index >= (len(t_policies) - 1):
                    loss += self.scale_gradient(
                        tf.reduce_mean(tf.square(t_policies[state_index] - tf.nn.softmax(policy))),
                        (0.5 / (config.num_unroll_steps * num_samples)))
                loss += self.scale_gradient(tf.reduce_mean(tf.square(value_target - value)),
                                            (0.5 / (config.num_unroll_steps * num_samples)))
                # update hiden state to next predicted state
                hidden_state = next_state
        # collate variables accross models (list concatenation via + operator)
        # v = self.p_model.variables + self.d_model.variables + self.r_model.variables + self.v_model.variables + self.reward_model.variables
        v = self.p_model.trainable_variables
        # get gradients, apply gradients, update number of train steps
        grads = t.gradient(loss, v)
        self.global_optimizer.apply_gradients(zip(grads, v))

        v = self.d_model.trainable_variables
        # get gradients, apply gradients, update number of train steps
        grads = t.gradient(loss, v)
        self.global_optimizer.apply_gradients(zip(grads, v))

        v = self.r_model.trainable_variables
        # get gradients, apply gradients, update number of train steps
        grads = t.gradient(loss, v)
        self.global_optimizer.apply_gradients(zip(grads, v))

        v = self.v_model.trainable_variables
        # get gradients, apply gradients, update number of train steps
        grads = t.gradient(loss, v)
        self.global_optimizer.apply_gradients(zip(grads, v))

        v = self.reward_model.trainable_variables
        # get gradients, apply gradients, update number of train steps
        grads = t.gradient(loss, v)
        self.global_optimizer.apply_gradients(zip(grads, v))

        self.num_train_steps += num_samples
        del t

    @tf.function
    def cross_entropy_inner(self, num_samples):
        with tf.GradientTape() as t:
            loss = 0
            # get random game from storage
            random_game = self.games[np.random.randint(0, len(self.games))]
            # unpack game and reshape data
            t_states, t_policies, t_values, t_actions, t_rewards = random_game
            t_states, t_policies, t_values, t_actions, t_rewards = self.process_game(t_states, t_policies, t_values,
                                                                                     t_actions, t_rewards)
            # get timestep to start at in game
            state_index = np.random.randint(0, len(t_states) - 1)
            # get timestep from which we bootstrap the reward
            bootstrap_index = state_index + config.boostrap_steps
            # calculate temporal difference value target
            value_target = np.sum(np.asarray(t_rewards[state_index:bootstrap_index])) + (
                        (config.discount ** (state_index - bootstrap_index)) * t_values[bootstrap_index])
            # initial inference, direct observation -> policy, value, hidden state
            policy, value, hidden_state = self.initial_inference(t_states[state_index])
            # calculate policy and value loss
            loss += self.scale_gradient(
                tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=t_policies[state_index], logits=policy)),
                (1 / num_samples))
            loss += self.scale_gradient(tf.reduce_mean(tf.square(value_target - value)),
                                        (1 / num_samples))
            # unroll from the start state to get loss values of dynamics/reward networks
            for s in range(config.num_unroll_steps):
                # get the next state, policy, value and reward via recurrrent inference (from hidden state rather than observation)
                policy, value, next_state, reward = self.recurrent_inference(hidden_state, t_actions[state_index])
                # calculate reward loss
                loss += self.scale_gradient(tf.reduce_mean(tf.square(t_rewards[state_index] - reward)),
                                            (1 / (config.num_unroll_steps * num_samples)))
                state_index += 1
                # calculate temporal difference value target
                value_target = np.sum(np.asarray(t_rewards[state_index:bootstrap_index])) + (
                            (config.discount ** (state_index - bootstrap_index)) * t_values[bootstrap_index])
                # calculate policy and value loss
                if not state_index >= (len(t_policies) - 1):
                    loss += self.scale_gradient(tf.reduce_mean(
                        tf.nn.softmax_cross_entropy_with_logits(labels=t_policies[state_index], logits=policy)),
                                                (0.5 / (config.num_unroll_steps * num_samples)))
                loss += self.scale_gradient(tf.reduce_mean(tf.square(value_target - value)),
                                            (0.5 / (config.num_unroll_steps * num_samples)))
                # update hiden state to next predicted state
                hidden_state = next_state
            # collate variables accross models (list concatenation via + operator)
            v = self.p_model.variables + self.d_model.variables + self.r_model.variables + self.v_model.variables + self.reward_model.variables
            # get gradients, apply gradients, update number of train steps
            grads = t.gradient(loss, v)
            self.global_optimizer.apply_gradients(zip(grads, v))
            self.num_train_steps += num_samples

    # train the networks using the mean squared error loss
    # @tf.function
    def train_mse(self):
        num_samples = min(128, len(self.games))
        for i in range(num_samples):
            self.mse_inner(num_samples)

    # train the networks using the cross entropy loss
    def train_cross_entropy(self):
        num_samples = min(128, len(self.games))
        for i in range(num_samples):
            self.cross_entropy_inner(num_samples)

    # takes a game's states, policies, values, actions and rewards and stores them in the buffer
    def store(self, states, policies, values, actions, rewards):
        game_events = (states, policies, values, actions, rewards)
        self.games.append(game_events)
        if len(self.games) > config.max_games:
            self.games.pop(0)

    def scalar_value_to_vector(self, v):
        value_vector = np.zeros(config.value_vector_size)
        value_vector[int(v)] = 1
        return value_vector

    def vector_value_to_scalar(self, v):
        return np.argmax(v)
