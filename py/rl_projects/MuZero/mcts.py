import math
import numpy as np

config = None
# sets the config file for internal use in the module
def set_mcts_config(c):
    global config
    config = c

# class used for normalisation within the tree so that all values remain in [0, 1]
class MinMax:
    def __init__(self):
        self.maximum = -float('inf')
        self.minimum = float('inf')

    # updates the current cached max and min values
    def update(self, v):
        self.maximum = max(self.maximum, v)
        self.minimum = min(self.minimum, v)

    # normalises a value based on the current max and min values
    def normalise(self, value):
        if self.maximum > self.minimum:
            return (value-self.minimum) / self.maximum
        return value

# class used for nodes in the tree
class Node(object):
    def __init__(self, prior: float):
        self.visit_count = 0
        self.to_play = 0
        self.prior = prior
        self.value_sum = 0
        self.children = {}
        self.hidden_state = None
        self.reward = 0

    def expanded(self) -> bool:
        return len(self.children) > 0

    def value(self) -> float:
        if self.visit_count == 0:
            return 0
        return self.value_sum / self.visit_count

# expanding a node: give the node a hidden state, policy and reward. Add unexpanded children
def expand_node(node, hidden_state, reward, policy):
    node.hidden_state = hidden_state.reshape(7, 7)
    node.reward = reward
    # e function as the policy is logits
    policy = np.exp(policy)
    for i in range(2):
        node.children[i] = Node(policy[i] / np.sum(policy))
    return node

# adds initial noise to the policy to increase exploration
def add_exploration_noise(node):
    actions = list(node.children.keys())
    noise = np.random.dirichlet([config.noise_alpha] * len(actions))
    frac = config.noise_alpha
    for a, n in zip(actions, noise):
        node.children[a].prior = node.children[a].prior * (1 - frac) + n * frac

# gets the new policy from the root node's visit counts to children
def get_r_pol(root):
    pol = np.zeros(config.num_actions)
    for i in range(config.num_actions):
        pol[i] = root.children[i].visit_count
    pol /= np.sum(pol)
    return pol

# gets the root node value
def get_r_val(root, n):
    v = np.ones(1)
    v *= root.value()
    return v

# finds a leaf from a tree root
def get_leaves(root, n):
    node = root
    search_path = [node]

    action = -1
    # keep selecting children if current node is expanded
    while node.expanded():
        action, node = select_child(node, n)
        search_path.append(node)

    parent = search_path[-2]
    action_array = np.zeros((1, 2))
    action_array[0, action] = 1

    return (parent.hidden_state, action_array, node, search_path, root)

# selects a child node with the highest upper confidence bound score
def select_child(node: Node, n):
    highest_ucb = -np.inf
    for i in range(len(node.children)):
        temp_ucb = ucb_score(node, node.children[i], n)
        if temp_ucb > highest_ucb:
            highest_ucb = temp_ucb
            child = node.children[i]
            action = i
    return action, child

# calculates the upper confidence bound score for a node
def ucb_score(parent: Node, child: Node, n) -> float:
    pb_c = math.log((parent.visit_count + 19652 + 1) /
                    19652) + 1.25
    pb_c *= math.sqrt(parent.visit_count) / (child.visit_count + 1)
    prior_score = pb_c * child.prior
    # casting to float to avoid eager tensor memory leak...
    value_score = float(n.normalise(child.value()))
    return prior_score + value_score

# passes the value back up the tree search path
def backpropagate(search_path, value, n):
    for node in search_path:
        node.value_sum += value
        node.visit_count += 1
        n.update(node.value())
        value = node.reward + config.discount * value
