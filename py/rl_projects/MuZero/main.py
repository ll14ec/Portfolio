import multiprocessing
import sys
from statistics import mean, median

import gym
import numpy as np

from cartpole_config import CartPoleConfig
from mcts import Node, expand_node, MinMax, get_leaves, backpropagate, add_exploration_noise, set_mcts_config
from networks import Networks, set_network_config

# class to store episode statistics e.g. states, values, policies, actions, rewards experienced in an episode by an agent
class Game:
    def __init__(self):
        self.states = []
        self.values = []
        self.policies = []
        self.rewards = []
        self.actions = []

    def reset(self):
        self.states = []
        self.values = []
        self.policies = []
        self.rewards = []
        self.actions = []

    def add_state(self, s):
        self.states.append(s)

    def add_value(self, v):
        self.values.append(v)

    def add_policy(self, p):
        self.policies.append(p)

    def add_reward(self, r):
        self.rewards.append(r)

    def add_action(self, a):
        self.actions.append(a)


# sets config files for the network and mcts modules
def set_configs(c):
    set_mcts_config(c)
    set_network_config(c)


# gets a numpy array of oberservations running up to and including the current time step
def get_past_frames(num_frames, past_states):
    # total number of states in buffer
    total_states = len(past_states)
    # initialise array of past frames
    past_frames = np.zeros((1, config.state_space_size[0], config.state_space_size[1]))
    # if there are enough frames in the buffer to fill the past states array -> fill
    if total_states >= num_frames:
        for i in range(num_frames):
            past_frames[:, :, i:(i + 1)] = past_states[-(i + 1)].reshape(1, 4, 1)
    # else fill the maximum amount of recent frames
    else:
        for i in range(total_states):
            past_frames[:, :, i:(i + 1)] = past_states[-(i + 1)].reshape(1, 4, 1)
    return past_frames


# print the status of the current batch of episodes
def print_status(s):
    if len(s) > 0:
        sto_or_greedy = "Running stochastic |" if stochastic else "Running greedy     |"
        message = "\r" + sto_or_greedy + " Total episodes: " + str(self_play_games) + \
                  " | Chunk mean: " + str(int(mean(s))) + " | Chunk median: " + str(int(median(s))) \
                  + "               "
        sys.stdout.write(message)
        sys.stdout.flush()


# store the average score of greedy tests
def add_average_score_to_greedy_scores(s):
    if len(s) > 0:
        greedy_run_average_scores.append(mean(s))


# switch the agent to act greedily wrt the policy, also reset envs for a clean run-through
def switch_to_greedy():
    global greedy, stochastic, greed_index, games, states, envs
    [games[i].reset() for i in range(config.num_environments)]
    states = [[envs[i].reset()] for i in range(config.num_environments)]
    greedy = True
    stochastic = False
    greed_index = 0
    print("Start greedy policy test.")


# switch the agent to act stochastically wrt the policy, also reset envs for a clean run-through
def switch_to_stochastic():
    global greedy, stochastic, greed_index, games, states, envs
    [games[i].reset() for i in range(config.num_environments)]
    states = [[envs[i].reset()] for i in range(config.num_environments)]
    greedy = False
    stochastic = True
    greed_index = 0


# train the network on the current stored games, reset the scores and increment the train steps
def train_and_update():
    global scores, train_steps
    print()
    print("Started training:", config.policy_loss_fn)
    for _ in range(config.batches_per_train_episode):
        if config.policy_loss_fn == 'mse':
            n.train_mse()
        elif config.policy_loss_fn == 'cross_entropy':
            n.train_cross_entropy()
        else:
            print("Error, loss function not supported. Choose either 'mse' or 'cross_entropy'.")
            quit()
    scores = []
    train_steps += config.batches_per_train_episode
    print("Completed", train_steps, "total training steps.")


# converts the raw policy value and hidden state into a expansion-friendly format
def get_node_expansion_inputs(p, v, h):
    pl = [p[i].numpy().reshape(config.action_space_size) for i in range(config.num_environments)]
    vl = [v[i].numpy().reshape((1, 1)) for i in range(config.num_environments)]
    hsl = [h[i].numpy().reshape(hidden_state_shape_tuple) for i in range(config.num_environments)]
    rl = [0 for _ in range(config.num_environments)]
    # initialise root nodes -> get roots
    r = [Node(0) for _ in range(config.num_environments)]
    return pl, vl, hsl, rl, r


# gets the new states, rewards and terminal status of all concurrent environments
def process_env_feedback(f):
    n_states = []
    n_rewards = []
    n_terminals = []
    # for each e nvironment running concurrently
    for val in f:
        new_state = val[0]
        new_reward = val[1]
        new_terminal = val[2]
        new_reward = new_reward if not new_terminal else -new_reward
        n_states.append(new_state)
        n_rewards.append(new_reward)          # scale reward
        n_terminals.append(new_terminal)
    return n_states, n_rewards, n_terminals


# gets the new policy from the root node's visit counts to children
def get_root_policy(root):
    pol = np.zeros(config.num_actions)
    for i in range(config.num_actions):
        pol[i] = root.children[i].visit_count
    pol /= np.sum(pol)
    return pol


# gets the root node value
def get_root_value(root):
    v = np.ones(1)
    v *= root.value()
    return v


# chooses action stochastically based on number of training steps completed and policy probs
def choose_action_stochastic_with_temperature(policy):
    probability_of_greedy = train_steps / TRAIN_STEPS
    x = np.random.rand()
    if probability_of_greedy > x:
        return np.argmax(policy)
    else:
        return np.random.choice(np.arange(config.action_space_size), p=policy)


# gets a policy and value based on the monte carlo tree search
def get_mcts_policy_and_value(p, v, h):
    normalisers = [MinMax() for _ in range(config.num_environments)]
    # process initial neural network assesment of policies, values and hidden states
    policy_lists, value_lists, hidden_state_lists, reward_lists, roots = get_node_expansion_inputs(p, v, h)
    # expand all root nodes
    roots = [expand_node(roots[i], hidden_state_lists[i], reward_lists[i], policy_lists[i]) for i in
             range(config.num_environments)]
    if stochastic:
        [add_exploration_noise(roots[i]) for i in range(config.num_environments)]
    # run the tree search on each root
    for _ in range(config.tree_depth):
        ans = pool.starmap(get_leaves, zip(roots, normalisers))
        p_hidden_state = np.asarray([x[0] for x in ans]).reshape(config.num_environments, config.hidden_state_shape[0],
                                                                 config.hidden_state_shape[1])
        action_array = np.asarray([x[1] for x in ans]).reshape(config.num_environments, config.action_space_size)
        leaf = [x[2] for x in ans]
        search_path = [x[3] for x in ans]
        roots = [x[4] for x in ans]
        policy, value, hidden_state, reward = n.recurrent_inference(p_hidden_state, action_array)
        hsl = [hidden_state[i].numpy() for i in range(config.num_environments)]
        pl = [policy[i].numpy() for i in range(config.num_environments)]
        rl = [reward[i].numpy() for i in range(config.num_environments)]
        vl = [value[i].numpy() for i in range(config.num_environments)]
        [expand_node(leaf[i], hsl[i], rl[i], pl[i]) for i in range(config.num_environments)]
        [backpropagate(search_path[i], vl[i], normalisers[i]) for i in range(config.num_environments)]
    mcts_policies = [get_root_policy(roots[i]) for i in range(config.num_environments)]
    mcts_values = [get_root_value(roots[i]) for i in range(config.num_environments)]
    return mcts_policies, mcts_values


# main function that runs the muzero algorithm
def main_loop():
    global stochastic, greedy, scores, self_play_games, next_train_step, greed_index, pool
    # get past n frames
    frames = [get_past_frames(num_past_states_list[i], states[i]) for i in range(config.num_environments)]
    # store frames in game histories
    [games[i].add_state(frames[i]) for i in range(config.num_environments)]
    # initial inference -> get policies, values, hidden states
    frames_as_array = np.asarray(frames).reshape(
        (config.num_environments, config.state_space_size[0], config.state_space_size[1]))
    policies, values, hidden_states = n.initial_inference(frames_as_array)
    # get improved policies and values from the monte carlo tree search
    mcts_policies, mcts_values = get_mcts_policy_and_value(policies, values, hidden_states)
    # append mcts policies and values to game lists
    [games[i].add_policy(mcts_policies[i]) for i in range(config.num_environments)]
    [games[i].add_value(mcts_values[i]) for i in range(config.num_environments)]
    # create action arrays and action indices
    action_arrays = [np.zeros(config.action_space_size) for _ in range(config.num_environments)]
    action_indices = None
    # get stochastic/greedy action depending on eploration/exploitation
    if stochastic:
        action_indices = [choose_action_stochastic_with_temperature(mcts_policies[i]) for i in range(config.num_environments)]
    elif greedy:
        action_indices = [np.argmax(mcts_policies[i]) for i in range(config.num_environments)]
    # store action
    for i in range(config.num_environments):
        action_arrays[i][action_indices[i]] = 1
    # add action to game history
    [games[i].add_action(action_arrays[i]) for i in range(config.num_environments)]
    # make action in envs
    env_feedback = [envs[i].step(action_indices[i]) for i in range(config.num_environments)]
    # process env return
    new_states, new_rewards, new_terminals = process_env_feedback(env_feedback)
    # append rewards to game lists
    [games[i].add_reward(new_rewards[i]) for i in range(config.num_environments)]
    # check for ended episodes, if ended store game and reset env
    for i in range(config.num_environments):
        # if an episoze is over
        if new_terminals[i]:
            # store score
            scores.append(sum(games[i].rewards))
            # store game in replay buffer, reset game history and environment
            n.store(games[i].states, games[i].policies, games[i].values, games[i].actions, games[i].rewards)
            # reset games, states and envs
            games[i].reset()
            states[i] = [envs[i].reset()]
            self_play_games += 1
            if greedy:
                greed_index += 1
        else:
            states[i] = [new_states[i]]

    print_status(scores)

    if self_play_games > next_train_step:
        train_and_update()
        next_train_step += config.episodes_per_train
        switch_to_greedy()

    if greed_index > config.greedy_steps:
        print()
        print("End greedy policy test.")
        switch_to_stochastic()
        add_average_score_to_greedy_scores(scores)
        scores = []


# resets variables to start fresh run
def reset_globals():
    global run, self_play_games, train_steps, greed_index, next_train_step, envs, states, num_past_states_list, games, greedy_run_average_scores
    run = 0
    self_play_games = 0
    train_steps = 0
    greed_index = 0
    next_train_step = config.episodes_per_train
    envs = [gym.make(GYM_ENVIRONMENT) for _ in range(config.num_environments)]
    states = [[e.reset()] for e in envs]
    num_past_states_list = [config.num_past_states for _ in range(config.num_environments)]
    games = [Game() for _ in range(config.num_environments)]
    greedy_run_average_scores = []
    switch_to_stochastic()

MSE = 'mse'
CROSS_ENTROPY = 'cross_entropy'

# set config list
configs = [
    CartPoleConfig(tree_depth=32,
                   learning_rate=0.008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-32,
                   policy_loss_fn=MSE,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

    CartPoleConfig(tree_depth=32,
                   learning_rate=0.008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-32,
                   policy_loss_fn=CROSS_ENTROPY,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

    CartPoleConfig(tree_depth=12,
                   learning_rate=0.008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-32,
                   policy_loss_fn=MSE,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

    CartPoleConfig(tree_depth=12,
                   learning_rate=0.008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-32,
                   policy_loss_fn=CROSS_ENTROPY,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

    CartPoleConfig(tree_depth=12,
                   learning_rate=0.0008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-32,
                   policy_loss_fn=MSE,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

    CartPoleConfig(tree_depth=12,
                   learning_rate=0.008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-6,
                   policy_loss_fn=MSE,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

    CartPoleConfig(tree_depth=12,
                   learning_rate=0.008,
                   max_games=2000,
                   batches_per_train_episode=1,
                   l2_reg=1e-4,
                   policy_loss_fn=MSE,
                   episodes_per_train=128,
                   greedy_steps=28,
                   default_node_count=16),

]

pool = multiprocessing.Pool(multiprocessing.cpu_count())
config = configs[0]

scores = []
greedy_run_average_scores = []
greedy_run_score_list = []
stochastic = True
greedy = False
GYM_ENVIRONMENT = "CartPole-v1"

TRAIN_STEPS = 500
run = 0

self_play_games = 0
train_steps = 0
greed_index = 0
next_train_step = config.episodes_per_train
hidden_state_shape_list = list(config.hidden_state_shape)
hidden_state_shape_list.insert(0, 1)
hidden_state_shape_tuple = tuple(hidden_state_shape_list)

envs = [gym.make(GYM_ENVIRONMENT) for _ in range(config.num_environments)]
states = [[e.reset()] for e in envs]
num_past_states_list = [config.num_past_states for _ in range(config.num_environments)]
games = [Game() for _ in range(config.num_environments)]

print("Training on:", config.train_device)
print("Self-play on:", config.self_play_device)
print("Number of concurrent environments:", config.num_environments)
print("MCTS Depth:", config.tree_depth)
print("Self-play games per training cycle:", config.episodes_per_train)
print("Greedy steps to test network:", config.greedy_steps)
print("Batches per train step:", config.batches_per_train_episode)

if __name__ == '__main__':
    for conf in configs:
        print("Setting new config")
        config = conf
        reset_globals()
        set_configs(conf)
        n = Networks()
        while train_steps < TRAIN_STEPS:
            main_loop()
        greedy_run_score_list.append(greedy_run_average_scores)
        print(greedy_run_score_list)
