class CartPoleConfig:
    def __init__(self, l2_reg=1e-22, envs=16, greedy_steps=20, episodes_per_train=60, max_games=60, tree_depth=12,
                 noise_alpha=0.25, discount=1.0, learning_rate=0.0001, num_unroll_steps=5, bootstrap_steps=5,
                 num_past_states=1, hidden_state_shape=(7, 7), hidden_state_neurons=49, batches_per_train_episode=1,
                 train_device="/device:GPU:0", self_play_device="/device:CPU:0", policy_loss_fn='mse',
                 value_vector_size=500, default_node_count=64):
        # state spaces
        self.num_past_states = num_past_states              # number of past states to feed into represenation network
        self.action_space_size = 2                          # number of possible actions
        self.num_actions = 2                                # @todo redundant, need to remove
        self.state_space_size = (4, num_past_states)        # size of representation network input
        self.hidden_state_shape = hidden_state_shape        # size of hidden state used by prediction network
        self.hidden_state_neurons = hidden_state_neurons    # number of hidden state neurons (should be same as hidden state shape dims)
        self.value_vector_size = value_vector_size
        self.l2_reg = l2_reg                                # amount of l2 regularisation in the networks
        self.policy_loss_fn = policy_loss_fn                # loss function to optimise the policy network
        self.default_node_count = default_node_count
        # tree search params
        self.tree_depth = tree_depth                        # depth of mcts tree search (number of leaves uncovered per action)
        self.noise_alpha = noise_alpha                      # level of exploration noise to add to the initial policy in the root of the tree
        self.discount = discount                            # value discount for temporal difference learning update
        # tensorflow devices
        self.train_device = train_device                    # @todo make these do something
        self.self_play_device = self_play_device
        # agent params
        self.num_environments = envs                        # number of environments for self-play to run in parallel (essentially number of actions)
        self.episodes_per_train = episodes_per_train        # number of episodes to experience per training cycle
        self.greedy_steps = greedy_steps                    # number of greedy (evaluation) steps to make per training cycle
        # network params
        self.batches_per_train_episode = batches_per_train_episode  # number of batches to train on per training cycle
        self.learning_rate = learning_rate
        self.num_unroll_steps = num_unroll_steps            # number of steps to unroll into the future to optimise dynamics net
        self.boostrap_steps = bootstrap_steps               # number of stpes to bootstrap to for temporal difference learning
        self.max_games = max_games                          # maximum number of experiences (self-play games) to store and sample from to optimise nets
