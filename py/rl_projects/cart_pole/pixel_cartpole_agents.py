import gym
import numpy as np
import tensorflow as tf
import keras.backend as K

GYM_ENVIRONMENT = "CartPole-v1"
DISCOUNT = 0.95
LR = 0.01


# a class for a deep Q learning agent
class DeepQAgent:

    def __init__(self, state_space, action_space):
        self.value_network = None
        self.experiences = []
        self.state_placeholder = None
        self.value_placeholder = None
        self.state_space = state_space
        self.action_space = action_space
        self.exploration_probability = 1
        self.exploration_decay = 0.995
        # create q network, tf session, initialise tf vars
        self.get_action, self.optimize = self.create_model()
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

    # creates the deep neural network Q function approximator
    def create_model(self):
        # define placeholders for input data (state) and training data (value)
        self.state_placeholder = tf.placeholder("float",
                                                [None, self.state_space[0], self.state_space[1], self.state_space[2]])
        self.value_placeholder = tf.placeholder("float", [None, self.action_space])
        # define neural network structure
        conv_layer_1 = tf.layers.conv2d(self.state_placeholder, filters=16, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_1 = tf.layers.max_pooling2d(conv_layer_1, pool_size=(4, 4), strides=(1, 1))
        conv_layer_2 = tf.layers.conv2d(pool_layer_1, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_2 = tf.layers.max_pooling2d(conv_layer_2, pool_size=(4, 4), strides=(1, 1))
        conv_layer_3 = tf.layers.conv2d(pool_layer_2, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_3 = tf.layers.max_pooling2d(conv_layer_3, pool_size=(2, 2), strides=(1, 1))
        conv_layer_4 = tf.layers.conv2d(pool_layer_3, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_4 = tf.layers.max_pooling2d(conv_layer_4, pool_size=(2, 2), strides=(1, 1))
        conv_layer_5 = tf.layers.conv2d(pool_layer_4, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_5 = tf.layers.max_pooling2d(conv_layer_5, pool_size=(2, 2), strides=(1, 1))
        flatten_layer = tf.layers.flatten(pool_layer_5)
        dense_layer_1 = tf.layers.dense(flatten_layer, 32, tf.nn.relu)
        output_layer = tf.layers.dense(dense_layer_1, self.action_space, None)

        # define loss function and optimiser
        mse = (output_layer - self.value_placeholder) ** 2
        opt = tf.train.AdamOptimizer(LR).minimize(mse)
        return output_layer, opt

    # obtains either a random action, or the optimal action from the Q network
    def act(self, state):
        # get the output of the network evaluated on the current state
        network_output = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})
        # with a certain probability, take a random action
        if np.random.rand() < self.exploration_probability:
            return np.random.randint(0, self.action_space), network_output
        # else take a greedy action
        else:
            return np.argmax(network_output), network_output

    # obtains the optimal action from the Q network
    def act_greedy(self, state):
        network_output = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})
        return np.argmax(network_output), network_output

    # saves experience data
    def store(self, state, action, action_values, reward, next_state, terminal):
        self.experiences.append((state, action, action_values, reward, next_state, terminal))
        return 0

    # optimises the Q network based on experiences
    def learn(self):
        # get the number of experiences, then randomly permute over the experiences to remove temporal biases
        n_samples = len(self.experiences)
        train_order = np.random.permutation(np.arange(n_samples))
        for i in range(n_samples):
            # acquire the experience data
            state, action, action_values, reward, next_state, terminal = self.experiences[train_order[i]]
            # get the value update for network optimisation
            if terminal:
                value_update = reward
            else:
                value_update = (reward + DISCOUNT * np.amax(
                    self.sess.run(self.get_action, feed_dict={self.state_placeholder: next_state})))
            # get the network's predicted values (either from memory or online evaluation)
            # predicted_values = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})[0]
            predicted_values = action_values[0]
            predicted_values[action] = value_update
            predicted_values = np.reshape(predicted_values, (1, self.action_space))
            # optimise the network parameters
            self.sess.run(self.optimize,
                          feed_dict={self.state_placeholder: state, self.value_placeholder: predicted_values})
        # decay exploration probability and empty experience list
        self.exploration_probability *= self.exploration_decay
        self.experiences = []


# a class for a REINFORCE style policy gradient agent
class PolicyGradientAgent:

    def __init__(self, state_space, action_space):
        self.value_network = None
        self.experiences = []
        self.state_placeholder = None
        self.value_placeholder = None
        self.state_space = state_space
        self.action_space = action_space
        self.baseline = 12
        # create policy network, tf session, initialise tf vars
        self.get_action, self.optimize = self.create_model()
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

    # creates the deep neural network pi function approximator
    def create_model(self):
        # define placeholders for input data (state) and training data (policy_value)
        self.state_placeholder = tf.placeholder("float",
                                                [None, self.state_space[0], self.state_space[1], self.state_space[2]])
        self.value_placeholder = tf.placeholder("float", [None, self.action_space])
        # define neural network structure
        conv_layer_1 = tf.layers.conv2d(self.state_placeholder, filters=16, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_1 = tf.layers.max_pooling2d(conv_layer_1, pool_size=(4, 4), strides=(1, 1))
        conv_layer_2 = tf.layers.conv2d(pool_layer_1, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_2 = tf.layers.max_pooling2d(conv_layer_2, pool_size=(4, 4), strides=(1, 1))
        conv_layer_3 = tf.layers.conv2d(pool_layer_2, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_3 = tf.layers.max_pooling2d(conv_layer_3, pool_size=(2, 2), strides=(1, 1))
        conv_layer_4 = tf.layers.conv2d(pool_layer_3, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_4 = tf.layers.max_pooling2d(conv_layer_4, pool_size=(2, 2), strides=(1, 1))
        conv_layer_5 = tf.layers.conv2d(pool_layer_4, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_5 = tf.layers.max_pooling2d(conv_layer_5, pool_size=(2, 2), strides=(1, 1))
        flatten_layer = tf.layers.flatten(pool_layer_5)
        dense_layer_1 = tf.layers.dense(flatten_layer, 32, tf.nn.relu)
        output_layer = tf.layers.dense(dense_layer_1, self.action_space, tf.nn.softmax)
        # define loss function and optimiser
        policy_grad_loss = -self.value_placeholder * K.log(output_layer + 1e-9)
        opt = tf.train.AdamOptimizer(LR).minimize(policy_grad_loss)
        return output_layer, opt

    # obtains an action stochastically from pi(s)
    def act(self, state):
        network_output = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})[0]
        action = np.random.choice(np.arange(network_output.shape[0]), p=network_output)
        return action, network_output

    # obtains an action greedily from pi(s)
    def act_greedy(self, state):
        network_output = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})[0]
        action = np.argmax(network_output)
        return action, network_output

    # saves experience data
    def store(self, state, action, action_distribution, reward, next_state, terminal):
        self.experiences.append((state, action, action_distribution, reward, next_state, terminal))
        return 0

    # optimises the policy network based on experiences
    def learn(self):
        # get the number of experiences, then randomly permute over the experiences to remove temporal biases
        n_samples = len(self.experiences)
        train_order = np.random.permutation(np.arange(n_samples))
        for i in range(n_samples):
            # acquire the experience data
            state, action, action_distribution, reward, next_state, terminal = self.experiences[train_order[i]]
            # aqcuire the distribution for the action we actually took, multiply by { return - baseline }
            action_probabilities = action_distribution * 0
            action_probabilities[action] = action_distribution[action] * (n_samples - self.baseline)
            predicted_values = np.reshape(action_probabilities, (1, self.action_space))
            # optimise the network parameters
            self.sess.run(self.optimize,
                          feed_dict={self.state_placeholder: state, self.value_placeholder: predicted_values})
        # empty experience list
        self.experiences = []


# a class for an advantage actor-critic agent
class AdvantageActorCriticAgent:

    def __init__(self, state_space, action_space):
        self.value_network = None
        self.experiences = []
        self.state_placeholder = None
        self.value_placeholder = None
        self.policy_placeholder = None
        self.state_space = state_space
        self.action_space = action_space
        self.get_action, self.get_value, self.optimize = self.create_model()
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

    # creates the deep neural network pi and Q function approximator
    def create_model(self):
        self.state_placeholder = tf.placeholder("float",
                                                [None, self.state_space[0], self.state_space[1], self.state_space[2]])
        self.value_placeholder = tf.placeholder("float", [None, self.action_space])
        self.policy_placeholder = tf.placeholder("float", [None, self.action_space])
        # define neural network structure
        conv_layer_1 = tf.layers.conv2d(self.state_placeholder, filters=16, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_1 = tf.layers.max_pooling2d(conv_layer_1, pool_size=(2, 2), strides=(1, 1))
        conv_layer_2 = tf.layers.conv2d(pool_layer_1, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_2 = tf.layers.max_pooling2d(conv_layer_2, pool_size=(2, 2), strides=(1, 1))
        conv_layer_3 = tf.layers.conv2d(pool_layer_2, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_3 = tf.layers.max_pooling2d(conv_layer_3, pool_size=(2, 2), strides=(1, 1))
        conv_layer_4 = tf.layers.conv2d(pool_layer_3, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_4 = tf.layers.max_pooling2d(conv_layer_4, pool_size=(2, 2), strides=(1, 1))
        conv_layer_5 = tf.layers.conv2d(pool_layer_4, filters=32, kernel_size=(3, 3), activation=tf.nn.relu)
        pool_layer_5 = tf.layers.max_pooling2d(conv_layer_5, pool_size=(2, 2), strides=(1, 1))
        flatten_layer = tf.layers.flatten(pool_layer_5)
        dense_layer_1 = tf.layers.dense(flatten_layer, 32, tf.nn.relu)
        # policy sub-network
        policy_dense_1 = tf.layers.dense(dense_layer_1, 32, tf.nn.relu)
        policy_dense_2 = tf.layers.dense(policy_dense_1, 32, tf.nn.relu)
        policy_output_layer = tf.layers.dense(policy_dense_2, self.action_space, tf.nn.softmax)
        policy_loss = -self.policy_placeholder * K.log(policy_output_layer + 1e-9)
        # value sub-network
        value_dense_1 = tf.layers.dense(dense_layer_1, 32, tf.nn.relu)
        value_dense_2 = tf.layers.dense(value_dense_1, 32, tf.nn.relu)
        value_output_layer = tf.layers.dense(value_dense_2, self.action_space, None)
        value_loss = ((self.value_placeholder - value_output_layer) ** 2) / 10
        # define optimiser
        opt = tf.train.AdamOptimizer(LR).minimize(policy_loss + value_loss)
        return policy_output_layer, value_output_layer, opt

    # obtains an action stochastically from pi(s)
    def act(self, state):
        network_output = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})[0]
        action = np.random.choice(np.arange(network_output.shape[0]), p=network_output)
        return action, network_output

    # obtains an action greedily from pi(s)
    def act_greedy(self, state):
        network_output = self.sess.run(self.get_action, feed_dict={self.state_placeholder: state})[0]
        action = np.argmax(network_output)
        return action, network_output

    # saves experience data
    def store(self, state, action, action_distribution, reward, next_state, terminal):
        self.experiences.append((state, action, action_distribution, reward, next_state, terminal))
        return 0

    # optimises the policy network based on experiences
    def learn(self):
        # get the number of experiences, then randomly permute over the experiences to remove temporal biases
        n_samples = len(self.experiences)
        train_order = np.random.permutation(np.arange(n_samples))
        for i in range(n_samples):
            state, action, action_distribution, reward, next_state, terminal = self.experiences[train_order[i]]
            # get value of current state and next state
            value = self.sess.run(self.get_value, feed_dict={self.state_placeholder: state})
            next_state_value = self.sess.run(self.get_value, feed_dict={self.state_placeholder: next_state})
            # initialise target for policy and value
            target_policy = np.zeros((1, self.action_space))
            target_value = value.copy()
            # get targets to train network
            if terminal:
                target_policy[0][action] = reward - value[0][action]
                target_value[0][action] = reward
            else:
                target_policy[0][action] = (reward + DISCOUNT * next_state_value[0][action]) - value[0][action]
                target_value[0][action] = (reward + DISCOUNT * next_state_value[0][action])
            # optimise network parameters
            self.sess.run(self.optimize, feed_dict={self.state_placeholder: state, self.value_placeholder: target_value,
                                                    self.policy_placeholder: target_policy})
        # empty experience list
        self.experiences = []


def cartpole(chosen_agent):
    state_space_size = (400, 600, 3)
    action_space_size = 2
    agent = chosen_agent(state_space_size, action_space_size)
    env = gym.make(GYM_ENVIRONMENT)
    run = 0
    while True:
        if run > 100000:
            break
        run += 1
        env.reset()
        # state = np.reshape(state, (1, state_space_size))
        step = 0
        # Exploration run
        if run % 2:
            while True:
                step += 1
                # get state as rgb pixel array
                state = env.render(mode='rgb_array').reshape((1, 400, 600, 3))
                action, action_distribution = agent.act(state)
                state_next, reward, terminal, info = env.step(action)
                # get next state as rgb pixel array
                state_next = env.render(mode='rgb_array').reshape((1, 400, 600, 3))
                reward = reward if not terminal else -reward
                agent.store(state, action, action_distribution, reward, state_next, terminal)
                state = state_next
                if terminal:
                    print("Run: " + str(run) + ", score: " + str(step))
                    agent.learn()
                    break
        # Exploitation run
        else:
            while True:
                step += 1
                # get state as rgb pixel array
                state = env.render(mode='rgb_array').reshape((1, 400, 600, 3))
                action, action_distribution = agent.act_greedy(state)
                state_next, reward, terminal, info = env.step(action)
                # get next state as rgb pixel array
                state_next = env.render(mode='rgb_array').reshape((1, 400, 600, 3))
                reward = reward if not terminal else -reward
                agent.store(state, action, action_distribution, reward, state_next, terminal)
                state = state_next
                if terminal:
                    print("Run: " + str(run) + ", score: " + str(step))
                    # agent.learn()
                    break


if __name__ == "__main__":
    agent_to_test = AdvantageActorCriticAgent
    cartpole(agent_to_test)
