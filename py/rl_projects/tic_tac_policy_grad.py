import numpy as np
from keras.layers import Input, Dense, Flatten
from keras import Model
from keras.optimizers import adam
import keras.backend as K
np.set_printoptions(threshold=np.inf)

class RL_Agent:
    def __init__(self, p, sm):
        self.pi = p
        self.state_machine = sm

    def learn(self, epochs):
        self.state_machine.init_state()
        for i in range(epochs):
            episode_end = False
            while not episode_end:
                state = self.state_machine.get_state()
                action = self.pi.get_policy(state)
                next_state, episode_end = self.state_machine.update(action)

            self.pi.train(self.state_machine.actions, self.state_machine.visited_states, self.state_machine.rewards)
            self.state_machine.init_state()

    def play(self):
        game_end = False
        self.state_machine.init_state()
        while not game_end:
            state = self.state_machine.get_state()
            action = self.pi.get_policy(state)
            next_state, game_end = self.state_machine.update(action)

    def play_random(self, games):
        p1_score = 0
        for i in range(games):
            game_end = False
            self.state_machine.init_state()
            while not game_end:
                state = self.state_machine.get_state()
                if self.state_machine.markov_process.player == 0:
                    action = self.pi.get_policy(state)
                else:
                    action = np.random.rand(self.pi.batch_size, 3, 3)
                next_state, game_end = self.state_machine.update(action)
            for g in range(self.pi.batch_size):
                p1_score += self.state_machine.rewards[g, 0]

        p1_score /= self.pi.batch_size
        print("Scored", p1_score, "in", games, "games against random play")

    def compete(self, games, p2_net):
        p1_score = 0
        for i in range(games):
            game_end = False
            self.state_machine.init_state()
            while not game_end:
                state = self.state_machine.get_state()
                if self.state_machine.markov_process.player == 0:
                    action = self.pi.get_policy(state)
                else:
                    action = p2_net.get_policy(state)
                next_state, game_end = self.state_machine.update(action)
            for g in range(self.pi.batch_size):
                p1_score += self.state_machine.rewards[g, 0]
            print("Finished game", i)
        print("Scored", p1_score, "in", games*self.pi.batch_size, "games against second network")

    def save(self, id):
        self.pi.p_model.save_weights('./checkpoints/_' + str(id))
        self.pi.v_model.save_weights('./checkpoints/_' + str(id))


class State_Machine:

    def __init__(self, mdp):
        self.markov_process = mdp
        self.visited_states = []
        self.actions = []
        self.state = self.init_state()
        self.rewards = None
        self.end_state = False

    def init_state(self):
        state = self.markov_process.start_state()
        self.visited_states = []
        self.actions = []
        self.visited_states.append(state.copy())
        return state

    def get_state(self):
        return self.markov_process.get_state()

    def set_state(self, state, player):
        self.markov_process.state = state
        self.markov_process.player = player
        self.state = state

    def update(self, probabilities):
        self.state, self.rewards, self.end_state, action = self.markov_process.action(probabilities)
        self.actions.append(action)
        if not self.end_state:
            self.visited_states.append(self.state.copy())
        return self.state, self.end_state


class TicTacToe_MDP:
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.start_state()
        self.player = 0
        self.winning_states = self.init_winning_states()
        self.rewards = np.zeros([self.batch_size, 2])
        self.games_running = np.ones([self.batch_size])

    def start_state(self):
        self.rewards = np.zeros([self.batch_size, 2])
        self.games_running = np.ones([self.batch_size])
        self.state = np.zeros([int(self.batch_size), 3, 3])
        self.player = 0
        return self.state

    def get_legal_actions(self):
        return (self.state == 0).astype(np.int8)

    def get_random_legal_actions(self):
        legal_actions = self.get_legal_actions()
        for i in range(self.batch_size):
            temp_action = legal_actions[i].reshape(9)
            random_action = np.random.choice(np.arange(temp_action.shape[0]), p=temp_action/np.sum(temp_action))
            action_array = np.zeros(9)
            action_array[random_action] = 1
            legal_actions[i] = action_array.reshape((3,3))
        return legal_actions


    def init_winning_states(self):
        winning_mask = np.array(
            [
                [[1, 1, 1],
                 [0, 0, 0],
                 [0, 0, 0]],
                [[0, 0, 0],
                 [1, 1, 1],
                 [0, 0, 0]],
                [[0, 0, 0],
                 [0, 0, 0],
                 [1, 1, 1]],
                [[1, 0, 0],
                 [1, 0, 0],
                 [1, 0, 0]],
                [[0, 1, 0],
                 [0, 1, 0],
                 [0, 1, 0]],
                [[0, 0, 1],
                 [0, 0, 1],
                 [0, 0, 1]],
                [[1, 0, 0],
                 [0, 1, 0],
                 [0, 0, 1]],
                [[0, 0, 1],
                 [0, 1, 0],
                 [1, 0, 0]]
            ]
        )
        return winning_mask

    def episode_ended(self):
        if self.player == 0:
            examine_state = self.state.clip(0, 1)
        else:
            examine_state = self.state.clip(-1, 0
                                            )
        potential_wins = [examine_state[state] * self.winning_states for state in range(examine_state.shape[0])]
        potential_wins = [potential_wins[pw].reshape(8, 9) for pw in range(len(potential_wins))]
        sums = [np.sum(potential_wins[pw], 1) for pw in range(len(potential_wins))]
        game = -1

        for s in sums:
            game += 1
            for i in range(len(s)):
                if s[i] == 3:
                    # print("game", s, "p1 won")
                    if self.games_running[game] == 1:
                        self.rewards[game, 0] = 1
                        self.rewards[game, 1] = -1
                        self.games_running[game] = 0
                if s[i] == -3:
                    # print("game", s, "p2 won")
                    if self.games_running[game] == 1:
                        self.rewards[game, 0] = -1
                        self.rewards[game, 1] = 1
                        self.games_running[game] = 0

        game = -1
        for s in self.state:
            game += 1
            if np.count_nonzero(s) == 9:
                self.games_running[game] = 0

    def action(self, probabilities):
        legal_actions = self.get_legal_actions()
        action = ((legal_actions * 1e-22) + probabilities) * legal_actions
        flat_actions = action.reshape(action.shape[0], 9) * 0
        for i in range(action.shape[0]):
            probs = action[i].reshape([9])
            val = np.random.choice(np.arange(probs.size), p=probs/np.sum(probs))
            flat_actions[i] = np.zeros([9])
            flat_actions[i, val] = 1
            action[i] = flat_actions[i].reshape([3,3])
        if self.player == 1:
            action *= -1
        self.state += action
        self.player = (self.player + 1) % 2
        self.episode_ended()
        return self.state, self.rewards, not np.any(self.games_running), flat_actions

    def get_state(self):
        if self.player == 1:
            return self.state * -1
        return self.state


class Policy_Value_Network:

    def __init__(self, batch_size, input_state_size, action_space_size):
        self.p_model = None
        self.v_model = None
        self.batch_size = batch_size
        self.input_state_size = input_state_size
        self.action_count = action_space_size
        self.create_p_model()
        self.create_v_model()

    def custom_loss(self, target, pred):
        return -K.log(pred + 1e-22) * target

    def create_p_model(self):
        input_layer = Input(shape=self.input_state_size)
        dense_1 = Dense(units=32, activation='relu')(input_layer)
        dense_2 = Dense(units=32, activation='relu')(dense_1)
        flat = Flatten()(dense_2)
        dense_3 = Dense(units=self.action_count, activation='softmax')(flat)
        m = Model(input_layer, dense_3)
        self.p_model = m
        opt = adam(0.01)
        self.p_model.compile(opt, loss=self.custom_loss)

    def create_v_model(self):
        input_layer = Input(shape=self.input_state_size)
        dense_1 = Dense(units=32, activation='relu')(input_layer)
        dense_2 = Dense(units=32, activation='relu')(dense_1)
        flat = Flatten()(dense_2)
        dense_3 = Dense(units=1, activation='tanh')(flat)
        m = Model(input_layer, dense_3)
        self.v_model = m
        opt = adam(0.01)
        self.v_model.compile(opt, loss='mse')

    def get_policy(self, model_input):
        prediction = self.p_model.predict([model_input], batch_size=self.batch_size)
        return prediction.reshape([prediction.shape[0], 3, 3])

    def get_value(self, model_input):
        prediction = self.v_model.predict([model_input], batch_size=self.batch_size)
        return prediction.reshape([prediction.shape[0], 1])

    def train(self, actions, states, rewards):
        state_list = []
        action_list = []
        reward_list = []

        for i in range(len(states)):
            for b in range(self.batch_size):
                if not i % 2:
                    if not rewards[b, 0] == 0:
                        state_list.append(states[i][b])
                        action_list.append(actions[i][b])
                        reward_list.append(rewards[b, 0])
                else:
                    if not rewards[b, 1] == 0:
                        state_list.append(states[i][b] * -1)
                        action_list.append(actions[i][b] * -1)
                        reward_list.append(rewards[b, 1])

        if len(reward_list):
            # print("Training")
            self.p_model.fit([state_list], [[a*r for a, r in zip(action_list, reward_list)]], batch_size=self.batch_size, verbose=0)
            self.v_model.fit([state_list], [reward_list], batch_size=self.batch_size, verbose=0)


def tic_tac_toe_rl():
    batch_size = 100
    total_epochs = 100000
    batches_per_save = 10

    c = TicTacToe_MDP(batch_size)
    s = State_Machine(c)
    input_shape = (3, 3,)
    output_size = 9
    pv_net = Policy_Value_Network(batch_size, input_shape, output_size)

    pi = pv_net
    state_machine = s
    a = RL_Agent(pi, state_machine)

    for e in range(total_epochs):
        a.play_random(100)
        a.learn(batches_per_save)
        print("Completed batch", e)


if __name__ == '__main__':
    tic_tac_toe_rl()
