#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "record.h"

//#define OPCOUNT //uncomment  to turn on operation counting

int NoOfOperations;      				//ext int from main.c
int NumberOfNodes;					//ext int from main.c

Record *HeadNode;					//Globally accessible head


/************************************************************
MakeHead() : a function to make the first record (headnode)
for the linked list structure.
*************************************************************/

void MakeHead()					//Make Head of list
{
#ifdef OPCOUNT
NoOfOperations+=2;    
#endif
HeadNode = (Record*)malloc(sizeof(Record));		//allocate memory for headnode
HeadNode -> data = strdup("HN");			//create arbitrary headnode value
HeadNode -> freq = 0;
HeadNode -> letters[0] = 0;
HeadNode -> next = NULL;				//Next pointer initialised to NULL
#ifdef OPCOUNT
NoOfOperations+=5;    
#endif
}

/************************************************************
AddNode() : a function to take an existing record, and add
it to the linked list structure.

Parameters: NextNode - the node to add
	    WordName - the string of data for the word itself
************************************************************/

void AddNode(Record *NextNode, char *WordName)			//add next node 
{
NextNode = (Record*)malloc(sizeof(Record));				//allocate memory
NextNode -> data = strdup(WordName);					//put word name into record.data
NextNode -> freq = 1;
NextNode ->letters[0] = 0;

Record *LastRecord = HeadNode;				//start looking for end from the head

#ifdef OPCOUNT
NoOfOperations+=5;    
#endif

while (LastRecord->next != NULL)			//keep going along list until last entry is found
{
LastRecord = LastRecord -> next;			//go to next record
#ifdef OPCOUNT
NoOfOperations++;    
#endif			
}

LastRecord->next = NextNode;				//add entry
NextNode->next = NULL;					//add null pointer to the new record - signifying the end of the list
NumberOfNodes++;					//increment number of nodes

#ifdef OPCOUNT
NoOfOperations+=3;    
#endif

}


/************************************************************
CheckNode() : a function to check the current records in the
list against a string.

Parameter : WordName - the string to search for

Return    : Pointer to the record where the word was found,
            or to the headnode if the record was not found. 
************************************************************/

Record *TempNode; 					//create a temporary record
char * temp;						//create a temporary string pointer

Record *CheckNode(char *WordName)
{
Record *LastRecord = HeadNode;				//start looking from the headnode

#ifdef OPCOUNT
NoOfOperations+=3;    
#endif

 while((strcmp(WordName , (LastRecord -> data)) != 0) && (LastRecord -> next)!=NULL)
 {
 LastRecord = LastRecord -> next;			//while string not found, walk down the list untill the end
 #ifdef OPCOUNT
 NoOfOperations++;    
 #endif
 } 

if (((LastRecord->next)==NULL) && (strcmp(WordName, (LastRecord -> data))!=0) )
{
LastRecord=HeadNode;           				//if word not found, return headnode
#ifdef OPCOUNT
NoOfOperations++;    
#endif
}

if (LastRecord!=HeadNode)				//if word found, return a pointer to that record
{
#ifdef OPCOUNT
NoOfOperations+=2;    
#endif
}

return(LastRecord);

} 

/************************************************************
LinkList() : A function to create the linked list

Parameter : FileToSplit - the string grabbed from the file
************************************************************/

void *LinkList(unsigned char * FileToSplit) 
{

char * temp;								//create temporary string pointer
temp = strtok(FileToSplit, " ,.=-!_[]{}()	|/~#"); 		//start slicing the string
AddNode(TempNode, temp);						//add a node with the first token as input word

#ifdef OPCOUNT
NoOfOperations+=3;    
#endif
 
while (temp != NULL)							//untill the end of the string is reached by strtok
 {
 if ((strcmp("HN" , CheckNode(temp)->data))==0)				//if checknode does not find the word
  {		
  AddNode(TempNode, temp);						//add a new record for the word
  #ifdef OPCOUNT
  NoOfOperations++;    
  #endif
  }
 else 									//if checknode did find the word
  {
  (CheckNode(temp) -> freq)++;						//increment the frequency of that word's record
  #ifdef OPCOUNT
  NoOfOperations++;    
  #endif
  }
 temp = strtok(NULL, " ,.-=!_[]{}()	|/~#");				//take the next token from the string

 #ifdef OPCOUNT
 NoOfOperations++;    
 #endif
 }

((HeadNode -> next) -> freq)--;						//bugfix

#ifdef OPCOUNT
NoOfOperations++;    
#endif

}

/************************************************************
PrintNodes() : a function to print the data from the list 
************************************************************/

void PrintNodes()
{

Record *NextNode = HeadNode;						//start printing from the headnode

int i = 1;

#ifdef OPCOUNT
NoOfOperations+=4;    
#endif

for (i=1; i<NumberOfNodes+1; i++)					//print all records from start to finish
{
NextNode = NextNode -> next;						
printf("|%s: " , NextNode -> data);					//print in the format : |word   34|
printf("%d|  " , NextNode -> freq);					//                      |data freq|
#ifdef OPCOUNT
NoOfOperations+=3;    
#endif
if (i%5 == 0)								//every fith record print a new line
{
printf("\n\n");
#ifdef OPCOUNT
NoOfOperations++;    
#endif
}
}
printf("\n\n");

#ifdef OPCOUNT
NoOfOperations+=4;    
#endif

}

/************************************************************
ReOrderNodes() : A function to reorder the records based on 
		 the alphabetical order of their data attribute 
************************************************************/

ReOrderNodes()
{
int i;
int j;
Record *Record1;							//create some record pointers
Record *Record2;
Record *Record3;

Record *TempRecord = (Record*)malloc(sizeof(Record)); 			//create a temporary record to store values

#ifdef OPCOUNT
NoOfOperations+=6;    
#endif

for (i=1; i<(NumberOfNodes); i++)					//loop through thr records
 {
 Record1 = HeadNode;							//reset to headnode
 Record2 = Record1->next;
 Record3 = Record2->next;
 
 #ifdef OPCOUNT
 NoOfOperations+=3;    
 #endif
 
 for (j=1; j<(NumberOfNodes); j++)
  {
  if ( (Record2 -> order) > (Record3 -> order) )			// if alphabet data shows record2 and 3 should be swapped
   {   
   TempRecord->data = Record2 -> data;					//swap data, order, and frequency data between records
   TempRecord->order = Record2 -> order;
   TempRecord->freq = Record2 -> freq;
   Record2->data = Record3->data;
   Record2->order = Record3->order;
   Record2->freq = Record3->freq;
   Record3->data = TempRecord->data;
   Record3->order = TempRecord->order;
   Record3->freq = TempRecord->freq;
   #ifdef OPCOUNT
   NoOfOperations+=9;    
   #endif
   } 
  Record1 = Record1->next;						//walk down the list
  Record2 = Record1->next;
  Record3 = Record1->next->next;
  #ifdef OPCOUNT
  NoOfOperations+=3;    
  #endif
  }
 }
}

/************************************************************
Power()   : a function to raise a number to a +ve power 
Parameters: Operand, Power. Eg 2^3 - Op = 2, Pw = 3
Return    : a double with the value of the operation
************************************************************/

double Power(int Operand, int Power)
{
int k=0;
double result = Operand;                                    		//start value
#ifdef OPCOUNT
NoOfOperations+=2;    
#endif
for (k=0; k<(Power-1); k++)						//times operand by itself for power times
 {
 result = result*Operand;
 #ifdef OPCOUNT
 NoOfOperations++;    
 #endif
 }
if (Power == 0)								//if power = 0 return 1
 {
 result = 1;
 #ifdef OPCOUNT
 NoOfOperations++;    
 #endif
 }
return(result);
}

/************************************************************
FindOrders() : a function to look at the alphabetical data of
	       a string and assign it a numerical value based
	       on the alphabetical order

The function works by saving the input sting's character 
values into an array e.g ok = 111 107, it then takes the 
first value (111) and adds a value based on the next letter
to it, the value added for the next letter is never high
enough to increase the first number, but it differentiates
between similar words. 

I wrote this function before I knew strcmp would do this in 
a simpler way, but after deliberation, I am decidedly too
proud to get rid of my function, even though it is probably 
slower.
************************************************************/

void FindOrders()					
{
Record *NextNode = HeadNode;						//start at headnode
int i;
int j;
double modifier;							
double counter;
int ascii=0;

#ifdef OPCOUNT
NoOfOperations+=6;    
#endif

for (i=0; i<NumberOfNodes-1; i++)					//walk through nodes
 {
 NextNode = NextNode -> next; 
 #ifdef OPCOUNT
 NoOfOperations++;    
 #endif
 for (j=0; j<strlen(NextNode -> data); j++)				//for the length of the string
  {
  ascii = ((NextNode -> data)[j]);					//save the ascii value into the letters[] array
  (NextNode -> letters)[j] = ascii;					
  #ifdef OPCOUNT
  NoOfOperations+=2;    
  #endif
  }
  for (j=0; j<strlen(NextNode -> data); j++)				//for the length of the string
  {
  counter = (NextNode -> letters)[j];					//set counter to the ascii value of the jth letter of the word
  modifier = (counter/(Power(125,j)));					//modify counter based on the value of j (which char in the word)
  (NextNode -> order) = (NextNode -> order) + modifier; 		//add this to the previous modifyer value
  #ifdef OPCOUNT
  NoOfOperations+=3;    
  #endif 
  }

 }
}
