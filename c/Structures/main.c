/************************************************************************************************
				WORDCOUNTER by 200450169 (6/1/16)

			A program to count the words in a user input file.

		The program compiles to the name WC if the command make is used.

************************************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#define OPCOUNT 						//uncomment  to turn on operation counting

time_t t0;							//initialise time variables
time_t t1;
time_t tt;

extern int NoOfOperations; 					//global variable to count operations
extern int Order;						//whether or not to order output
extern int NumberOfNodes;					//number of unique word records
extern int error;  						//error tracker
int error=0;							//set error to 0

int main(int argc, char *argv[])
{


t0 = time(NULL);						//get begin time (s)	

if (argc<3)							//if insufficient input arguments
{
printf("\nPlease specify -a (array) or -l (list) after filename\n\n");
printf("Additionally -o can be added to order the output alphabetically.\n\n");
printf("Example Input: ./WC afile -a -o.\n\n");
error=1;

#ifdef OPCOUNT
NoOfOperations+=3;    
#endif
}

if (argc>3)							//if user uses -o tag
 {
  if (strcmp(argv[3],"-o")==0)
  {
  Order=1;							//set order variable to 1
  #ifdef OPCOUNT
  NoOfOperations+=3;    
  #endif
  } 
 }

if (error==0)							//if no error
{

if ((strcmp(argv[2],"-a")==0) && (argc >2))			//if user specifies array structure
{
printf("\nUsing array:\n");
MakeArray(filereader(argv[1]));					//call makearray function with the output of filereader and filename
printf("\n\n");
#ifdef OPCOUNT
NoOfOperations+=4;    
#endif
}

if ((strcmp(argv[2],"-l")==0) && (argc >2))			//if user specifies list structure
{
printf("\nUsing Linked List:\n");
MakeHead();							//makes headnode
LinkList(filereader(argv[1]));					//creates list records
#ifdef OPCOUNT
NoOfOperations+=4;    
#endif 
 if (argc > 3)
  {
  if (Order)  
   {
   printf("Ordering...\n\n");					//if user wishes to order list
   FindOrders();						//get order data
   ReOrderNodes();						//reallocate record data
   #ifdef OPCOUNT
   NoOfOperations+=5;    
   #endif
   }
  } 
PrintNodes();							//print the record data
#ifdef OPCOUNT
NoOfOperations++;    
#endif
}

t1 = time(NULL);						// take end time
tt = t1 - t0;							//find difference between t0 and t1
printf("%d Total Unique Words\n" , NumberOfNodes);		//print total nodes
printf("Start: %d\n"  , t0);					//print start, end, and elapsed time
printf("End: %d\n"  , t1);
printf("Time Elapsed: %ds\n\n"  , tt);
if (NoOfOperations!=0)						//if opcount enabled, print the total operations
{
printf("Operation Count: %d\n", NoOfOperations);
}

if (NoOfOperations == 0)					//if opcount disabled, show instructions
 {
 printf("Uncomment 'define OPCOUNT' in compatible files to count operations.\n");
 printf("Compatible files: main.c, filereader.c, makenodes.c and makearray.c.\n");
 printf("Their results will be summed and the total displayed.\n\n");
 } 	

#ifdef OPCOUNT
NoOfOperations+=10;    
#endif

}    


return(0);



}
