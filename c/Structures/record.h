typedef struct record      			//structure of node in linked list
{
struct record *next;				//pointer to next node
int freq;					//frequency of word
int letters[250];				//array for storing ascii data of each character in the word (only used for ordering)
double order;					//variable for storing alphabetical order data
char *data;					//the words itself
} Record;

typedef struct WordStructure			//structure of word in dynamic array
{
char *data;					//the word
int freq;					//its frequency
}Word;

