#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#define OPCOUNT //uncomment  to turn on operation counting


int NoOfOperations;							//extern int from main
int OpCount;								//extern int from main
int error;								//extern int from main


/****************************************************************
filereader() : a function to read from an input file and save the
	       result into a string

parameters   : char *argv1 - second input argment from the user
   	       (the filename to open)

return       : returns a pointer to the string filereader
*****************************************************************/

unsigned char *filereader(char *argv1)
{
unsigned char *filereader;						//create string pointer and temp string pointer
unsigned char *temp;
int i=0;
int sizeoffile;								//create int for size of file 

#ifdef OPCOUNT
NoOfOperations+=3;    
#endif

FILE *UserFile;								//open user file
UserFile = fopen(argv1, "r");


#ifdef OPCOUNT
NoOfOperations+=2;    
#endif

if (UserFile == NULL)							//error message if unsuccesfully opened
 {
 printf("\nInvalid file specified, please enter the full filename and extension.\n\n");
 error=1;
 exit(1); 
 #ifdef OPCOUNT
 NoOfOperations+=3;    
 #endif
 }


fseek(UserFile,0,SEEK_END);						//get size of string within file
sizeoffile = (ftell(UserFile));						//save into sizeoffile int
fseek(UserFile,0,SEEK_SET);


#ifdef OPCOUNT
NoOfOperations+=3;    
#endif

filereader = (unsigned char*) malloc(sizeoffile);			//allocate memory for the characters 
fread(filereader, (sizeoffile), 1, UserFile);				//copy the data from the file to the string
//strncat(filereader, " ",1);

#ifdef OPCOUNT
NoOfOperations+=3;    
#endif

for (i=0; i<strlen(filereader); i++)					//string preprocessing
 {
 #ifdef OPCOUNT
 NoOfOperations++;    
 #endif
 if ((filereader[i]>64) && (filereader[i]<91))				//covert all letters to lower case
  {
  filereader[i] = (filereader[i] + 32);
  #ifdef OPCOUNT
  NoOfOperations+=2;    
  #endif
  }
  if ((ispunct(filereader[i]) && filereader[i]!= 39) || (filereader[i] >-1) && (filereader[i] < 32) || (filereader[i] > 122) || (filereader[i]>47 && filereader[i] <58))							//convert all punctuation to whitespace
  { 									//exception for the ' character (39)
  filereader[i] = 32;
  #ifdef OPCOUNT
  NoOfOperations+=6;    
  #endif
  } 
  if (filereader[i] == 39) 						//if char == ' (39)
   {i--;
   if (filereader[i] == 32)
    {
    i++;  								//check char behind, if == to a space (32), overwrite the ' with a space
    filereader[i] = 32;   
    }
   i+=2;
   if ((filereader[i]) ==32)						//check char behind, if == to a space (32), overwrite the ' with a space
    {
    i--;
    filereader[i] = 32;   						//if neither the char before or after the ' (39) character is a space
    }									//assume the ' is part of a word
  }
 }

return(filereader);							//return pointer to the string created

}
