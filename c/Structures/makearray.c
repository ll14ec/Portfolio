#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "record.h"

int NumberOfNodes;							//ext int from main
int Order;								//ext int from main	
int NoOfOperations;							//ext int from main	

//#define OPCOUNT //comment out to turn on operation counting

Word aword; 								// create single word record, and a pointer to an array of word records
Word *words;


/********************************************************************
MakeArray() : Makes a dynamic array to store records of all words 
	      found within a string then print the data. If a word
	      is found that already has a record, the frequency of
	      the word is incremented. The words, and frequency data
	      are then printed to the terminal.

Parameter   : unsigned char *FileToSplit - the string that is obtained
	      from the user file (output by filereader.c)
*********************************************************************/

void MakeArray(unsigned char * FileToSplit)
{
char * temp;								//initialise temp pointer to string
int i=0;								//counter reset/init
int j=0;
int foundindex;								//array entry where the word was found
int wordfound;								//non zero if the word already has a record

temp = strtok(FileToSplit, " ,.=-!_[]{}()	|/~#");			//take first token from string, save to temp
words =(Word*) malloc((NumberOfNodes+1)*sizeof(Word));			//allocate space for the first word record

#ifdef OPCOUNT
NoOfOperations+=7;    
#endif

while (temp != NULL)							//keep chopping till the end of the word
 {
 wordfound = 0;								//reset wordfound
 #ifdef OPCOUNT
 NoOfOperations+=2;    
 #endif
 for (j=0; j<NumberOfNodes; j++)					//check string token against all existing records
  {
  #ifdef OPCOUNT
  NoOfOperations++;    
  #endif
  if (strcmp(temp,words[j].data)==0)					//if the string token == the data value for a record
   {
   wordfound=1;								//wordfound variable set to non zero
   foundindex = j;							//save the position of the found word (same as loop iteration)
   #ifdef OPCOUNT
   NoOfOperations+=3;    
   #endif
   }
  }
 if (wordfound == 1)							//if word was found above
  {
  words[foundindex].freq++;						//increase the frequency of the word at array index = foundindex
  #ifdef OPCOUNT
  NoOfOperations+=1;    
  #endif
  }
 else 
  {
  aword.data = temp;							//save the word data from strtok (temp) into the record aword
  words[i] = aword;							//save the word data into words[i].data
  words[i].freq = 1;							//set frequency of new word to 1
  NumberOfNodes++;							//increment the numberofnodes variable
  i++;									//increment i;
  #ifdef OPCOUNT
  NoOfOperations+=5;    
  #endif
  }
 temp = strtok(NULL, " ,.=-!_[]{}()	|/~#");				//take next token
 words = realloc(words, (NumberOfNodes+1)*sizeof(Word));		//reallocate memory for the array to accomodate the next word
 #ifdef OPCOUNT
 NoOfOperations+=2;    
 #endif
 }

if (Order)								//if the user wants the output to be ordered alphabetically
 {
printf("Ordering...\n\n");						//show order was succesfully selected with -o
#ifdef OPCOUNT
NoOfOperations++;    
#endif
 for (j=0; j<(NumberOfNodes-1); j++)					//loop over the nodes from 1->NumberOfNodes for NumberOfNodes iterations
  {
  #ifdef OPCOUNT
  NoOfOperations++;    
  #endif
  for (i=0; i<(NumberOfNodes-1); i++)
   {
   #ifdef OPCOUNT
   NoOfOperations++;    
   #endif
   if (( strcmp(words[i].data, words[(i+1)].data) > 0 ))		//strcmp shows alphabetical relationship between words
    {   								// >0 if words need to be swapped
    aword = words[i];							//swap the contents of words[i] with words[i+1]
    words[i] = words [(i+1)];
    words[(i+1)] = aword;
    #ifdef OPCOUNT
    NoOfOperations+=4;    
    #endif
    } 
   }
  }
 }


#ifdef OPCOUNT
NoOfOperations++;    
#endif

for (i=1; i<(NumberOfNodes+1); i++)					// THIS PART PRINTS ALL THE RECORDS
 {
 printf("|%s: ", words[i-1].data);					//in the format |word 1|
 printf("%d|  ", words[i-1].freq);
 #ifdef OPCOUNT
 NoOfOperations+=3;    
 #endif
 if (i%5 == 0)								//for every 5th record printed, print a new line
  {
  printf("\n\n");
   #ifdef OPCOUNT
   NoOfOperations+=2;    
   #endif
  }
 }

return;
}


