/*
________________________________________________________________

A PROGRAM TO PRINT USER INPUTS FROM THE KEYBOARD TO THE TERMINAL
________________________________________________________________

The output for each user input is in the form of an arrangement
of 7*5 asterisks in the terminal window. 

The program will terminate when the user inputs only the ENTER
key, displaying the total number of characters processed - i.e.
the total number characters printed to the terminal (including 
spaces and punctuation).

The program is works with the range of ascii characters 33-127.
________________________________________________________________


*/

#include <stdio.h>      //include standard c library
#include <string.h>	//include string manipulation library


char userin[1000];      // creates a string for user input

int ascii;              // declares an integer, ascii

char starline[64][9];	// declares a 2D array of strings for all star combinations/bold 				
			// star combinations (2^5=32)*2=64

char *r1[127];		// creates pointers for star combinations for row 1
char *r2[127];		//         "                   "              row 2
char *r3[127];		// 	   "                   "              row 3
char *r4[127];		// 	   "                   "              row 4
char *r5[127];		// 	   "                   "              row 5
char *r6[127];		// 	   "                   "              row 6
char *r7[127];		// 	   "                   "              row 7

int totalchars;		// creates a variable to track number of characters processed

void getinput()  

/* a function to read user input from the keyboard and save into the string 'userin'*/
{
    
    fgets(userin, sizeof(userin), stdin);   
}


void starinit()



/* A function to assign all combinations of 1x5 asterisks into values starline[1] = "     " to starline[32] = "*****".*/

{
    
    strcpy(starline[1] ,"       ");
    strcpy(starline[2] ,"    *  ");
    strcpy(starline[3] ,"   *   ");
    strcpy(starline[4] ,"   **  ");
    strcpy(starline[5] ,"  *    ");
    strcpy(starline[6] ,"  * *  ");
    strcpy(starline[7] ,"  **   ");
    strcpy(starline[8] ,"  ***  ");
    strcpy(starline[9] ," *     ");
    strcpy(starline[10]," *  *  ");
    strcpy(starline[11]," * *   ");
    strcpy(starline[12]," * **  ");
    strcpy(starline[13]," **    ");
    strcpy(starline[14]," ** *  ");
    strcpy(starline[15]," ***   ");
    strcpy(starline[16]," ****  ");
    strcpy(starline[17],"*      ");
    strcpy(starline[18],"*   *  ");
    strcpy(starline[19],"*  *   ");
    strcpy(starline[20],"*  **  ");
    strcpy(starline[21],"* *    ");
    strcpy(starline[22],"* * *  ");
    strcpy(starline[23],"* **   ");
    strcpy(starline[24],"* ***  ");
    strcpy(starline[25],"**     ");
    strcpy(starline[26],"**  *  ");
    strcpy(starline[27],"** *   ");
    strcpy(starline[28],"** **  ");
    strcpy(starline[29],"***    ");
    strcpy(starline[30],"*** *  ");
    strcpy(starline[31],"****   ");
    strcpy(starline[32],"*****  ");

    
}

void pointerinit(int n)

{
    
    /*a function to point rows 1-6 of characters (with corresponding ascii numbering) to the correct star combinations*/
   
    /*assign all row pointers that use starline[1] */
    
r1[9]= r1[32]= r1[35]= r1[36]= r1[37]= r1[38]= r1[42]= r1[43]= r1[44]= r1[45]= r1[46]= r1[47]= r1[58]= r1[59]= r1[61]= r1[92]= r1[95]= r1[97]= r1[99]= r1[101]= r1[103]= r1[105]= r1[106]= r1[109]= r1[110]= r1[111]= r1[112]= r1[113]= r1[114]= r1[115]= r1[117]= r1[118]= r1[119]= r1[120]= r1[121]= r1[122]= r1[126]= r2[9]= r2[32]= r2[35]= r2[37]= r2[38]= r2[43]= r2[44]= r2[45]= r2[46]= r2[58]= r2[59]= r2[61]= r2[95]= r2[97]= r2[99]= r2[101]= r2[103]= r2[109]= r2[110]= r2[111]= r2[112]= r2[113]= r2[114]= r2[115]= r2[117]= r2[118]= r2[119]= r2[120]= r2[121]= r2[122]= r2[126]= r3[9]= r3[32]= r3[34]= r3[35]= r3[39]= r3[44]= r3[45]= r3[46]= r3[58]= r3[59]= r3[61]= r3[94]= r3[95]= r3[96]= r3[97]= r3[99]= r3[105]= r3[106]= r3[109]= r3[110]= r3[111]= r3[114]= r3[117]= r3[118]= r3[119]= r3[122]= r3[126]= r4[9]= r4[32]= r4[34] = r4[39]= r4[44]= r5[45]= r4[46]= r4[94]= r4[95]= r4[96]= r5[9]= r5[32]= r5[34]= r5[39]= r5[44]= r5[46]= r5[58]= r5[59]= r5[61]= r5[94]= r5[95]= r5[96]= r6[9]= r6[32]= r6[33]= r6[34]= r6[39]= r6[43]= r6[45]= r6[46]= r6[63]= r6[94]= r6[95]= r6[96]= r6[126]= r7[9]= r7[32]= r7[34]= r7[36]= r7[39]= r7[42]= r7[43]= r7[45]= r7[47]= r7[58]= r7[61]= r7[92]= r7[94]= r7[96]= r7[126]= starline[(1+n)];
    
 /*assign all row pointers that use starline[2] */
    
r1[40]= r1[60]= r1[100]= r2[47]= r2[51]= r2[100]=  r3[47]= r3[51]= r3[63]= r3[100]= r4[53]= r5[44]= r5[51]= r5[53]= r5[57]= r5[83]= r6[44]= r6[51]= r6[53]= r6[57]= r6[83]= r6[92]= r6[103]= r7[40]= r7[60]= starline[(2+n)];
    
 /*assign all row pointers that use starline[3] */
r2[40]= r2[55]=  r2[60]= r2[90]= r3[40]= r3[47]= r3[50]= r3[55]= r3[123]= r4[37]= r4[40]= r4[37]= r4[62]= r5[40]= r5[55]= r5[92]= r5[123]= r6[40]= r6[52]= r6[55]= r6[60]= r6[113]= r6[115]= r7[44]= r7[52]= r7[55]= starline[(3+n)];

 /*assign all row pointers that use starline[4] */
r1[123]=r7[113]= r7[123]= starline[(4+n)];

 /*assign all row pointers that use starline[5] */
r1[33]= r1[39]= r1[49]= r1[94]= r1[116]= r1[124]= r2[33]= r2[36]= r2[39]= r2[41]= r2[42]= r2[73]= r2[74]= r2[84]= r2[105]= r2[106]= r2[116]= r2[123]= r2[124]= r2[125]= r3[33]= r3[41]=r3[43]= r3[60]= r3[62]= r3[73]= r3[74]= r3[84]= r3[90]= r3[116]= r3[124]= r4[33]= r4[41]= r4[47]= r4[49]=r4[50]= r4[73]= r4[74]= r4[84]= r4[88]= r4[89]= r4[92]= r4[105]= r4[106]= r4[124]= r5[33]= r5[37]= r5[41]= r5[43]= r5[49]= r5[60]= r5[62] = r5[63]= r5[73]= r5[74]= r5[84]= r5[89]= r5[105]= r5[106]= r5[116]= r5[120]= r5[121]= r5[122]= r5[124]= r6[36]= r6[41]= r6[42]= r6[49]= r6[73]= r6[74]= r6[84]= r6[89]= r6[105]= r6[106]= r6[116]= r6[121]= r6[123]= r6[124]= r6[125]= r7[33]= r7[63]= r7[84]= r7[86]= r7[89]= r7[118]= r7[121]= r7[124]= starline[(5+n)];
    
 /*assign all row pointers that use starline[7] */
r1[34]= r1[102]= r2[34]= r4[63]= r4[123]= r7[116] = starline[(7+n)];

 /*assign all row pointers that use starline[8] */   
r1[67]= r1[71]= r7[67]= r7[71]= starline[(8+n)];

 /*assign all row pointers that use starline[9] */
r1[41]= r1[108]= r2[62]= r2[67]=  r2[71]=  r2[93]=  r2[96]=  r2[102]= r2[108]=   r3[92]= r3[93]= r3[102]= r3[108]= r3[125]= r4[59]= r4[60]= r4[93]= r4[108]=  r5[47]= r5[50]= r5[90]= r5[93]= r5[102]= r5[108]= r5[125] =r6[37]= r6[59]= r6[62]= r6[67]= r6[93]= r6[102]= r6[108]= r6[122]=  r7[41] = r7[102] = starline[(9+n)];
    
 /*assign all row pointers that use starline[10] */
r6[71]= starline[(10+n)];

 /*assign all row pointers that use starline[11] */    
r2[94]= r3[88]= r3[89]= r4[35]= r4[120]= r4[121]= r5[88]= r6[86]= r6[118]= r6[120]= r7[87]= r7[119]=  starline[(11+n)];

 /*assign all row pointers that use starline[13] */   
r1[81]= r2[49]= r3[38]= r4[90]= r4[111]= r4[125]=  r5[115]= r7[111]= r7[117] = starline[(13+n)];

 /*assign all row pointers that use starline[14] */    
r5[38]= r7[38]= r7[81]= r7[97]= starline[(14+n)];
  

 /*assign all row pointers that use starline[15] */    
r1[48]= r1[50]= r1[56]= r1[57]= r1[63]= r1[64]= r1[65]= r1[79]= r3[36]= r3[101]= r3[103]= r3[113]= r3[115]= r4[42]= r4[45]= r4[51]= r4[56]= r4[83]= r4[97]= r4[99]= r4[114]= r4[116]=  r5[36]= r5[113]= r7[48]= r7[54]= r7[56]= r7[64]= r7[79]= r7[85]= r7[99]= r7[105]=  starline[(15+n)];
    
 /*assign all row pointers that use starline[16] */    
r1[54]= r1[83]= r4[55]= r4[57]= r4[100]= r5[103]= r7[100]= r7[101] = starline[(16+n)];

 /*assign all row pointers that use starline[17] */    
r1[52]= r1[62]= r1[76]= r1[96]= r1[98]= r1[104]= r1[107]= r2[52]= r2[53]= r2[54]= r2[69]= r2[70]= r2[76]= r2[83]= r2[91]= r2[92]= r2[98]= r2[104]= r2[107]= r3[67]= r3[69]= r3[70]= r3[71]= r3[76]= r3[83]= r3[91]= r3[98]= r3[104]= r4[36]= r4[38]= r4[58]= r4[67]= r4[76]= r4[91]= r4[115]= r5[67]= r5[69]= r5[70]= r5[76]= r5[80]= r5[91]= r5[99]= r5[114]= r6[47]= r6[50]= r6[58]= r6[64]= r6[69]= r6[70]= r6[76]= r6[80]= r6[90]= r6[91]= r6[99]= r6[101]= r6[112]= r6[114]= r7[46]= r7[59]= r7[62]= r7[70]= r7[80]= r7[112]= r7[114]= starline[(17+n)];

 /*assign all row pointers that use starline[18] */    
r1[72]= r1[75]= r1[77]= r1[78]= r1[85]= r1[86]= r1[87]= r1[88]= r1[89]= r2[48]= r2[50]= r2[56]= r2[57]= r2[63]= r2[64]= r2[65]= r2[66]= r2[68]= r2[72]= r2[78]= r2[79]= r2[80]= r2[82]= r2[85]= r2[86]= r2[87]= r2[88]= r2[89]= r3[37]= r3[48]= r3[56]= r3[57]= r3[65]= r3[66]= r3[68]= r3[72]= r3[79]= r3[80]= r3[82]= r3[85]= r3[86]= r3[87]= r3[120]= r3[121]= r4[54]= r4[68]= r4[77]= r4[79]= r4[85]= r4[86]= r4[87]= r4[101]= r4[103]= r4[109]= r4[118]= r4[119]= r5[48]= r5[54]= r5[56]= r5[65]= r5[66]= r5[68]= r5[71]= r5[72]= r5[77]= r5[79]= r5[85]= r5[86]= r5[98]= r5[100]= r5[104]= r5[118]= r5[119]= r6[48]= r6[54]= r6[56]= r6[65]= r6[66]= r6[68]= r6[72]= r6[77]= r6[78]= r6[79]= r6[85]= r6[88]= r6[98]= r6[100]= r6[104]= r7[37]= r7[65]= r7[72]= r7[75]= r7[77]= r7[78]= r7[88]= r7[104]= r7[109]= r7[120]= starline[(18+n)];

 /*assign all row pointers that use starline[19] */    
r2[75]= r2[81]= r3[52]= r3[81]= r3[107]= r4[52]= r4[81]= r4[110]= r4[112]= r4[113]= r4[117]= r5[81]= r5[97]= r5[111]= r5[117]= r6[38]= r6[75]= r6[81]= r6[97]= r6[111]= r6[117]= r7[107]= r7[110]= starline[(19+n)];
    
 /*assign all row pointers that use starline[20] */
r4[71]= r5[78]= r7[82] = starline[(20+n)];

 /*assign all row pointers that use starline[21] */
r3[49]= r3[75]= r4[107]= r5[75]= r6[82]= r6[107]= r7[35]=  starline[(21+n)];
 
 /*assign all row pointers that use starline[22] */   
r3[42]= r3[77]= r4[48]= r4[64]= r4[78]= r5[42]= r5[87]= r6[87]= r6[109]= r6[119]= starline[(22+n)];

 /*assign all row pointers that use starline[23] */
r6[110] = starline[(23+n)];

 /*assign all row pointers that use starline[24] */    
r3[64]= r5[64]= r5[126]= starline[(24+n)];

 /*assign all row pointers that use starline[25] */    
r1[91]= r1[93]= r1[125]= r4[75]= r5[82]= r5[107]= r7[91]= r7[93]= r7[125] = starline[(25+n)];

 /*assign all row pointers that use starline[26] */    
r3[78] = starline[(26+n)];
    
 /*assign all row pointers that use starline[27] */
r5[110] = starline[(27+n)];
 
r2[77] = r5[109] = starline[(28+n)];

 /*assign all row pointers that use starline[28] */    
r3[112]= r4[69]= r4[70]= r4[102]= r5[112]= r7[106]= r7[108]= r7[115]= starline[(29+n)];
  
 /*assign all row pointers that use starline[30] */  
r4[126] = starline[(30+n)];

 /*assign all row pointers that use starline[31] */   
r1[51]= r1[55]= r1[66]= r1[68]= r1[80]= r1[82]= r3[53]= r3[54]= r4[66]= r4[80]= r4[82]= r4[98]= r4[104]= r4[122]= r5[35]= r5[101]= r6[35]= r7[51]= r7[53]= r7[57]= r7[66]= r7[68]= r7[74]= r7[76]= r7[83]= r7[98]= r7[103]= r7[122]= starline[(31+n)];

 /*assign all row pointers that use starline[32] */    
r1[53]= r1[69]= r1[70]= r1[73]= r1[74]= r1[84]= r1[90]= r4[43]= r4[61]= r4[65]= r4[72]= r5[52]= r6[61]= r7[49]= r7[50]= r7[69]= r7[73]= r7[90]= r7[95]=  starline[(32+n)];
    
}
void printlines(int i)

/* a function to print the characters of userin in stars, one row at a time using their ascii values */

{
    
    if (strcmp(userin, "\n")!=0) //if the user input is not only a carriage return
        
    {
        
        
        i=0;
        
	printf("\n");				//newline for formating

        for (i=0;i<(strlen(userin)-1); i++) 	//read characters one by one
						//stop reading at the end of the string
        {
            ascii = userin[i];		//assign the ascii value from the character - 
					//because ascii is an int and userin[i] is a char
					//if we assign the char to an int, C will assign
					//the ascii value of the character


            printf("%s", r1[ascii]);    //print the star combination for row one
					//according to the ascii value (ascii values
					//are reflected in the r array, eg r1[65]
					//would print row one of the stars to make
					//the ascii character 65 (capital A)
        }
        printf("\n"); 			//after all the first row of the user inuput
					//has printed, start printing row 2 on a new line
        
        
        i=0;
        
        for (i=0;i<(strlen(userin)-1); i++)	//exactly same functionality as above 
						//but for row 2
					    
        {
            ascii = userin[i];
            printf("%s", r2[ascii]);
        }
        printf("\n");
        
        i=0;
        for (i=0;i<(strlen(userin)-1); i++)	//exactly same functionality as above 
						//but for row 3
        {
            ascii = userin[i];
            printf("%s", r3[ascii]);
        }
        printf("\n");
        
        i=0;
        for (i=0;i<(strlen(userin)-1); i++)	//exactly same functionality as above 
						//but for row 4
        {
            ascii = userin[i];
            printf("%s", r4[ascii]);
        }
        printf("\n");
        
        i=0;
        for (i=0;i<(strlen(userin)-1); i++)	//exactly same functionality as above 
						//but for row 5
        {
            ascii = userin[i];
            printf("%s", r5[ascii]);
        }
        printf("\n");
        
        i=0;
        for (i=0;i<(strlen(userin)-1); i++)	//exactly same functionality as above 
						//but for row 6
        {
            ascii = userin[i];
            printf("%s", r6[ascii]);
        }
        printf("\n");
        
        
        i=0;
        for (i=0;i<(strlen(userin)-1); i++)	//exactly same functionality as above 
						//but for row 7
        {
            ascii = userin[i];
            printf("%s", r7[ascii]);
        }
        printf("\n");
    }
    
    
    
    
    
    
}


int main()				//begin program
{
    totalchars=0;			//set characters processed to 0

    starinit();				//assign strings of stars to star array

    pointerinit(0);			//set up pointers for letters
    
    do{					
       	printf("\nType text followed by ENTER to turn it into stars!\nThen press ENTER again when you are done :)\n\n");		//prompt user input	
        getinput(0);					//get user in
        totalchars= (strlen(userin)-1)+totalchars;	//update charaters processed  								//(-1 for null char)
        printlines(0);					//echo user input
        printf("\n");					/* new line to seperate outputs */
    }while (strcmp(userin, "\n")!=0);	//keep repeating above untill the user
					// enters only a carriage return
    

    printf("%d total characters processed.\n\n", totalchars);	//print number of processed chars (including spaces)
    
    return(0);				//end program
    
}






