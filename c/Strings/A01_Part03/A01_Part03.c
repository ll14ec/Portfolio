/*
_____________________________________________________________________________

	A PROGRAM TO PRINT USER INPUTS FROM THE KEYBOARD TO THE TERMINAL

WITH BOLD AND ITALIC FORMATTING, UPPER, LOWER AND FIRST LETTER CAPITALISATION
_____________________________________________________________________________

The program will display an output echo of what the user types in. There will
if an &B or &I tag is active, there will be bold and italic foratting on the 
text. The program removes any tags from the previous line of input for the next.
Multiple tags are supported, as are the characters &b and &i to turn off bold
and italic respectively.

The tags &L, &U and &C turn on lower, upper and first letter transformations 
respectively. &l, &u and &c can be used to turn them off. Multiple tags in the 
same line are supported, but of course only one can be active at any time.

The output for each user input is in the form of an arrangement of 7*5 asterisks
 in the terminal window. 

The program will terminate when the user inputs only the ENTER key, displaying
 the total number of characters processed - i.e. the total number characters 
printed to the terminal (including spaces and punctuation).

The program is works with the range of ascii characters 33-127.
______________________________________________________________________________


*/


#include <stdio.h>      //include standard c library
#include <string.h>	//include string manipulation library


char userin[1000];      // creates a string for user input

int ascii;              // declares an integer, ascii

char starline[64][9];	// declares a 2D array of strings for all star combinations/bold 				
			// star combinations (2^5=32)*2=64

char *r1[127];		// creates pointers for star combinations for row 1
char *r2[127];		//         "                   "              row 2
char *r3[127];		// 	   "                   "              row 3
char *r4[127];		// 	   "                   "              row 4
char *r5[127];		// 	   "                   "              row 5
char *r6[127];		// 	   "                   "              row 6
char *r7[127];		// 	   "                   "              row 7
int totalchars;		// creates a variable to track number of characters processed
const int bold=32;	//create the variable 'bold' = 32 (for +n transformation)
const int regular=0;	//create the variable 'regular' = 0 (also for +n)

void getinput()
{

fgets(userin, sizeof(userin), stdin);





}


void starinit() 

/* a function to assign all combinations of 1x5 asterisks into values starline[1] to starline[32], and to also assign their bold equivalents from 33-64*/

{

strcpy(starline[1] ,"       ");
strcpy(starline[2] ,"    *  ");
strcpy(starline[3] ,"   *   ");
strcpy(starline[4] ,"   **  ");
strcpy(starline[5] ,"  *    ");
strcpy(starline[6] ,"  * *  ");
strcpy(starline[7] ,"  **   ");
strcpy(starline[8] ,"  ***  ");
strcpy(starline[9] ," *     ");
strcpy(starline[10]," *  *  ");
strcpy(starline[11]," * *   ");
strcpy(starline[12]," * **  ");
strcpy(starline[13]," **    ");
strcpy(starline[14]," ** *  ");
strcpy(starline[15]," ***   ");
strcpy(starline[16]," ****  ");
strcpy(starline[17],"*      ");
strcpy(starline[18],"*   *  ");
strcpy(starline[19],"*  *   ");
strcpy(starline[20],"*  **  ");
strcpy(starline[21],"* *    ");
strcpy(starline[22],"* * *  ");
strcpy(starline[23],"* **   ");
strcpy(starline[24],"* ***  ");
strcpy(starline[25],"**     ");
strcpy(starline[26],"**  *  ");
strcpy(starline[27],"** *   ");
strcpy(starline[28],"** **  ");
strcpy(starline[29],"***    ");
strcpy(starline[30],"*** *  ");
strcpy(starline[31],"****   ");
strcpy(starline[32],"*****  ");
strcpy(starline[33] ,"       ");
strcpy(starline[34] ,"    ** ");
strcpy(starline[35] ,"   **  ");
strcpy(starline[36] ,"   *** ");
strcpy(starline[37] ,"  **   ");
strcpy(starline[38] ,"  **** ");
strcpy(starline[39] ,"  ***  ");
strcpy(starline[40] ,"  **** ");
strcpy(starline[41] ," **    ");
strcpy(starline[42]," ** ** ");
strcpy(starline[43]," ****  ");
strcpy(starline[44]," ***** ");
strcpy(starline[45]," ***   ");
strcpy(starline[46]," ***** ");
strcpy(starline[47]," ****  ");
strcpy(starline[48]," ***** ");
strcpy(starline[49],"**     ");
strcpy(starline[50],"**  ** ");
strcpy(starline[51],"** **  ");
strcpy(starline[52],"** *** ");
strcpy(starline[53],"****   ");
strcpy(starline[54],"*****  ");
strcpy(starline[55],"*****  ");
strcpy(starline[56],"*****  ");
strcpy(starline[57],"***    ");
strcpy(starline[58],"*** ** ");
strcpy(starline[59],"*****  ");
strcpy(starline[60],"****** ");
strcpy(starline[61],"****   ");
strcpy(starline[62],"****** ");
strcpy(starline[63],"*****  ");
strcpy(starline[64],"****** ");

}

void pointerinit(int n)

{

/*a function to point rows 1-6 of characters (with corresponding ascii numbering) to the correct star combinations*/

/* assign all row pointers that use starline[1] to position [1][0] of the string*/



r1[9]= r1[32]= r1[35]= r1[36]= r1[37]= r1[38]= r1[42]= r1[43]= r1[44]= r1[45]= r1[46]= r1[47]= r1[58]= r1[59]= r1[61]= r1[92]= r1[95]= r1[97]= r1[99]= r1[101]= r1[103]= r1[105]= r1[106]= r1[109]= r1[110]= r1[111]= r1[112]= r1[113]= r1[114]= r1[115]= r1[117]= r1[118]= r1[119]= r1[120]= r1[121]= r1[122]= r1[126]= r2[9]= r2[32]= r2[35]= r2[37]= r2[38]= r2[43]= r2[44]= r2[45]= r2[46]= r2[58]= r2[59]= r2[61]= r2[95]= r2[97]= r2[99]= r2[101]= r2[103]= r2[109]= r2[110]= r2[111]= r2[112]= r2[113]= r2[114]= r2[115]= r2[117]= r2[118]= r2[119]= r2[120]= r2[121]= r2[122]= r2[126]= r3[9]= r3[32]= r3[34]= r3[35]= r3[39]= r3[44]= r3[45]= r3[46]= r3[58]= r3[59]= r3[61]= r3[94]= r3[95]= r3[96]= r3[97]= r3[99]= r3[105]= r3[106]= r3[109]= r3[110]= r3[111]= r3[114]= r3[117]= r3[118]= r3[119]= r3[122]= r3[126]= r4[9]= r4[32]= r4[34] = r4[39]= r4[44]= r5[45]= r4[46]= r4[94]= r4[95]= r4[96]= r5[9]= r5[32]= r5[34]= r5[39]= r5[44]= r5[46]= r5[58]= r5[59]= r5[61]= r5[94]= r5[95]= r5[96]= r6[9]= r6[32]= r6[33]= r6[34]= r6[39]= r6[43]= r6[45]= r6[46]= r6[63]= r6[94]= r6[95]= r6[96]= r6[126]= r7[9]= r7[32]= r7[34]= r7[36]= r7[39]= r7[42]= r7[43]= r7[45]= r7[47]= r7[58]= r7[61]= r7[92]= r7[94]= r7[96]= r7[126]= starline[(1+n)];


r1[40]= r1[60]= r1[100]= r2[47]= r2[51]= r2[100]=  r3[47]= r3[51]= r3[63]= r3[100]= r4[53]= r5[44]= r5[51]= r5[53]= r5[57]= r5[83]= r6[44]= r6[51]= r6[53]= r6[57]= r6[83]= r6[92]= r6[103]= r7[40]= r7[60]= starline[(2+n)];


r2[40]= r2[55]=  r2[60]= r2[90]= r3[40]= r3[47]= r3[50]= r3[55]= r3[123]= r4[37]= r4[40]= r4[37]= r4[62]= r5[40]= r5[55]= r5[92]= r5[123]= r6[40]= r6[52]= r6[55]= r6[60]= r6[113]= r6[115]= r7[44]= r7[52]= r7[55]= starline[(3+n)];

r1[123]=r7[113]= r7[123]= starline[(4+n)];

r1[33]= r1[39]= r1[49]= r1[94]= r1[116]= r1[124]= r2[33]= r2[36]= r2[39]= r2[41]= r2[42]= r2[73]= r2[74]= r2[84]= r2[105]= r2[106]= r2[116]= r2[123]= r2[124]= r2[125]= r3[33]= r3[41]=r3[43]= r3[60]= r3[62]= r3[73]= r3[74]= r3[84]= r3[90]= r3[116]= r3[124]= r4[33]= r4[41]= r4[47]= r4[49]=r4[50]= r4[73]= r4[74]= r4[84]= r4[88]= r4[89]= r4[92]= r4[105]= r4[106]= r4[124]= r5[33]= r5[37]= r5[41]= r5[43]= r5[49]= r5[60]= r5[62] = r5[63]= r5[73]= r5[74]= r5[84]= r5[89]= r5[105]= r5[106]= r5[116]= r5[120]= r5[121]= r5[122]= r5[124]= r6[36]= r6[41]= r6[42]= r6[49]= r6[73]= r6[74]= r6[84]= r6[89]= r6[105]= r6[106]= r6[116]= r6[121]= r6[123]= r6[124]= r6[125]= r7[33]= r7[63]= r7[84]= r7[86]= r7[89]= r7[118]= r7[121]= r7[124]= starline[(5+n)];


r1[34]= r1[102]= r2[34]= r4[63]= r4[123]= r7[116] = starline[(7+n)];

r1[67]= r1[71]= r7[67]= r7[71]= starline[(8+n)];

r1[41]= r1[108]= r2[62]= r2[67]=  r2[71]=  r2[93]=  r2[96]=  r2[102]= r2[108]=   r3[92]= r3[93]= r3[102]= r3[108]= r3[125]= r4[59]= r4[60]= r4[93]= r4[108]=  r5[47]= r5[50]= r5[90]= r5[93]= r5[102]= r5[108]= r5[125] =r6[37]= r6[59]= r6[62]= r6[67]= r6[93]= r6[102]= r6[108]= r6[122]=  r7[41] = r7[102] = starline[(9+n)];

r6[71]=   starline[(10+n)];

r2[94]= r3[88]= r3[89]= r4[35]= r4[120]= r4[121]= r5[88]= r6[86]= r6[118]= r6[120]= r7[87]= r7[119]=  starline[(11+n)];

r1[81]= r2[49]= r3[38]= r4[90]= r4[111]= r4[125]=  r5[115]= r7[111]= r7[117] = starline[(13+n)];

r5[38]= r7[38]= r7[81]= r7[97]= starline[(14+n)];


r1[48]= r1[50]= r1[56]= r1[57]= r1[63]= r1[64]= r1[65]= r1[79]= r3[36]= r3[101]= r3[103]= r3[113]= r3[115]= r4[42]= r4[45]= r4[51]= r4[56]= r4[83]= r4[97]= r4[99]= r4[114]= r4[116]=  r5[36]= r5[113]= r7[48]= r7[54]= r7[56]= r7[64]= r7[79]= r7[85]= r7[99]= r7[105]=  starline[(15+n)];


r1[54]= r1[83]= r4[55]= r4[57]= r4[100]= r5[103]= r7[100]= r7[101] = starline[(16+n)];

r1[52]= r1[62]= r1[76]= r1[96]= r1[98]= r1[104]= r1[107]= r2[52]= r2[53]= r2[54]= r2[69]= r2[70]= r2[76]= r2[83]= r2[91]= r2[92]= r2[98]= r2[104]= r2[107]= r3[67]= r3[69]= r3[70]= r3[71]= r3[76]= r3[83]= r3[91]= r3[98]= r3[104]= r4[36]= r4[38]= r4[58]= r4[67]= r4[76]= r4[91]= r4[115]= r5[67]= r5[69]= r5[70]= r5[76]= r5[80]= r5[91]= r5[99]= r5[114]= r6[47]= r6[50]= r6[58]= r6[64]= r6[69]= r6[70]= r6[76]= r6[80]= r6[90]= r6[91]= r6[99]= r6[101]= r6[112]= r6[114]= r7[46]= r7[59]= r7[62]= r7[70]= r7[80]= r7[112]= r7[114]= starline[(17+n)];


r1[72]= r1[75]= r1[77]= r1[78]= r1[85]= r1[86]= r1[87]= r1[88]= r1[89]= r2[48]= r2[50]= r2[56]= r2[57]= r2[63]= r2[64]= r2[65]= r2[66]= r2[68]= r2[72]= r2[78]= r2[79]= r2[80]= r2[82]= r2[85]= r2[86]= r2[87]= r2[88]= r2[89]= r3[37]= r3[48]= r3[56]= r3[57]= r3[65]= r3[66]= r3[68]= r3[72]= r3[79]= r3[80]= r3[82]= r3[85]= r3[86]= r3[87]= r3[120]= r3[121]= r4[54]= r4[68]= r4[77]= r4[79]= r4[85]= r4[86]= r4[87]= r4[101]= r4[103]= r4[109]= r4[118]= r4[119]= r5[48]= r5[54]= r5[56]= r5[65]= r5[66]= r5[68]= r5[71]= r5[72]= r5[77]= r5[79]= r5[85]= r5[86]= r5[98]= r5[100]= r5[104]= r5[118]= r5[119]= r6[48]= r6[54]= r6[56]= r6[65]= r6[66]= r6[68]= r6[72]= r6[77]= r6[78]= r6[79]= r6[85]= r6[88]= r6[98]= r6[100]= r6[104]= r7[37]= r7[65]= r7[72]= r7[75]= r7[77]= r7[78]= r7[88]= r7[104]= r7[109]= r7[120]= starline[(18+n)];

r2[75]= r2[81]= r3[52]= r3[81]= r3[107]= r4[52]= r4[81]= r4[110]= r4[112]= r4[113]= r4[117]= r5[81]= r5[97]= r5[111]= r5[117]= r6[38]= r6[75]= r6[81]= r6[97]= r6[111]= r6[117]= r7[107]= r7[110]= starline[(19+n)];


r4[71]= r5[78]= r7[82] = starline[(20+n)];

r3[49]= r3[75]= r4[107]= r5[75]= r6[82]= r6[107]= r7[35]=  starline[(21+n)];

r3[42]= r3[77]= r4[48]= r4[64]= r4[78]= r5[42]= r5[87]= r6[87]= r6[109]= r6[119]= starline[(22+n)];

r6[110] = starline[(23+n)];

r3[64]= r5[64]= r5[126]= starline[(24+n)];

r1[91]= r1[93]= r1[125]= r4[75]= r5[82]= r5[107]= r7[91]= r7[93]= r7[125] = starline[(25+n)];

r3[78] = starline[(26+n)];

r5[110] = starline[(27+n)];

r2[77] = r5[109] = starline[(28+n)];

r3[112]= r4[69]= r4[70]= r4[102]= r5[112]= r7[106]= r7[108]= r7[115]= starline[(29+n)];

r4[126] = starline[(30+n)];

r1[51]= r1[55]= r1[66]= r1[68]= r1[80]= r1[82]= r3[53]= r3[54]= r4[66]= r4[80]= r4[82]= r4[98]= r4[104]= r4[122]= r5[35]= r5[101]= r6[35]= r7[51]= r7[53]= r7[57]= r7[66]= r7[68]= r7[74]= r7[76]= r7[83]= r7[98]= r7[103]= r7[122]= starline[(31+n)];

r1[53]= r1[69]= r1[70]= r1[73]= r1[74]= r1[84]= r1[90]= r4[43]= r4[61]= r4[65]= r4[72]= r5[52]= r6[61]= r7[49]= r7[50]= r7[69]= r7[73]= r7[90]= r7[95]=  starline[(32+n)];

}

void printlines(int i, int italon, int italtag, int captag, int uptag, int lowtag, int printed)

{

/*

---------------------------------------------------------------------------

THIS IS A FUNCTION TO PRINT LINES OF ASCII STARS BASED ON USER INPUT

THE FUNCTION WILL GO THROUGH LETTERS AND DETERMIN THEIR ACSII VALUES

---------------------------------------------------------------------------

IF THE FUCNTION COMES ACROSS A '&' THIS WILL TRIGGER THE TAG SECTION,

VARIABLES KEEP TRACK OF BOLD AND ITALIC SELECTION BASED ON &b, &B, &I, &i.

---------------------------------------------------------------------------

THE FUNCTION ALSO SUPPORTS THE &C &U AND &L TAGS. &C CAPITALISES EVERY FIRST

LETTER OF USER INPUT, &U MAKES IT ALL UPPER CASE, AND &L MAKES IT ALL LOWER.

---------------------------------------------------------------------------

ONCE THE INPUT IS NOT A & TAG THE FUNCTION ASSESSES WHETHER OR NOT TO PRINT

SPACES BEFORE THE LINES OF STARS TO MAKE TEXT ITALIC.

---------------------------------------------------------------------------

AFTER THIS THE FUNCTION PRINTS THE RELEVANT LINE OF STARS TO THE CHARACTER 

AND ROW
---------------------------------------------------------------------------

*/



if (strcmp(userin, "\n")!=0)			/* keep looping untill the user input is "\n" */

{

printf("\n");					/* some formatting */

pointerinit(regular);				/*reset characters to regular stars*/
i=0;
italtag=0;					/*reset all tags*/
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)   		/* loop through the user inputted string string */
	{			      		/* stop looping before the null string terminator */
	ascii = userin[i];
	if (ascii == 38)   	      		/* if an ampersand is found */
		{
		i++;
		ascii = userin[i];		/* move to next character */
		
		if (ascii == 66){	 	 /* bold on if character is a B */
			pointerinit(bold); 	/* switch pointers to bold (+32)*/
			continue;		/* return to top of loop and increment i */			
			}
		else if (ascii == 98){		/* bold off */
			pointerinit(regular); 	/*switch pointers to regular (0)*/
			continue;	
			}
		else if (ascii == 73){		/* italics on */
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ 	/* italics off */
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ 		/* if input is && then print & and increment i*/
			printf("%s", r1[38]);
			continue;
			}
		else if (ascii == 67){   	/* turn on variable that tracks capitalisation */
			captag=1;
			uptag=0;		/*turn off other case transformations*/
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   	/* turn off variable that tracks capitalisation */
			captag=0;
			continue;
			}	
		else if (ascii == 85){  	/* turn on variable that tracks upper case */
			uptag=1;
			lowtag=0;		/*turn off other case transformations*/
			captag=0;
			continue;
			}	

		else if (ascii == 117){    	/* turn off variable that tracks upper case */
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   	/* turn on variable that tracks lower case */
			lowtag=1;
			uptag=0;		/*turn off other case transformations*/
			captag=0;
			continue;
			}	
		else if (ascii == 108){   	/* turn off variable that tracks lower case */
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)	/* if the last character left the italic tag triggered, and italics isnt on already */
		{			/* decrement 'i' as we haven't read a letter before finishing the loop */
		i--;			/* print spaces dependent upon what row we are on */
		printf("      ");
		italon=1;		/* turn 'italon' on, meaning we pass the transformation on the next iteration */
		}
	else if (!italtag && italon)	/* if the last character left the italic tag off and italics is on already */

		{
		i--;			/* decrement 'i' as we haven't read a letter before finishing the loop */
		printf("");		/* print spaces dependent upon what row we are on */
		italon=0;
		}	 	 	/* turn 'italon' off, meaning we pass the transformation on the next iteration */
	else	
		{
		if (captag && ascii>96 && ascii<123) 	/* if capitalisation on, and letter is lower case */
			{
			if (!printed)			/*in the case it is the first letter after the tag with no space*/
				{
				ascii=ascii-32;		/*transform character to upper case*/
				}

			if (userin[i-1]==' ')		/*if the preceeding character is a space*/
					{
					ascii=ascii-32;	/* transform the character to upper case */
					}
			}

		if(lowtag && ascii>64 && ascii<91){	/* if lower case is on and the character is a capital letter */
		ascii=ascii+32;				/* make the letter lower case */
		}

		if(uptag && ascii>96 && ascii<123){	/* if capitals is on and the character is a lower case letter */
		ascii=ascii-32;				/* make the letter upper case */
		}


		
		printf("%s", r1[ascii]);		/* print row of stars character */
		printed=1;				/* let the loop know we are no longer on the first character after*/
		}					/* any text transformation tag found e.g. capitalisation */
	}
	
printf("\n");


/****************    REPEAT	 FOR	 NEXT	 6 	ROWS     *******************/


pointerinit(regular);		/*    R O W   2    */
i=0;
italtag=0;
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)
	{
ascii = userin[i];
	if (ascii == 38)
		{
		i++;
		ascii = userin[i];
		
		if (ascii == 66){	  ////bold on	
			pointerinit(bold);
			continue;					
			}
		else if (ascii == 98){	//bold off
			pointerinit(regular);
			continue;	
			}
		else if (ascii == 73){	//italics on
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ //italics off
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ 		/* if input is && then print & and increment i*/
			printf("%s", r2[38]);
			continue;
			}
		else if (ascii == 67){   ///cap on
			captag=1;
			uptag=0;	
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   ///cap off
			captag=0;
			continue;
			}	
		else if (ascii == 85){   ///upper on
			uptag=1;
			lowtag=0;
			captag=0;
			continue;
			}	

		else if (ascii == 117){   ///upper off
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   ///lower on
			lowtag=1;
			uptag=0;
			captag=0;
			continue;
			}	
		else if (ascii == 108){   ///lower off
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)
		{
		i--;
		printf("     ");
		italon=1;
		}
	else if (!italtag && italon)

		{
		i--;
		printf(" ");
		italon=0;
		}	 	 
	else	
		{
		if(captag && ascii>96 && ascii<123){
			if(!printed){ascii=ascii-32;}
			if(printed&&userin[i-1]==' '){
				
					
					ascii=ascii-32;
					
					}
				
		
		}

		if(lowtag && ascii>64 && ascii<91){
		ascii=ascii+32;
		}

		if(uptag && ascii>96 && ascii<123){
		ascii=ascii-32;
		}


		
		printf("%s", r2[ascii]);
		printed=1;
		}
	}
	
printf("\n");



pointerinit(regular); 		/*    R O W   3    */
i=0;
italtag=0;
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)
	{
ascii = userin[i];
	if (ascii == 38)
		{
		i++;
		ascii = userin[i];
		
		if (ascii == 66){	  ////bold on	
			pointerinit(bold);
			continue;					
			}
		else if (ascii == 98){	//bold off
			pointerinit(regular);
			continue;	
			}
		else if (ascii == 73){	//italics on
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ //italics off
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ 	/* if input is && then print & and increment i*/
			printf("%s", r3[38]);
			continue;
			}
		else if (ascii == 67){   ///cap on
			captag=1;
			uptag=0;	
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   ///cap off
			captag=0;
			continue;
			}	
		else if (ascii == 85){   ///upper on
			uptag=1;
			lowtag=0;
			captag=0;
			continue;
			}	

		else if (ascii == 117){   ///upper off
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   ///lower on
			lowtag=1;
			uptag=0;
			captag=0;
			continue;
			}	
		else if (ascii == 108){   ///lower off
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)
		{
		i--;
		printf("    ");
		italon=1;
		}
	else if (!italtag && italon)

		{
		i--;
		printf("  ");
		italon=0;
		}	 	 
	else	
		{
		if(captag && ascii>96 && ascii<123){
			if(!printed){ascii=ascii-32;}
			if(printed&&userin[i-1]==' '){
				
					
					ascii=ascii-32;
					
					}
				
		
		}

		if(lowtag && ascii>64 && ascii<91){
		ascii=ascii+32;
		}

		if(uptag && ascii>96 && ascii<123){
		ascii=ascii-32;
		}


		
		printf("%s", r3[ascii]);
		printed=1;
		}
	}
	
printf("\n");



pointerinit(regular); 		/*    R O W   4    */
i=0;
italtag=0;
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)
	{
ascii = userin[i];
	if (ascii == 38)
		{
		i++;
		ascii = userin[i];
		
		if (ascii == 66){	  ////bold on	
			pointerinit(bold);
			continue;					
			}
		else if (ascii == 98){	//bold off
			pointerinit(regular);
			continue;	
			}
		else if (ascii == 73){	//italics on
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ //italics off
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ /* if input is && then print & and increment i*/
			printf("%s", r4[38]);
			continue;
			}
		else if (ascii == 67){   ///cap on
			captag=1;
			uptag=0;	
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   ///cap off
			captag=0;
			continue;
			}	
		else if (ascii == 85){   ///upper on
			uptag=1;
			lowtag=0;
			captag=0;
			continue;
			}	

		else if (ascii == 117){   ///upper off
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   ///lower on
			lowtag=1;
			uptag=0;
			captag=0;
			continue;
			}	
		else if (ascii == 108){   ///lower off
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)
		{
		i--;
		printf("   ");
		italon=1;
		}
	else if (!italtag && italon)

		{
		i--;
		printf("   ");
		italon=0;
		}	 	 
	else	
		{
		if(captag && ascii>96 && ascii<123){
			if(!printed){ascii=ascii-32;}
			if(printed&&userin[i-1]==' '){
				
					
					ascii=ascii-32;
					
					}
				
		
		}

		if(lowtag && ascii>64 && ascii<91){
		ascii=ascii+32;
		}

		if(uptag && ascii>96 && ascii<123){
		ascii=ascii-32;
		}


		
		printf("%s", r4[ascii]);
		printed=1;
		}
	}
	
printf("\n");



pointerinit(regular);		/*    R O W   5    */
i=0;
italtag=0;
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)
	{
ascii = userin[i];
	if (ascii == 38)
		{
		i++;
		ascii = userin[i];
		
		if (ascii == 66){	  ////bold on	
			pointerinit(bold);
			continue;					
			}
		else if (ascii == 98){	//bold off
			pointerinit(regular);
			continue;	
			}
		else if (ascii == 73){	//italics on
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ //italics off
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ /* if input is && then print & and increment i*/
			printf("%s", r5[38]);
			continue;
			}
		else if (ascii == 67){   ///cap on
			captag=1;
			uptag=0;	
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   ///cap off
			captag=0;
			continue;
			}	
		else if (ascii == 85){   ///upper on
			uptag=1;
			lowtag=0;
			captag=0;
			continue;
			}	

		else if (ascii == 117){   ///upper off
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   ///lower on
			lowtag=1;
			uptag=0;
			captag=0;
			continue;
			}	
		else if (ascii == 108){   ///lower off
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)
		{
		i--;
		printf("  ");
		italon=1;
		}
	else if (!italtag && italon)

		{
		i--;
		printf("    ");
		italon=0;
		}	 	 
	else	
		{
		if(captag && ascii>96 && ascii<123){
			if(!printed){ascii=ascii-32;}
			if(printed&&userin[i-1]==' '){
				
					
					ascii=ascii-32;
					
					}
				
		
		}

		if(lowtag && ascii>64 && ascii<91){
		ascii=ascii+32;
		}

		if(uptag && ascii>96 && ascii<123){
		ascii=ascii-32;
		}


		
		printf("%s", r5[ascii]);
		printed=1;
		}
	}
	
printf("\n");



pointerinit(regular);		/*    R O W   6    */
i=0;
italtag=0;
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)
	{
ascii = userin[i];
	if (ascii == 38)
		{
		i++;
		ascii = userin[i];
		
		if (ascii == 66){	  ////bold on	
			pointerinit(bold);
			continue;					
			}
		else if (ascii == 98){	//bold off
			pointerinit(regular);
			continue;	
			}
		else if (ascii == 73){	//italics on
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ //italics off
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ /* if input is && then print & and increment i*/
			printf("%s", r6[38]);
			continue;
			}
		else if (ascii == 67){   ///cap on
			captag=1;
			uptag=0;	
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   ///cap off
			captag=0;
			continue;
			}	
		else if (ascii == 85){   ///upper on
			uptag=1;
			lowtag=0;
			captag=0;
			continue;
			}	

		else if (ascii == 117){   ///upper off
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   ///lower on
			lowtag=1;
			uptag=0;
			captag=0;
			continue;
			}	
		else if (ascii == 108){   ///lower off
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)
		{
		i--;
		printf(" ");
		italon=1;
		}
	else if (!italtag && italon)

		{
		i--;
		printf("     ");
		italon=0;
		}	 	 
	else	
		{
		if(captag && ascii>96 && ascii<123){
			if(!printed){ascii=ascii-32;}
			if(printed&&userin[i-1]==' '){
				
					
					ascii=ascii-32;
					
					}
				
		
		}

		if(lowtag && ascii>64 && ascii<91){
		ascii=ascii+32;
		}

		if(uptag && ascii>96 && ascii<123){
		ascii=ascii-32;
		}


		
		printf("%s", r6[ascii]);
		printed=1;
		}
	}
	
printf("\n");



pointerinit(regular);		/*    R O W   7    */
i=0;
italtag=0;
italon=0;
captag=0;
uptag=0;
lowtag=0;
for (i=0;i<(strlen(userin)-1); i++)
	{
ascii = userin[i];
	if (ascii == 38)
		{
		i++;
		ascii = userin[i];
		
		if (ascii == 66){	  ////bold on	
			pointerinit(bold);
			continue;					
			}
		else if (ascii == 98){	//bold off
			pointerinit(regular);
			continue;	
			}
		else if (ascii == 73){	//italics on
			italtag = 1;
			continue;	
			}		
		else if (ascii == 105){ //italics off
			italtag = 0;
			continue;
			}
		else if (ascii == 38){ /* if input is && then print & and increment i*/
			printf("%s", r7[38]);
			continue;
			}
		else if (ascii == 67){   ///cap on
			captag=1;
			uptag=0;	
			lowtag=0;
			printed=0;
			continue;
			}	
		else if (ascii == 99){   ///cap off
			captag=0;
			continue;
			}	
		else if (ascii == 85){   ///upper on
			uptag=1;
			lowtag=0;
			captag=0;
			continue;
			}	

		else if (ascii == 117){   ///upper off
			uptag=0;
			continue;
			}	
		else if (ascii == 76){   ///lower on
			lowtag=1;
			uptag=0;
			captag=0;
			continue;
			}	
		else if (ascii == 108){   ///lower off
			lowtag=0;
			continue;
			}		
		}
	else if (italtag && !italon)
		{
		i--;
		printf("");
		italon=1;
		}
	else if (!italtag && italon)

		{
		i--;
		printf("      ");
		italon=0;
		}	 	 
	else	
		{
		if(captag && ascii>96 && ascii<123){
			if(!printed){ascii=ascii-32;}
			if(printed&&userin[i-1]==' '){
				
					
					ascii=ascii-32;
					
					}
				
		
		}

		if(lowtag && ascii>64 && ascii<91){
		ascii=ascii+32;
		}

		if(uptag && ascii>96 && ascii<123){
		ascii=ascii-32;
		}


		
		printf("%s", r7[ascii]);
		printed=1;
		}
	}
	
printf("\n");





	}
		
return; /* 	      E N D 	F U N C T I O N		 */

}

int main()			/* begin main */
{
    totalchars=0;  		/* reset total chars */
    
    starinit();			/* initialise star rows */
    pointerinit(0);		/* set up pointers for regular typeface */
    
    do{				/* take inputs untill input is just the ENTER key */
        printf("\nInput text followed by ENTER. Font transforming &B, &I, &b, &i tags supported.\nCase transforming &U, &C, &L, &u, &c, &l tags supported for upper/capitalisation/lower.\nHave some fun then hit ENTER again to end.\n\n");
        getinput(0);		/* get user input and save into userin */
        totalchars= (strlen(userin)-1)+totalchars; /* increment total chars */
        printlines(0,0,0,0,0,0,0);	/* print text as stars (with formatting) */
        
    }while (strcmp(userin, "\n")!=0); /* condition (ENTER) */
    
    
    printf("%d total characters processed.\n", totalchars); /* informm user of chars processed */
    
return(0);
    
}




