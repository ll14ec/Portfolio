/*
_________________________________________________________________

A PROGRAM TO PRINT USER INPUTS FROM THE KEYBOARD TO THE TERMINAL

THE PROGRAM WILL FORMAT ONE OR MULTIPLE LINES OF INPUTTED C CODE

INTO READABLE CODE, OUTPUT WITH BOLD KEYWORDS AND ITALIC COMMENTS
_________________________________________________________________

Nested braces are on a new line and indented properly.

Functionality with conditional and compound statements.

Comments are rendered in italics.
This program is only fully compatible with *//* style coments.

The keywords that are printed in bold are, FOR, IF, DO, CONST, 
CONTINUE, DEFAULT, CASE, WHILE, ELSE, BREAK, DOUBLE, INT, SHORT,
LONG, FLOAT, GOTO, RETURN, SWITCH, SIGNED, UNSIGNED and CHAR.

The output for each user input is in the form of an arrangement
of 7*5 asterisks in the terminal window. 

The program will terminate when the user inputs only the ENTER
key.

The program is works with the range of ascii characters 33-127.
_________________________________________________________________


*/



#include <stdio.h>
#include <string.h>

char inputline[100][1000]; /* for user inputs from stdin */
char fullstring[99999];	   /* all user inputs are copied into here */
char cond[4]="\n";	   /* conditional input is a trigger to output the text */
int stringcount=0;	   /* how many finalstrings there are  */
char editstring[9999] = "";/* the formatted user input is copied into here */
char *finalstring;	   /* the full output string after editing, before splitting */
char finalstrings[100][1000]; /* the output string after editing and splitting */
int asciin1, asciin2, asciin3, asciin4, asciin5, asciin6, asciin7, asciin8, asciin9, asciin10;	/* variables to keep track of characters' ascii values */
int ascii, ascii2, ascii3, ascii4, ascii5, ascii6, ascii7, ascii8,ascii9;

/* variables to keep track of keywords*/
int shorttrig, inttrig, longtrig, floattrig, dubtrig, dotrig, whiletrig, fortrig, gotrig, returntrig, breaktrig, conttrig, iftrig, elsetrig, switchtrig, casetrig, deftrig, unsigntrig, signtrig, chartrig, consttrig;

             
char starline[65][9];	/* declares a 2D array of strings for all star combinations/bold 				// star combinations (2^5=32)*2=64*/
char *r1[127];		// pointers for star combinations for row 1
char *r2[127];		// "                   "              row 2
char *r3[127];		// "                   "              row 3
char *r4[127];		// "                   "              row 4
char *r5[127];		// "                   "              row 5
char *r6[127];		// "                   "              row 6
char *r7[127];		// "                   "              row 7


const int bold=32;    /*for the +n part of pointerinit so code is more readable*/
const int regular=0;

void starinit()

/* A function to assign all combinations of 1x5 asterisks into values starline[1] to starline[32], and to also assign their bold equivalents from 33-64*/

{
    
    strcpy(starline[1] ,"       ");
    strcpy(starline[2] ,"    *  ");
    strcpy(starline[3] ,"   *   ");
    strcpy(starline[4] ,"   **  ");
    strcpy(starline[5] ,"  *    ");
    strcpy(starline[6] ,"  * *  ");
    strcpy(starline[7] ,"  **   ");
    strcpy(starline[8] ,"  ***  ");
    strcpy(starline[9] ," *     ");
    strcpy(starline[10]," *  *  ");
    strcpy(starline[11]," * *   ");
    strcpy(starline[12]," * **  ");
    strcpy(starline[13]," **    ");
    strcpy(starline[14]," ** *  ");
    strcpy(starline[15]," ***   ");
    strcpy(starline[16]," ****  ");
    strcpy(starline[17],"*      ");
    strcpy(starline[18],"*   *  ");
    strcpy(starline[19],"*  *   ");
    strcpy(starline[20],"*  **  ");
    strcpy(starline[21],"* *    ");
    strcpy(starline[22],"* * *  ");
    strcpy(starline[23],"* **   ");
    strcpy(starline[24],"* ***  ");
    strcpy(starline[25],"**     ");
    strcpy(starline[26],"**  *  ");
    strcpy(starline[27],"** *   ");
    strcpy(starline[28],"** **  ");
    strcpy(starline[29],"***    ");
    strcpy(starline[30],"*** *  ");
    strcpy(starline[31],"****   ");
    strcpy(starline[32],"*****  ");
    strcpy(starline[33] ,"       ");
    strcpy(starline[34] ,"    ** ");
    strcpy(starline[35] ,"   **  ");
    strcpy(starline[36] ,"   *** ");
    strcpy(starline[37] ,"  **   ");
    strcpy(starline[38] ,"  **** ");
    strcpy(starline[39] ,"  ***  ");
    strcpy(starline[40] ,"  **** ");
    strcpy(starline[41] ," **    ");
    strcpy(starline[42]," ** ** ");
    strcpy(starline[43]," ****  ");
    strcpy(starline[44]," ***** ");
    strcpy(starline[45]," ***   ");
    strcpy(starline[46]," ***** ");
    strcpy(starline[47]," ****  ");
    strcpy(starline[48]," ***** ");
    strcpy(starline[49],"**     ");
    strcpy(starline[50],"**  ** ");
    strcpy(starline[51],"** **  ");
    strcpy(starline[52],"** *** ");
    strcpy(starline[53],"****   ");
    strcpy(starline[54],"*****  ");
    strcpy(starline[55],"*****  ");
    strcpy(starline[56],"*****  ");
    strcpy(starline[57],"***    ");
    strcpy(starline[58],"*** ** ");
    strcpy(starline[59],"*****  ");
    strcpy(starline[60],"****** ");
    strcpy(starline[61],"****   ");
    strcpy(starline[62],"****** ");
    strcpy(starline[63],"*****  ");
    strcpy(starline[64],"****** ");
    
}

void pointerinit(int n)

{
    
    /*a function to point rows 1-6 of characters (with corresponding ascii numbering) to the correct star combinations*/
   
    /*assign all row pointers that use starline[1] */
    
r1[9]= r1[32]= r1[35]= r1[36]= r1[37]= r1[38]= r1[42]= r1[43]= r1[44]= r1[45]= r1[46]= r1[47]= r1[58]= r1[59]= r1[61]= r1[92]= r1[95]= r1[97]= r1[99]= r1[101]= r1[103]= r1[105]= r1[106]= r1[109]= r1[110]= r1[111]= r1[112]= r1[113]= r1[114]= r1[115]= r1[117]= r1[118]= r1[119]= r1[120]= r1[121]= r1[122]= r1[126]= r2[9]= r2[32]= r2[35]= r2[37]= r2[38]= r2[43]= r2[44]= r2[45]= r2[46]= r2[58]= r2[59]= r2[61]= r2[95]= r2[97]= r2[99]= r2[101]= r2[103]= r2[109]= r2[110]= r2[111]= r2[112]= r2[113]= r2[114]= r2[115]= r2[117]= r2[118]= r2[119]= r2[120]= r2[121]= r2[122]= r2[126]= r3[9]= r3[32]= r3[34]= r3[35]= r3[39]= r3[44]= r3[45]= r3[46]= r3[58]= r3[59]= r3[61]= r3[94]= r3[95]= r3[96]= r3[97]= r3[99]= r3[105]= r3[106]= r3[109]= r3[110]= r3[111]= r3[114]= r3[117]= r3[118]= r3[119]= r3[122]= r3[126]= r4[9]= r4[32]= r4[34] = r4[39]= r4[44]= r5[45]= r4[46]= r4[94]= r4[95]= r4[96]= r5[9]= r5[32]= r5[34]= r5[39]= r5[44]= r5[46]= r5[58]= r5[59]= r5[61]= r5[94]= r5[95]= r5[96]= r6[9]= r6[32]= r6[33]= r6[34]= r6[39]= r6[43]= r6[45]= r6[46]= r6[63]= r6[94]= r6[95]= r6[96]= r6[126]= r7[9]= r7[32]= r7[34]= r7[36]= r7[39]= r7[42]= r7[43]= r7[45]= r7[47]= r7[58]= r7[61]= r7[92]= r7[94]= r7[96]= r7[126]= starline[(1+n)];

 /*assign all row pointers that use starline[2] */
    
r1[40]= r1[60]= r1[100]= r2[47]= r2[51]= r2[100]=  r3[47]= r3[51]= r3[63]= r3[100]= r4[53]= r5[44]= r5[51]= r5[53]= r5[57]= r5[83]= r6[44]= r6[51]= r6[53]= r6[57]= r6[83]= r6[92]= r6[103]= r7[40]= r7[60]= starline[(2+n)];
    
 /*assign all row pointers that use starline[3] */
r2[40]= r2[55]=  r2[60]= r2[90]= r3[40]= r3[47]= r3[50]= r3[55]= r3[123]= r4[37]= r4[40]= r4[37]= r4[62]= r5[40]= r5[55]= r5[92]= r5[123]= r6[40]= r6[52]= r6[55]= r6[60]= r6[113]= r6[115]= r7[44]= r7[52]= r7[55]= starline[(3+n)];

 /*assign all row pointers that use starline[4] */
r1[123]=r7[113]= r7[123]= starline[(4+n)];

 /*assign all row pointers that use starline[5] */
r1[33]= r1[39]= r1[49]= r1[94]= r1[116]= r1[124]= r2[33]= r2[36]= r2[39]= r2[41]= r2[42]= r2[73]= r2[74]= r2[84]= r2[105]= r2[106]= r2[116]= r2[123]= r2[124]= r2[125]= r3[33]= r3[41]=r3[43]= r3[60]= r3[62]= r3[73]= r3[74]= r3[84]= r3[90]= r3[116]= r3[124]= r4[33]= r4[41]= r4[47]= r4[49]=r4[50]= r4[73]= r4[74]= r4[84]= r4[88]= r4[89]= r4[92]= r4[105]= r4[106]= r4[124]= r5[33]= r5[37]= r5[41]= r5[43]= r5[49]= r5[60]= r5[62] = r5[63]= r5[73]= r5[74]= r5[84]= r5[89]= r5[105]= r5[106]= r5[116]= r5[120]= r5[121]= r5[122]= r5[124]= r6[36]= r6[41]= r6[42]= r6[49]= r6[73]= r6[74]= r6[84]= r6[89]= r6[105]= r6[106]= r6[116]= r6[121]= r6[123]= r6[124]= r6[125]= r7[33]= r7[63]= r7[84]= r7[86]= r7[89]= r7[118]= r7[121]= r7[124]= starline[(5+n)];
    
 /*assign all row pointers that use starline[7] */
r1[34]= r1[102]= r2[34]= r4[63]= r4[123]= r7[116] = starline[(7+n)];

 /*assign all row pointers that use starline[8] */   
r1[67]= r1[71]= r7[67]= r7[71]= starline[(8+n)];

 /*assign all row pointers that use starline[9] */
r1[41]= r1[108]= r2[62]= r2[67]=  r2[71]=  r2[93]=  r2[96]=  r2[102]= r2[108]=   r3[92]= r3[93]= r3[102]= r3[108]= r3[125]= r4[59]= r4[60]= r4[93]= r4[108]=  r5[47]= r5[50]= r5[90]= r5[93]= r5[102]= r5[108]= r5[125] =r6[37]= r6[59]= r6[62]= r6[67]= r6[93]= r6[102]= r6[108]= r6[122]=  r7[41] = r7[102] = starline[(9+n)];
    
 /*assign all row pointers that use starline[10] */
r6[71]= starline[(10+n)];

 /*assign all row pointers that use starline[11] */    
r2[94]= r3[88]= r3[89]= r4[35]= r4[120]= r4[121]= r5[88]= r6[86]= r6[118]= r6[120]= r7[87]= r7[119]=  starline[(11+n)];

 /*assign all row pointers that use starline[13] */   
r1[81]= r2[49]= r3[38]= r4[90]= r4[111]= r4[125]=  r5[115]= r7[111]= r7[117] = starline[(13+n)];

 /*assign all row pointers that use starline[14] */    
r5[38]= r7[38]= r7[81]= r7[97]= starline[(14+n)];
  

 /*assign all row pointers that use starline[15] */    
r1[48]= r1[50]= r1[56]= r1[57]= r1[63]= r1[64]= r1[65]= r1[79]= r3[36]= r3[101]= r3[103]= r3[113]= r3[115]= r4[42]= r4[45]= r4[51]= r4[56]= r4[83]= r4[97]= r4[99]= r4[114]= r4[116]=  r5[36]= r5[113]= r7[48]= r7[54]= r7[56]= r7[64]= r7[79]= r7[85]= r7[99]= r7[105]=  starline[(15+n)];
    
 /*assign all row pointers that use starline[16] */    
r1[54]= r1[83]= r4[55]= r4[57]= r4[100]= r5[103]= r7[100]= r7[101] = starline[(16+n)];

 /*assign all row pointers that use starline[17] */    
r1[52]= r1[62]= r1[76]= r1[96]= r1[98]= r1[104]= r1[107]= r2[52]= r2[53]= r2[54]= r2[69]= r2[70]= r2[76]= r2[83]= r2[91]= r2[92]= r2[98]= r2[104]= r2[107]= r3[67]= r3[69]= r3[70]= r3[71]= r3[76]= r3[83]= r3[91]= r3[98]= r3[104]= r4[36]= r4[38]= r4[58]= r4[67]= r4[76]= r4[91]= r4[115]= r5[67]= r5[69]= r5[70]= r5[76]= r5[80]= r5[91]= r5[99]= r5[114]= r6[47]= r6[50]= r6[58]= r6[64]= r6[69]= r6[70]= r6[76]= r6[80]= r6[90]= r6[91]= r6[99]= r6[101]= r6[112]= r6[114]= r7[46]= r7[59]= r7[62]= r7[70]= r7[80]= r7[112]= r7[114]= starline[(17+n)];

 /*assign all row pointers that use starline[18] */    
r1[72]= r1[75]= r1[77]= r1[78]= r1[85]= r1[86]= r1[87]= r1[88]= r1[89]= r2[48]= r2[50]= r2[56]= r2[57]= r2[63]= r2[64]= r2[65]= r2[66]= r2[68]= r2[72]= r2[78]= r2[79]= r2[80]= r2[82]= r2[85]= r2[86]= r2[87]= r2[88]= r2[89]= r3[37]= r3[48]= r3[56]= r3[57]= r3[65]= r3[66]= r3[68]= r3[72]= r3[79]= r3[80]= r3[82]= r3[85]= r3[86]= r3[87]= r3[120]= r3[121]= r4[54]= r4[68]= r4[77]= r4[79]= r4[85]= r4[86]= r4[87]= r4[101]= r4[103]= r4[109]= r4[118]= r4[119]= r5[48]= r5[54]= r5[56]= r5[65]= r5[66]= r5[68]= r5[71]= r5[72]= r5[77]= r5[79]= r5[85]= r5[86]= r5[98]= r5[100]= r5[104]= r5[118]= r5[119]= r6[48]= r6[54]= r6[56]= r6[65]= r6[66]= r6[68]= r6[72]= r6[77]= r6[78]= r6[79]= r6[85]= r6[88]= r6[98]= r6[100]= r6[104]= r7[37]= r7[65]= r7[72]= r7[75]= r7[77]= r7[78]= r7[88]= r7[104]= r7[109]= r7[120]= starline[(18+n)];

 /*assign all row pointers that use starline[19] */    
r2[75]= r2[81]= r3[52]= r3[81]= r3[107]= r4[52]= r4[81]= r4[110]= r4[112]= r4[113]= r4[117]= r5[81]= r5[97]= r5[111]= r5[117]= r6[38]= r6[75]= r6[81]= r6[97]= r6[111]= r6[117]= r7[107]= r7[110]= starline[(19+n)];
    
 /*assign all row pointers that use starline[20] */
r4[71]= r5[78]= r7[82] = starline[(20+n)];

 /*assign all row pointers that use starline[21] */
r3[49]= r3[75]= r4[107]= r5[75]= r6[82]= r6[107]= r7[35]=  starline[(21+n)];
 
 /*assign all row pointers that use starline[22] */   
r3[42]= r3[77]= r4[48]= r4[64]= r4[78]= r5[42]= r5[87]= r6[87]= r6[109]= r6[119]= starline[(22+n)];

 /*assign all row pointers that use starline[23] */
r6[110] = starline[(23+n)];

 /*assign all row pointers that use starline[24] */    
r3[64]= r5[64]= r5[126]= starline[(24+n)];

 /*assign all row pointers that use starline[25] */    
r1[91]= r1[93]= r1[125]= r4[75]= r5[82]= r5[107]= r7[91]= r7[93]= r7[125] = starline[(25+n)];

 /*assign all row pointers that use starline[26] */    
r3[78] = starline[(26+n)];
    
 /*assign all row pointers that use starline[27] */
r5[110] = starline[(27+n)];
 
r2[77] = r5[109] = starline[(28+n)];

 /*assign all row pointers that use starline[28] */    
r3[112]= r4[69]= r4[70]= r4[102]= r5[112]= r7[106]= r7[108]= r7[115]= starline[(29+n)];
  
 /*assign all row pointers that use starline[30] */  
r4[126] = starline[(30+n)];

 /*assign all row pointers that use starline[31] */   
r1[51]= r1[55]= r1[66]= r1[68]= r1[80]= r1[82]= r3[53]= r3[54]= r4[66]= r4[80]= r4[82]= r4[98]= r4[104]= r4[122]= r5[35]= r5[101]= r6[35]= r7[51]= r7[53]= r7[57]= r7[66]= r7[68]= r7[74]= r7[76]= r7[83]= r7[98]= r7[103]= r7[122]= starline[(31+n)];

 /*assign all row pointers that use starline[32] */    
r1[53]= r1[69]= r1[70]= r1[73]= r1[74]= r1[84]= r1[90]= r4[43]= r4[61]= r4[65]= r4[72]= r5[52]= r6[61]= r7[49]= r7[50]= r7[69]= r7[73]= r7[90]= r7[95]=  starline[(32+n)];
    
}


void printlines(int i, int stringno, int italon, int italtag, int i2, int uptag, int lowtag, int printed)

{


/*

A FUNCTION TO PRINT THE STRINGS FROM FINALSTRINGS[] TO THE TERMINAL IN 5X7 ASTERISKS

THE FUNCTION WILL TURN *//* STYLE BRACKETS TO ITALIC AND KEYWORDS TO BOLD.


*/


stringno=stringcount;  /* do not print initial blank lines, number varies depending upon stringcount magnitude */
i=0;  


    for (stringno=stringcount; stringno<=stringcount; stringno++) /* all strings of user input */
        
    {
  
    pointerinit(regular);   /* reset pointers from bold font */
    	

         for (i=0;i<strlen(finalstrings[stringno]); i++)
        {
	
/* here we go through each string printing untill the end */
/* at the end of the string, if there are more we print the next one etc. */






/* get ascii values of preceeding chars and chars after the one we are printing */

/* this will allow us to assess if they are special words, and also look at */

/* more than one case of sequential punctuation to format code accurately */
	
	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;	
					/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf("      ");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf("");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

	
  	if (((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1))) // capitalised
	{
	if ((ascii>96) && (ascii<123))  /* if a keyword and it was lower case */

			{
			ascii=ascii-32;  /* make it upper case */
			}

	}
	

		
	

            printf("%s", r1[ascii]); /* print character for row 1 */
        }

        printf("\n");

/* 	R E P E A T 	F O R 	A L L 	R O W S 	*/


				/* 	R O W 	2	*/


 for (i=0;i<strlen(finalstrings[stringno]); i++)
        {
	
	
	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;				/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf("     ");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf(" ");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

  	if ((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1)) // capitalised
	{
	if ((ascii>96) && (ascii<123))

			{
			ascii=ascii-32;
			}

	}
		
		
	

            printf("%s", r2[ascii]);
        }
        printf("\n");

				/* 	R O W 	3	*/
        
 for (i=0;i<strlen(finalstrings[stringno]); i++)
        {
	
	
	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;					/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf("    ");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf("  ");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

  	if ((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1)) // capitalised
	{
	if ((ascii>96) && (ascii<123))

			{
			ascii=ascii-32;
			}

	}
	
		
		
	

            printf("%s", r3[ascii]);
        }
        printf("\n");

					/* 	R O W 	4	*/
        
 for (i=0;i<strlen(finalstrings[stringno]); i++)
        {
	
	
	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;					/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf("   ");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf("   ");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

  	if ((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1)) // capitalised
	{
	if ((ascii>96) && (ascii<123))

			{
			ascii=ascii-32;
			}

	}
				
	

            printf("%s", r4[ascii]);
        }
        printf("\n");

					/* 	R O W 	5	*/
        
 for (i=0;i<strlen(finalstrings[stringno]); i++)
        {
	
	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;					/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf("  ");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf("    ");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

  	if ((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1)) // capitalised
	{
	if ((ascii>96) && (ascii<123))

			{
			ascii=ascii-32;
			}

	}
	
		
		
	

            printf("%s", r5[ascii]);
        }
        printf("\n");

				/* 	R O W 	6	*/
        
 for (i=0;i<strlen(finalstrings[stringno]); i++)
        {
	
	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;					/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf(" ");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf("     ");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

  	if ((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1)) // capitalised
	{
	if ((ascii>96) && (ascii<123))

			{
			ascii=ascii-32;
			}

	}
	
		
		
	

            printf("%s", r6[ascii]);
        }
        printf("\n");

				/* 	R O W 	7	*/
        
 for (i=0;i<strlen(finalstrings[stringno]); i++)
       
	{

	asciin1 = finalstrings[stringno][i-1];
	asciin2 = finalstrings[stringno][i-2];
	asciin3 = finalstrings[stringno][i-3];
	asciin4 = finalstrings[stringno][i-4];
	asciin5 = finalstrings[stringno][i-5];
	asciin6 = finalstrings[stringno][i-6];
	asciin7 = finalstrings[stringno][i-7];
	asciin8 = finalstrings[stringno][i-8];
	asciin9 = finalstrings[stringno][i-9];
	asciin10 = finalstrings[stringno][i-10];
	ascii2 = finalstrings[stringno][i+1];
	ascii3 = finalstrings[stringno][i+2];
	ascii4 = finalstrings[stringno][i+3];
	ascii5 = finalstrings[stringno][i+4];
	ascii6 = finalstrings[stringno][i+5];
	ascii7 = finalstrings[stringno][i+6];
	ascii8 = finalstrings[stringno][i+7];
	ascii9 = finalstrings[stringno][i+8];	
        ascii = finalstrings[stringno][i];
        italon=0;					/* reset italics */
	

/* if we find a keyword, we first switch pointers over to their bold star line equivolent */

/* before getting to the bottom of the loop, if the program recognises that it is at the */

/* end of a keyword, it will switch back to regular font */

	if (ascii==47 && ascii2==42 && italon==0) /*if a comment*/
	{
	printf("");				/* print spaces to make italic */
	italon=1;					/* italics is on */
	}
	
	if (asciin2==42 && asciin1==47 && italon==1) /* if a closing comment*/

	{
	printf("      ");					/* print spaces to remove italic */
	italon=0;					/* italics is off */
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==70 || ascii==102)&& (ascii2==79 || ascii2==111)&& (ascii3==82 || ascii3==114)&& ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123)) ) 

/* if before the word for, preceeded and followed by a non letter char/space */

	{
	pointerinit(bold);
	fortrig=1;
	}


	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==70 || asciin3==102)&& (asciin2==79 || asciin2==111)&& (asciin1==82 || asciin1==114)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) ) // if after the word for
	{
	pointerinit(regular);
	fortrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==79 || ascii2==111)&& (ascii3==78 || ascii3==110)&& (ascii4==83 || ascii4==115) && (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 const
	{
	pointerinit(bold);
	consttrig=1;
	}

	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==99 || asciin5==67)&& (asciin4==79 || asciin4==111)&& (asciin3==78 || asciin3==110)&& (asciin2==83 || asciin2==115) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after const
	{
	pointerinit(regular);
	consttrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==99 || ascii==67)&& (ascii2==72 || ascii2==104)&& (ascii3==65 || ascii3==97) && (ascii4==82 || ascii4==114) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 char
	{
	pointerinit(bold);
	chartrig=1;
	}
	
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==99 || asciin4==67)&& (asciin3==72 || asciin3==104)&& (asciin2==65 || asciin2==97) && (asciin1==82 || asciin1==114) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 char
	{
	pointerinit(regular);
	chartrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83 || ascii==115)&& (ascii2==73 || ascii2==105)&& (ascii3==71 || ascii3==103) && (ascii4==78 || ascii4==110) && (ascii5==69 || ascii5==101) && (ascii6==68 || ascii6==100) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 signed
	{
	pointerinit(bold);
	signtrig=1;
	}
	

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if b4 signed
	{
	pointerinit(regular);
	signtrig=0;
	}
	
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==85||ascii==117)&&(ascii2==78||ascii2==110)&&(ascii3==83 || ascii3==115)&& (ascii4==73 || ascii4==105)&& (ascii5==71 || ascii5==103) && (ascii6==78 || ascii6==110) && (ascii7==69 || ascii7==101) && (ascii8==68 || ascii8==100) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123))) // if b4 unsigned
	{
	pointerinit(bold);
	unsigntrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==85||asciin8==117)&&(asciin7==78||asciin7==110)&&(asciin6==83 || asciin6==115)&& (asciin5==73 || asciin5==105)&& (asciin4==71 || asciin4==103) && (asciin3==78 || asciin3==110) && (asciin2==69 || asciin2==101) && (asciin1==68 || asciin1==100) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after unsigned
	{
	pointerinit(regular);
	unsigntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==83||ascii==115)&&(ascii2==72||ascii2==104)&&(ascii3==79 || ascii3==111)&& (ascii4==82 || ascii4==114)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 short
	{
	pointerinit(bold);
	shorttrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==83||asciin5==115)&&(asciin4==72||asciin4==104)&&(asciin3==79 || asciin3==111)&& (asciin2==82 || asciin2==114)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after short
	{
	pointerinit(regular);
	shorttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==73||ascii==105)&&(ascii2==78||ascii2==110)&&(ascii3==84 || ascii3==116) && ((ascii4<64 || ascii4>91)&&(ascii4<96 || ascii4>123))) //before int
	{
	pointerinit(bold);
	inttrig=1;
	}

	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) &&(asciin3==73||asciin3==105)&&(asciin2==78||asciin2==110)&&(asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  //after int
	{
	pointerinit(regular);
	inttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==76||ascii==108)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==71 || ascii4==103) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 long
	{
	pointerinit(bold);
	longtrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==76||asciin4==108)&&(asciin3==79||asciin3==111)&&(asciin2==78 || asciin2==110)&& (asciin1==71 || asciin1==103) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after long
	{
	pointerinit(regular);
	longtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==70||ascii==102)&&(ascii2==76||ascii2==108)&&(ascii3==79 || ascii3==111)&& (ascii4==65 || ascii4==97)&& (ascii5==84 || ascii5==116) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 float
	{
	pointerinit(bold);
	floattrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==70||asciin5==102)&&(asciin4==76||asciin4==108)&&(asciin3==79 || asciin3==111)&& (asciin2==65 || asciin2==97)&& (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after float
	{
	pointerinit(regular);
	floattrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&&(ascii3==85 || ascii3==117)&& (ascii4==66 || ascii4==98)&& (ascii5==76 || ascii5==108)&& (ascii6==69 || ascii6==101)&& ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 double
	{
	pointerinit(bold);
	dubtrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) &&(asciin6==68||asciin6==100)&&(asciin5==79||asciin5==111)&&(asciin4==85 || asciin4==117)&& (asciin3==66 || asciin3==98)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after double
	{
	pointerinit(regular);
	dubtrig=0;
	}
	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==79||ascii2==111)&& ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 do
	{
	pointerinit(bold);
	dotrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==68||asciin2==100)&&(asciin1==79||asciin1==111)&& ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after do
	{
	pointerinit(regular);
	dotrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==87||ascii==119)&&(ascii2==72||ascii2==104)&&(ascii3==73 || ascii3==105)&& (ascii4==76 || ascii4==108)&& (ascii5==69 || ascii5==101) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 while
	{
	pointerinit(bold);
	whiletrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) &&(asciin5==87||asciin5==119)&&(asciin4==72||asciin4==104)&&(asciin3==73 || asciin3==105)&& (asciin2==76 || asciin2==108)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after while
	{
	pointerinit(regular);
	whiletrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==71||ascii==103)&&(ascii2==79||ascii2==111)&&(ascii3==84 || ascii3==116)&& (ascii4==79 || ascii4==111) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 goto
	{
	pointerinit(bold);
	gotrig=1;
	}
	
	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) &&(asciin4==71||asciin4==103)&&(asciin3==79||asciin3==111)&&(asciin2==84 || asciin2==116)&& (asciin1==79 || asciin1==111) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after goto
	{
	pointerinit(regular);
	gotrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==82||ascii==114)&&(ascii2==69||ascii2==101)&&(ascii3==84 || ascii3==116)&& (ascii4==85 || ascii4==117)&& (ascii5==82 || ascii5==114)&& (ascii6==78 || ascii6==110) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 return
	{
	pointerinit(bold);
	returntrig=1;
	}

	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==82||asciin6==114)&&(asciin5==69||asciin5==101)&&(asciin4==84 || asciin4==116)&& (asciin3==85 || asciin3==117)&& (asciin2==82 || asciin2==114)&& (asciin1==78 || asciin1==110) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after return
	{
	pointerinit(regular);
	returntrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==66||ascii==98)&&(ascii2==82||ascii2==114)&&(ascii3==69 || ascii3==101)&& (ascii4==65 || ascii4==97)&& (ascii5==75 || ascii5==107) && ((ascii6<64 || ascii6>91)&&(ascii6<96 || ascii6>123))) // if b4 break
	{
	pointerinit(bold);
	breaktrig=1;
	}
	
	if (((asciin6<64 || asciin6>91)&&(asciin6<96 || asciin6>123)) && (asciin5==66||asciin5==98)&&(asciin4==82||asciin4==114)&&(asciin3==69 || asciin3==101)&& (asciin2==65 || asciin2==97)&& (asciin1==75 || asciin1==107) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after break
	{
	pointerinit(regular);
	breaktrig=0;
	}

	
	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) &&(ascii==67||ascii==99)&&(ascii2==79||ascii2==111)&&(ascii3==78 || ascii3==110)&& (ascii4==84 || ascii4==116)&& (ascii5==73 || ascii5==105) && (ascii6==78 || ascii6==110) && (ascii7==85 || ascii7==117) && (ascii8==69 || ascii8==101) && ((ascii9<64 || ascii9>91)&&(ascii9<96 || ascii9>123)) ) // if b4 continue
	{
	pointerinit(bold);
	conttrig=1;
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) &&(asciin8==67||asciin8==99)&&(asciin7==79||asciin7==111)&&(asciin6==78 || asciin6==110)&& (asciin5==84 || asciin5==116)&& (asciin4==73 || asciin4==105) && (asciin3==78 || asciin3==110) && (asciin2==85 || asciin2==117) && (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)) )  // if after continue
	{
	pointerinit(regular);
	conttrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==73||ascii==105)&&(ascii2==72||ascii2==102) && ((ascii3<64 || ascii3>91)&&(ascii3<96 || ascii3>123))) // if b4 if
	{
	pointerinit(bold);
	iftrig=1;
	}

	if (((asciin3<64 || asciin3>91)&&(asciin3<96 || asciin3>123)) && (asciin2==73||asciin2==105)&&(asciin1==72||asciin1==102) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after if
	{
	pointerinit(regular);
	iftrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==69||ascii==101)&&(ascii2==76||ascii2==108)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 else
	{
	pointerinit(bold);
	elsetrig=1;
	}
	
	if (((asciin4<64 || asciin4>91)&&(asciin4<96 || asciin4>123)) && (asciin3==69||asciin3==101)&&(asciin2==76||asciin2==108)&&(asciin1==83 || asciin1==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123)))  // if after else
	{
	pointerinit(regular);
	elsetrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==83||ascii==115)&&(ascii2==87||ascii2==119)&&(ascii3==73 || ascii3==105)&& (ascii4==84 || ascii4==116)&& (ascii5==67 || ascii5==99) && (ascii6==72 || ascii6==104) && ((ascii7<64 || ascii7>91)&&(ascii7<96 || ascii7>123))) // if b4 switch
	{
	pointerinit(bold);
	switchtrig=1;

	}
	
	if (((asciin7<64 || asciin7>91)&&(asciin7<96 || asciin7>123)) && (asciin6==83||asciin6==115)&&(asciin5==87||asciin5==119)&&(asciin4==73 || asciin4==105)&& (asciin3==84 || asciin3==116)&& (asciin2==67 || asciin2==99) && (asciin1==72 || asciin1==104) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after switch
	{
	pointerinit(regular);
	switchtrig=0;
	}

	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==67||ascii==99)&&(ascii2==65||ascii2==97)&&(ascii3==83 || ascii3==115)&& (ascii4==69 || ascii4==101) && ((ascii5<64 || ascii5>91)&&(ascii5<96 || ascii5>123))) // if b4 case
	{
	pointerinit(bold);
	casetrig=1;
		
	}

	if (((asciin5<64 || asciin5>91)&&(asciin5<96 || asciin5>123)) && (asciin4==67||asciin4==99)&&(asciin3==65||asciin3==97)&&(asciin2==83 || asciin2==115)&& (asciin1==69 || asciin1==101) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after case
	{
	pointerinit(regular);
	casetrig=0;
	}


	if (((asciin1<64 || asciin1>91)&&(asciin1<96 || asciin1>123)) && (ascii==68||ascii==100)&&(ascii2==69||ascii2==101)&&(ascii3==70 || ascii3==102)&& (ascii4==65 || ascii4==97)&& (ascii5==85 || ascii5==117) && (ascii6==76 || ascii6==108) && (ascii7==84 || ascii7==116) && ((ascii8<64 || ascii8>91)&&(ascii8<96 || ascii8>123))) // if b4 default
	{
	pointerinit(bold);
	deftrig=1;
		
	}

	if (((asciin9<64 || asciin9>91)&&(asciin9<96 || asciin9>123)) && (asciin8==68||asciin8==100)&&(asciin7==69||asciin7==101)&&(asciin6==70 || asciin6==102)&& (asciin4==65 || asciin4==97)&& (asciin3==85 || asciin3==117) && (asciin2==76 || asciin2==108) && (asciin1==84 || asciin1==116) && ((ascii<64 || ascii>91)&&(ascii<96 || ascii>123))) // if after default
	{
	pointerinit(regular);
	deftrig=0;
	}

  	if ((deftrig==1)|| (shorttrig==1)|| (inttrig==1) || (longtrig==1) || (floattrig==1) || (dubtrig==1) || (dotrig==1) || (whiletrig==1) ||(gotrig==1) || (returntrig==1) || (breaktrig==1) || (conttrig==1) || (iftrig==1) || (elsetrig==1) || (switchtrig==1) || (casetrig==1) || (unsigntrig==1) || (signtrig==1) || (chartrig==1) || (consttrig==1) || (fortrig==1)) // capitalised
	{
	if ((ascii>96) && (ascii<123))

			{
			ascii=ascii-32;
			}

	}
	
		
		
	

            printf("%s", r7[ascii]);
        }
        printf("\n\n");
            
    }
     
return;			/* 	E N D	 F U N C T I O N	*/
 
}

void formatter(int character, int ascvalue1, int ascvalue2, int ascvalue3, int ascvalue4, int ascvalue5, int brax, int paren, int controlz, int cx, int comment, int i)

	{

/*

A FUNCTION TO FORMAT USER INPUTTED STRINGS INTO READABLE C CODE.

THE FUNCTION COPIES CHARACTERS SEQUENTIALLY FROM FULLSTRING INTO EDITSTRING.

IF THE FUNTION COMES ACCROSS SOMETHING IN THE CODE THAT NEEDS TO BE ON A NEW LINE

IT WILL ADD THE \N CHARACTER TO THE STRING.

ONCE THIS FUNCTION IS DONE THE OUTPUT IS A SINGLE, FORMATTED STRING: EDITSTRING.


*/

					/* int character variable keeps track of which leter we are copying onto new string 'editstring'*/
		 
					/* int brax keeps track of number of nested {} */
					/* int paren keeps track of pairs of () */
					/* int controlz keeps track of if it is a control statement */
					/* int cx counter for producing indentation */
					/* int comment keeps track of whether copying comments */
			

	
		/* THE FOLLOWING PART COPIES THE USER INPUT(S) INTO A FINAL STRING WITH CODE-STYLE FORMATTING,
		   PUTTING A \N CHARACTER WHERE NEW LINES SHOULD BEGIN AND 4 SPACES FOR INDENTATION           */



		for (character=0;character<(strlen(fullstring));character++)
			{
			
			ascvalue1 = fullstring[character];		/*get ascii values for char and 5 chars in front*/
			ascvalue2 = fullstring[character+1];
			ascvalue3 = fullstring[character+2];
			ascvalue4 = fullstring[character+3];
			ascvalue5 = fullstring[character+5];
			
			


			if((ascvalue1==10) || (ascvalue1==32 && ascvalue2==32)  && (comment==0)) /* if newline char or 2 spaces */	
			
				{
				continue;				/* skip these and do not copy to full string */
				}					/* this reduces mutliple spaces to 1 and consolidates multiple inputs */
			
			if(ascvalue1==40 && (comment==0)) 		/* if we find a (, increment the parenthesis counter */
				{					
				paren=paren+1;			
				}
			if(ascvalue1==41 && paren!=0 && (comment==0)) 	/* if we find a ), decrement the parenthesis counter */
				{
				paren=paren-1;			
				}				
				
			if(ascvalue1==123 && (comment==0))		/* if we find a { */
				{
				
				strcat(editstring, "\n");		/* newline */		
				brax++;					/* increment nested bracket pair counter */
				for(cx=0; cx<brax; cx++)
					{
					strcat(editstring, "    ");	/* print indentation consistent with number of bracket pairs */
					}
				strncat(editstring, &fullstring[character],1);	/* copy the { character to output */
			
				strcat(editstring, "\n");		/* newline */
				
				for(cx=0; cx<brax; cx++)		/* print same indentation as before */
					{
					strcat(editstring, "    ");
					}
				
				continue;				/* move on to next char of input */
				}
			if((ascvalue1==125) && (brax>0) && (comment==0))/* if we find a } and there is already a { preceeding it */
				{
				strcat(editstring, "\n");		/* newline */
				brax--;					/* decrement counter for number of nested {} */
				for(cx=0; cx<=brax; cx++)		/* print indentation */
					{
					strcat(editstring, "    ");	
					}
				strncat(editstring, &fullstring[character],1 ); /* copy { character to output string */
				strcat(editstring, "\n");		/* new line */
				strcat(editstring, "\n");
			
				for(cx=0; cx<brax; cx++)		/* indent again before next output */
					{
					strcat(editstring, "    ");
					}
				continue;
				}
			if(ascvalue1==47&&ascvalue2==42&&comment==0||ascvalue1==47&&ascvalue2==47&&comment==0) /* put coments on own line \/*/
				{	
				strcat(editstring, "\n ");				/* if comments are open */
				comment=1;				/* increment comment tracker */
				}
			if(ascvalue1==42&&ascvalue2==47&&comment==1)    /* if comments are closed */
				{
				strncat(editstring, &fullstring[character], 1);	/* copy '*' to the output line */
				character++;
				strncat(editstring, &fullstring[character], 1);		/* copy '/' to output line */
				strcat(editstring, " \n ");		/* newline */
				if (ascvalue3!=123)
				{				
				strcat(editstring, " \n ");		/* extra newline if next char not { */
				}

				for(cx=0; cx<=brax; cx++)		/* indent the next line */
					{
					strcat(editstring, "    ");
					} 				/*  then continue (add another to i (for'/') and start from the top) */
				comment=0;
				continue;
				}
			
			if((ascvalue1==70||ascvalue1==102) && (ascvalue2==79|| ascvalue2==111) && (ascvalue3==82 || ascvalue3==114) && (comment==0)) 
			
				{
				controlz++;  	/* if we find an expression (FOR) associated with a control statement */
				
				}
				
			if((ascvalue1==73||ascvalue1==105) && (ascvalue2==70||ascvalue2==102)&& (comment==0)) 
			
				{
				controlz++;	 /* if we find an expression (IF) associated with a control statement */
				
				}
		
			if((ascvalue1==87||ascvalue1==119) && (ascvalue2==72|| ascvalue2==104) && (ascvalue3==73 || ascvalue3==105) && (ascvalue4==76 || ascvalue4==108) && (ascvalue5==69 || ascvalue5==101)&& (comment==0)) 
			
				{
				controlz++;	 /* if we find an expression (WHILE) associated with a control statement */
				
				}
				
			if((ascvalue1==69||ascvalue1==101) && (ascvalue2==76|| ascvalue2==108) && (ascvalue3==83 || ascvalue3==115) && (ascvalue4==69 || ascvalue4==101)&& (comment==0)) 
			
				{
				controlz++;	 /* if we find an expression (ELSE) associated with a control statement */
				
				}
				
			if (controlz>0 && ascvalue1==41 && ascvalue2!= 59 && ascvalue2!=123 &&paren<=0 && (comment==0)) 

						/* if a bracket after a control statement (ignore if semicolon after)*/
			
				{
				strncat(editstring, &fullstring[character],1);  /* copy bracket to output */
				strcat(editstring, "\n");			/* newline */
				for(cx=0; cx<(brax+controlz+1); cx++)		/* indent appropriately */
					{
					strcat(editstring, "    ");
					}
					
				controlz--;					/* control statement over */
				continue;
				
				}
			
			 if (paren<=0 && ascvalue1==59 &&ascvalue2!=125 && (comment==0)) /* if we find a semicolon not within a pair or pairs of () */
			
				{
				strncat(editstring, &fullstring[character],1 );	/* copy semicolon to output */
				strcat(editstring, "\n");			/* newline */
				for(cx=0; cx<(brax); cx++)			/* indent appropriately */	
					{
					strcat(editstring, "    ");
					}
				continue;
				
				}
			 
			
				{							/* if there are no more special triggers */
				strncat(editstring, &fullstring[character],1 );		/* simply copy the output to editstring */
				} 
			}
return; 			/* 	   E N D      F U N C T I O N        */

}



int main()

	{
	
	
	
	starinit();							/* set the star rows up */
	pointerinit(0);							/* set the correct pointers to star rows up for ascii characters */

	printf("Enter C code in one or multiple lines, then hit ENTER again when you are done.\nThe code will be formatted, and output in text and stars.\n");					

	fgets(inputline[1], (sizeof(inputline[1])), stdin);		/* fgets takes the user input from stdin */
	
	
	

	int i = 2; 							/*set i variable (the entry number in the array of lines of text)*/
	
		do
			
			{
				printf("Input line %d of your code. (hit the ENTER) key when you are done\n", i);			/* prompt user to input text */		
			
				fgets(inputline[i], sizeof(inputline[i]), stdin);	/* fgets takes the user input from stdin */
				
				stringcount=i;					/* increase the variable keeping track of string count */
				i++;
				
			}	while (strcmp(inputline[i-1], cond) != 0);	/* stop taking inputs when input == \n */


		
		
			



		printf("\n");
		printf("The output is:\n \n");
		
		strcpy(fullstring, inputline[1]);			/* copy first line of user input into 'fullstring' */

		int j=2; 					
		for (j=2; j<i; j++)
		{			
		strcat(fullstring, inputline[j]);			/* keep adding subsequent user inputs onto the endof 'fullstring' */
	
		
		}

	

		
			

	formatter(0,0,0,0,0,0,0,0,0,0,0,0);				/* format the c code (indentation etc), outputting 'editstring' */
		
			



finalstring = strtok (editstring,"\n");				/* split editstring at the first \n character and copy into 'finalstring' */
  while (finalstring != NULL)					/* loop until strtok hits a NULL character */		
  {					
    strcpy(finalstrings[i], finalstring);	/* copy finalstring segment into the finalstrings[] array  */
    finalstring = strtok(NULL, "\n"); 		/* copy next string until a \n char */
	strcat(finalstrings[i], " ");		/* space to help identify end of line */
    
    stringcount = i;				/* increment stringcount */
    i++;					/* increment i */
  
  }


printlines(0,0,0,0,0,0,0,0);	 /* print finalstrings[1-stringcount in stars with bold and italic fomatting */

printf("The formatted pure text output:\n\n");


i=3;
for (i=3;i<=stringcount;i++)     /* print the finalstrings[] array (formatted correctly in puretext) */
{
printf("%s\n", finalstrings[i]);		/* i set at 3 initially to avoid the empty strings */
}

printf("\n\n");

	return(0);

	}	
