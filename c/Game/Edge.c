#include "head.h"

//tracks the collector of the crates
int lastCollector;

// Create Edges - allocates memory and basic values to the game objects

struct Edges* createEdges(){

	//allocate memory for the object locations structure
	struct Edges * gameEdges = (struct Edges*)malloc(sizeof(struct Edges));
	gameEdges->p1 = (SDL_Rect *)malloc(sizeof(SDL_Rect));
	gameEdges->p2 = (SDL_Rect *)malloc(sizeof(SDL_Rect));
	gameEdges->crate = (SDL_Rect *)malloc(sizeof(SDL_Rect));
	int i=0;
	for (i=0;i<100;i++){
		gameEdges->p1Bullets[i]= (SDL_Rect *)malloc(sizeof(SDL_Rect));
		gameEdges->p2Bullets[i]= (SDL_Rect *)malloc(sizeof(SDL_Rect));
		//set height and width parameters for the bullets
		gameEdges->p1Bullets[i]->w = BULLET_WIDTH;
		gameEdges->p1Bullets[i]->h = BULLET_HEIGHT;
		gameEdges->p2Bullets[i]->w = BULLET_WIDTH;
		gameEdges->p2Bullets[i]->h = BULLET_HEIGHT;
	}
	//set height and width for the players and the crates
	gameEdges->p1->w = PLAYER_WIDTH;
	gameEdges->p1->h = PLAYER_HEIGHT;
	gameEdges->p2->w = PLAYER_WIDTH;
	gameEdges->p2->h = PLAYER_HEIGHT;
	gameEdges->crate->w = 24;
	gameEdges->crate->h = 24;

	return (gameEdges);

}

// Update Edges - updates the locations of the game objects

void updateEdges(struct Edges* gameEdges, struct Player* p1, struct Player* p2, struct Crate * c){

	// update the positions of the objects
	gameEdges->p1->x = p1->xPos - (PLAYER_WIDTH/2.0);
	gameEdges->p1->y = p1->yPos - (PLAYER_HEIGHT/2.0);
	gameEdges->p2->x = p2->xPos - (PLAYER_WIDTH/2.0);
	gameEdges->p2->y = p2->yPos - (PLAYER_HEIGHT/2.0);
	gameEdges->crate->x = c->xPos - (12);
	gameEdges->crate->y = c->yPos - (12);

	int i =0;
	for (i=0;i<100;i++){
		if (p1->bullets[i]->active==1){
			gameEdges->p1Bullets[i]->x = (p1->bullets[i]->xPos - (BULLET_WIDTH/2.0));
			gameEdges->p1Bullets[i]->y = (p1->bullets[i]->yPos - (BULLET_HEIGHT/2.0));
		}

		if (p2->bullets[i]->active==1){
			gameEdges->p2Bullets[i]->x = p2->bullets[i]->xPos - (BULLET_WIDTH/2.0);
			gameEdges->p2Bullets[i]->y = p2->bullets[i]->yPos - (BULLET_HEIGHT/2.0);
		}
	}
}

// Detect Collisions - detects when borders of game objects intersect and handles the collisions

void detectCollisions(struct Edges* gameEdges, struct Player* p1, struct Player* p2, int tick, struct Crate * crate){

	//get information for players armour and shoot power
	float p1Armour = (100.0/p1->armour);
	float p2Armour = (100.0/p2->armour);
	float p1shoot1 = 10.0 * (p1->shoot1 / 1000.0);
	float p2shoot1 = 10.0 * (p2->shoot1 / 1000.0);
	float shoot2 = 20.0;

	//standard bullet collision detection
	int i=0;
	for (i=0;i<100;i++){
		if (p1->bullets[i]->active==1){
			//if a player bullet hits an active crate, reduce the health of the crate
			if(SDL_HasIntersection(gameEdges->p1Bullets[i], gameEdges->crate)){
				p1->bullets[i]->active=0;
				if (crate->hitPoints<(p1shoot1 * 100)){
					crate->hitPoints = 0;
				}
				else{
					crate->hitPoints-=(p1shoot1 * 100);
				}
			}
			//if an active from one player bullet hits the other
			if(SDL_HasIntersection(gameEdges->p1Bullets[i], gameEdges->p2)){
				p1->bullets[i]->active=0;
				if (p2->hitPoints<(p1shoot1 * p2Armour)){
					//reduce player's health
					p2->hitPoints = 0;
				}
				else{
					p2->hitPoints-=(p1shoot1 * p2Armour);
				}
			}
		}
		if (p2->bullets[i]->active==1){
			if(SDL_HasIntersection(gameEdges->p2Bullets[i], gameEdges->crate)){
				p2->bullets[i]->active=0;
				if (crate->hitPoints<(p2shoot1 * 100)){
					crate->hitPoints = 0;
				}
				else{
					crate->hitPoints-=(p2shoot1* 100);
				}
			}
			if(SDL_HasIntersection(gameEdges->p2Bullets[i], gameEdges->p1)){
				p2->bullets[i]->active=0;
				if (p1->hitPoints<(p2shoot1 * p1Armour)){
					p1->hitPoints = 0;
				}
				else{
					p1->hitPoints-=(p2shoot1* p1Armour);
				}
			}
		}
	}
	//close range gun detection (shoot2)

	//calculate the x and y distances between the two players
	double xDist = sqrt((p1->xPos - p2->xPos) * (p1->xPos - p2->xPos));
	double yDist = sqrt((p1->yPos - p2->yPos) * (p1->yPos - p2->yPos));
	//players -> crates
	double crateDist1x = sqrt((p1->xPos - crate->xPos) * (p1->xPos - crate->xPos));
	double crateDist1y = sqrt((p1->yPos - crate->yPos) * (p1->yPos - crate->yPos));
	double crateDist2x = sqrt((p2->xPos - crate->xPos) * (p2->xPos - crate->xPos));
	double crateDist2y = sqrt((p2->yPos - crate->yPos) * (p2->yPos - crate->yPos));

	//cauculate the current damage boundary for the attack
	double explosionRadius = (tick/0.10);

	//if player 1 shoots within range of a crate, reduce health of the crate
	if (p1->shoot2 == 1){
		if (crate->active==1){
			if (crateDist1x<explosionRadius && crateDist1y<explosionRadius){

				if (crate->hitPoints<(shoot2*100)){
					crate->hitPoints = 0;
				}
				else{
					crate->hitPoints-=(shoot2*100);
				}
			}
		}
		//if player 1 shoots within range of player 2, reduce the health of the player
		if (xDist < explosionRadius && yDist < explosionRadius){
			if (p2->hitPoints<(shoot2*p2Armour)){
				p2->hitPoints = 0;
			}
			else{
				p2->hitPoints-=(shoot2*p2Armour);
			}
		}
	}

	if (p2->shoot2 == 1){
		if (crate->active==1){
			if (crateDist2x<explosionRadius && crateDist2y<explosionRadius){
				if (crate->hitPoints<(shoot2*100)){
					crate->hitPoints = 0;
				}
				else{
					crate->hitPoints-=(shoot2*100);
				}
			}
		}

		if (xDist< explosionRadius && yDist< explosionRadius){
			if (p1->hitPoints<(shoot2*p1Armour)){
				p1->hitPoints = 0;
			}
			else{
				p1->hitPoints-=(shoot2*p1Armour);
			}
		}
	}

	//if a player gets the crate, assign a random power-up to the player
	if(SDL_HasIntersection(gameEdges->p1, gameEdges->crate)&&(crate->active==1)){
		crate->hitPoints=0;
		crate->active=0;
		p1->powerUp=((rand() % 3 )+ 1);
		lastCollector = p1->id;
	}

	if(SDL_HasIntersection(gameEdges->p2, gameEdges->crate)&&(crate->active==1)){
		crate->active=0;
		crate->hitPoints=0;
		p2->powerUp=((rand() % 3) + 1);
		lastCollector = p2->id;
	}
}

// Free Edges - De-allocates memory from the game abject boundaries structure
void freeEdges(struct Edges* gameEdges){
	int i = 0 ;
	for (i=0;i<100;i++){
		free(gameEdges->p1Bullets[i]);
		free(gameEdges->p2Bullets[i]);
	}
	free(gameEdges);
}

