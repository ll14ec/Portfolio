#include "head.h"
//used to determine the crateID parameter
int lastCrate = 0;

// Generate Crate - sets up a new crate

int generateCrate(struct Crate* c, float prob){

	if (prob>9990 && c->active==0){
		c->active=1;
		c->id=lastCrate;
		lastCrate++;
		if (lastCrate>20){
			lastCrate-=20;
		}
		c->maxHitPoints = 10000;
		c->hitPoints = 10000;
		return 1;
	}
	return 0;
}

//destroys a generated crate

void destroyCrate(struct Crate* crate){
	if (crate->active==1){
		if (crate->hitPoints <= 0){
			crate->active=0;
		}
	}
}
