/*			POPPIN' SHOTZ - @ll14ec 2016
 *
 *	Game designed by ll14ec. Credit to lazyfoo.net SDL tutorials and opengameart.org for some images.
 *
 *	This is an SDL game written in C.
 *
 *	REQUIREMENTS
 *
 *	The following must be present on the system to run:
 *
 *	SDL2, SDL2_image, gcc compiler
 *
 *	GAME DESCRIPTION
 *
 *	This is a two player 2d shooter game where each player controls a character in a 2d environment.
 *
 *	The object of the game is to use weapons and shields, and movement effectively to kill the opponent.
 *
 *	Each player's hitpoints is showed by the top rectangle, and the game will end when either player's
 *
 *	hitpoints reaches 0.
 *
 *	Each player also has a power bar below hitpoints. This allows players to use special weapons or shields
 *
 *	which are character dependent. These specials will drain this bar. The bar will re-spawn over time, at a
 *
 *	speed that is dictated by the regen stat.
 *
 *	There are 6 different characters to choose from that each have unique stats that all effect the balance
 *
 *	of the fight.
 *
 *	Power-ups are collectible from crates that spawn randomly, in random locations throughout the battle.
 *
 *	GAME CONTROLS
 *
 *	The game uses keyboard input for both players.
 *
 *	P1
 *
 *	Up-Arrow		- Up
 *	Left-Arrow		- Left
 *	Down-Arrow		- Down
 *	Right-Arrow		- Right
 *	ALT-GR			- Standard gun
 *	.				- Close-Ranged Attack
 *	RIGHT-CTRL		- Special Gun/Shield
 *
 *	P2
 *
 *	W				- Up
 *	A				- Left
 *	S				- Down
 *	D				- Right
 *	SHIFT				- Standard gun
 *	\				- Close-Ranged Attack
 *	Z				- Special Gun/Shield
 */

#include "head.h"

//input handling
SDL_Event e;
int quit = 0;
//used to determine which menu/game function to run display
extern int state;
int stateChoice;
//character selection choices of the players
int p1Choice=1;
int p2Choice=1;
//window and renderer
extern SDL_Renderer* gRenderer;
extern SDL_Window* gWindow;
//structures for players, images, crates, and object positions
struct Player* p[2];
struct Textures * textures;
struct Crate * crate;
struct Edges* gameEdges;

//texture and surface for rendering
SDL_Texture* newTexture;
SDL_Surface* loadedSurface;

//rectangles that render player status information
SDL_Rect * p1Health;
SDL_Rect * p2Health;
SDL_Rect * p1Power;
SDL_Rect * p2Power;
//game timing variables
int bgtimer;
int tick;
int gameTick;
int overlayTimer;
int shield1;
int shield2;
double spinAngle;
//array of crate locations [0] = x [1] = y
float crateLocations[2][20];
//used for special functions such as power-ups and crate collection
int powerUp1;
int powerUp2;
int crateID = 0;
extern int lastCollector;





//Main Menu - first function to be called by main, displays the level select screen

void mainMenu(){

	//first reset players from previous game if needed
	resetPlayers(p[1],p[0], p1Choice, p2Choice);
	//reset/initialise some values
	overlayTimer = 0;
	powerUp1=0;
	powerUp2=0;
	gameTick=0;
	crateID = 0;

	//render the background for the main menu
	renderBackground(textures->mainMenu);

	int l=0;
	//generate random locations for crates
	for (l=0;l<20;l++){
		crateLocations[0][l] = (((rand() % 100) + 1) * (SWIDTH-24))/100;	//x
		crateLocations[1][l] = (((rand() % 100) + 1) * (SHEIGHT-24))/100;	//y
	}
	//variables to track the location of a click
	int mouseX = 0;
	int mouseY = 0;
	//rectangles for the images where the user clicks to select level choice
	SDL_Rect * space = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	space->w = 182;
	space->h = 165;
	space->x = 64;
	space->y = 315;

	SDL_Rect * underwater = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	underwater->w = 182;
	underwater->h = 165;
	underwater->x = 315;
	underwater->y = 315;

	SDL_Rect * atom = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	atom->w = 182;
	atom->h = 165;
	atom->x = 555;
	atom->y = 315;

	if( e.type == SDL_MOUSEBUTTONUP ){
		//when the user clicks, get mouse coordinates
		mouseX = e.button.x;
		mouseY = e.button.y;
		//get level from player choice
		if ((mouseX>space->x) && (mouseX<space->x+space->w) && (mouseY>space->y) && (mouseY<space->y+space->h))
		{
			stateChoice =SPACEZONE;
			state = CHARMENU;
		}

		else if ((mouseX>underwater->x) && (mouseX<underwater->x+space->w) && (mouseY>underwater->y) && (mouseY<underwater->y+space->h))
		{
			stateChoice =UNDERWATERZONE;
			state = CHARMENU;
		}

		else if ((mouseX>atom->x) && (mouseX<atom->x+atom->w) && (mouseY>atom->y) && (mouseY<atom->y+atom->h))
		{
			stateChoice =ATOMZONE;
			state = CHARMENU;
		}

	}

	free(space);
	free(underwater);
	free(atom);

}

//Character Select Menu - displayed after the user has selected the level

void charMenu(){

	//render the background for the Character menu
	renderBackground(textures->charMenus[p1Choice][p2Choice]);
	//render the press enter to continue overlay
	renderContinue(textures->overlays[9]);

	//if the user presses enter, go to the game
	if (e.type == SDL_KEYDOWN){
		if(e.key.keysym.sym == SDLK_RETURN){
			state=GAMESTATE;
		}
	}

	int mouseX = 0;
	int mouseY = 0;

	//create rectangles that contain the 3x2 character images

	SDL_Rect * p1 = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	SDL_Rect * p2 = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	p1->w = 95;
	p1->h = 105;
	p1->x = 59;
	p1->y = 147;

	p2->w = 95;
	p2->h = 105;
	p2->x = 434;
	p2->y = 129;

	//if the user clicks, change their choice to the character they choose

	if( e.type == SDL_MOUSEBUTTONUP ){
		//get mouse co-ordinates
		mouseX = e.button.x;
		mouseY = e.button.y;

		if ((mouseX>p1->x) && (mouseX<p1->x+p1->w) && (mouseY>p1->y) && (mouseY<p1->y+p1->h)){
			p1Choice = 1;
			p[1]->choice = 1;
		}
		if ((mouseX>p1->x+p1->w) && (mouseX<p1->x+p1->w +p1->w) && (mouseY>p1->y) && (mouseY<p1->y+p1->h)){
			p1Choice = 2;
			p[1]->choice = 2;
		}
		if ((mouseX>p1->x+p1->w+p1->w) && (mouseX<p1->x+p1->w +p1->w+p1->w) && (mouseY>p1->y) && (mouseY<p1->y+p1->h)){
			p1Choice = 3;
			p[1]->choice = 3;
		}
		if ((mouseX>p1->x) && (mouseX<p1->x+p1->w) && (mouseY>p1->y +p1->h) && (mouseY<p1->y+p1->h +p1->h)){
			p1Choice = 4;
			p[1]->choice = 4;
		}
		if ((mouseX>p1->x+p1->w) && (mouseX<p1->x+p1->w +p1->w) && (mouseY>p1->y +p1->h) && (mouseY<p1->y+p1->h +p1->h)){
			p1Choice = 5;
			p[1]->choice = 5;
		}
		if ((mouseX>p1->x+p1->w+p1->w) && (mouseX<p1->x+p1->w +p1->w+p1->w) && (mouseY>p1->y +p1->h) && (mouseY<p1->y+p1->h +p1->h)){
			p1Choice = 6;
			p[1]->choice = 6;
		}
		if ((mouseX>p2->x) && (mouseX<p2->x+p2->w) && (mouseY>p2->y) && (mouseY<p2->y+p2->h)){
			p2Choice = 1;
			p[0]->choice = 1;
		}
		if ((mouseX>p2->x+p2->w) && (mouseX<p2->x+p2->w +p2->w) && (mouseY>p2->y) && (mouseY<p2->y+p2->h)){
			p2Choice = 2;
			p[0]->choice = 2;
		}
		if ((mouseX>p2->x+p2->w+p2->w) && (mouseX<p2->x+p2->w +p2->w+p2->w) && (mouseY>p2->y) && (mouseY<p2->y+p2->h)){
			p2Choice = 3;
			p[0]->choice = 3;
		}
		if ((mouseX>p2->x) && (mouseX<p2->x+p2->w) && (mouseY>p2->y +p2->h) && (mouseY<p2->y+p2->h +p2->h)){
			p2Choice = 4;
			p[0]->choice = 4;
		}
		if ((mouseX>p2->x+p2->w) && (mouseX<p2->x+p2->w +p2->w) && (mouseY>p2->y +p2->h) && (mouseY<p2->y+p2->h +p2->h)){
			p2Choice = 5;
			p[0]->choice = 5;
		}
		if ((mouseX>p2->x+p2->w+p1->w) && (mouseX<p2->x+p1->w +p1->w+p1->w) && (mouseY>p2->y +p1->h) && (mouseY<p2->y+p1->h +p1->h)){
			p2Choice = 6;
			p[0]->choice = 6;
		}
	}
	//free the rectangles
	free(p1);
	free(p2);

	//final reset of stats/choices/positions before the game commences
	resetPlayers(p[1],p[0], p1Choice, p2Choice);
}

void game(){

	//these variables increments 1 for every frame
	gameTick+=1;
	bgtimer+=1;
	//reset the backround timer if it goes over the size of the images
	if (bgtimer>= 16000){
		bgtimer=0;
	}

	//updates the positions of all game objects
	updateEdges(gameEdges, p[1], p[0], crate);
	//check for collisions of objects
	detectCollisions(gameEdges, p[1], p[0], tick, crate);
	//set the correct colour to render the power and hp bars
	setRendererColour(gRenderer, stateChoice);
	//update the status bars based on the players' stats
	updateStatusBars(p[1], p1Health, p1Power, p[0], p2Health, p2Power, gRenderer);
	//render the correct background and text overlays
	renderBackgroundMoving(textures->backgrounds[stateChoice], bgtimer);
	//extra background layer
	if (stateChoice == SPACEZONE || stateChoice == ATOMZONE ){
		renderBackgroundMoving(textures->backgrounds[stateChoice+3], bgtimer*2);
	}
	//render the lables for the status bars
	renderTextOverlays(textures->overlays[3],textures->overlays[4], textures->overlays[5]);
	//render the players
	if (p[1]->direction==0){
		renderPlayerToScreen(p[1]->xPos, p[1]->yPos, textures->characters[(p1Choice*2)-1]);
	}
	if (p[1]->direction==1){
		renderPlayerToScreen(p[1]->xPos, p[1]->yPos, textures->characters[p1Choice*2]);
	}
	if (p[0]->direction==0){
		renderPlayerToScreen(p[0]->xPos, p[0]->yPos, textures->characters[(p2Choice*2)-1]);
	}
	if (p[0]->direction==1){
		renderPlayerToScreen(p[0]->xPos, p[0]->yPos, textures->characters[p2Choice*2]);
	}

	//start the powerup timer
	if (powerUp1==0 && p[0]->powerUp!=0){
		powerUp1 = gameTick;
	}
	if (powerUp2==0 && p[1]->powerUp!=0){
		powerUp2 = gameTick;
	}
	//after the correct ticks, remove the powerup
	if ( p[0]->powerUp!=0 && powerUp1<gameTick-POWERUP_TIMER){
		p[0]->powerUp =0;
		powerUp1 = 0;
		p[0]->armour = p[0]->maxArmour;
	}
	if (p[1]->powerUp!=0 && powerUp2<gameTick-POWERUP_TIMER){
		p[1]->powerUp =0;
		powerUp2 = 0;
		p[1]->armour = p[1]->maxArmour;
	}

	//move players and objects
	movePlayer(p[0]);
	movePlayer(p[1]);
	moveBullets(p[0]);
	moveBullets(p[1]);
	moveXBullets(p[1], p[0]);
	moveXBullets(p[0], p[1]);

	//render bullets
	renderBulletsToScreen(p[0], textures->weapons[p2Choice]);
	renderBulletsToScreen(p[1], textures->weapons[p1Choice]);
	renderXBulletsToScreen(p[0], textures->weapons[13]);
	renderXBulletsToScreen(p[1], textures->weapons[13]);

	//generate crate (small chance)
	generateCrate(crate, (rand() % 10000 + 1));
	destroyCrate(crate);

	//render power-up text
	if (p[0]->powerUp!=0){
		renderPowerUpOverlay(textures->overlays[6], textures->overlays[7], textures->overlays[8], p[0]);
	}
	if (p[1]->powerUp!=0){
		renderPowerUpOverlay(textures->overlays[6], textures->overlays[7], textures->overlays[8], p[1]);
	}

	//if there has been a new crate spawned reset the power-up timer of the player that got it
	if (crateID != (crate->id)){
		if(lastCollector==1){powerUp1=0;}
		if(lastCollector==2){powerUp2=0;}
	}
	crateID = (crate->id);

	//render the crate based on its hitpoints
	if (crate->active==1){
		if ((crate->hitPoints / crate->maxHitPoints)>0.85){
			renderCrate(textures->scenery[0], crateLocations[0][crate->id], crateLocations[1][crate->id]);
		}
		else if ((crate->hitPoints / crate->maxHitPoints)>0.7){
			renderCrate(textures->scenery[1], crateLocations[0][crate->id], crateLocations[1][crate->id]);
		}
		else if ((crate->hitPoints / crate->maxHitPoints)>0.55){
			renderCrate(textures->scenery[2], crateLocations[0][crate->id], crateLocations[1][crate->id]);
		}
		else if ((crate->hitPoints / crate->maxHitPoints)>0.4){
			renderCrate(textures->scenery[3], crateLocations[0][crate->id], crateLocations[1][crate->id]);
		}
		else if ((crate->hitPoints / crate->maxHitPoints)>0.2){
			renderCrate(textures->scenery[4], crateLocations[0][crate->id], crateLocations[1][crate->id]);
		}
		else if (crate->hitPoints<0){
			crate->active=0;
		}
		crate->xPos = crateLocations[0][crate->id];
		crate->yPos = crateLocations[1][crate->id];
	}

	//restores hitpoints if power-up active
	if (p[0]->powerUp==3){
		if (p[0]->hitPoints < (p[0]->maxHitpoints -3)){
			p[0]->hitPoints+=2;
		}
	}
	if (p[1]->powerUp==3){
		if (p[1]->hitPoints < (p[1]->maxHitpoints -3)){
			p[1]->hitPoints+=2;
		}
	}
	//activates shield if powerup active
	if (p[0]->powerUp==2){
		shield1++;
		if (shield1>50){
			shield1=0;
		}
		freeShield(p[0], shield1, textures->weapons[20]);
	}
	if (p[1]->powerUp==2){
		shield2++;
		if (shield2>50){
			shield2=0;
		}
		freeShield(p[1], shield2, textures->weapons[20]);
	}

	//generate shields from powerup
	if (shield(p[0], shield1, textures->weapons[20])){
		shield1++;
		if (shield1>50){
			shield1=0;
			p[0]->armour=p[0]->maxArmour;
			p[0]->shield=0;
		}

	}
	if (shield(p[1], shield2, textures->weapons[20])){
		shield2++;
		if (shield2>50){
			shield2=0;
			p[1]->armour=p[1]->maxArmour;
			p[1]->shield=0;
		}

	}
	//increment the spinangle variable
	spinAngle+=15;
	if (spinAngle>=360){
		spinAngle=0;
	}

	//render the close range attack to the screen
	if(p[0]->shoot2 == 1){
		if (tick<10){
			renderExplosionToScreen(p[0], textures->weapons[p2Choice+6], tick);
			tick++;
		}
		else{
			tick = 0;
			p[0]->shoot2=0;
		}
	}

	if(p[1]->shoot2 == 1){
		if (tick<10){
			renderExplosionToScreen(p[1], textures->weapons[p1Choice+6], tick);
			tick++;
		}
		else{
			tick = 0;
			p[1]->shoot2=0;
		}
	}
	/*
	if ((rand()% 100) > 40){

		teleport(p[0], p[1]);

	}
	 */

	//check the players stats for the bars
	statusCheck(p[1], p[0]);

	//draw the status bars
	SDL_RenderDrawRect(gRenderer, p1Health);
	SDL_RenderDrawRect(gRenderer, p2Health);
	SDL_RenderDrawRect(gRenderer, p1Power);
	SDL_RenderDrawRect(gRenderer, p2Power);
	//check for death
	if (isPlayerDead(p[1],p[0], textures, overlayTimer)){
		overlayTimer++;
	}

}

void tidy(){
	SDL_DestroyTexture(newTexture);
	free(newTexture);
	free(loadedSurface);
	free(p1Health);
	free(p1Power);
	free(p2Health);
	free(p2Power);
	free(crate);
	freeEdges(gameEdges);
	clearTextures(textures);
	clearRender();
	clearPlayers(p[0], p[1]);
}

int main(){
	//init sdl
	init();
	//start at the main menu
	state=MAINMENU;
	//create players
	p[0] = createPlayer(1);
	p[1] = createPlayer(2);

	//create object positions
	gameEdges = createEdges();
	//create memory for a crate and the players' ammunition
	crate =  (struct Crate*)malloc(sizeof(struct Crate));
	crate->active=0;
	crate->xPos=1;
	crate->yPos=1;
	crate->hitPoints = 10000;

	makeBullets(p[1]);
	makeBullets(p[0]);
	makeXBullets(p[0]);
	makeXBullets(p[1]);
	//create the rectangles for the health and power bars
	p1Health = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	p2Health = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	p1Power = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	p2Power = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	//set up the health and power bars
	setUpStatusBars(p1Health, p1Power, p2Health, p2Power);
	//load the texture images from file
	textures = loadTextures(newTexture, loadedSurface);
	//initialise/reset variables
	bgtimer = 0;
	tick = 0;
	spinAngle = 0;
	shield1=0;
	shield2=0;

	while( !quit ){
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 ){
			//User requests quit
			if( e.type == SDL_QUIT ){
				quit = 1;
			}
			//Handle input for players 1 and 2
			react(e, p[0], p[1]);
			SDL_Delay(10);
		}

		//Clear screen
		SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
		SDL_RenderClear( gRenderer );

		if (state==MAINMENU){								//main menu
			mainMenu();
		}

		if (state ==CHARMENU){								//character select menu
			charMenu();
		}


		if (state==GAMESTATE){								//main game state
			game();
		}

		//Update screen
		SDL_RenderPresent( gRenderer );

	}

	//free memory
	tidy();

	SDL_Quit();

	return(0);
}
