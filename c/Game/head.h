#include <stdlib.h>
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <stdio.h>
#include "math.h"
#include "time.h"
#include "stdbool.h"

/*
 * STRUCT DEFINITIONS - COLLECTED DATA FOR GAME ENTITIES
 */

/*
 * BULLET - Used to render bullets to the screen
 */

struct Bullet{
	float xPos;
	float yPos;
	int height;
	int width;
	float xV;
	float yV;
	int active;
};

/*
 * CRATE - Used to render crates to the screen
 */

struct Crate{
	float xPos;
	float yPos;
	int height;
	int width;
	int active;
	float hitPoints;
	int maxHitPoints;
	int id;
};


/*
 * XBULLET - used for targeted bullets
 */

struct XBullet{
	float xPos;
	float yPos;
	int height;
	int width;
	float xV;
	float yV;
	int active;
};

/*
 * 	PLAYER - used to render the player to screen and collate the players' state
 */

struct Player{
	float xPos;
	float yPos;
	float xVelocity;
	float yVelocity;
	float speed;
	float hitPoints;
	float power;
	float regen;
	int height;
	int width;
	int lastXVelocity;
	int lastYVelocity;
	int direction;
	float armour;
	float maxArmour;
	int maxHitpoints;
	int maxPower;
	struct Bullet * bullets[100];
	struct XBullet * xBullets[15];
	int shoot1;
	int shoot2;
	int shield;
	int choice;
	int powerUp;
	int id;
};

/*
 *  EDGES - used for collision detection of in-game objects
 */

struct Edges{
	SDL_Rect * p1;
	SDL_Rect * p2;
	SDL_Rect * p1Bullets[100];
	SDL_Rect * p2Bullets[100];
	SDL_Rect * crate;
};

/*
 *  TEXTURES - used for saving all rendered textures in an organised way
 */

struct Textures{
	SDL_Texture * backgrounds [12];
	SDL_Texture * charMenus[7][7];
	SDL_Texture * characters[13];
	SDL_Texture * mainMenu;
	SDL_Texture * weapons[24];
	SDL_Texture * overlays[10];
	SDL_Texture * scenery[10];
};

/*
 *  FUNCTION PROTOTYPES --->
 *
 *
 * LOAD/INITIALISE FUNCTIONS
 *
 */

void init();
SDL_Texture * loadTexture(const char *, SDL_Texture*, SDL_Surface*);
struct Textures * loadTextures(SDL_Texture*, SDL_Surface*);

/*
 * RENDER FUNCTIONS
 */

void renderTextureToScreen();
void renderPlayerToScreen(int, int, SDL_Texture*);
void renderBulletsToScreen(struct Player*, SDL_Texture *);
void renderXBulletsToScreen(struct Player*, SDL_Texture *);
void renderBackgroundMoving(SDL_Texture* , int );
void renderExplosionToScreen(struct Player*, SDL_Texture *, int);
void renderBackground(SDL_Texture* );
void renderOverlay(SDL_Texture *);
void renderTextOverlays(SDL_Texture *, SDL_Texture *, SDL_Texture *);
void setRendererColour(SDL_Renderer *, int);
void renderCrate(SDL_Texture *, float, float);
void renderPowerUpOverlay(SDL_Texture *, SDL_Texture *,  SDL_Texture *,struct Player *);
void renderShield (struct Player* , int, SDL_Texture *);
void renderContinue(SDL_Texture*);

/*
 * SHOOT FUNCTIONS
 */

void shootX(struct Player*);
void shoot(struct Player*);
void shoot2(struct Player*);
int shield(struct Player *, int timer, SDL_Texture *);
int freeShield(struct Player *, int, SDL_Texture *);

/*
 *  MEMORY ALLOCATION FUNCTIONS
 */

struct Player* createPlayer(int);
void makeBullets(struct Player*);
struct Edges* createEdges();
void createBulletEdges(struct Edges*);
void makeXBullets(struct Player*);
int generateCrate(struct Crate* , float);

/*
 *  MOVE OBJECT FUNCTIONS
 */

void movePlayer(struct Player*);
void movePlayer(struct Player*);
void moveBullet(struct Bullet*);
void moveBullets(struct Player*);
void moveXBullet(struct XBullet*, struct Player*);
void moveXBullets(struct Player*, struct Player*);

/*
 *  STATE-REALATED FUNCTIONS
 */

void react(SDL_Event, struct Player *, struct Player*);
void updateEdges(struct Edges*, struct Player *, struct Player*, struct Crate *);
void detectCollisions(struct Edges*, struct Player*, struct Player*, int, struct Crate*);
int getInput(SDL_Event);
void updateStatusBars(struct Player*, SDL_Rect *, SDL_Rect *, struct Player*, SDL_Rect *, SDL_Rect*, SDL_Renderer *);
void setUpStatusBars(SDL_Rect *, SDL_Rect *, SDL_Rect *, SDL_Rect *);
void statusCheck(struct Player*, struct Player*);
void normaliseHp(struct Player*, struct Player*);
int isPlayerDead(struct Player*, struct Player*, struct Textures *, int);
void resetPlayers(struct Player* p1, struct Player * p2, int, int);
void damage(struct Player*, int, int);
void reducePower(struct Player*, int);

/*
 *  DESTROY/FREE FUNCTIONS
 */

void destroyCrate(struct Crate* );
void close(SDL_Texture*);
void freeEdges(struct Edges *);
void clearRender();
void clearPlayers(struct Player*, struct Player*);
void clearTextures(struct Textures*);

/*
 * CONSTANTS --->
 *
 * Used instead of numeric constants for easy alterations and code legibilty
 *
 */

//screen dimensions
#define SHEIGHT 600
#define SWIDTH 800

//player/projectile dimensions
#define PLAYER_HEIGHT 45
#define PLAYER_WIDTH  45
#define BULLET_HEIGHT 6
#define BULLET_WIDTH  6

//projectile speeds
#define BULLET_SPEED 2
#define XBULLET_SPEED 8

//reductive constants
#define SHOOT3DAMAGE 200
#define SHOOT3POWER 6000
#define SHIELDPOWER 420
#define SHOOT2POWER 2350

//timing constants
#define POWERUP_TIMER 420

//input handling constants
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
#define STOP_UP 4
#define STOP_DOWN 5
#define STOP_LEFT 6
#define STOP_RIGHT 7
#define SHOOT1 8
#define XUP 9
#define XDOWN 10
#define XLEFT 11
#define XRIGHT 12
#define XSTOP_UP 13
#define XSTOP_DOWN 14
#define XSTOP_LEFT 15
#define XSTOP_RIGHT 16
#define XSHOOT1 17
#define SHOOT2 18
#define XSHOOT2 19
#define SHOOT3 20
#define XSHOOT3 21

//state definition constants
#define MAINMENU 0
#define GAMESTATE 1
#define SPACEZONE 1
#define UNDERWATERZONE 2
#define ATOMZONE 3
#define CHARMENU 4
