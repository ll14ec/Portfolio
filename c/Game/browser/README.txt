 *	This is the browser version of the game. 
 *
 *	It can be loaded in mozilla firefox on any platform that can run JavaScript.
 *
 *	INSTRUCTIONS
 *
 *	1) Unzip the file PoppinShotz.zip
 *
 *	2) Run the file generated with the .html extension in FireFox. 
 *
 *	3) Wait a few seconds for the game to load.
 *
 *
 *	GAME CONTROLS
 *
 *	The game uses keyboard input for both players.
 *
 *	P1
 *
 *	Up			- Up
 *	Left			- Left
 *	Down			- Down
 *	Right			- Right
 *	,			- Standard gun
 *	.			- Close-Ranged Attack
 *	RIGHT-CTRL		- Special Gun/Shield
 *
 *	P2
 *
 *	W			- Up
 *	A			- Left
 *	S			- Down
 *	D			- Right
 *	SHIFT			- Standard gun
 *	\			- Close-Ranged Attack
 *	Z			- Special Gun/Shield
