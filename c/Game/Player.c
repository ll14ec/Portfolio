#include "head.h"

// Create Player - allocates memory to the player's native type members and sets up initial values

struct Player* createPlayer(int id){

	struct Player * player = (struct Player*)malloc(sizeof(struct Player));
	player->width = 20;
	player->height = 20;
	player->xVelocity = 0;
	player->yVelocity = 0;
	player->xPos = 0;
	player->yPos = 0;
	player->shoot2 = 0;
	player->speed = 10;
	player->direction = 1;
	player->hitPoints = 1000;
	player->maxHitpoints = 1000;
	player->maxPower = 10000;
	player->power = 1000;
	player->armour = 10;
	player->regen = 1000;
	player->shoot1 = 1000;
	player->choice=1;
	player->maxArmour=10;
	player->powerUp=0;
	player->id = id;
	player->shield = 0;
	return player;
}

// Reset Players  - sets the players to their standard stats, based on character choice

void resetPlayers(struct Player* p1, struct Player * p2, int p1Char, int p2Char ){

	switch (p1Char){
	case 1:
		p1->xVelocity = 0;
		p1->yVelocity = 0;
		p1->xPos = 120;
		p1->yPos = 150;
		p1->shoot2 = 0;
		p1->speed = 7;
		p1->direction = 1;
		p1->hitPoints = 800;
		p1->maxHitpoints = 800;
		p1->power = 10000;
		p1->direction = 0;
		p1->armour = 100;
		p1->regen = 600;
		p1->shoot1 = 800;
		p1->maxArmour=100;
		break;
	case 2:
		p1->xVelocity = 0;
		p1->yVelocity = 0;
		p1->xPos = 120;
		p1->yPos = 150;
		p1->shoot2 = 0;
		p1->speed = 8;
		p1->direction = 1;
		p1->hitPoints = 700;
		p1->maxHitpoints = 700;
		p1->power = 10000;
		p1->direction = 0;
		p1->armour = 75;
		p1->regen = 700;
		p1->shoot1 = 1000;
		p1->maxArmour=75;
		break;
	case 3:
		p1->xVelocity = 0;
		p1->yVelocity = 0;
		p1->xPos = 120;
		p1->yPos = 150;
		p1->shoot2 = 0;
		p1->speed = 7;
		p1->direction = 1;
		p1->hitPoints = 800;
		p1->maxHitpoints = 800;
		p1->power = 10000;
		p1->direction = 0;
		p1->armour = 75;
		p1->regen = 1000;
		p1->shoot1 = 700;
		p1->maxArmour=75;
		break;
	case 4:
		p1->xVelocity = 0;
		p1->yVelocity = 0;
		p1->xPos = 120;
		p1->yPos = 150;
		p1->shoot2 = 0;
		p1->speed = 8;
		p1->direction = 1;
		p1->hitPoints = 800;
		p1->maxHitpoints = 800;
		p1->power = 10000;
		p1->direction = 0;
		p1->armour = 87.5;
		p1->regen = 600;
		p1->shoot1 = 800;
		p1->maxArmour=87.5;
		break;
	case 5:
		p1->xVelocity = 0;
		p1->yVelocity = 0;
		p1->xPos = 120;
		p1->yPos = 150;
		p1->shoot2 = 0;
		p1->speed = 5;
		p1->direction = 1;
		p1->hitPoints = 1000;
		p1->maxHitpoints = 1000;
		p1->power = 10000;
		p1->direction = 0;
		p1->armour = 87.5;
		p1->regen = 800;
		p1->shoot1 = 700;
		p1->maxArmour=87.5;
		break;
	case 6:
		p1->xVelocity = 0;
		p1->yVelocity = 0;
		p1->xPos = 120;
		p1->yPos = 150;
		p1->shoot2 = 0;
		p1->speed = 10;
		p1->direction = 1;
		p1->hitPoints = 700;
		p1->maxHitpoints = 700;
		p1->power = 10000;
		p1->direction = 0;
		p1->armour = 75;
		p1->regen = 1000;
		p1->shoot1 = 600;
		p1->maxArmour=75;
		break;
	}

	switch (p2Char){
	case 1:
		p2->xVelocity = 0;
		p2->yVelocity = 0;
		p2->xPos = 640;
		p2->yPos = 440;
		p2->shoot2 = 0;
		p2->speed = 7;
		p2->direction = 1;
		p2->hitPoints = 800;
		p2->maxHitpoints = 800;
		p2->power = 10000;
		p2->direction = 1;
		p2->armour = 100;
		p2->regen = 600;
		p2->shoot1 = 800;
		p2->maxArmour=100;
		break;
	case 2:
		p2->xVelocity = 0;
		p2->yVelocity = 0;
		p2->xPos = 640;
		p2->yPos = 440;
		p2->shoot2 = 0;
		p2->speed = 8;
		p2->direction = 1;
		p2->hitPoints = 700;
		p2->maxHitpoints = 700;
		p2->power = 10000;
		p2->direction = 1;
		p2->armour = 75;
		p2->regen = 700;
		p2->shoot1 = 1000;
		p2->maxArmour=75;
		break;
	case 3:
		p2->xVelocity = 0;
		p2->yVelocity = 0;
		p2->xPos = 640;
		p2->yPos = 440;
		p2->shoot2 = 0;
		p2->speed = 7;
		p2->direction = 1;
		p2->hitPoints = 800;
		p2->maxHitpoints = 800;
		p2->power = 10000;
		p2->direction = 1;
		p2->armour = 75;
		p2->regen = 1000;
		p2->shoot1 = 700;
		p2->maxArmour=75;
		break;
	case 4:
		p2->xVelocity = 0;
		p2->yVelocity = 0;
		p2->xPos = 640;
		p2->yPos = 440;
		p2->shoot2 = 0;
		p2->speed = 8;
		p2->direction = 1;
		p2->hitPoints = 800;
		p2->maxHitpoints = 800;
		p2->power = 10000;
		p2->direction = 1;
		p2->armour = 87.5;
		p2->regen = 600;
		p2->shoot1 = 800;
		p2->maxArmour=87.5;
		break;
	case 5:
		p2->xVelocity = 0;
		p2->yVelocity = 0;
		p2->xPos = 640;
		p2->yPos = 440;
		p2->shoot2 = 0;
		p2->speed = 5;
		p2->direction = 1;
		p2->hitPoints = 1000;
		p2->maxHitpoints = 1000;
		p2->power = 10000;
		p2->direction = 1;
		p2->armour = 87.5;
		p2->regen = 800;
		p2->shoot1 = 700;
		p2->maxArmour=87.5;
		break;
	case 6:
		p2->xVelocity = 0;
		p2->yVelocity = 0;
		p2->xPos = 640;
		p2->yPos = 440;
		p2->shoot2 = 0;
		p2->speed = 10;
		p2->direction = 1;
		p2->hitPoints = 700;
		p2->maxHitpoints = 700;
		p2->power = 10000;
		p2->direction = 1;
		p2->armour = 75;
		p2->regen = 1000;
		p2->shoot1 = 600;
		p1->maxArmour=75;
		break;
	}
}

// Move Player - changes a player's x y position based on the current velocity

void movePlayer(struct Player * p){

	p->xPos += p->xVelocity * (p->speed/10.0);
	p->yPos += p->yVelocity * (p->speed/10.0);

	if (p->xVelocity > 0){
		p->direction = 0;
	}
	if (p->xVelocity < 0){
		p->direction = 1;
	}

	if (p->xVelocity!=0 && p->yVelocity!=0) {
		p->lastXVelocity = p->xVelocity;
		p->lastYVelocity = p->yVelocity;
	}
	if (p->xVelocity==0 && p->yVelocity!=0) {
		p->lastYVelocity = p->yVelocity;
		p->lastXVelocity = 0;
	}
	if (p->xVelocity!=0 && p->yVelocity==0) {
		p->lastXVelocity = p->xVelocity;
		p->lastYVelocity = 0;
	}

	if (p->xPos<0){
		p->xPos = 0;
	}
	if (p->yPos<0){
		p->yPos = 0;
	}

	if (p->xPos>SWIDTH-PLAYER_WIDTH){
		p->xPos = SWIDTH-PLAYER_WIDTH;
	}
	if (p->yPos>SHEIGHT-PLAYER_HEIGHT){
		p->yPos = SHEIGHT-PLAYER_HEIGHT;
	}
}

// Shoot - activates bullets when a player shoots - 2 modes, standard and shotgun

void shoot(struct Player* p){
	//if standard gun
	if (p->powerUp!=1){
		int i=1;
		int j=0;
		//find first non-active bullet slot
		while (i==p->bullets[j]->active){
			j++;
		}
		//activate bullet and set statistics based on player velocity/player's last velocity
		p->bullets[j]->active = 1;
		p->bullets[j]->xPos=p->xPos;
		p->bullets[j]->yPos=p->yPos;
		p->bullets[j]->yV=(p->yVelocity * 2);
		p->bullets[j]->xV=(p->xVelocity * 2);

		if (p->xVelocity == 0 && p->yVelocity == 0){
			p->bullets[j]->xV = p->lastXVelocity * 2;
			p->bullets[j]->yV = p->lastYVelocity * 2;
		}
	}
	//if shotgun
	if (p->powerUp==1){
		int k=0;
		//4 bullets per shot
		for (k=0;k<4;k++){
			int i=1;
			int j=0;
			//find next inactive bullet
			while (i==p->bullets[j]->active){
				j++;
			}
			//activate bullet
			p->bullets[j]->active = 1;
			//assign random values within an acceptable range for velocity/position to create spread effect
			p->bullets[j]->xPos=p->xPos + (rand() % 16) - (rand() % 16);
			p->bullets[j]->yPos=p->yPos + (rand() % 16) - (rand() % 16);
			p->bullets[j]->yV=(p->yVelocity * 2)+ ((rand() % 16)/10) - ((rand() % 16)/10);
			p->bullets[j]->xV=(p->xVelocity * 2)+ ((rand() % 16)/10) - ((rand() % 16)/10);

			if (p->xVelocity == 0 && p->yVelocity == 0){
				p->bullets[j]->xV = (p->lastXVelocity * (2))+ ((rand() % 16)/10) - ((rand() % 16)/10);
				p->bullets[j]->yV = (p->lastYVelocity * (2))+ ((rand() % 16)/10) - ((rand() % 16)/10);
			}
		}
	}
}

// ShootX  - Shoots a home-in missile or activates a shield based on character choice

void shootX(struct Player* p){

	int i=1;
	int j=0;
	// if characters 4, 2, or 6
	if (p->choice == 4 || p->choice == 2 || p->choice == 6){
		//if the player has enough energy
		if (p->power>=SHOOT3POWER){
			//find first de-activated bullet
			while (i==p->xBullets[j]->active){
				j++;
			}
			//actiate and assign velocity and position
			p->xBullets[j]->active = 1;
			p->xBullets[j]->xPos=p->xPos;
			p->xBullets[j]->yPos=p->yPos;
			p->xBullets[j]->yV=(p->yVelocity * 2);
			p->xBullets[j]->xV=(p->xVelocity * 2);

			if (p->xVelocity == 0 && p->yVelocity == 0){
				p->xBullets[j]->xV = p->lastXVelocity * 2;
				p->xBullets[j]->yV = p->lastYVelocity * 2;
			}
			//reduce player's power
			reducePower(p, SHOOT3);
		}
	}
	// if character 1, 3 or 5
	else {
		//if player has enough power, activate shield
		if (p->power >= SHIELDPOWER){
			p->shield=1;
		}
	}
}

// Shoot2 - Activates a close ranged attack

void shoot2(struct Player *p){
	//if player has enough power, activate the shoot2 member and reduce power
	if (p->power >= SHOOT2POWER){
		p->shoot2 = 1;
		p->power-=SHOOT2POWER;
	}
}

// Shield - Activates a shield for the player

int shield(struct Player * p, int timer, SDL_Texture * t){

	//if shield is deactivated, return 0
	if(p->shield==0){
		return 0;
	}
	//if shield is activated, increase armour, render shield and reduce power, return 1
	else{
		p->armour=420;
		renderShield(p, timer, t);
		p->power-=33;
		return 1;
	}
}

// Shield - Activates a shield for the player, uses no power (given from crates)

int freeShield(struct Player * p, int timer, SDL_Texture * t){

	//if shield is deactivated, return 0
	if(p->shield==0 && p->powerUp == 0){
		return 0;
	}
	//if shield is activated, increase armour, render shield and return 1
	if (p->shield==1 || p->powerUp==2){
		p->armour=420;
		renderShield(p, timer, t);
	}
	return 1;
}

void clearPlayers(struct Player* p1, struct Player* p2){

	int i  = 0;
	for (i=0;i<100;i++){
		free (p1->bullets[i]);
		free (p2->bullets[i]);
	}
	for (i=0;i<15;i++){
		free (p1->xBullets[i]);
		free (p2->xBullets[i]);
	}
	free (p1);
	free (p2);
}
