#include "head.h"

//external from Main.c
SDL_Renderer* gRenderer;

// Load Texture - Loads a file, creates a surface, creates a texture from the surface
//
// This function uses modified code that was found in the lazyfoo.net tutorials on SDL2

SDL_Texture * loadTexture(const char * path, SDL_Texture* newTexture,SDL_Surface* loadedSurface){

	newTexture = NULL;
	//load image into surface
	loadedSurface = IMG_Load(path);
	if( loadedSurface == NULL ){
		printf( "Image %s failed to load! \nError: %s\n", path, IMG_GetError() );
	}
	else{
		//Colour key image
		//SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL ){
			printf( "Texture creation from %s failed! \nError: \n %s\n", path, SDL_GetError() );
		}
		//free the loaded surface
		SDL_FreeSurface( loadedSurface );
	}
	return newTexture;

}



// Load Textures - loads all of the games textures

struct Textures * loadTextures( SDL_Texture* newTexture,SDL_Surface* loadedSurface){

	//allocate the memory for the textures
	struct Textures* textures =(struct Textures*)malloc(sizeof(struct Textures));

	//load main menu, overlays, scenery, backgrounds, menus, characters, weapons,
	textures->mainMenu    = loadTexture("Img/menus/mainmenu.png", newTexture, loadedSurface);
	textures->overlays[0] = loadTexture("Img/menus/P1.png", newTexture, loadedSurface);
	textures->overlays[1] = loadTexture("Img/menus/P2.png", newTexture, loadedSurface);
	textures->overlays[2] = loadTexture("Img/menus/tie.png", newTexture, loadedSurface);
	textures->overlays[3] = loadTexture("Img/menus/P1HP.png", newTexture, loadedSurface);
	textures->overlays[4] = loadTexture("Img/menus/P2HP.png", newTexture, loadedSurface);
	textures->overlays[5] = loadTexture("Img/menus/power.png", newTexture, loadedSurface);
	textures->overlays[6] = loadTexture("Img/menus/shotgun.png", newTexture, loadedSurface);
	textures->overlays[7] = loadTexture("Img/menus/shield.png", newTexture, loadedSurface);
	textures->overlays[8] = loadTexture("Img/menus/hp.png", newTexture, loadedSurface);
	textures->overlays[9] = loadTexture("Img/menus/continue.png", newTexture, loadedSurface);

	textures->scenery[0] = loadTexture("Img/scenery/c1.png", newTexture, loadedSurface);
	textures->scenery[1] = loadTexture("Img/scenery/c2.png", newTexture, loadedSurface);
	textures->scenery[2] = loadTexture("Img/scenery/c3.png", newTexture, loadedSurface);
	textures->scenery[3] = loadTexture("Img/scenery/c4.png", newTexture, loadedSurface);
	textures->scenery[4] = loadTexture("Img/scenery/c5.png", newTexture, loadedSurface);

	textures->backgrounds[1] = loadTexture("Img/backgrounds/space.png", newTexture, loadedSurface);
	textures->backgrounds[2] = loadTexture("Img/backgrounds/sea.png", newTexture, loadedSurface);
	textures->backgrounds[3] = loadTexture("Img/backgrounds/atom(1).png", newTexture, loadedSurface);
	textures->backgrounds[4] = loadTexture("Img/backgrounds/space2.png", newTexture, loadedSurface);
	//textures->backgrounds[6] = loadTexture("Img/backgrounds/atom(1).png");

	textures->charMenus[1][1]=loadTexture("Img/menus/cs11.png", newTexture, loadedSurface);
	textures->charMenus[1][2]=loadTexture("Img/menus/cs12.png", newTexture, loadedSurface);
	textures->charMenus[1][3]=loadTexture("Img/menus/cs13.png", newTexture, loadedSurface);
	textures->charMenus[1][4]=loadTexture("Img/menus/cs14.png", newTexture, loadedSurface);
	textures->charMenus[1][5]=loadTexture("Img/menus/cs15.png", newTexture, loadedSurface);
	textures->charMenus[1][6]=loadTexture("Img/menus/cs16.png", newTexture, loadedSurface);

	textures->charMenus[2][1]=loadTexture("Img/menus/cs21.png", newTexture, loadedSurface);
	textures->charMenus[2][2]=loadTexture("Img/menus/cs22.png", newTexture, loadedSurface);
	textures->charMenus[2][3]=loadTexture("Img/menus/cs23.png", newTexture, loadedSurface);
	textures->charMenus[2][4]=loadTexture("Img/menus/cs24.png", newTexture, loadedSurface);
	textures->charMenus[2][5]=loadTexture("Img/menus/cs25.png", newTexture, loadedSurface);
	textures->charMenus[2][6]=loadTexture("Img/menus/cs26.png", newTexture, loadedSurface);

	textures->charMenus[3][1]=loadTexture("Img/menus/cs31.png", newTexture, loadedSurface);
	textures->charMenus[3][2]=loadTexture("Img/menus/cs32.png", newTexture, loadedSurface);
	textures->charMenus[3][3]=loadTexture("Img/menus/cs33.png", newTexture, loadedSurface);
	textures->charMenus[3][4]=loadTexture("Img/menus/cs34.png", newTexture, loadedSurface);
	textures->charMenus[3][5]=loadTexture("Img/menus/cs35.png", newTexture, loadedSurface);
	textures->charMenus[3][6]=loadTexture("Img/menus/cs36.png", newTexture, loadedSurface);

	textures->charMenus[4][1]=loadTexture("Img/menus/cs41.png", newTexture, loadedSurface);
	textures->charMenus[4][2]=loadTexture("Img/menus/cs42.png", newTexture, loadedSurface);
	textures->charMenus[4][3]=loadTexture("Img/menus/cs43.png", newTexture, loadedSurface);
	textures->charMenus[4][4]=loadTexture("Img/menus/cs44.png", newTexture, loadedSurface);
	textures->charMenus[4][5]=loadTexture("Img/menus/cs45.png", newTexture, loadedSurface);
	textures->charMenus[4][6]=loadTexture("Img/menus/cs46.png", newTexture, loadedSurface);

	textures->charMenus[5][1]=loadTexture("Img/menus/cs51.png", newTexture, loadedSurface);
	textures->charMenus[5][2]=loadTexture("Img/menus/cs52.png", newTexture, loadedSurface);
	textures->charMenus[5][3]=loadTexture("Img/menus/cs53.png", newTexture, loadedSurface);
	textures->charMenus[5][4]=loadTexture("Img/menus/cs54.png", newTexture, loadedSurface);
	textures->charMenus[5][5]=loadTexture("Img/menus/cs55.png", newTexture, loadedSurface);
	textures->charMenus[5][6]=loadTexture("Img/menus/cs56.png", newTexture, loadedSurface);					/// NEEDS FIXING

	textures->charMenus[6][1]=loadTexture("Img/menus/cs61.png", newTexture, loadedSurface);
	textures->charMenus[6][2]=loadTexture("Img/menus/cs62.png", newTexture, loadedSurface);
	textures->charMenus[6][3]=loadTexture("Img/menus/cs63.png", newTexture, loadedSurface);
	textures->charMenus[6][4]=loadTexture("Img/menus/cs64.png", newTexture, loadedSurface);
	textures->charMenus[6][5]=loadTexture("Img/menus/cs65.png", newTexture, loadedSurface);
	textures->charMenus[6][6]=loadTexture("Img/menus/cs66.png", newTexture, loadedSurface);

	textures->characters[1] = loadTexture("Img/characters/dubmarineR.png", newTexture, loadedSurface);
	textures->characters[2] = loadTexture("Img/characters/dubmarineL.png", newTexture, loadedSurface);

	textures->characters[3] = loadTexture("Img/characters/spacemanR.png", newTexture, loadedSurface);
	textures->characters[4] = loadTexture("Img/characters/spacemanL.png", newTexture, loadedSurface);

	textures->characters[5] = loadTexture("Img/characters/ammoniteR.png", newTexture, loadedSurface);
	textures->characters[6] = loadTexture("Img/characters/ammoniteL.png", newTexture, loadedSurface);

	textures->characters[7] = loadTexture("Img/characters/dragonR.png", newTexture, loadedSurface);
	textures->characters[8] = loadTexture("Img/characters/dragonL.png", newTexture, loadedSurface);

	textures->characters[9] = loadTexture("Img/characters/tangleL.png", newTexture, loadedSurface);
	textures->characters[10] = loadTexture("Img/characters/tangleL.png", newTexture, loadedSurface);

	textures->characters[11] = loadTexture("Img/characters/electronR.png", newTexture, loadedSurface);
	textures->characters[12] = loadTexture("Img/characters/electronL.png", newTexture, loadedSurface);

	textures->weapons[1] = loadTexture("Img/weapons/standard.png", newTexture, loadedSurface);
	textures->weapons[2] = loadTexture("Img/weapons/laser.png", newTexture, loadedSurface);
	textures->weapons[3] = loadTexture("Img/weapons/blue.png", newTexture, loadedSurface);
	textures->weapons[4] = loadTexture("Img/weapons/fire.png", newTexture, loadedSurface);
	textures->weapons[5] = loadTexture("Img/weapons/green.png", newTexture, loadedSurface);
	textures->weapons[6] = loadTexture("Img/weapons/e.png", newTexture, loadedSurface);
	textures->weapons[7] = loadTexture("Img/weapons/redboom.png", newTexture, loadedSurface);
	textures->weapons[8] = loadTexture("Img/weapons/alienclose.png", newTexture, loadedSurface);
	textures->weapons[9] = loadTexture("Img/weapons/sonar.png", newTexture, loadedSurface);
	textures->weapons[10] = loadTexture("Img/weapons/fireclose.png", newTexture, loadedSurface);
	textures->weapons[11] = loadTexture("Img/weapons/slime.png", newTexture, loadedSurface);
	textures->weapons[12] = loadTexture("Img/weapons/electronclose.png", newTexture, loadedSurface);
	textures->weapons[13] = loadTexture("Img/weapons/torpedo.png", newTexture, loadedSurface);
	textures->weapons[20] = loadTexture("Img/weapons/shield.png", newTexture, loadedSurface);
	return textures;
}

// Clear Textures - frees memory used by the textures structure

void clearTextures(struct Textures* t){

	int i = 0;
	int k = 0;
	for (i=1;i<5;i++){
		if (t->backgrounds[i]){
			SDL_DestroyTexture( t->backgrounds[i]);
		}

	}
	for (i=1;i<7;i++){
		for (k=1;k<7;k++){
			if(t->charMenus[i][k]){
				SDL_DestroyTexture (t->charMenus[i][k]);
			}
		}

	}
	for (i=1;i<13;i++){
		if (t->characters[i]){
			SDL_DestroyTexture (t->characters[i]);
		}

	}
	for (i=1;i<14;i++){
		if (t->weapons[i]){
			SDL_DestroyTexture (t->weapons[i]);
		}

	}
	for (i=0;i<10;i++){
		if (t->overlays[i]) {
			SDL_DestroyTexture (t->overlays[i]);
		}

	}
	/*
	for (i=0;i<10;i++){
		if (t->scenery[i]) {
			free (t->scenery[i]);
		}

	}
	 */
	SDL_DestroyTexture (t->mainMenu);
	free(t);

}
