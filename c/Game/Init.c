#include "head.h"

//declare game renderer and window (external from main)
SDL_Renderer* gRenderer;
SDL_Window * gWindow;

// Init - initialises all fundamental elements of the program
//
// This function uses modified code that was found in the lazyfoo.net tutorials on SDL2

void init(){

	//initialise SDL
	if(!SDL_INIT_EVERYTHING){
		printf("SDL failed to initialise");
	}
	//create a game window
	gWindow= SDL_CreateWindow( "Poppin' Shotz", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SWIDTH, SHEIGHT, SDL_WINDOW_SHOWN );
	if( gWindow == NULL ){
		printf( "Window creation failed. \nError: \n%s\n", SDL_GetError() );
	}
	//create renderer
	gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
	if( gRenderer == NULL ){
		printf( "Render creation failed. \nError: \n%s\n", SDL_GetError() );
	}
	else{
		//initialise renderer colour
		SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

		//initialise PNG loading
		int flag = IMG_INIT_PNG;
		if( !( IMG_Init( flag ) & flag ) ){
			printf( "PNG access denied. \nError: \n%s\n",  IMG_GetError() );
		}
	}
}

//Clear Render - Destroy the renderer and window

void clearRender(){

	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
}
