#include "head.h"

// Get Input - gets user input from the keyboard - returns a unique number for each command
int getInput(SDL_Event e){

	//if a key was pressed
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 ){
		switch( e.key.keysym.sym )
		{
		case SDLK_UP: return UP;
		case SDLK_DOWN: return DOWN;
		case SDLK_LEFT: return LEFT;
		case SDLK_RIGHT: return RIGHT;
		case SDLK_RALT: return SHOOT1;
		case SDLK_w: return XUP;
		case SDLK_s: return XDOWN;
		case SDLK_a: return XLEFT;
		case SDLK_d: return XRIGHT;
		case SDLK_LSHIFT: return XSHOOT1;
		case SDLK_RCTRL: return SHOOT2;
		case SDLK_z: return XSHOOT2;
		}
	}
	//if a key was pressed
	if( e.type == SDL_KEYDOWN){
		switch( e.key.keysym.sym ){
		case SDLK_PERIOD: return SHOOT3;
		case SDLK_BACKSLASH :  return XSHOOT3;
		}
	}

	//if a key was released
	else if( e.type == SDL_KEYUP){
		switch( e.key.keysym.sym )
		{
		case SDLK_UP: return STOP_UP;
		case SDLK_DOWN: return STOP_DOWN;
		case SDLK_LEFT: return STOP_LEFT;
		case SDLK_RIGHT: return STOP_RIGHT;
		case SDLK_w: return XSTOP_UP;
		case SDLK_s: return XSTOP_DOWN;
		case SDLK_a: return XSTOP_LEFT;
		case SDLK_d: return XSTOP_RIGHT;
		}
	}
	return -1000;
}

// React - takes the return value of getInput() and controls the game

void react(SDL_Event e, struct Player* p1, struct Player * p2){
	switch(getInput(e)){
	//change speed
	case 0: p1->yVelocity = -3; break;
	case 1:	p1->yVelocity = 3; break;
	case 2: p1->xVelocity = -3; break;
	case 3: p1->xVelocity = 3; break;
	case 4: if (p1->yVelocity < 0){p1->yVelocity = 0;} break;
	case 5:	if (p1->yVelocity > 0){p1->yVelocity = 0;} break;
	case 6:	if (p1->xVelocity < 0){p1->xVelocity = 0;} break;
	case 7: if (p1->xVelocity > 0){p1->xVelocity = 0;} break;
	//shoot
	case 8: shoot(p1); break;
	//change speed
	case 9: p2->yVelocity = -3; break;
	case 10: p2->yVelocity = 3; break;
	case 11: p2->xVelocity = -3; break;
	case 12: p2->xVelocity = 3; break;
	case 13: if (p2->yVelocity < 0){p2->yVelocity = 0;} break;
	case 14: if (p2->yVelocity > 0){p2->yVelocity = 0;} break;
	case 15: if (p2->xVelocity < 0){p2->xVelocity = 0;} break;
	case 16: if (p2->xVelocity > 0){p2->xVelocity = 0;} break;
	//shoot
	case 17: shoot(p2); break;
	case 18: shoot2(p1); break;
	case 19: shoot2(p2); break;
	case 20: shootX(p1); break;
	case 21: shootX(p2); break;
	}
}
