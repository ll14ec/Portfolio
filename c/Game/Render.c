#include "head.h"

//external from Main.c
SDL_Renderer* gRenderer;

// Render Player to Screen  - renders the player sprite

void renderPlayerToScreen(int x,int y, SDL_Texture * t){

	//create rectangle based on position and size
	SDL_Rect * destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	destRect->x = x;
	destRect->y = y;
	destRect->h = PLAYER_HEIGHT;
	destRect->w = PLAYER_WIDTH;
	//render the player texture to the rectangle
	SDL_RenderCopy(gRenderer,t,NULL,destRect);
	free(destRect);
}

// Render Bullets to Screen - renders any active bullets

void renderBulletsToScreen(struct Player* p, SDL_Texture* t){

	int i=0;
	for (i=0;i<100;i++){
		//find active bullets
		if(p->bullets[i]->active==1){
			//create rectangle based on position and size
			SDL_Rect * destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
			destRect->x = p->bullets[i]->xPos + 8;
			destRect->y = p->bullets[i]->yPos + 8;
			destRect->h = BULLET_HEIGHT;
			destRect->w = BULLET_WIDTH;
			//render the bullet texture to the rectangle
			SDL_RenderCopy(gRenderer,t,NULL,destRect);
			free(destRect);
		}
	}
}

// Render Bullets to Screen - renders any active xBullets

void renderXBulletsToScreen(struct Player* p, SDL_Texture* t){
	int i=0;
	double angle=0;
	for (i=0;i<5;i++){
		//find active xBullets
		if(p->xBullets[i]->active==1){
			//create rectangle based on position and size
			SDL_Rect * destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
			destRect->x = p->xBullets[i]->xPos + 8;
			destRect->y = p->xBullets[i]->yPos + 8;
			destRect->h = BULLET_HEIGHT * 3;
			destRect->w = BULLET_WIDTH * 3;

			//obtain angle to render the bullet (based on velocity)
			if (p->xBullets[i]->xV > 0){	//if +ve x velocity
				if(p->xBullets[i]->yV > 0){	//if +ve y velocity
					//find the angle, convert from radians to degrees
					angle = atan((p->xBullets[i]->yV)/(p->xBullets[i]->xV));
					angle = (angle/(3.14159));
					angle = angle*180;
				}
			}

			if (p->xBullets[i]->xV > 0){	//if +ve x velocity
				if(p->xBullets[i]->yV < 0){	//if -ve y velocity
					angle = atan((p->xBullets[i]->yV)/(p->xBullets[i]->xV));
					angle = (angle/(3.14159));
					angle = angle*180;
				}
			}
			if (p->xBullets[i]->xV < 0){	//if -ve x velocity
				if(p->xBullets[i]->yV > 0){	//if +ve y velocity

					angle = atan((p->xBullets[i]->yV)/(p->xBullets[i]->xV));
					angle = (angle/(3.14159));
					angle = angle*180;
					angle = 180+angle;
				}
			}
			if (p->xBullets[i]->xV < 0){	//if -ve x velocity
				if(p->xBullets[i]->yV < 0){	//if -ve y velocity

					angle = atan((p->xBullets[i]->yV)/(p->xBullets[i]->xV));
					angle = (angle/(3.14159));
					angle = angle*180;
					angle = 180+angle;
				}
			}
			//render the xBullet texture to the rectangle with the calculated angle
			SDL_RenderCopyEx(gRenderer, t, NULL, destRect, angle, NULL, SDL_FLIP_NONE);
			free(destRect);
		}
	}
}

// Render Explosion to Screen - renders a plauer's closed range attack, expanding outwards based on the tick

void renderExplosionToScreen(struct Player * p, SDL_Texture *t, int tick){

	//create a rectangle to render the texture
	SDL_Rect * destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	//define the render position of the rectangle based on the tick
	destRect->x = (p->xPos-(tick/0.2)+(PLAYER_HEIGHT/2));
	destRect->y = (p->yPos-(tick/0.2)+(PLAYER_WIDTH/2));
	//calculate and assign the height and width of the explosion based on the tick
	float rHeight =  (100.0 * (tick/10.0));
	float rWidth = (100.0 * (tick/10.0));
	destRect->h = rHeight;
	destRect->w = rWidth;
	//render the explosion texture to the rectangle
	SDL_RenderCopy(gRenderer,t,NULL,destRect);
	free(destRect);
}

// Render Backround - Renders a full window-sized texture the the screen

void renderBackground(SDL_Texture* t){

	//create and define dest rectangle parameters
	SDL_Rect* destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	destRect->x = destRect->y = 0;
	destRect->h = SHEIGHT;
	destRect->w = SWIDTH;
	//render the texture to the rectangle
	SDL_RenderCopy(gRenderer,t,NULL,destRect);
	free(destRect);
}

// Render Background Moving - Renders a scrolling, full sized texture (used as a moving background)

void renderBackgroundMoving(SDL_Texture* t, int i){

	//create/define the 2rectangles; the part of the image to be copied
	//and the destination of the screen to render to
	SDL_Rect* destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	SDL_Rect* sourceRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	destRect->x = destRect->y = 0;
	destRect->h = SHEIGHT;
	destRect->w = SWIDTH;
	sourceRect->y=0;
	sourceRect->h = SHEIGHT;
	sourceRect->w = SWIDTH;
	//change x position based on the tick value i
	sourceRect->x = i/5.0;
	SDL_RenderCopy(gRenderer,t,sourceRect,destRect);
	free(destRect);
	free(sourceRect);
}

// Render Shield - Renders and active player shield

void renderShield(struct Player * p, int stage, SDL_Texture * t){

	//create and define the destination rectangle
	SDL_Rect* destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	destRect->x = p->xPos-16;
	destRect->y = p->yPos-18;
	destRect->w = PLAYER_WIDTH*2;
	destRect->h = PLAYER_HEIGHT*2;
	//render the shield texture to the rectangle
	SDL_RenderCopy(gRenderer,t,NULL,destRect);
	free(destRect);
}

// Render Crate - renders any spawned crate

void renderCrate(SDL_Texture * t, float xPos, float yPos){
	//create/define the 2rectangles; the part of the image to be copied
	//and the destination of the screen to render to
	SDL_Rect * destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	destRect->x = xPos;
	destRect->y = yPos;
	destRect->w = 24;
	destRect->h = 24;

	SDL_Rect* sourceRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	sourceRect->w = 24;
	sourceRect->h = 24;
	sourceRect->x = 0;
	sourceRect->y = 0;

	//render the crate texture to the rectangle
	SDL_RenderCopy(gRenderer,t,sourceRect,destRect);
	free(sourceRect);
	free(destRect);
}

// Render Overlay - renders an active overlay (full screen width * 400 H)

void renderOverlay(SDL_Texture* t){
	//create/assign a destination rectangle
	SDL_Rect * overlay = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	overlay->x = 0;
	overlay->y = 150;
	overlay->w = SWIDTH;
	overlay->h = 400;
	//render the overlay to the rectangle
	SDL_RenderCopy(gRenderer,t,NULL,overlay);
	free (overlay);
}

// Render Overlay - renders a power up overlay overlay

void renderPowerUpOverlay(SDL_Texture * t, SDL_Texture * t2, SDL_Texture * t3, struct Player* p){

	//create/assign a destination rectangle based on the power-up rendered
	SDL_Rect * destRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	if (p->id==2){
		destRect->x = 25;
	}
	if (p->id==1){
		destRect->x = 680;
	}
	destRect->y = 80;
	destRect->h = 20;
	destRect->w = 100;
	//render the power-up texture to the rectangle
	if (p->powerUp==1){
		SDL_RenderCopy(gRenderer,t,NULL,destRect);
	}
	if (p->powerUp==2){
		if (p->id==1){
			destRect->x =690;
		}
		if (p->id==2){
			destRect->x =15;
		}
		SDL_RenderCopy(gRenderer,t2,NULL,destRect);
	}
	if (p->powerUp==3){
		if (p->id==1){
			destRect->x = 710;
		}

		if (p->id==2){
			destRect->x  = -5;
		}
		SDL_RenderCopy(gRenderer,t3,NULL,destRect);
	}
	free(destRect);
}

// Render Text Overlays - Renders the HP and POWER text overlays

void renderTextOverlays(SDL_Texture * p1hp, SDL_Texture * p2hp, SDL_Texture * power){

	//create/assign the destination rectangles
	SDL_Rect* p1hpRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	SDL_Rect* p2hpRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	SDL_Rect* p1powerRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	SDL_Rect* p2powerRect = (SDL_Rect*)malloc(sizeof(SDL_Rect));

	p1hpRect->x = 25;
	p1hpRect->y = 8;
	p1hpRect->h = 20;
	p1hpRect->w = 100;

	p2hpRect->x = 673;
	p2hpRect->y = 8;
	p2hpRect->h = 20;
	p2hpRect->w = 100;

	p1powerRect->x = 25;
	p1powerRect->y = 41;
	p1powerRect->h = 16;
	p1powerRect->w = 100;

	p2powerRect->x = 701;
	p2powerRect->y = 41;
	p2powerRect->h = 16;
	p2powerRect->w = 100;

	//render the overlays to the rectangles
	SDL_RenderCopy(gRenderer,p1hp,NULL,p1hpRect);
	SDL_RenderCopy(gRenderer,p2hp,NULL,p2hpRect);
	SDL_RenderCopy(gRenderer,power,NULL, p1powerRect);
	SDL_RenderCopy(gRenderer,power,NULL,p2powerRect);

	free(p1hpRect);
	free(p2hpRect);
	free(p1powerRect);
	free(p2powerRect);
}

void renderContinue(SDL_Texture * overlay){
	SDL_Rect* dest = (SDL_Rect*)malloc(sizeof(SDL_Rect));
	dest->h = 33;
	dest->w = 362;
	dest->x = ((SWIDTH-362)/2.0);
	dest->y = SHEIGHT - 39;
	SDL_RenderCopy(gRenderer, overlay, NULL, dest);
	free(dest);
}

// Set Renderer Colour - Sets the colours of the hp/power bars depending on the level

void setRendererColour(SDL_Renderer* gRenderer,int stateChoice){
	switch(stateChoice){
	case SPACEZONE:  SDL_SetRenderDrawColor( gRenderer, 255, 0, 0, 255 ); break;
	case UNDERWATERZONE: SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 ); break;
	case ATOMZONE: SDL_SetRenderDrawColor( gRenderer, 255, 0, 0, 255 ); break;
	}
}
