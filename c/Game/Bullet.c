#include "head.h"


// Make Bullets = assigns bullet members of player memory and initialises values

void makeBullets(struct Player* p){
	int i=0;
	for (i=0;i<100;i++){
		p->bullets[i] = (struct Bullet*)malloc(sizeof(struct Bullet));
		p->bullets[i]->active=0;
		p->bullets[i]->height = 5;
		p->bullets[i]->width = 5;
	}
}

// Make Bullets = assigns xBullet members of player memory and initialises values

void makeXBullets(struct Player *p){
	int i=0;
	for (i=0;i<15;i++){
		p->xBullets[i] = (struct XBullet*)malloc(sizeof(struct XBullet));
		p->xBullets[i]->active=0;
		p->xBullets[i]->height=7;
		p->xBullets[i]->width=7;
		p->xBullets[i]->xPos= p->xPos;
		p->xBullets[i]->yPos= p->yPos;
	}

}

// Move Bullet - Moves active bullets according to their veloctity

void moveBullet(struct Bullet* b){
	//change bullet position
	b->xPos  += b->xV * BULLET_SPEED;	// x
	b->yPos += b->yV * BULLET_SPEED;	// y
	//deactivate any bullet that goes out of bounds
	if (b->yPos < 0 || b->xPos < 0 || b->xPos > SWIDTH || b->yPos > SHEIGHT){
		b->active=0;
		b->xPos = 0;
		b->yPos = 0;
		b->xV = 0;
		b->yV = 0;
	}
}

// Move xBullet - Moves the xBullet

void moveXBullet(struct XBullet* b, struct Player* target){

	// find x(p1)/x(p2) and y(p1)/y(p2)
	float xRatio = (target->xPos - b->xPos)/(target->yPos - b->yPos);
	float yRatio = (target->yPos - b->yPos)/(target->xPos - b->xPos);
	// find x(p1) - x(p2) and y(p1) - y(p2)
	float xDifference = target->xPos - b->xPos;
	float yDifference = target->yPos - b->yPos;
	//normalise any negative values
	xRatio = sqrt(xRatio* xRatio);
	yRatio = sqrt(yRatio* yRatio);
	if (xDifference<0){
		xRatio = xRatio * (-1);
	}
	if (yDifference<0){
		yRatio = yRatio * (-1);
	}

	//give the xBullet a velocity according to the relative positions of the bullet and the target
	b->xV = xRatio*XBULLET_SPEED;
	b->yV = yRatio*XBULLET_SPEED;

	//when the bullet hits - reset bullet, damage player
	if (sqrt((b->xPos - target->xPos)*(b->xPos - target->xPos))<15){
		if (sqrt((b->yPos - target->yPos)*(b->yPos - target->yPos))<15){
			b->xV = b->yV = 0;
			b->active=0;
			damage(target, target->armour, SHOOT3);
		}
	}
	//move the xBullet
	b->xPos  += b->xV;
	b->yPos += b->yV;
	//deactivate the bullet if out of bounds
	if (b->yPos < 0 || b->xPos < 0 || b->xPos > SWIDTH || b->yPos > SHEIGHT){
		b->active=0;
		b->xPos = 0;
		b->yPos = 0;
		b->xV = 0;
		b->yV = 0;
	}
}

// Move Bullets - calls moveBullet() on all active bullets

void moveBullets(struct Player* p){
	int i=0;
	for (i=0;i<100;i++){
		if(p->bullets[i]->active==1){
			moveBullet(p->bullets[i]);
		}
	}
}

// Move Bullets - calls moveXBullet() on all active xBullets

void moveXBullets(struct Player* p, struct Player * target){
	int i=0;
	for (i=0;i<5;i++){
		if(p->xBullets[i]->active==1){
			moveXBullet(p->xBullets[i], target);
		}
	}
}
