#include "head.h"
int state;

// Set Up Status Bars - creates the initial values for the players' status bars

void setUpStatusBars(SDL_Rect * p1H, SDL_Rect * p1P, SDL_Rect * p2H, SDL_Rect *p2P){

	p1H->x = 25;
	p1H->y = 30;
	p1H->h = 10;
	p1H->w = 350;

	p1P->x = 25;
	p1P->y = 60;
	p1P->h = 10;
	p1P->w = 350;

	p2H->x = 425;
	p2H->y = 30;
	p2H->h = 10;
	p2H->w = 350;

	p2P->x = 425;
	p2P->y = 60;
	p2P->h = 10;
	p2P->w = 350;
}

// Update Status Bars - Updates the dimensions of the bars based on player hp and power

void updateStatusBars(struct Player * p1, SDL_Rect * p1H, SDL_Rect * p1P, struct Player * p2, SDL_Rect * p2H, SDL_Rect * p2P, SDL_Renderer * r){
	p1H->w = (350.0 *((p1->hitPoints)/(p1->maxHitpoints)));
	p1P->w = (350.0 *(p1->power/p1->maxPower));
	p2H->w = (350.0 *(p2->hitPoints/p2->maxHitpoints));
	p2P->w = (350.0 *(p2->power/p2->maxPower));
}

// Status Check - Normalises any negative values and regenerates players' power

void statusCheck(struct Player* p1, struct Player* p2){

	normaliseHp(p1,p2);

	//regen hp based on regen rate
	float p1Regen = (p1->regen)/100.0;
	float p2Regen = (p2->regen)/100.0;

	if (p1->power<p1->maxPower){
		p1->power+= p1Regen;
	}
	if (p2->power<p2->maxPower){
		p2->power+= p2Regen;
	}
}

// Normaise HP - Resets a players hp to 0 if it is negative for any reason

void normaliseHp(struct Player* p1, struct Player* p2){
	if (p1->hitPoints<0){
		p1->hitPoints = 0;
	}
	if (p2->hitPoints<0){
		p2->hitPoints = 0;
	}
}

// Damage - Removes hp from the player based on their armour and the attack sustained

void damage(struct Player* target, int armour, int attackstyle){
	switch(attackstyle){
	case SHOOT3: target->hitPoints -= (100.0f/armour) * SHOOT3DAMAGE; break;
	}

}

// ReducePower - Reduces a players power based on the attack style

void reducePower(struct Player* p, int attackstyle){

	switch(attackstyle){
	case SHOOT3: p->power -= SHOOT3POWER; break;
	}

}

// Is Player Dead? - Checks for a player death

int isPlayerDead(struct Player* p1, struct Player* p2, struct Textures* t, int i){

	//tie
	if (p1->hitPoints<=0 && p2->hitPoints<=0){
		i++;
		renderOverlay(t->overlays[2]);
		if (i>100){
			state=0;
		}
		return 1;
	}
	//p1 wins
	else if (p2->hitPoints<=0){
		i++;
		renderOverlay(t->overlays[0]);
		if (i>100){
			state=0;
		}
		return 1;
	}
	//p2 wins
	else if (p1->hitPoints<=0){
		i++;
		renderOverlay(t->overlays[1]);
		if (i>100){
			state=0;
		}
		return 1;
	}
	return 0;
}
