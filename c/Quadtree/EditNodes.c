#include "Tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

Node *makeNode(double x, double y, int level){

	if (x>2 || y>2 || x<0 || y<0){
		printf("X and/or Y value are out of bounds, please enter values between 0 and 2.\n");
		return NULL;
	}

	int i;
	// allocate the data structure
	Node *node = (Node*)malloc(sizeof(Node));
	// set the node data
	node->level = level;
	node->xy[0] = x;
	node->xy[1] = y;
	node->nextLeaf= NULL;
	// set children and nextLeaf to NULL
	for( i=0; i<4; ++i ){
		node->child[i] = NULL;
	}
	return node;
}

void makeChildren(Node *parent){
	// parent data
	double x = parent->xy[0];
	double y = parent->xy[1];
	int level = parent->level;
	// child edge length
	double z = (level+1.0)*(-1.0);
	double hChild = pow(2.0,z);
	// create children at level+1
	parent->child[0] = makeNode( x,y, level+1 );
	parent->child[1] = makeNode( x+hChild,y, level+1 );
	parent->child[2] = makeNode( x+hChild,y+hChild, level+1);
	parent->child[3] = makeNode( x,y+hChild, level+1 );
	return;
}

/* ___ FUNCTION ___ "grow" ___
 *
 * DESCRIPTION -
 *
 * Traverses the nextLeaf linked list and adds children to each leaf.
 *
 * ARGUMENTS -
 *
 * head - Pointer to the top of the list.
 */

void grow(Node *head){

	int i=0;

	//if there are no leaves already (note; buildList() must be called before grow to avoid this on a non empty tree)
	if (head->nextLeaf==NULL){
		printf("Growing children on headNode...\n");
		makeChildren(head);
		printf("Grow complete, only headNode children created.\n");
		return;
	}
	//make children in all the leaf nodes in the list
	while (head->nextLeaf !=NULL){
		head = head->nextLeaf;
		makeChildren(head);
		i++;
	}
	//user prompt
	if (i>1){
		printf("Grow complete; added %d nodes.\n" , i);
	}
	return;
}
