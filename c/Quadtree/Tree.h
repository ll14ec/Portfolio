#include <stdio.h>
#include "stdbool.h"

/* ___ STRUCTURE - "Node" ___
 *
 * DESCRIPTION -
 *
 * Contains the data fields for the Node structure.
 *
 * FIELDS -
 *
 * level - the level of the Node in the tree (head at 0 and so forth).
 * xy[2] - the spatial information of the node.
 * child[4] - pointers that can be allocated to other Nodes as children in a quad tree.
 * *nextLeaf - pointer that can be allocated to the next leaf in a list of leaves in the tree (leaves have no children).
 *
 */

typedef struct node{
	int level;
	double xy[2];
	struct node *child[4];
	struct node *nextLeaf;
}
Node;


// PROTOTYPES

// PRE-WRITTEN FUNCTIONS @MARK WALKLEY

Node *makeNode(double, double, int);
void makeChildren(Node *);
void writeTree(Node *);
void writeNode(FILE *, Node *);
void printOut(FILE *, Node *);
double dataFunction( double, double, int);
bool indicator( Node *, double, int );

// NEW FUNCTIONS @ll14ec

void buildList( Node *, Node *, Node*, Node*, int);
void addToList(Node *, Node*);
void grow(Node *);
void growVia(Node *, Node *, Node *, double, int );
void writeList(Node *, Node *);
void deleteTree(Node*);

// TEST FUNCTIONS @ll14ec

void printOutTest(Node *);
void writeTreeTest(Node *);
void writeNodeTest(Node *);
void dataFunctionTest(double, double);
void addToListTest(Node *, Node *, Node *, Node *);
void buildListTest(Node*, Node*, Node*, Node*);
void writeListTest(Node *, Node *);
void makeChildrenTest(Node *);
