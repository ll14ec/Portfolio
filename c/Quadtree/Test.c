#include <stdio.h>
#include "math.h"
#include "Tree.h"
#include "stdlib.h"


void makeNodeTest(Node *node){


	node = makeNode(0,0,0);

	int i;

	//print size of data fields
	printf("Size of node->level = %d bytes.\n", sizeof(node->level));
	for (i=0;i<2;i++){
		printf("Size of node->xy[%d] = %d bytes.\n" ,i, sizeof(node->xy[i]));
	}
	for (i=0;i<4;i++){
		printf("Size of node->child[%d] = %d bytes.\n" ,i, sizeof(node->child[i]));
	}

	//set values to data fields
	node->level = 20;
	node->xy[0] = 1;
	node->xy[1] = 2;
	node->child[0] = node;

	//print values from data fields
	printf("level set to %d.\n" , node->level);
	printf("xy[0] set to %f.\n" , node->xy[0]);
	printf("xy[1] set to %f.\n" , node->xy[1]);

	for (i=0;i<4;i++){
		if (node->child[i]){
			printf("Child %d has a value saved in it.\n" , i);
		}
		if (!node->child[i]){
			printf("Child %d does not have a value saved in it.\n" , i);
		}
	}

	free(node);

}

void printOutTest(Node *node){

	//set values

	node->level = 9;
	node->xy[0] = 10000;
	node->xy[1] = 100;

	//open file and write

	FILE *file = fopen("file.out","w");

	printOut(file, node);

	fclose(file);
}

void writeNodeTest(Node* node){

	//set values
	node->level = 5;
	node->xy[0] = 100;
	node->xy[1] = 100;

	//open file and write
	FILE *file = fopen("file.out","w");
	writeNode(file, node);
	fclose(file);
}

void writeTreeTest(Node* head){
	writeTree(head);
}

void makeChildrenTest(Node* node){
	makeChildren(node);
	makeChildren(node->child[2]);
	int i = 0;
	int k = 0;
	printf("First level:\n\n");
	for (i=0; i<4; i++){
		if (node->child[i]){
			printf("Node -> Child %d has memory allocated\n" ,i);
		}
		if (!node->child[i]){
			printf("Node -> Child %d does not have memory allocated\n" ,i);
		}
	}
	printf("\nSecond level:\n\n");
	for (i=0; i<4; i++){
		for (k=0; k<4; k++){
			if(node->child[i]->child[k]){
				printf("Node -> Child %d -> Child %d has memory allocated\n" ,i,k);
			}
			if(!node->child[i]->child[k]){
				printf("Node -> Child %d -> Child %d does not have memory allocated\n" ,i,k);
			}
		}
	}

}

void dataFunctionTest(double x, double y){
	int i=-2;
	for (i=-2;i<5;i++){
		printf("Return value of data function ( x = %g | y = %g | choice = %d )  ==>  %g  \n" ,x,y,i,dataFunction(x,y,i));
	}
}

void addToListTest(Node* head, Node *temp, Node* node1, Node* node2){

	//begin null
	head->nextLeaf = NULL;
	temp = head;

	//add head to list
	addToList(head,head);

	//print status
	if (head->nextLeaf){
		printf("head->nextLeaf is no longer NULL.\n");
	}
	if (!head->nextLeaf){
		printf("head->nextLeaf is still NULL.\n");
	}
	//add a node
	addToList(node1, head);

	//print status
	if (head->nextLeaf){
		printf("head->nextLeaf is no longer NULL.\n");
	}
	if (!head->nextLeaf){
		printf("head->nextLeaf is still NULL.\n");
	}
	//print node
	if (head->nextLeaf == node1){
		printf("head->nextLeaf contains Node 1.\n");
	}

	if (head->nextLeaf == node2){
		printf("head->nextLeaf contains Node 2.\n");
	}
	//reset list to null
	head->nextLeaf = NULL;

	//add 2 modes to list
	addToList(node1, head);
	addToList(node2, head);
	addToList(head , head);

	int i=0;

	while (head){
		if (head==node1){
			printf("Node 1 is at position %d.\n" , i);
		}
		if (head==node2){
			printf("Node 2 is at position %d.\n" , i);
		}
		i++;
		head = head->nextLeaf;
	}

}

void buildListTest(Node *node, Node *head, Node *generic2, Node *generic){
	//pass buildlist and writeNodes the same list count the variable of nodes added

	int buildListModified( Node *node, Node *head, Node *generic2Node, Node *genericNode,int mode){

		int i;
		int leafCount;

		//mode set as 0 on initial call
		if (mode == 0){

			//this loop deallocates previous pointers
			if (head->nextLeaf!=NULL){

				//create a generic node and node pointer to not lose it as we deallocate the pointers
				genericNode = head;
				generic2Node = head;

				//de-allocates pointers
				while(genericNode!=NULL){
					genericNode = genericNode->nextLeaf;
					head->nextLeaf = NULL;
					head = genericNode;
				}
				//points head back to the top of the list
				head = generic2Node;

			}
		}

		//found a node -->> add to list structure
		if (node->child[0] == NULL ){
			addToList(node, head);

			//****** INCREMENT LEAF COUNT *******//
			leafCount++;
		}

		else{
			//recursively call function with 1 to skip pointer deallocation
			for ( i=0; i<4; ++i )
			{
				buildListModified(node->child[i], head, genericNode, generic2Node, 1);
			}
		}
		return leafCount;
	}

	int writeNodeModified( Node *node ){
		int i;
		int leafCount;
		if( node->child[0] == NULL ){

			//****** INCREMENT LEAF COUNT *******//
			leafCount++;
		}
		else{
			for ( i=0; i<4; ++i )
			{
				writeNodeModified(node->child[i]);
			}
		}
		return leafCount;
	}

	if (buildListModified(node, head, generic2, generic, 0) == writeNodeModified(head)){
		printf("both functions return the same number of total leaves.\n");
	}
	else{
		printf("functions are not equal.\n");
	}


}

void writeListTest(Node *head, Node * temp){
	//show the output of this and writeNodes is the same, SIMPLE NODE AND COMPLICATED NODE STRUCTURE

	void printOutMod(Node *node ) {

		// node data
		double x = node->xy[0];
		double y = node->xy[1];
		int level = node->level;
		double h = pow(2.0,-level);

		// print out the corner points
		printf(" %g %g\n",x,y);
		printf(" %g %g\n",x+h,y);
		printf(" %g %g\n",x+h,y+h);
		printf(" %g %g\n",x,y+h);
		printf(" %g %g\n\n",x,y);

		return;
	}

	void writeListModified(Node *head, Node *tempNode){

		buildList(head, head ,tempNode, tempNode, 0);

		//catch the possibility of there being no list of leaves
		if (head->nextLeaf==NULL){
			printf("There is only the head node to write to file. Either no list exists or buildList() must be called.\n");
			return;
		}

		while(head->nextLeaf!=NULL){
			printOutMod(head->nextLeaf);
			head=head->nextLeaf;
		}
	}

	void writeNodeModified(Node *node ){
		int i;
		if( node->child[0] == NULL )
			printOutMod(node);
		else{
			for ( i=0; i<4; ++i ){
				writeNodeModified(node->child[i] );
			}
		}
		return;
	}
	writeListModified(head, temp);
	writeNodeModified(head);
}

