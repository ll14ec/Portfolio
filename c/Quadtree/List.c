#include "Tree.h"
#include <stdio.h>

/* ___ FUNCTION ___ "addToList" ___
 *
 * DESCRIPTION -
 *
 * Adds a designated leaf node to the list of leaf nodes. denoted by Node->nextLeaf
 *
 * ARGUMENTS -
 *
 * newNode - Pointer to a leaf node to add
 * head - Pointer to the beginning of the list
 *
 */

void addToList(Node *newNode, Node *head){
	//tree cannot point to itself
	if (newNode == head){
		return;
	}
	//traverse the list until a  null pointer is found
	while (head->nextLeaf!=NULL){
		head = head->nextLeaf;
	}
	//add the leaf at the first null pointer
	if (head->nextLeaf == NULL){
		head->nextLeaf = newNode;
	}
	//remove possibility of infinite looping
	if (head->nextLeaf == head){
		head->nextLeaf = NULL;
	}
}

/* ___ FUNCTION - "buildList" ___
 *
 * DESCRIPTION -
 *
 * Finds nodes that are leaf nodes, and calls addToList with these leaves
 *
 * ARGUMENTS -
 *
 * newNode - Pointer to a leaf node to add
 * head - Pointer to the beginning of the list, used to pass to addToList()
 *
 * RETURNS -
 *
 * Node pointer to the first leaf in the list.
 *
 */

void buildList( Node *node, Node *head, Node *generic2Node, Node *genericNode,int mode){

	int i;
	//mode set as 0 on initial call
	if (mode == 0){

		//this loop deallocates previous pointers
		if (head->nextLeaf!=NULL){

			//create a generic node and node pointer to not lose it
			//as we deallocate the pointers
			genericNode = head;
			generic2Node = head;

			//de-allocates pointers
			while(genericNode!=NULL){
				genericNode = genericNode->nextLeaf;
				head->nextLeaf = NULL;
				head = genericNode;
			}
			//points head back to the top of the list
			head = generic2Node;

		}
	}

	//found a node -->> add to list structure
	if (node->child[0] == NULL ){
		addToList(node, head);
	}

	else{
		for ( i=0; i<4; ++i ){
			//recursively call function with 1 to skip pointer deallocation
			buildList(node->child[i], head, genericNode, generic2Node, 1);
		}
	}
	return;
}

/* ___ FUNCTION ___ "writeList" ___
 *
 * DESCRIPTION -
 *
 * Traverses the nextLeaf linked list and prints each leaf to a new file 'quad.out'.
 *
 * ARGUMENTS -
 *
 * head - Pointer to the top of the list.
 */

void writeList(Node *head, Node *tempNode){

	//first update the list
	buildList(head, head ,tempNode, tempNode, 0);

	//catch the possibility of there being no list of leaves
	if (head->nextLeaf==NULL){
			printf("There is only the head node to write to file. Either no list exists or buildList() must be called.\n");
			//write the head to file
			writeTree(head);
			//exit function
			return;
		}

	//open file and write leaves
	FILE *fp = fopen("quad.out", "w");

	while(head->nextLeaf!=NULL){
		printOut(fp, head->nextLeaf);
		head=head->nextLeaf;
	}

	fclose(fp);
}

