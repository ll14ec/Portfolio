#include  "Tree.h"
#include <stdlib.h>

/* ___ FUNCTION ___ "deleteTree" ___
 *
 * DESCRIPTION -
 *
 * Recursively finds and deletes nodes in the quad-tree starting with children then parents.
 *
 * ARGUMENTS -
 *
 * node - Pointer to the head node of the tree
 *
 */

void deleteTree(Node *node){

	int i=0;
	//if a node is found
	if (node){
		//call function on its children
		for (i=0; i<4; i++){
			deleteTree(node->child[i]);
		}
		//delete
		free(node);
	}
	return;

}
