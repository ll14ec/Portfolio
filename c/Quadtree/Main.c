/* A PROGRAM TO MANIPULATE THE QUAD TREE DATA STRUCTURE
 *
 * HEADER FILES -
 *
 * Tree.h
 *
 * SOURCE FILES -
 *
 * Automatia.c , EditNodes.c , List.c , RemoveTree.c , WriteNodes.c , Test.c
 *
 * Please see Test.pdf for a more detailed description of the modules and contained functions.
 *
 * EXAMPLE USEAGE BELOW ------->
 *
 * @ll1ec 2016
 *
 */

#include <stdlib.h>
//header file contains all prototypes from other modules
#include "Tree.h"

int main(){
	//create some generic nodes used by other functions
	Node *node1 = (Node*)malloc(sizeof(Node));
	Node *node2 = (Node*)malloc(sizeof(Node));

	//make the head node of the tree structure
	Node *head = makeNode(0,0,0);

	//add one set of children to the structure
	makeChildren(head);

	//grow using a predefined data model
	//0.3 = tolerance (lower values make higher node density) - should be kept above 0.
	//1 = data model (options of 0, 1, or 2) - on option 2 lower tolerances are recommended
	growVia(head, node1, node2, 0.3, 1);

	//write the leaves from a linked list to file
	writeList(head, node1);

	//free up memory
	deleteTree(head);
	free(node1);
	free(node2);

	return(0);
}
