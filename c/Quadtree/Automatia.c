#include "math.h"
#include "stdbool.h"
#include  "Tree.h"

#define MAX(a,b) ( ((a)>(b)) ? (a):(b) )
#define MIN(a,b) ( ((a)<(b)) ? (a):(b) )

// Define a function that is computed
// on the quadtree grid
//
// Given a location (x,y) return a value
//
// Choice allows different functions
// to be selected

double dataFunction( double x, double y, int choice ){

	double value;

	if( choice == 0 )
		value = exp(-(y-x)*(y-x)/0.01) + exp(-(x-y)*(x-y)/0.01);

	else if( choice == 1 )
		value = exp(-(x-0.5)*(x-0.5)/0.01) + exp(-(y-0.5)*(y-0.5)/0.01);

	else if( choice == 2 )
		value = exp(-((x-0.72)*(x-0.72)+(y-0.23)*(y-0.23))/0.01);

	// default uniform data
	else
		value = 1.0;

	return value;
}

// Compute the max and min values
// of the data on the quadtree node
//
// If the difference is too big request
// the node is split into children

bool indicator( Node *node, double tolerance, int choice ) {

	double v[4],vmin,vmax;
	double h = pow(2,-node->level);

	v[0] = dataFunction(node->xy[0],node->xy[1], choice);
	v[1] = dataFunction(h+node->xy[0],node->xy[1], choice);
	v[2] = dataFunction(node->xy[0],h+node->xy[1], choice);
	v[3] = dataFunction(h+node->xy[0],h+node->xy[1], choice);

	vmin = MIN( MIN( MIN(v[0],v[1]), v[2]), v[3]);
	vmax = MAX( MAX( MAX(v[0],v[1]), v[2]), v[3]);

	if( (vmax-vmin) < tolerance )
		return true;
	else
		return false;
}

/* ___ FUNCTION - "growVia" ___
 *
 * DESCRIPTION -
 *
 * Passes over the quad tree and grows nodes in a pattern.
 *
 * ARGUMENTS -
 *
 * headNode - pointer to the top of the tree.
 * tolerance - changes the recursion of the pattern generated.
 * model - defines which pattern to grow.
 *
 */


void growVia(Node *headNode, Node *tempNode, Node *genericNode ,double tolerance,  int model){

	//stop any infinite looping through invalid tolerance choices

	if (tolerance<=0){
		printf("Error - Please enter a tolerance value greater than 0.\n");
		return;
	}

	int falseResults;

	buildList(headNode, headNode ,tempNode, genericNode, 0);

	//automatically detect if children need to be made (needs 1 set of children for options 1 & 0 and two for option 2)

	//i believe this is because of some way that dataFunction() and indicator() will not return false unless there are
	//some nodes to start with.

	//if mode = 1 and no children in head
	if (headNode->nextLeaf == NULL){
		printf("Adding children once to headNode for required data model.\n");
		grow(headNode);
	}
	//if mode = 2 and only one set of children in head
	if ((!headNode->child[0]->child[0] )&& model == 2){
		printf("Growing again for data model.\n");
		buildList(headNode, headNode ,tempNode, genericNode, 0);
		grow(headNode);
	}

	do{
		//set false resuts to 0
		falseResults = 0;
		//build a new list
		buildList(headNode, headNode, tempNode, genericNode , 0);
		//store the value of head
		tempNode = headNode;
		//go to the first leaf node in the list (if one exists)
		if (headNode->nextLeaf!=NULL){
			headNode = headNode->nextLeaf;
		}
		//traverse the list of leaf nodes and use indicator to decide what to do
		while (headNode!=NULL){
			//if indicator returns false then make children and increment falseResults
			if (indicator(headNode, tolerance, model)== false){
				makeChildren(headNode);
				falseResults++;
			}
			headNode = headNode->nextLeaf;
		}
		headNode = tempNode;
		//keep adding children until no children are added for the entire list of leaves
	}while(falseResults!=0);

	return;
}
