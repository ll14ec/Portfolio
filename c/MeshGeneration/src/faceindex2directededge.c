#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structs.h"

//------globals------

int vChunkCounter = 1;	//number of vertex chunks to be allocated
int fChunkCounter = 1;	//number of face chunks to be allocated
int faceCount = 0;	//number of faces counted in .face file
int edgeCount = 0;	//number of directed half-edges created
int vertexCount = 0;	//number of unique vertices in .face file

vertex * v;		//array of unique vertices
tFace* faces;		//array of faces
int* pairs;		//array of directed half-edge pair indices
edge* edges;		//array of directed half-edges
int* firstDirected;	//array of first directed edge for each vertex


//------functions------

//VERTEX CREATOR
//adds a vertex to the end of the vertex array
void vertexCreator(float x, float y, float z)
{
	v[vertexCount].x = x; 
	v[vertexCount].y = y; 
	v[vertexCount].z = z; 
	//printf("Vertex # : %d | x : %g | y : %g | z : %g\n", vertexCount, x, y, z);
}


//FACE CREATOR
//adds a face to the end of the face array
void faceCreator(float a, float b, float c)
{
	faces[faceCount].a = a;
	faces[faceCount].b = b;
	faces[faceCount].c = c;	
	//printf("Face # : %d | a : %g | b : %g | c : %g\n", faceCount, a, b, c);
}


//LINE HANDLER
//takes each line from the input file sequentially
//allocates memory as the number of vertices/faces increases
//sends data to the vertex creator in the order x,y,z
//sends data to the face creator in the order a,b,c
void lineHandler(char* l, int lineIndex)
{
	char* type;
	char* index;
	char* a;
	char* b;
	char* c;
	//initial memory allocation
	if (lineIndex==0){
		v = (vertex*)malloc(CHUNK_SIZE*sizeof(vertex));
		faces = (tFace*)malloc(CHUNK_SIZE*sizeof(tFace));
	}
	//if another 100 vertices needed, allocate more memory
	if (vertexCount>=(CHUNK_SIZE*vChunkCounter)){
		++vChunkCounter;
		v = (vertex*)realloc(v, CHUNK_SIZE*vChunkCounter*sizeof(vertex));
	}
	//if another 100 faces needed, allocate more memory
	if (faceCount>=(CHUNK_SIZE*fChunkCounter)){
		++fChunkCounter;
		faces = (tFace*)realloc(faces, CHUNK_SIZE*fChunkCounter*sizeof(tFace));
	}

	type = strtok(l, " ");
	//if we find vertex data, create a new vertex
	if (strcmp(type,"Vertex")==0){
		index = strtok(NULL, " ");
		a = strtok(NULL, " ");
		b = strtok(NULL, " ");
		c = strtok(NULL, " ");
		if ( (a) && (b) && (c) ){
			//printf("%d", vertexCount);
			vertexCreator(atof(a),atof(b),atof(c));
			vertexCount++;
		}
		else{
			printf("Bad file format.\n");
		}
	}	
	//if we find face data, create a new face
	else if (strcmp(type,"Face")==0){
		index = strtok(NULL, " ");
		a = strtok(NULL, " ");
		b = strtok(NULL, " ");
		c = strtok(NULL, " ");
		if ( (a) && (b) && (c) ){
			faceCreator(atof(a),atof(b),atof(c));
			faceCount++;
		}
		else{
			printf("Bad file format.\n");
		}
	}
	else{
		printf("Bad file format.\n");
	}	
}


//ARE VERTICES EQUAL
//takes two vertices as arument and returns true if they are equal, else false
int areVerticesEqual(vertex first, vertex second)
{
	if ( (first.x == second.x) && (first.y == second.y) && (first.z == second.z) ){
		return 1;
	} 
	return 0;
}


//IS PAIR (of edges)
//takes two pairs of vertices representing two directed half edges
//returns true if they are a pair - i.e. to/from order is reversed, else false
int isPair(vertex firstEdge[2], vertex secondEdge[2])
{
	if ( (areVerticesEqual(firstEdge[0], secondEdge[1])) && (areVerticesEqual(firstEdge[1],secondEdge[0])) ){
		return 1;
	}
	return 0;
}


//GET PAIR
//takes an edge index and searches for it's pair, returns [ edge found? edge index : -1 ]
int getPair(int edgeIndex)
{
	int i = edgeIndex;	
	int currentFace;
	vertex compEdge[2] = {edges[edgeIndex].from, edges[edgeIndex].to};
	//find pair
	for (i=edgeIndex;i<edgeCount;i++)
	{	
		vertex tempEdge[2] = {edges[i].from, edges[i].to}; 
		currentFace=i/3;
		if (isPair(compEdge, tempEdge)){
			//printf("Found pair, { %d } and { %d }.\n", edgeIndex, i);
			return i;
		}
	}
	return -1;
}


//GET FIRST DIRECTED EDGE
//takes a vertex and it's index (for debug only)
//returns the index of the first directed edge
int getFirstDirectedEdge(vertex from, int index)
{
	int i = 0;
	for (i=0;i<edgeCount;i++){
		if (areVerticesEqual(from, edges[i].from)){
			//printf("\nFDE for Vertex %g %g %g : Edge from %g %g %g \n", from.x, from.y, from.z, edges[i].from.x, edges[i].from.y, edges[i].from.z);
			//printf("Vertex index %d Edge Index %d\n", index, i);
			return i;
		}
	}
}


//GET DIRECTED EDGES
//gets first directed edges for each vertex in the vertex array
void getDirectedEdges()
{
	firstDirected = (int*)malloc(vertexCount * sizeof(int));
	int i = 0;
	for (i=0;i<vertexCount;i++){
		firstDirected[i] = getFirstDirectedEdge(v[i],i);
	}
}


//PRINT EDGE
//for debug - prints the start and end point of each edge
void printEdge(edge e)
{
	printf("| Edge from %g %g %g to %g %g %g.\n", e.from.x, e.from.y, e.from.z, e.to.x, e.to.y, e.to.z);
}


//PRINT EDGE VERTEX INDICES
//takes an edge and prints the index of the vertics that make up the edge (in no specific order)
void printEdgeVertexIndices(edge e)
{
	int i = 0;
	printf("| Edge is between vertices");
	for (i=0;i<vertexCount;i++){
		if (areVerticesEqual(e.from, v[i])){
			printf(" %d ", i);
		}
		if (areVerticesEqual(e.to, v[i])){
			printf(" %d ", i);
		}
	}	
	printf(".");
}


//CALCULATE EDGES
//initialises memory for edge array
//assigns each edge from the face indices
int calculateEdges()
{
	edgeCount = faceCount*3;
	edges = (edge*)malloc(edgeCount*sizeof(edge));
	int currentFace = 0;
	int i = 0;
	for (i=0;i<faceCount*3;i++){
		currentFace=i/3;
		//if 0th of face c -> a
		if (i%3 == 0){
			edges[i].from = v[faces[currentFace].c];
			edges[i].to = v[faces[currentFace].a];
		}
		//if 1st of face a -> b
		else if (i%3 == 1){
			edges[i].from = v[faces[currentFace].a];
			edges[i].to = v[faces[currentFace].b];
		}
		//if 2nd of face b -> c
		else { // (i%3 == 2)
			edges[i].from = v[faces[currentFace].b];
			edges[i].to = v[faces[currentFace].c];
		}
		//printEdge(edges[i]);
	}
}


//EDGE HANDLER
//allocates memory to the pairs array
//assigns the pairs of each edge
//checks that there is no more than one pair found per edge
void edgeHandler()
{
	int i = 0;
	int currentFace;
	int foundIndex = -1;
	int foundError = 0;
	printf("| Total edges : %d \n", edgeCount);
	//init pairs to -1
	pairs = (int*)malloc(edgeCount*sizeof(int));
	for (i=0;i<edgeCount;i++){
		pairs[i] = -1;
	}
	//get pairs
	for (i=0;i<edgeCount;i++){
		foundIndex = getPair(i);
		if (foundIndex!=-1){
			//assign pair if neither entries have pair
			if ( (pairs[i] == -1) && (pairs[foundIndex]==-1) ){
				pairs[i] = foundIndex;
				pairs[foundIndex] = i;
			}
			else{
				//print message for first error only
				if (!foundError){
					printf("\n| Error, found pair to edge with pair already assigned.\n");
					printf("| Edge %d paired with %d and/or edge %d with %d.\n", i, pairs[i],foundIndex,pairs[foundIndex]);
					printf("| All edges must pair with one edge only. Failed.\n");
					printf("| Not manifold due to multiple edge pairs:\n");
					printEdge(edges[i]);
					printEdge(edges[foundIndex]);
					printEdgeVertexIndices(edges[i]);
					foundError = 1;
				}
			}
			//printEdge(edges[i]);	
			//printf("paired with: ");
			//printEdge(edges[foundIndex]);
			//printf("\n");
		}
	}
	//print pairs
	//for (i=0;i<edgeCount;i++){
	//	printf("Edge Pair: %d %d\n", i,pairs[i]);
	//}
}


//GET ORDER
//calculates the order of a vertex (how many faces contain the vertex)
int getOrder(int vIndex)
{
	int i = 0;
	int order = 0;
	//get vertex based on index
	vertex testVertex = v[vIndex];
	//count the number of faces that contain the vertex 
	for (i=0;i<faceCount;i++){
		vertex temp = v[faces[i].a];
		if (areVerticesEqual(temp, testVertex)) ++order;
		temp = v[faces[i].b];
		if (areVerticesEqual(temp, testVertex)) ++order;
		temp = v[faces[i].c];
		if (areVerticesEqual(temp, testVertex)) ++order;
	}
	return order;
}


//TEST MANIFOLD
//checks the cycle length of edges around each vertex against it's calculated order
int testManifold()
{
	int vFreq;			//stores the calculated vertex order
	int i = 0;
	int edgesAboutVertex = 0;	//stores the vertex cycle length
	int currentEdge = -1;		
	int firstDirectedEdge = -2;

	//check cycle length equal to vertex order for each vertex
	for (i=0;i<vertexCount;i++){
		edgesAboutVertex = 0;	
		vFreq = getOrder(i);	
		//printf("V %d order %d\n", i, vFreq);
		//start at first directed edge
		firstDirectedEdge = currentEdge = firstDirected[i];	
		do{
			//go to next edge about vertex
			currentEdge = NEXT(pairs[currentEdge]);		
			++edgesAboutVertex;
			//if greater than the order, there is an error
			if (edgesAboutVertex>vFreq){			
				currentEdge=firstDirectedEdge;	
				printf("\n| Not manifold error detected.\n");
				printf("| Vertex %d is order %d, but found longer cycle.\n",i,vFreq);		
				return 0;
			}
		} while (currentEdge!=firstDirectedEdge);
		//if not equal, there is an error
		if (vFreq!=edgesAboutVertex){				
			printf("\n| Not manifold error detected\n");
			printf("| Vertex %d is order %d, but found cycle length = %d.\n",i,vFreq,edgesAboutVertex);
			return 0;
		}	
	}	
	//if we have made it this far, the mesh is manifold	
	return 1;	
}


//EULER
//calculates the Euler genus of the surface
int euler()
{
	int genus = -1;
	// must divide edgecount by two because they are half edge pairs
	printf("| V=%d E=%d F=%d \n", vertexCount, (edgeCount/2), faceCount);
	genus = (vertexCount-(edgeCount/2)+faceCount-2)/-2;
	return genus;
}


//READ DATA
//opens the file and reads the contents line by line
//passes the lines to the line handler
void readData(char* i)
{
	//open file and read line by line
	FILE* ifp;
	char * l = NULL;
	size_t s = 0;
	ssize_t lengthRead;
	ifp = fopen(i,"r");
	int lineIndex = 0;
	vertexCount = 0;	
	faceCount = 0;
	//if file has been opened
	if (ifp){
		while ((lengthRead = getline(&l, &s, ifp)) != -1){	
			//printf("%s\n", l);
			//if line is not blank then pass to line handler
			if ( (lengthRead>1) && (l[0]!='#') ){
				//printf("Line %d", lineIndex);
				lineHandler(l, lineIndex);
				++lineIndex;
			}			
		}
	}
	//if file io error
	else{
		printf("Invalid file handle.\n");
	}
	//close file
	close(ifp);
}


//WRITE DATA
//writes the vertices, directed edges, faces and edge pairs to file
void writeData(char* o)
{
	char* shapeName = (char*)malloc(9999*sizeof(char));
	shapeName = strcpy(shapeName,o);
	strcat(o, ".diredge");
	FILE* f = fopen(o,"w");

	if (f){
		//print header
		fprintf(f,"# University of Leeds 2018-2019\n");
		fprintf(f,"# COMP 5812M Assignment 2\n");
		fprintf(f,"# Edward Cottle\n");
		fprintf(f,"# 200450169\n");
		fprintf(f,"#\n");
		fprintf(f,"# Object Name: %s\n", shapeName);
		fprintf(f,"# Vertices=%d Faces=%d\n", vertexCount,faceCount);
		fprintf(f,"#\n");

		//print vertices
		int i=0;
		for (i=0;i<vertexCount;i++){
			fprintf(f, "Vertex %d ", i);
			fprintf(f, "%g ", v[i].x);
			fprintf(f, "%g ", v[i].y);
			fprintf(f, "%g \n", v[i].z);
		}
		//print first directed edges
		for (i=0;i<vertexCount;i++){
			fprintf(f, "FirstDirectedEdge %d %d\n", i,firstDirected[i]);
		}
		//print faces
		for (i=0;i<faceCount;i++){
			fprintf(f, "Face %d ", i);
			fprintf(f, "%d ", faces[i].a);
			fprintf(f, "%d ", faces[i].b);
			fprintf(f, "%d \n", faces[i].c);
		}
		//print other halves
		for (i=0;i<edgeCount;i++){
			fprintf(f, "OtherHalf %d %d\n", i,pairs[i]);
		}
	}
	//free filename string
	free(shapeName);
	//close output file
	fclose(f);
}


//FINISH
//frees the dynamically allocated memory
void finish()
{
	free(v);
	free(faces);
	free(pairs);
	free(edges);
	free(firstDirected);
}


//MAIN
//reads the file, calculates the edges, calculates edge pairs, calculate directed edges, 
//tests if the mesh is manifold, writes data to file and cleans up the memory
int main(int argc, char* argv[])
{
	//check for input and output filenames
	if (argc < 3){
		printf("Not enough argumets, you must supply input and output filenames.\n");
	}
	else{
		//read the file and init data structures
		printf("-----------------------------------------------------------------");
		printf("\n| Faceindex2directededge - Reading file: %s \n", argv[1]);
		readData(argv[1]);
		//calculate edges
		calculateEdges();
		//compute edge pairs
		edgeHandler();
		//compute the first direted edge for each vertex
		getDirectedEdges();
		//manifold test
		if (testManifold()){
			printf("\n| Manifold test passed \n\n");	
			printf("| Euler test shows genus = %d \n", euler());
		}
		//write the data structures' contents to file
		printf("\n| Writing file: %s.diredge \n", argv[2]);
		printf("-----------------------------------------------------------------\n\n");
		writeData(argv[2]);
		//clean memory
		finish();
	}
	return 0;
}
