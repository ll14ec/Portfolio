//size of memory chunks allocated
#define CHUNK_SIZE 100
//next and previous edge operations
#define NEXT(edge) (((edge/3)*3) + (((edge%3)+1)%3))
#define PREV(edge) (((edge/3)*3) + (((edge%3)+2)%3))


//------structs------

//vertex made of 3 doubles x,y,z
typedef struct
{
	double x,y,z;
}
vertex; 


//edge - a directed edge structure with two vertices, edge goes [from] -> [to]
typedef struct 
{
	vertex from, to;
}
edge;


//tFace - triangular face with three integers a,b,c referencing the index of each vertex
typedef struct
{
	int a,b,c;
}
tFace;
