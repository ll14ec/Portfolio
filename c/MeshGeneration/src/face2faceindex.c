#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "structs.h"

//------globals------

int chunkCounter = 1;	//amount of memory chunks to be allocated
int triangleCount = 0;	//number of triangles read from first line of .tri file
int faceCount = 0;	//number of faces that have been created in output file
int vertexCount = 0;	//number of verteces listed in .tri file (not unique only)
int UVCount = 0;	//number of unique verteces listed in the .tri file

vertex * v;		//array of raw vertices read from .tri file
vertex* UV;		//array of unique vertices from .tri file
tFace* faces;		//array of faces


//------functions------

//IS VERTEX UNIQUE?
//Tests a vertex v against all entries in the unique vertex list
int isVertexUnique(vertex v)
{
	int i = 0;
	int unique = 1;
	for (i=0;i<UVCount;i++){
		if ( (UV[i].x==v.x) && (UV[i].y==v.y) && (UV[i].z==v.z) ){
			unique=0;
		}
	}
	return unique;
}


//TRIM VERTICES
//Takes a list of raw vertices and adds all unique entries to the array of unique vertices
void trimVertices()
{
	int i = 0;
	//calloc to init to zero
	UV =  (vertex*)calloc(CHUNK_SIZE*chunkCounter, sizeof(vertex));
	for (i=0;i<vertexCount;i++){
		if (isVertexUnique(v[i])){
			UV[UVCount].x = v[i].x;
			UV[UVCount].y = v[i].y;
			UV[UVCount].z = v[i].z;
			++UVCount;
		}
	}
}


//GET UNIQUE VERTEX INDEX
//returns the index of a given vertex in the array of unique vertices, if not found returns -1
int getUVIndex(vertex temp)
{
	int i = 0;
	for (i=0;i<(UVCount+1);i++){
		if ( (temp.x == UV[i].x) && (temp.y == UV[i].y) && (temp.z == UV[i].z) ){
			return (i);
		}
	}
	return -1;
}


//CALCULATE FACES
//populates the faces array with indices a,b,c that are the index of each face's 3 vertices in the array of unique vertices
void calculateFaces()
{
	int i = 0;
	int j = 0;
	int face = 0;
	
	faces = (tFace*)malloc(CHUNK_SIZE*chunkCounter*sizeof(face));

	for (i=0;i<vertexCount;i+=3){
		faces[j].a = getUVIndex(v[i]);
		faces[j].b = getUVIndex(v[i+1]);
		faces[j].c = getUVIndex(v[i+2]);
		//printf("%d %d %d\n", faces[j].a,faces[j].b,faces[j].c);
		++j;
	}	
	faceCount = j;
}


//VERTEX CREATOR
//adds a vertex to the end of the vertex array
void vertexCreator(float x, float y, float z)
{
	v[vertexCount].x = x; 
	v[vertexCount].y = y; 
	v[vertexCount].z = z; 
}


//LINE HANDLER
//takes each line from the input file sequentially
//allocates memory as the number of vertices increases
//sends the floats found to the vertex creator in the order x,y,z
void lineHandler(char* l, int lineIndex)
{
	char* x;
	char* y;
	char* z;
	int i = 0;

	if (lineIndex==0){
		triangleCount = atof(l);
		printf("| Triangles = %d\n", triangleCount);
		v = (vertex*)malloc(CHUNK_SIZE*sizeof(vertex));
	}
	else{
		// if another 100 vertices needed, allocate more memory
		if (vertexCount>=(CHUNK_SIZE*chunkCounter)){
			++chunkCounter;
			v = (vertex*)realloc(v, CHUNK_SIZE*chunkCounter*sizeof(vertex));
			//printf("Reallocing");
		}
		//split into floats
		do{
			x = strtok(l, " ");
			l=NULL;
			y = strtok(NULL, " ");
			z = strtok(NULL, " ");
			if ( (x) && (y) && (z) ){
				vertexCreator(atof(x),atof(y),atof(z));
				vertexCount++;
			}	
		} while ( (x) && (y) && (z) );
	}
}


//READ DATA
//opens the input file and reads lines from it
//ignores lines of length 0
//passes each non zero line to the line handler
//i is argv[1] - the input file name
void readData(char* i)
{
	//open file and read line by line
	FILE* ifp;
	char * l = NULL;
	size_t s = 0;
	ssize_t lengthRead;
	ifp = fopen(i,"r");
	int lineIndex = 0;
	
	//if file has been opened
	if (ifp){
		while ((lengthRead = getline(&l, &s, ifp)) != -1){
			//printf("%s\n", l);
			//if line is not blank then pass to line handler
			if (lengthRead>1){
				lineHandler(l, lineIndex);
			}
			++lineIndex;
		}
	}
	//if file io error
	else{
		printf("Invalid file handle.\n");
	}
	//close input file
	fclose(ifp);
}


//WRITE DATA
//writes the vertices and faces to file
//o is argv[2] - the output file name without extension
void writeData(char* o)
{
	char* shapeName = (char*)malloc(9999*sizeof(char));
	shapeName = strcpy(shapeName,o);
	strcat(o, ".face");
	FILE* f = fopen(o,"w");

	if (f){
		//print header
		fprintf(f,"# University of Leeds 2018-2019\n");
		fprintf(f,"# COMP 5812M Assignment 2\n");
		fprintf(f,"# Edward Cottle\n");
		fprintf(f,"# 200450169\n");
		fprintf(f,"#\n");
		fprintf(f,"# Object Name: %s\n", shapeName);
		fprintf(f,"# Vertices = %d Faces =%d\n", UVCount,faceCount);
		fprintf(f,"#\n");
		//print verteces
		int i=0;
		for (i=0;i<UVCount;i++){
			fprintf(f, "Vertex %d ", i);
			fprintf(f, "%g ", UV[i].x);
			fprintf(f, "%g ", UV[i].y);
			fprintf(f, "%g \n", UV[i].z);
		}
		//print faces
		for (i=0;i<faceCount;i++){
			fprintf(f, "Face %d ", i);
			fprintf(f, "%d ", faces[i].a);
			fprintf(f, "%d ", faces[i].b);
			fprintf(f, "%d \n", faces[i].c);
		}
	}
	//close file
	fclose(f);
	//free filename string
	free(shapeName);
}


//FINISH
//cleans up dynamically allocated memory
void finish()
{
	free(v);
	free(UV);
	free(faces);
}


//MAIN
//reads the file, trims the vertices, calculates the faces, writes data to file and cleans up the memory
int main(int argc, char* argv[])
{
	//check for input and output filenames
	if (argc < 3){
		printf("Not enough argumets, you must supply input and output filenames.\n");
	}
	else{
		//read the file and init data structures
		printf("-----------------------------------------------------------------\n");
		printf("| Face2faceindex - Reading file: %s \n", argv[1]);
		readData(argv[1]);
		//get unique vertices
		trimVertices();
		//calculate face indices
		calculateFaces();
		printf("| Total verteces: %d\n", vertexCount);
		//write the data structures' contents to file
		printf("| Writing file: %s.face\n", argv[2]);
		printf("-----------------------------------------------------------------\n\n");
		writeData(argv[2]);
		//clean memory
		finish();
	}
	return 0;
}
