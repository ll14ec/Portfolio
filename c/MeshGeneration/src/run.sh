#!/bin/bash

rm -rf output.txt

cd ../models/
for i in *.tri; do
	[ -f "$i" ] || break
	../src/face2faceindex $i ../models/${i//.tri} | tee -a ../src/output.txt
done

for j in *.face; do
	[ -f "$j" ] || break
	../src/faceindex2directededge $j ../models/${j//.face} | tee -a ../src/output.txt
done
