#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "converter.c"

int errortrack = 0;
int errors(int error, int stringcount, char command[])
{

if (stringcount<2)
	{
	printf("\nERROR - You entered %d input arguments. The minimum number of input arguments is 2.\n\n", stringcount);
	error++;
	return;
	}

if (stringcount>4)
	{
	printf("\nERROR - You entered %d input arguments. The maximum number of input arguments is 3.\n\n", stringcount);
	error++;
	return;
	}

if (strcmp(command, "pgmb2pgmt")!=0)

{
printf("Sorry,'%s' is not a function supported by this program.\n", command);
error++;
}
errortrack = error;
return(0);



}


int main(int argc, char *argpointer[])
{

errors(0, argc, argpointer[1]);

if(errortrack==0)
{
converter(argpointer[2], argpointer[3]);
}



return(0);

}
