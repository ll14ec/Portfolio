#include <stdio.h>
#include <string.h>
#include <stdlib.h>


char input[1000];
char rawinput[1000];
char *tempholder;
char inputs[10][1000];
int stringcount=0;
char pgmb2pgmt[10];
char *newfilename;
int complete = 0;
int error=0;
int i;
int space;

void getinput()
{
error=0;
space=0;

fgets(input, sizeof(input), stdin);

for (i=0;i<strlen(input);i++){
int asc= input[i];
if (asc == 32){space++;}
}

if (space<2)

{
printf("Please follow the target filename with a space.\n\n");
return;
}


strcpy(pgmb2pgmt, "pgmb2pgmt");
stringcount=0;
i = 1;


tempholder = strtok(input," ");	
			/* split input at the first space and copy into 'tempholder' */
  while (tempholder != NULL)	

				/* loop until strtok hits a NULL character */		
  {		
		
    strcpy(inputs[i], tempholder);	/* copy finalstring segment into the finalstrings[] array  */
    tempholder = strtok(NULL, " "); 		/* copy next string until a \n char */
    stringcount = i;				/* increment stringcount */
   i++;					/* increment i */
  
  }




if (strlen(inputs[3])<3){printf("Outputfilename: b%s\n", inputs[2]);strcpy(inputs[3], "b"); strcat(inputs[3],inputs[2]);}
newfilename = (char*) malloc (strlen(inputs[3])-1);
strncpy (newfilename, inputs[3], (strlen(inputs[3])-1));
printf("%s", inputs[2]);

i = 1;

if (strcmp(inputs[1], pgmb2pgmt) != 0) /*if invalid operator*/
{
printf("\nSorry, '%s' is not a function supported by this program.\n\n", inputs[1]);
error++;
}

if (stringcount>3)
	{
	printf("\nERROR - You entered %d input arguments. The maximum number of input arguments is 3.\n\n", stringcount);
	error++;
	return;
	}

  



if(error==0){

FILE *userfile, *newfile;

userfile = fopen (inputs[2], "rb");

newfile = fopen (newfilename, "a");

if (userfile == NULL)
{
printf("Unable to find file '%s', perhaps you typed it wrong.\n\n",inputs[2]);
}



int hash=0;
int copyer;
int pflag=0;
int pstop=1;
int fiveto2=0;
int sixto3=0;
int pgmbin=0;
int ppmbin=0;
int hundreds =48;
int tens = 48;
int ones = 48;
int lastline=0;
int newline=0;
char *buffer;
int colour=0;


while(copyer!=EOF)    {       //Statement   2

copyer = fgetc(userfile);	
if (lastline==0 && copyer == 35){hash=1;}
if (copyer == 80 && pstop==1){pflag = 1; pstop=0;} 
if (copyer == 53 && pflag==1){pgmbin = 1; fiveto2= 1; pflag=0;}
if (copyer == 54 && pflag==1){ppmbin = 1; sixto3= 1; pflag=0;}
if (fiveto2 == 1){fiveto2=0;copyer = 50;}
if (sixto3 == 1){sixto3=0;copyer = 51;}
if (copyer == 10){newline=newline+1;}
if (newline-hash<3){fputc(copyer,newfile);}


if (newline-hash>2 && (pgmbin=1)){
if(copyer==10 && lastline==0){fputc(copyer,newfile); lastline=1; continue;}
if(copyer!=10 || lastline==1){
if(copyer>199){copyer=copyer-100; hundreds++;}
if(copyer>99){copyer=copyer-100; hundreds++;}
if(copyer>89){copyer=copyer-10; tens++;}
if(copyer>79){copyer=copyer-10; tens++;}
if(copyer>69){copyer=copyer-10; tens++;}
if(copyer>59){copyer=copyer-10; tens++;}
if(copyer>49){copyer=copyer-10; tens++;}
if(copyer>39){copyer=copyer-10; tens++;}
if(copyer>29){copyer=copyer-10; tens++;}
if(copyer>19){copyer=copyer-10; tens++;}
if(copyer>9){copyer=copyer-10; tens++;}
if(copyer>8){copyer=copyer-1; ones++;}
if(copyer>7){copyer=copyer-1; ones++;}
if(copyer>6){copyer=copyer-1; ones++;}
if(copyer>5){copyer=copyer-1; ones++;}
if(copyer>4){copyer=copyer-1; ones++;}
if(copyer>3){copyer=copyer-1; ones++;}
if(copyer>2){copyer=copyer-1; ones++;}
if(copyer>1){copyer=copyer-1; ones++;}
if(copyer>0){copyer=copyer-1; ones++;}
fputc(hundreds, newfile);
fputc(tens, newfile);
fputc(ones, newfile);
fputc(32, newfile);
hundreds = tens = ones = 48;

}

}

if ((newline-hash>2 && (ppmbin=1)) || colour==1){
colour=1;
if(copyer==10 && lastline==0){fputc(copyer,newfile); lastline=1;}
if(copyer!=10 || lastline==1){
if(copyer>199){copyer=copyer-100; hundreds++;}
if(copyer>99){copyer=copyer-100; hundreds++;}
if(copyer>89){copyer=copyer-10; tens++;}
if(copyer>79){copyer=copyer-10; tens++;}
if(copyer>69){copyer=copyer-10; tens++;}
if(copyer>59){copyer=copyer-10; tens++;}
if(copyer>49){copyer=copyer-10; tens++;}
if(copyer>39){copyer=copyer-10; tens++;}
if(copyer>29){copyer=copyer-10; tens++;}
if(copyer>19){copyer=copyer-10; tens++;}
if(copyer>9){copyer=copyer-10; tens++;}
if(copyer>8){copyer=copyer-1; ones++;}
if(copyer>7){copyer=copyer-1; ones++;}
if(copyer>6){copyer=copyer-1; ones++;}
if(copyer>5){copyer=copyer-1; ones++;}
if(copyer>4){copyer=copyer-1; ones++;}
if(copyer>3){copyer=copyer-1; ones++;}
if(copyer>2){copyer=copyer-1; ones++;}
if(copyer>1){copyer=copyer-1; ones++;}
if(copyer>0){copyer=copyer-1; ones++;}
fputc(hundreds, newfile);
fputc(tens, newfile);
fputc(ones, newfile);
hundreds = tens = ones = 48;

}



}

printf("Conversion complete.\n\n");
complete=1;

}


}


}

int main()

{

printf("\nFILE CONVERTER VERSION 1.0\n\nPlease enter an operation, a file, and if necessary a file to write to.\n\nSeperate terms with a single space only.\n\n");

while (complete !=1)

{
getinput();

}

return(0);

}
