#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "converter.c"
int complete  = 0;
int errortrack = 0;



int errors(int error, int stringcount, char command[])
{

if (stringcount<2)
	{
	printf("\nERROR - You entered %d input arguments. The minimum number of input arguments is 2.\n\n", stringcount);
	error++;
	return;
	}

if (stringcount>4)
	{
	printf("\nERROR - You entered %d input arguments. The maximum number of input arguments is 3.\n\n", stringcount);
	error++;
	return;
	}

if (strcmp(command, "pgmb2pgmt")!=0)

{
printf("Sorry,'%s' is not a function supported by this program.\n", command);
error++;
}
errortrack = error;
return(0);



}


void converter(char usrfile[], char nwfile[])

{
FILE *userfile, *newfile;

userfile = fopen (usrfile, "rb");

newfile = fopen (nwfile, "a");

if (userfile == NULL)
{
printf("Unable to find file '%s', perhaps you typed it wrong.\n\n",usrfile);
}



int hash=0;
int copyer;
int pflag=0;
int pstop=1;
int fiveto2=0;
int sixto3=0;
int pgmbin=0;
int ppmbin=0;
int hundreds =48;
int tens = 48;
int ones = 48;
int lastline=0;
int newline=0;
char *buffer;
int colour=0;


while(copyer!=EOF)    {       //Statement   2

copyer = fgetc(userfile);	
if (lastline==0 && copyer == 35){hash=1;}
if (copyer == 80 && pstop==1){pflag = 1; pstop=0;} 
if (copyer == 53 && pflag==1){pgmbin = 1; fiveto2= 1; pflag=0;}
if (copyer == 54 && pflag==1){ppmbin = 1; sixto3= 1; pflag=0;}
if (fiveto2 == 1){fiveto2=0;copyer = 50;}
if (sixto3 == 1){sixto3=0;copyer = 51;}
if (copyer == 10){newline=newline+1;}
if (newline-hash<3){fputc(copyer,newfile);}


if (newline-hash>2 && (pgmbin=1)){
if(copyer==10 && lastline==0){fputc(copyer,newfile); lastline=1; continue;}
if(copyer!=10 || lastline==1){
if(copyer>199){copyer=copyer-100; hundreds++;}
if(copyer>99){copyer=copyer-100; hundreds++;}
if(copyer>89){copyer=copyer-10; tens++;}
if(copyer>79){copyer=copyer-10; tens++;}
if(copyer>69){copyer=copyer-10; tens++;}
if(copyer>59){copyer=copyer-10; tens++;}
if(copyer>49){copyer=copyer-10; tens++;}
if(copyer>39){copyer=copyer-10; tens++;}
if(copyer>29){copyer=copyer-10; tens++;}
if(copyer>19){copyer=copyer-10; tens++;}
if(copyer>9){copyer=copyer-10; tens++;}
if(copyer>8){copyer=copyer-1; ones++;}
if(copyer>7){copyer=copyer-1; ones++;}
if(copyer>6){copyer=copyer-1; ones++;}
if(copyer>5){copyer=copyer-1; ones++;}
if(copyer>4){copyer=copyer-1; ones++;}
if(copyer>3){copyer=copyer-1; ones++;}
if(copyer>2){copyer=copyer-1; ones++;}
if(copyer>1){copyer=copyer-1; ones++;}
if(copyer>0){copyer=copyer-1; ones++;}
fputc(hundreds, newfile);
fputc(tens, newfile);
fputc(ones, newfile);
fputc(32, newfile);
hundreds = tens = ones = 48;

}

}

if ((newline-hash>2 && (ppmbin=1)) || colour==1){
colour=1;
if(copyer==10 && lastline==0){fputc(copyer,newfile); lastline=1;}
if(copyer!=10 || lastline==1){
if(copyer>199){copyer=copyer-100; hundreds++;}
if(copyer>99){copyer=copyer-100; hundreds++;}
if(copyer>89){copyer=copyer-10; tens++;}
if(copyer>79){copyer=copyer-10; tens++;}
if(copyer>69){copyer=copyer-10; tens++;}
if(copyer>59){copyer=copyer-10; tens++;}
if(copyer>49){copyer=copyer-10; tens++;}
if(copyer>39){copyer=copyer-10; tens++;}
if(copyer>29){copyer=copyer-10; tens++;}
if(copyer>19){copyer=copyer-10; tens++;}
if(copyer>9){copyer=copyer-10; tens++;}
if(copyer>8){copyer=copyer-1; ones++;}
if(copyer>7){copyer=copyer-1; ones++;}
if(copyer>6){copyer=copyer-1; ones++;}
if(copyer>5){copyer=copyer-1; ones++;}
if(copyer>4){copyer=copyer-1; ones++;}
if(copyer>3){copyer=copyer-1; ones++;}
if(copyer>2){copyer=copyer-1; ones++;}
if(copyer>1){copyer=copyer-1; ones++;}
if(copyer>0){copyer=copyer-1; ones++;}
fputc(hundreds, newfile);
fputc(tens, newfile);
fputc(ones, newfile);
hundreds = tens = ones = 48;

}



}


complete=1;

}

printf("\nFile converted flawlessly.\n\n");


}



int main(int argc, char *argpointer[])
{

errors(0, argc, argpointer[1]);

if(errortrack==0)
{
converter(argpointer[2], argpointer[3]);
}



return(0);

}
